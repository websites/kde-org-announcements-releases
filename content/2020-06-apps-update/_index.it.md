---
layout: page
publishDate: 2020-06-11 12:00:00
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di giugno 2020
type: announcement
---
È sempre un piacere quando la famiglia KDE cresce, ecco perché questo mese diamo uno speciale benvenuto al gestore delle copie di sicurezza Kup e a un nuovo sistema completo di impacchettamento: Homebrew.

# Nuovi rilasci

## Kup 0.8

[Kup](https://store.kde.org/p/1127689) è uno strumento per creare copie di sicurezza che puoi utilizzare per tenere al sicuro i tuoi file.

In precedenza sviluppato al di fuori di KDE, questo mese ha passato la fase Incubation e si è unito alla nostra comunità, diventando ufficialmente un progetto KDE. Lo sviluppatore responsabile, Simon Persson, ha festeggiato con uno nuovo rilascio.

Ecco le modifiche che troverai nella nuova versione:

* È stato cambiato il modo in cui le copie di sicurezza di tipo rsync vengono memorizzate quando è selezionata una sola cartella sorgente. Questa modifica cerca di minimizzare il rischio di eliminazione dei file per l'utente che sceglie una cartella non vuota come destinazione. È stato aggiunto codice di migrazione per il rilevamento e lo spostamento dei tuoi file al primo avvio, al fine di evitare di copiare di nuovo tutto e raddoppiare gli elementi memorizzati.

* È stata aggiunta un'opzione avanzata che ti permette di specificare un file da cui Kup legga gli schemi di esclusione; questo ti permette, per esempio, di istruire Kup a non salvare mai i file *.bak.

* Sono state cambiate le impostazioni predefinite, nella speranza di averle migliorate.

* Sono stati ridotti gli avvisi sui file che non vengono inclusi, in quanto si generavano troppi falsi allarmi.

* Kup non chiederà più una password per sbloccare i dispositivi esterni cifrati quando si cerca di visualizzare quanto spazio è disponibile.

* È stato corretto, sia per rsync sia per bup, il fatto di considerare un salvataggio come non riuscito solo perché dei file sono spariti durante l'operazione.

* Sono ora eseguite in parallelo le verifiche di integrità e le riparazioni, in base al numero di CPU.

* È stato aggiunto il supporto per i metadati bup versione 3, che era stato aggiunto in bup versione 0.30.

* Molte correzioni all'interfaccia utente.

Kup può creare copie utilizzando rsync o creare copie di sicurezza con versione con lo strumento in Python Bup. Bup funziona attualmente solo con Python 2, il che significa che questa opzione non sarà disponibile in molte distribuzioni; è in preparazione una conversione a Python 3.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

Per saperne di più su Kup, [Average Linux User lo ha recensito](https://averagelinuxuser.com/kup-backup/) e ha creato un video sul programma non molto tempo fa:

{{< youtube y53IE0G8Brg >}}

# Krita sui tablet Android

Grazie al duro lavoro di Sharaf Zaman, Krita è ora disponibile nel Google Play Store per i tablet Android e i Chromebook (ma non per i cellulari Android).

È una versione beta basata su Krita 4.2.9, è la versione desktop completa di Krita, dunque non possiede alcuna interfaccia per schermi tattili speciale. Però è presente e la puoi provare.

A differenza degli store Windows e Steam, il Google Play Store non chiede soldi per Krita, dato che è il solo modo per installare il programma in tali dispositivi. Per sostenerne lo sviluppo puoi tuttavia acquistare il tesserino di sostenitore dall'interno del programma.

Per installare

* Scarica [Krita da Google Play](https://play.google.com/store/apps/details?id=org.krita)

* In alternativa, nel Play Store passa alla scheda «Accesso in anteprima» e cerca org.krita (vedi le istruzioni ufficiali di Google sull'Accesso in anteprima. Fino a quando non otterremo un numero ragionevole di scaricamenti dovrai scorrere un bel po').

* Puoi anche [scaricare i file apk da te](https://files.kde.org/krita/android/). NON chiedere aiuto per installare questi file.

* Questi sono tutti depositi ufficiali. Non installare Krita da altre fonti, non possiamo garantirne la sicurezza.

Note

* Supporta i tablet Android e i Chromebook. Versioni Android supportate: Android 6 (Marshmallow) e superiori.

* Attualmente non compatibile con: telefoni Android.

* Se hai installato una delle compilazioni di Sharaf o ne hai compilato una firmata da te, devi prima disinstallarla, per tutti gli utenti!

{{< img class="text-center" src="krita-android.jpg" caption="Krita in Android" style="width: 600px">}}

# In arrivo

[KIO Fuse](https://techbase.kde.org/Projects/KioFuse) questo mese ha affettuato il suo primo [rilascio beta](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html).

## Correzioni di errori

Sono state rilasciate correzioni di errori per

* Il gestore di raccolte [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html), con una finestra di dialogo di filtraggio aggiornata che consente la ricerca di testo vuoto.

* Il browser di rete locale [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) ha risolto il problema del salvataggio delle impostazioni in chiusura.

* I programmatori dell'IDE [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) hanno fatto un aggiornamento legato allo spostamento dei depositi di KDE.

# App Store

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

Mentre noi in Linux stiamo gradualmente imparando a installare applicazioni singole da uno store, sta avvenendo il contrario nell'universo di macOS e Windows. Per questi sistemi, i gestori di pacchetti sono stati introdotti per quelli che desiderano avere una fonte unica per controllare tutto il loro sistema.

Il deposito di pacchetti open source principale per macOS è Homebrew, gestito da un «crack team» di sviluppatori, incluso un vecchio sviluppatore di KDE, Mike McQuaid.

Questo mese il progetto [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde), che per un po' è stato gestito esternamente a KDE, è stato spostato al suo interno, diventando parte integrante della nostra comunità.

Puoi aggiungere il deposito KDE Homebrew per macOS e scaricare i sorgenti compilati per te e pronti all'uso.

Abbiamo contattato lo sviluppatore principale Yurii Kolesnykov e gli abbiamo chiesto del progetto.

### Parlaci di te, come ti chiami, da dove vieni, quali sono i tuoi interessi in KDE e Mac, cosa fai per vivere?

Mi chiamo Yurii Kolesnykov e sono originario dell'Ucraina. La mia passione per il software libero risale a quando ne ho sentito parlare per la prima volta, pressapoco alla fine delle scuola superiore. Credo che KDE sia semplicemente il migliore ambiente desktop per Linux e i sistemi Unix, con molte e ottime applicazioni. Il mio interesse per Mac deriva dal mio lavoro principale, sviluppo applicazioni mobili per iOS.

### Che cos'è Homebrew?

Homebrew è il gestore di pacchetti macOS più popolare, proprio come apt o yum. Dato che macOS è Unix e Apple fornisce un buon compilatore e strumenti per esso, le persone hanno deciso di creare dei gestori di pacchetti, in questo modo puoi installare su Mac un sacco di software libero e open source. Homebrew contiene anche un sotto-progetto chiamato Homebrew Cask, che ti permette di installare molte applicazioni eseguibili, ossia applicazioni a interfaccia grafica o proprietarie. Questo perché le applicazioni a interfaccia grafica sono difficili da integrare col sistema, se sono installate tramite Homebrew.

### Quali pacchetti KDE hai creato per Homebrew?

Ho da poco dato uno sguardo sul nostro sistema e ho visto che ci sono 110 pacchetti in totale, di cui 67 framework e circa 39 applicazioni. Sono già presenti la maggior parte delle applicazioni popolari, tipo Kate, Dolphin e KDevelop, in base alla richiesta degli utenti.

### Come utente Mac cosa bisogna fare affinché le applicazioni vengano installate?

Prima di tutto, se non l'hai ancora fatto, devi seguire la guida all'installazione di Homebrew, è disponibile all'indirizzo [brew.sh](https://brew.sh). Poi devi fare tap al nostro deposito col comando seguente:

`brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git`

Purtroppo molti pacchetti KDE non funzionano al volo, abbiamo comunque creato uno script che esegue tutti i passaggi necessari, dunque dopo aver eseguito l'operazione tap devi eseguire il comando seguente:

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### Quanto è popolare Homebrew come metodo per ottenere app per Mac?

Buona domanda. Attualmente non abbiamo ancora una statistica, la aggiungerò alla mia lista delle cose da fare. Però, dato che Homebrew è il gestore di pacchetti più popolare per Mac e richiede agli utenti di non essere combinato con altri progetti simili per installare software su Mac, a causa di conflitti, credo che, sì, sia molto popolare.

### Di quanto lavoro hai bisogno affinché le applicazioni KDE funzionino in Homebrew?

Durante la creazione degli attuali pacchetti abbiamo già affrontato molti problemi condivisi, aggiungere dunque nuovo software è relativamente facile. Prometto di scrivere una guida per questo, gli utenti l'hanno richiesta molte volte.

### Attualmente, i pacchetti devono essere compilati localmente, ci saranno disponibili pacchetti precompilati?

Homebrew ti consente di installare tramite Bottles, ossia pacchetti eseguibili precompilati. Il processo di creare «bottles» è però strettamente integrato con l'infrastruttura Homebrew, ossia abbiamo la necessità di eseguire CI con test su ogni pacchetto prima che sia imbottigliato. Abbiamo deciso dunque di integrare quanti più pacchetti possibili all'interno del deposito brew principale per eliminare i carichi di manutenzione.

### Esiste altro software desktop disponibile in Homebrew?

Sì. In generale, se un'applicazione è popolare e possiede un canale di distribuzione esterno al Mac AppStore, allora esiste un'alta possibilità che sia disponibile per l'installazione tramite un Brew Cask.

### Come possono aiutare gli autori di applicazioni KDE affinché il loro software sia inserito in Homebrew?

L'hardware Apple è molto costoso, avere un Mac per ogni sviluppatore KDE non sarebbe una buona idea. Per quanto riguarda adesso, è ben accetta la creazione di Feature Request nel nostro deposito. Poi i responsabili o gli utenti di Homebrew KDE segnalano errori se qualcosa non funziona come dovrebbe. E stiamo cercando di fornire più informazioni possibili in base alle richieste degli sviluppatori KDE. Al momento ci sono molti ticket in attesa per le applicazioni KDE, con pochi errori ma piuttosto fastidiosi. Spero che riusciremo a integrarci meglio con l'infrastruttura KDE, ossia di poter collegare gli errori presenti nei nostri depositi con i progetti upstream. Abbiamo già migrato a KDE Invent e spero anche che KDE Bugs venga presto spostato da Bugzilla a KDE Invent.

### L'altro modo per ottenere applicazioni KDE compilate per Mac è tramite Craft. È possibile un confronto tra le applicazioni compilate in Homebrew con quelle compilate con Craft?

Credo ancora che Homebrew sia più facile da usare per gli utenti finali. Il suo processo di installazione è così semplice quanto eseguire una riga di comando. Per aggiungere il nostro deposito e iniziare a installare applicazioni devi eseguire altre due righe.

### Grazie per il tempo dedicatoci, Yurii.

# Rilasci 20.04.2

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri vengono rilasciati in massa. Il gruppo 20.04.2 dei progetti è stato rilasciato oggi e sarà presto disponibile negli app store nelle distribuzioni. Consulta la [pagina dei rilasci 20.04.2](https://www.kde.org/announcements/releases/20.04.2.php) per i dettagli.

Alcune delle correzioni nel rilascio odierno:

* Le scritture sono spezzate in richieste multiple per i server SFTP che limitano la dimensione dei trasferimenti

* Konsole aggiorna la posizione del cursore per i metodi di immissione (quale IBus o Fcitx) e non si blocca più quando viene chiuso tramite il menu

* KMail genera un HTML migliore quando si aggiunge una firma HTML ai messaggi

[note di rilascio 20.04](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Pagina wiki per lo scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Pagina informativa sui sorgenti
20.04.2](https://kde.org/info/releases-20.04.2) &bull; [Elenco completo delle
modifiche 20.04.2](https://kde.org/announcements/changelog-
releases.php?version=20.04.2)