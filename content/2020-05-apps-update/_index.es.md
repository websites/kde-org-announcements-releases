---
layout: page
publishDate: 2020-05-15 13:00:00
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de mayo de 2020"
type: announcement
---
# Nuevos lanzamientos

## Kid3

[Kid3](https://kid3.kde.org/) es un práctico aunque potente programa de etiquetado de música que le permite editar las etiquetas ID3 y otros formatos similares en MP3 y otros archivos de música.

Este mes se ha movido para quedar bajo el amparo de KDE y ha realizado su primer lanzamiento como aplicación de KDE. En la nota de lanzamiento se puede leer:

«Además de correcciones de errores, este lanzamiento proporciona mejoras de usabilidad, accesos rápidos de teclado adicionales y scripts de acciones del usuario. Estamos muy agradecidos a varias personas de KDE, que han traducido la interfaz del usuario y el manual a nuevos idiomas».

Kid3 está disponible para Linux, Windows, Mac y Android. Puede descargarlo del sitio web, a través de su distribución o de las tiendas Chocolatey, Homebrew y F-droid.

{{< img class="text-center" src="kid3-android.png" caption="Kid3 en Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="Kid3 en Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="Kid3 en Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="Kid3 está en FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="Kid3 está en Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="Kid3 está en Chocolatey" style="width: 175px">}}

## Calligra

[Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) es un paquete ofimático de completas funcionalidades que cumple los estándares. No ha tenido ningún lanzamiento durante varios años, pero ahora ha vuelto con estilo.

La versión 3.2 contiene Gemini, el paquete ofimático especialmente diseñado para dispositivos 2 en 1, es decir, para portátiles con pantallas táctiles que se pueden doblar como tablets, se ha portado a nuestra bella infraestructura Kirigami. El programa de dibujo, Karbon, permite ahora documentos que contienen múltiples páginas. Stage, el editor de presentaciones de Calligra, permite ahora la transición automática de diapositivas.

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Dibujo de Karbon" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Presentaciones de Stage" style="width: 400px">}}

Calligra está disponible a través de su distribución Linux.

## Tellico 3.3 actualiza fuentes de datos

[Tellico](https://tellico-project.org/tellico-3-3-released/) es un gestor de colecciones para llevar la cuenta de todo lo que pueda coleccionar, como libros, bibliografías, vídeos, música, videojuegos, monedas, sellos, tarjetas coleccionables, cómics y vinos.

Para facilitar esta labor, dispone de diversos motores de datos para mostrarle lo que posee. Esta nueva versión 3.3 añade una fuente de datos para [Colnect](https://colnect.com/), un amplio catálogo de artículos de colección. También se han actualizado numerosas fuentes de datos, como Amazon y MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico con el modelo de datos Colnect" >}}

# Nuevos proyectos

Los nuevos proyectos de KDE de este mes incluyen a pvfviewer, un visor de patrones de PC Stitch con el que puede crear bellos lienzos, y Alligator, un lector RSS que usa nuestra excelente infraestructura Kirigami.

# AppImage de KDE

Cada mes nos gustaría describir uno de los nuevos formatos contenedores de paquetes. AppImage no es nuevo, ya que ha estado presente desde hace aproximadamente una década, pero merece la pena recordarnos a nosotros mismos el número de aplicaciones de KDE que lo usan.

## ¿Qué es una AppImage?

AppImage es un formato de empaquetado que proporciona a los desarrolladores «upstream» un modo para poder distribuir binarios «nativos» para los usuarios de GNU/Linux de la misma forma que pueden hacer en otros sistemas operativos. Permite empaquetar aplicaciones para cualquier sistema operativo común que esté basado en Linux, como Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora, etc. Las AppImages contienen todas las dependencias que no se puede suponer que formen parte de cada sistema de destino en una versión lo suficientemente reciente, y funcionarán en la mayoría de distribuciones de Linux sin más modificaciones.

## ¿Qué debe instalar para que funcione mejor?

Para ejecutar una AppImage necesita el núcleo de Linux (superior a 2.6) y `libfuse`. Esas dependencias están presentes en la mayoría de distribuciones de GNU/Linux, por lo que no habrá necesidad de instalar nada especial.

Por desgracia, el soporte de AppImage en los entornos de escritorio principales (KDE y Gnome) no es completo, por lo que es posible que necesite una herramienta adicional para crear una entrada en el menú para la aplicación. En tal caso, según el UX que prefiera, puede escoger entre: - [AppImageLauncher](https://www.appimagehub.com/p/1228228) para una ventana emergente de integración en la primera ejecución, o - [appimaged](https://github.com/AppImage/appimaged) para una integración automatizada de cada AppImage existente en su carpeta personal.

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) para una ventana emergente de integración de la primera ejecución o

* [appimaged](https://github.com/AppImage/appimaged) para una integración automática de cada AppImage existente en su carpeta personal.

Para actualizar las AppImages puede usar [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Está integrada en AppImageLauncher, por lo que si ya la tenía instalada no necesitará instalar nada más. Solo tiene que hacer clic con el botón derecho sobre el archivo de la AppImage y elegir «Actualizar». Tenga en cuenta que no todos los empaquetadores integran la información de actualización en los binarios, por lo que es posible que en algún caso tenga que descargar la nueva versión de forma manual.



## ¿Qué aplicaciones de KDE funcionan de este modo?

Existen varias aplicaciones de KDE que ya se distribuyen como AppImage. Las de más relevancia son: - kdenlive - krita - kdevelop

* kdenlive

* krita

* kdevelop

## ¿Qué es appimagehub?

El modo recomendado de obtener AppImages es a través de los autores originales de las aplicaciones, aunque esto no es muy práctico cuando no conoce la aplicación que necesita. Aquí es cuando entra en juego [AppImageHub](appimagehub.com). Se trata de una tienda de software dedicada solo a AppImages en la que puede encontrar un catálogo con más de 600 aplicaciones para su trabajo diario.

Este sitio web forma parte de la plataforma [OpenDesktop.org](opendesktop.org), que proporciona un completo ecosistema para usuarios y desarrolladores de aplicaciones libres y de código abierto.

## ¿Cómo crear una AppImage?

Crear una AppImage consiste en empaquetar todas las dependencias de una aplicación en un único directorio (AppDir), que luego se empaqueta en una imagen «squashfs» y se añade a un «runtime» que posibilita su ejecución.

Para llevar a cabo esta tarea, puede usar las siguientes herramientas:

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

En la [Guía de empaquetado AppImage](https://docs.appimage.org/packaging-guide/index.html) puede encontrar una extensa documentación sobre cómo usar estas herramientas. También se puede unir al [canal IRC de AppImage](irc://irc.freenode.net/#AppImage).



# Lanzamientos 20.04.1

Algunos de nuestros proyectos se publican según su propio calendario, mientras que otros se publican en masa. El paquete de proyectos 20.04.1 se ha publicado hoy y debería estar disponible próximamente en las tiendas de aplicaciones y en las distribuciones. Consulte la [página de lanzamientos 20.04.1](https://www.kde.org/info/releases-20.04.1.php).

Estas son algunas de las correcciones de errores que incluye este lanzamiento:

* kio-fish: Solo se guarda la contraseña en KWallet si el usuario lo solicita.

* En [Umbrello](https://umbrello.kde.org/) se ha corregido la posibilidad de añadir comentarios multilínea de C++.

* El comportamiento del desplazamiento en el visor de documentos [Okular](https://kde.org/applications/en/graphics/org.kde.okular) se ha mejorado y es más usable con ruedas de ratón de giro libre.

* Se ha corregido una regresión que hacía que el reproductor de música [JuK](https://kde.org/applications/en/multimedia/org.kde.juk) fallase algunas veces durante su inicio.

* El [editor de vídeo Kdenlive](https://kde.org/applications/en/multimedia/org.kde.kdenlive) contiene muchas mejoras de estabilidad, incluyendo una corrección para la creación de capítulos de DVD y otra que mejora el manejo de códigos de tiempo, mejorando el manejo de clips ausentes, el dibujo de marcos de «foto» en clips de imágenes para diferenciarlos de los clips de vídeo, y vistas previas en la línea de tiempo.

* [KMail](https://kde.org/applications/en/office/org.kde.kmail2) ya maneja correctamente las carpetas de «maildir» existentes cuando se añade un nuevo perfil de «maildir» y ya no falla al añadir demasiados destinatarios.

* Las preferencias de importación y exportación de [Kontact](https://kde.org/applications/en/office/org.kde.kontact) se han mejorado para incluir más datos.

[Notas del lanzamiento de
20.04](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Página
wiki de descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Página de información principal sobre
20.04.1](https://kde.org/info/releases-20.04.1) &bull; [Registro completo de
cambios 20.04.1](https://kde.org/announcements/changelog-
releases.php?version=20.04.1)