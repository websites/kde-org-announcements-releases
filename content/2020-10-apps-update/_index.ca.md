---
draft: false
layout: page
publishDate: '2020-09-10T12:00:00+00:00'
summary: "Qu\xE8 ha passat a les aplicacions del KDE aquest mes"
title: "Actualitzaci\xF3 de les aplicacions del KDE d'octubre de 2020"
type: announcement
---
Després de l'Akademy, ha arribat a les aplicacions del KDE un mes addicional d'actualitzacions noves de programari i correccions d'errors.

# Llançaments nous

## digiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="digiKam" style="width: 600px">}}

El mes va començar amb la publicació de la popular [aplicació de gestió de fotos, el digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

Una implementació millor per a les metadades CR3 de Canon. Els formats RAW d'imatge de les càmeres moltes vegades no estan documentats i són difícils de processar. Els desenvolupadors del digiKam 7.1.0 han escrit una interfície de les metadades per a CR3 basada en la «libraw», i ara l'aplicació pot llegir una part importat de les etiquetes Exif, incloent la informació del GPS, el perfil de color, i per suposat, els contenidors IPTC i XMP estàndard.

S'ha introduït un connector nou de Gestió de cues per lots que permet aplicar textures sobre d'imatges i un altre que corregeix automàticament els píxels cremats, és a dir, els «píxels dolents» que s'obtenen en usar velocitats baixes de l'obturador o treballant amb imatges nocturnes.

El [Model d'intercanvi d'informació IPTC](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) és el format de metadades habitual antic però encara usat per les fotos i aquest llançament millora la seva implementació de l'Unicode.

El digiKam és disponible a les distribucions de Linux com un arxiu tar de codi, paquets AppImage de 32/64 bits, una paquet de macOS i instal·ladors de Windows 32/64 bits. [Baixeu aquí el digiKam 7.1.0 here](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

El LabPlot és una aplicació KDE per a la creació interactiva de gràfics i l'anàlisi de dades científiques. El LabPlot proporciona una manera senzilla de crear, gestionar i editar gràfics i portar a terme anàlisis de dades. El [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) s'ha publicat després d'un any de treball.

A la versió 2.8, ara és més fàcil accedir a molts recursos en línia que proporcionen conjunts amb finalitats educatives. Ara hi ha cinc col·leccions amb més de 2000 conjunts de dades documentats. Vegeu aquest vídeo per a més informació:

{{< youtube pyP-5J3seQ8 >}}

Aquesta publicació aporta dos objectes nous de full de treball: línies de referència i elements d'imatge. Les línies de referència es poden col·locar lliurement al gràfic per a ressaltar uns valors determinats a les dades visualitzades. Els elements d'imatge permeten afegir una imatge al gràfic o al full de treball.

El full de càlcul proporciona estadístics descriptius dels conjunts de dades. S'ha ampliat aquesta informació afegint el calcul de quartils, trimean i del mode estadístic.

Gràcies a l'esforç realitzat en el Cantor durant els darrers mesos, els desenvolupadors han pogut afegir la compatibilitat amb els fitxers de projecte Jupyter en aquesta publicació del LabPlot.

S'ha posat més esforç en la versió macOS del LabPlot. A banda de moltes correccions petites específiques del macOS, també s'ha afegit la implementació de la barra tàctil que és disponible als models més nous del MacBook Pro.

[El Labplot es pot baixar](https://labplot.kde.org/download/) per al Linux, el Windows i el Mac.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

El nostre IDE de desenvolupament de programari, el [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released) aporta mig any de treball, enfocat principalment en l'estabilitat, el rendiment, i el manteniment futur. Afegeix una funcionalitat útil que [mostra opcionalment notes en línia dels problemes al final de la línia](https://blog.david-redondo.de/kde/2020/02/28/problems.html).

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

L'aplicació de calendari basada en el mòbil [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html) ja és aquí! Encara que ja s'han etiquetat un parell de versions, aquesta és la primera publicació estable de Calindori com a aplicació KDE.

El Calindori és disponible al KDE Neon en el Plasma Mobile així com en el postmartketOS. També és disponible al repositori «nightlies kdeapps» de flatpak per a ARM i x84_64. Finalment, també es pot construir des del codi font a la vostra estació de treball d'escriptori Linux. El codi font i les signatures es poden baixar des de [download.kde.org](https://download.kde.org/stable/calindori/).

## Kid3 3.8.4

El [Kid3](https://invent.kde.org/websites/kid3-kde-org) millora l'aplicació d'etiquetatge de fitxers de música.

A banda de la correcció d'errors, aquesta publicació proporciona millores d'usabilitat com més dreceres de teclat i la possibilitat de personalitzar-les. La detecció del tipus de fitxer és més tolerant.

Obteniu-lo com a [paquet Chocolatey de Windows](https://chocolatey.org/packages/kid3/), [paquet Homebrew de macOS](https://formulae.brew.sh/cask/kid3), [paquet F-Droid d'Android](https://f-droid.org/en/packages/net.sourceforge.kid3/), [Flatpak](https://flathub.org/apps/details/org.kde.kid3), [executable de macOS](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download), [executable de Windows](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download)

# Correcció d'errors

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

Per als desenvolupadors, s'ha creat una [publicació de correcció d'errors del Heaptrack](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0).El «heaptrack» rastreja totes les assignacions de memòria i anota aquests esdeveniments amb seguiments de pila. Les eines d'anàlisi dedicades permeten interpretar després el perfil de memòria en monticles.

Hi ha millores a l'estabilitat del Heaptrack que el fan més resilient a errors en enregistrar dades. Addicionalment, s'han efectuat ajustos menors a l'eina d'anàlisi gràfic que l'han fet més eficient. També s'ha millorat lleugerament el rendiment del pas d'anàlisi. Finalment, s'ha corregit un error important a l'algorisme de diferència de dades, que hauria de tornar a fer més útil aquesta funcionalitat.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

El KDiff3 permet comparar fins a tres fitxers a la vegada.

[Aquesta actualització](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html) corregeix: errors de tecleig, recàrrega de fitxers en el Windows, i la còpia en xarxa.

El KDiff3 es pot [baixar](https://download.kde.org/stable/kdiff3/) per al Linux, el Mac i el Windows.

## Tellico 3.3.3

El gestor de col·leccions [Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) ha millorat la font de dades Entrez (Pubmed) per a usar una clau d'API quan estigui disponible i respecta el límit de velocitat. Els desenvolupadors també han millorat la font de dades ISDBdb per cercar amb valors múltiples de l'ISBN.

## Konversation 1.7.6

L'aplicació de xat per IRC [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) ha corregit un error per construir-se amb més versions de les Qt i corregeix la càrrega del tema d'icones de sobrenoms.

## Connectors de visualització del Markdown

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="Foo" style="width: 600px">}}

Aquest mes s'han actualitzat un parell de connectors. La [Markdown Viewer KPart](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) i el [KMarkdownWebView](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) tenen publicacions noves que actualitzen les traduccions i corregeixen la construcció amb les Qt antigues. Aquests dos connectors donen vistes prèvies dels fitxers formatats amb Markdown al Kate i altres aplicacions KDE.

# Entrades

El KTechLab, un IDE per a microcontroladors i electrònica, és una aplicació antiga que no s'ha via publicat des de fa temps perquè usava una tecnologia antiga. El codi base del KTechLab s'ha actualitzat i ara ja hi ha la [versió 0.50.0](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) per provar-ho.

# Llançament 20.08.2

Diversos projectes es publiquen segons la seva pròpia escala de temps i altres es publiquen conjuntament. El paquet de projectes 20.08.2 s'ha publicat avui amb dotzenes de correccions d'errors i aviat estarà disponible a través de les botigues d'aplicacions i de les distribucions. Vegeu la [pàgina del llançament 20.08.2](https://www.kde.org/info/releases-20.08.2php) per als detalls.

Algunes de les correccions al llançament d'avui:

* S'ha corregit un error que provocava que l'editor de text Kate ocultava elements de la IU després de tancar una pestanya

* Ja no falla la baixada de documents de l'Office mitjançant la integració del GDrive al Dolphin en gestionar la redirecció requerida del servidor

* S'ha corregit el desplaçament en diagonal (p. ex. amb ratolins tàctils) a la vista de terminal del Konsole

* El visor de documents Okular ara permet fer clic als enllaços que estan coberts per anotacions

* Al Kontact, la configuració de l'arxivat automàtic del correu o el reenviament en línia d'un missatge ja no provoca una fallada

* El retallat automàtic de les divisions d'escenes a l'editor de vídeo Kdenlive ara retallen realment el vídeo, i s'ha fet més robusta l'obertura de projectes amb clips de vídeo que manquen

* L'importador d'imatges del Gwenview ja no es penja després de tancar

* L'eina Krfb de compartició remota de l'escriptori ara gestiona correctament les pantalles escalades

* Les connexions del telèfon amb el KDE Connect s'han fet més robustes després d'una revisió de seguretat

[Notes de llançament de la versió
20.08](https://community.kde.org/Releases/20.08_Release_Notes) &bull; [Pàgina
wiki de baixada de
paquets](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Pàgina d'informació del codi font de la versió
20.08.2](https://kde.org/info/releases-20.08.2) &bull; [Registre complet de
canvis de la versió 20.08.01](https://kde.org/announcements/changelog-
releases.php?version=20.08.2)