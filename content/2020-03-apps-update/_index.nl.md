---
layout: page
publishDate: 2020-03-05 15:50:00
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in maart 2020
type: announcement
---
## Nieuwe uitgaven

### Choqok 1.7

Februari begon met een lang verwachte update naar onze microblogging app [Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

Belangrijke reparaties bevatten de 280 tekenlimiet op Twitter, waarmee accounts uitschakelen en ondersteuning van uitgebreide tweets zijn toegestaan.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore en KDE Partition Manager 4.1.0

Ons programma voor formatteren van schijven Partition Manager heeft een nieuwe uitgave gekregen met veel werk gedaan aan de bibliotheek KPMCore die ook wordt gebruikt door het installatieprogramma van de Calamares distributie. De toepassing zelf heeft ondersteuning toegevoegd voor de Minix bestandssystemen.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partitiebeheerder" >}}

### KPhotoAlbum 5.6

Als u honderden of zelfs duizenden afbeeldingen hebt op uw vaste schijf, dan wordt het onmogelijk om het verhaal achter elke enkele afbeelding of de namen van de gefotografeerde personen te onthouden. KPhotoAlbum is gemaakt om u te helpen uw afbeeldingen te beschrijven en daarna zoeken in de grote stapel afbeeldingen snel en efficient te doen.

Deze uitgave brengt specifiek enorme prestatieverbeteringen bij tags aanbrengen in een groot aantal afbeeldingen en prestatieverbeteringen voor miniatuurvoorbeelden. Veel dank aan Robert Krawitz voor het opnieuw in de code duiken en plaatsen te vinden voor optimalisatie en verwijderen van dubbel werk!

Deze uitgave voegt bovendien ondersteuning toe voor het plug-in-framework met een doel van KDE.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Inkomend

De MP3 ID-tagbewerker Kid3 is verplaatst naar kdereview, de eerste stap naar het krijgen van een nieuwe uitgave.

En toepassing voor Rocket.chat Ruqola is door kdereview gegaan en is verplaatst om gereed te zijn voor uitgave. Het woord van de onderhouder Laurent Montel is zo dat herschrijven met Qt Widgets is gepland zodat er nog steeds veel werk gedaan moet worden.

## App Store

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Nieuw in de Microsoft Store voor Windows is [Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

Een moderne en prachtige muziekspeler met liefde gemaakt door KDE. Blader door en speel uw muziek heel gemakkelijk af! Elisa zou in staat moeten zijn om bijna alle muziekbestanden te lezen. Elisa is toegankelijk gemaakt voor iedereen.

Intussen is Digikam gereed om te worden toegestuurd naar the Microsoft Store.

## Bijgewerkte website

[KDE Connect](https://kdeconnect.kde.org/), ons handige hulpmiddel voor integratie van telefoon en bureaublad kreeg een nieuwe website.

[Kid3](https://kid3.kde.org/) heeft een glanzende nieuwe website met nieuws en download-links naar alle stores waaruit het beschikbaar is.

## Uitgave van 19.12.3

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en sommigen in massa vrijgegeven. De bundel 19.12.3 van projecten is vandaag vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor toepassingen en distributies. Zie de [19.12.3 uitgavepagina](https://www.kde.org/info/releases-19.12.3.php) voor details. Deze bundel was eerder genaamd KDE Applications maar heeft geen naam meer om een uitgaveservice te worden om verwarring te voorkomen met alle andere toepassingen door KDE en omdat het tientallen verschillende producten zijn in plaats van een enkel geheel.

Enige van de reparaties ingevoegd in deze uitgave zijn:

* Ondersteuning voor bestandattributen voor SMB-shares is verbeterd, [verschillende commits](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)

* K3b acceptseert nu WAV bestanden als audiobestanden [BUG 399056](https://bugs.kde.org/show_bug.cgi?id=399056) [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)

* Gwenview kan nu afbeeldingen op afstand laden via https [Commit](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [19.12 release notes](https://community.kde.org/Releases/19.12_Release_Notes)
voor informatie over tarballs en bekende problemen. 💻 [Wikipagina over
downloaden van
pakketten](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻
[19.12.3 infopagina over broncode](https://kde.org/info/releases-19.12.3) 💻
[19.12 full changelog](https://kde.org/announcements/changelog-
releases.php?version=19.12.3) 💻