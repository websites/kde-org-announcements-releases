---
layout: page
publishDate: 2020-06-11 12:00:00
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de junio de 2020"
type: announcement
---
Siempre resulta placentero que la familia de KDE crezca. Por ello, este mes nos sentimos especialmente felices al dar la bienvenida al gestor de copias de seguridad Kup y a un intento de empaquetado completamente nuevo: Homebrew.

# Nuevos lanzamientos

## Kup 0.8

[Kup](https://store.kde.org/p/1127689) es una herramienta de copias de seguridad que puede usar para mantener seguros sus archivos.

Anteriormente se había desarrollado fuera de KDE, pero este mes ha pasado el proceso de incubación y se ha unido a nuestra comunidad, convirtiéndose oficialmente en un proyecto de KDE. Su desarrollador principal, Simon Persson, lo ha celebrado con un nuevo lanzamiento.

Estos son los cambios que encontrará en la nueva versión:

* Se ha cambiado el modo en que se guardan las copias de seguridad de tipo «rsync» cuando solo se selecciona una carpeta de origen. Este cambio trata de minimizar el riesgo de borrar archivos para el usuario que seleccione una carpeta de destino que no esté vacía. Se ha añadido código de migración para detectar y mover archivos durante la primera ejecución, evitando así que se vuelva a copiar todo de nuevo y se duplique el espacio de almacenamiento.

* Se ha añadido una opción avanzada que permite indicar un archivo del que Kup leerá los patrones a excluir. Esto le permite, por ejemplo, decirle a Kup que nunca guarde archivos «*.bak».

* Se han modificado las preferencias predeterminadas, con la esperanza de mejorarlas.

* Se han reducido las advertencias sobre archivos que no se incluyen, ya que se producían demasiadas alarmas falsas.

* Kup ya no solicita contraseñas para desbloquear las unidades externas cifradas solo con el propósito de mostrar el espacio disponible.

* Se ha corregido que no se tratara una copia de seguridad como fallida solo porque los archivos no estuvieran disponibles durante la operación, tanto para «rsync» como para «bup».

* Ahora se comienzan a ejecutar comprobaciones y reparaciones de la integridad de la copia de seguridad en paralelo según el número de CPU.

* Ahora se puede usar la versión 3 de los metadatos de «bup», que se añadieron en la versión 0.30 de «bup».

* Se han realizado gran cantidad de correcciones en la interfaz de usuario.

Kup puede realizar copias de seguridad usando «rsync» o hacer copias de seguridad con versiones mediante la herramienta «Bup» de Python. En la actualidad, «Bup» funciona solamente con Python 2, lo que significa que esta opción no estará disponible en muchas distribuciones, aunque se está trabajando en una adaptación a Python 3.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

Para saber más sobre Kup, el sitio [Average Linux User escribió un artículo](https://averagelinuxuser.com/kup-backup/) e hizo un vídeo sobre Kup no hace mucho:

{{< youtube y53IE0G8Brg >}}

# Krita en tablets Android

Gracias al duro trabajo de Sharaf Zaman, Krita ya está disponible en la tienda Google Play para tablets con Android y Chromebooks (aunque no para teléfonos con Android).

Esta versión beta, basada en Krita 4.2.9, es la versión completa de Krita para el escritorio, por lo que no dispone de una interfaz táctil especial. Pero ya está ahí y se puede usar.

A diferencia de las tiendas para Windows y Steam, no se solicita dinero para Krita en esta tienda, ya que es el único modo de instalar Krita en dichos dispositivos. No obstante, se puede comprar una insignia de apoyo desde Krita para sostener su desarrollo.

Instalación

* Obtenga [Krita en la Google Play](https://play.google.com/store/apps/details?id=org.krita).

* De forma alternativa, cambie en la Play Store a la pestaña «Acceso anticipado» y busque «org.krita». (Consulte las instrucciones oficiales de Google para acceso anticipado. Hasta que tengamos un número considerable de descargas tendrá que desplazar el contenido de la página un poco).

* También puede [descargar los archivos apk usted mismo](https://files.kde.org/krita/android/). Por favor, **no** solicite ayuda para instalar estos archivos.

* Estos son todos los lugares oficiales en los que se puede obtener Krita. Por favor, **no** instale Krita usando otras fuentes, ya que no podemos garantizar su seguridad.

Notas

* Se puede ejecutar en tablets con Android y en Chromebooks. Versiones de Android compatibles: Android 6 (Marshmallow) y posteriores.

* En la actualidad no es compatible con los teléfonos con Android.

* Si había instalado previamente Krita usando un paquete compilado por Sharaf o compilado y firmado por usted mismo, deberá desinstalarlo previamente (para todos los usuarios).

{{< img class="text-center" src="krita-android.jpg" caption="Krita en Android" style="width: 600px">}}

# Novedades

[KIO Fuse](https://techbase.kde.org/Projects/KioFuse) ha realizado su primer [lanzamiento beta](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html) este mes.

## Corrección de errores

También han llegado correcciones de errores para:

* El gestor de colecciones [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html), con un diálogo de filtrado actualizado que le permite realizar búsquedas con texto vacío.

* En el navegador de la red local [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) se ha corregido la forma de guardar preferencias durante el cierre.

* Los desarrolladores del IDE [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) han realizado una actualización para los repositorios de KDE que se han trasladado.

# Tienda de aplicaciones

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

Mientras que en Linux nos vamos acostumbrando gradualmente a poder instalar aplicaciones individuales desde una tienda de aplicaciones, está ocurriendo lo contrario en el mundo de macOS y de Windows. En estos sistemas se están introduciendo gestores de paquetes para quienes prefieran tener una única fuente para controlar todo en su sistema.

La fuente más importante de repositorio de paquetes para macOS es Homebrew, administrada por un equipo de desarrolladores de primera línea que incluye al antiguo desarrollador de KDE Mike McQuaid.

Este mes, el proyecto [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde), que ha estado funcionando de forma externa a KDE durante un tiempo, se ha trasladado a KDE para convertirse en parte de nuestra comunidad.

Puede añadir el repositorio KDE Homebrew para macOS y descargar fuentes de KDE compiladas y listas para ejecutar.

Nos hemos puesto al día con el desarrollador principal Yurii Kolesnykov y le hemos preguntado sobre el proyecto.

### Háblanos sobre ti. ¿Cómo te llamas? ¿De dónde eres? ¿Cuál es tu interés en KDE y en Mac? ¿Cómo te ganas la vida?

Me llamo Yurii Kolesnykov y soy de Ucrania. Soy un apasionado del software libre desde la primera vez que oí hablar sobre él, aproximadamente cuando estaba terminando la escuela superior. Creo que KDE es simplemente el mejor entorno de escritorio para los sistemas Linux y Unix, con muchas grandes aplicaciones. Mi interés en Mac viene de mi ocupación principal: vivo del desarrollo de software móvil para iOS.

### ¿Qué es Homebrew?

Homebrew es el gestor de paquetes más popular para macOS, semejante a «apt» o «yum». Como macOS es un tipo de Unix y Apple proporciona un buen compilador y herramientas de desarrollo para él, alguien decidió crear gestores de paquetes para él, por lo que es posible instalar gran cantidad de software libre y de código abierto en Mac. Homebrew también dispone de un subproyecto denominado Homebrew Cask, que permite instalar muchas aplicaciones binarias, por ejemplo, tanto propietarias como con interfaz gráfica, ya que las aplicaciones con interfaz gráfica son difíciles de integrar con el sistema si se han instalado a través de Homebrew.

### ¿Qué paquetes de KDE has preparado para Homebrew?

He ejecutado «grep» en nuestro *tap* y he visto que tenemos un total de 110 paquetes, de los que 67 son *frameworks* y 39 son aplicaciones, aproximadamente. Ya tenemos las aplicaciones más populares, como Kate, Dolphin y KDevelop, a petición de los usuarios.

### Como usuario de Mac, ¿qué se necesita para instalar aplicaciones?

En primer lugar, es necesario seguir la guía de instalación de Homebrew, que está disponible en [brew.sh](https://brew.sh). Luego es necesario hacer *tap* de nuestro repositorio con lo siguiente:

`brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git`

Desafortunadamente, muchos paquetes de KDE no funcionan sin más, aunque hemos creado un guion que realiza todos pasos necesarios. Así que tras hacer *tap*, hay que ejecutar la siguiente orden:

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### ¿Es muy popular Homebrew como modo de obtener aplicaciones para Mac?

Buena pregunta. Por desgracia, todavía no hemos preparado ningún análisis, aunque lo añadiré a mi lista de cosas por hacer. Pero dado que Homebrew es el gestor de paquetes más popular para Mac y que requiere que el usuario no lo mezcle con otros proyectos similares para instalar software en el mismo Mac (por los conflictos que podría generar), sí, pienso que es bastante popular.

### ¿Cuánto trabajo has necesitado para que las aplicaciones de KDE funcionen en Homebrew?

Durante la creación de los paquetes actuales, hemos corregido bastantes problemas comunes, por lo que proporcionar nuevo software es relativamente fácil. Prometo escribir un *howto* para esto, algo que los usuarios ya me han pedido en muchas ocasiones.

### En la actualidad, los paquetes se deben compilar localmente. ¿Se dispondrá de paquetes precompilados?

Homebrew permite instalar software mediante *Bottles*, es decir, paquetes binarios precompilados. Pero el proceso de creación de *bottles* está estrechamente integrado en la infraestructura de Homebrew. Es decir, necesitamos ejecutar *CI* con pruebas en cada paquetes antes de crear su *bottle*. Por ello, hemos decidido integrar tantos paquetes como sea posible en el repositorio principal de *brew* para eliminar la carga de su mantenimiento.

### ¿Existe más software de escritorio disponible en Homebrew?

Sí. En general, si una aplicación es popular y posee un canal de distribución distinto de la Mac AppStore, existe una probabilidad muy alta de que ya esté disponible para instalar usando Brew Cask.

### ¿Cómo pueden los autores de aplicaciones de KDE tener su software en Homebrew?

El hardware de Apple es bastante caro, por lo que disponer de un Mac para cada desarrollador de KDE no sería una buena idea. Por ello, hasta ahora eran libres de crear una solicitud de funcionalidad en nuestro repositorio. A continuación, los encargados o los usuarios de Homebrew KDE informan de errores si algo no funciona como estaba previsto. Y estamos intentando proporcionar tanta información como sea posible a petición de los desarrolladores de KDE. Aunque hasta ahora tenemos un buen número de solicitudes pendientes para aplicaciones de KDE con pequeños aunque irritantes fallos. Espero que en el futuro estemos más integrados con la infraestructura de KDE; es decir, que podamos enlazar fallos de nuestro repositorio con los proyectos del proveedor. Ya hemos realizado la migración a KDE Invent y espero que el sistema de seguimiento de fallos de KDE se migre pronto de Bugzilla a KDE Invent.

### El otro modo de conseguir que una aplicación de KDE se compile para Mac es mediante Craft. ¿Cómo se comparan las aplicaciones compiladas con Homebrew con las compiladas con Craft?

Sigo pensando que Homebrew es más amigable para los usuarios finales. Su proceso de instalación es fácil como ejecutar una única línea. Para añadir nuestro repositorio y empezar a instalar aplicaciones contenidas en él, es necesario ejecutar otro par de líneas.

### Gracias por tu tiempo, Yurii.

# Lanzamiento de 20.04.2

Algunos de nuestros proyectos se publican según su propio calendario, mientras que otros se publican en masa. El paquete de proyectos 20.04.2 se ha publicado hoy y debería estar disponible próximamente en las tiendas de aplicaciones y en las distribuciones. Consulte la [página de lanzamientos 20.04.2](https://www.kde.org/info/releases-20.04.2.php) para más detalles.

Algunas de las correcciones de los lanzamientos de hoy:

* Las escrituras se descomponen en múltiples peticiones para los servidores SFTP que limitan el tamaño de las transferencias

* Konsole actualiza la posición del cursor para los métodos de entrada (como IBus o Fcitx) y ya no falla al cerrarse usando el menú

* KMail genera mejor HTML al añadir una firma en HTML a los mensajes

[Notas del lanzamiento de
20.04](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Página
wiki de descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Página de información principal sobre
20.04.2](https://kde.org/info/releases-20.04.2) &bull; [Registro completo de
cambios 20.04.2](https://kde.org/announcements/changelog-
releases.php?version=20.04.2)