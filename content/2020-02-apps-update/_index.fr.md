---
layout: page
publishDate: 2020-02-06 12:00:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE.
title: "Mise \xE0 jour des applications de KDE de F\xE9vrier 2020"
type: announcement
---
Ce mois a vu un très grand nombre de corrections de bogues. Bien que tous, nous aimons les nouvelles fonctionnalités, nous savons aussi que vous appréciez que les erreurs et les plantages soient identifiés et résolus.

# Nouvelles mises à jour

## KDevelop 5.5

La mise à jour la plus importante de ce mois concerne [KDevelop 5.5] (https://www.kdevelop.org/news/kdevelop-550-released), l'environnement de développement de KDE, rendant plus facile l'écriture de programmes en C++, Python et PHP. Les captures d'init Lambda dans C++ ont été ajoutés. Les sélections de checkset prédéfinis configurables sont présents dans « Clazy-tidy » et « Clang ». Le complètement anticipé est renforcée. Le langage PHP intègre les propriétés typées de PHP 7.4 et les tableaux de visibilité constante de type et de classe sont pris en charge. Pour Python, la version 3.8 est maintenant prise en charge.

KDevelop est disponible pour votre distribution Linux comme un exécutable « AppImage ».

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) est notre traqueur de liste de tâches à faire, intégré dans Kontact. Les mises à jour de Kontact ont rompu le lien avec Zanshin. Les utilisateur été tenus informés des tâches à réaliser pour les corrections. Mais, ce mois-ci, une mise à jour a été faite pour le [rendre de nouveau opérationnel] (https://jriddell.org/2020/01/14/zanshin-0-5-71/). Maintenant, nous pouvons tous trouver les tâches que nous avons de réaliser avec Zanshin !

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte-dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

L'éditeur de code hexa-décimal de KDE, [Okteta] (https://kde.org/applications/utilities/org.kde.okteta), a été mis à jour pour corriger des bogues. Il intègre une nouvelle fonctionnalités, un algorithme de calcul de somme de contrôle « CRC-64 ». Par ailleurs, Okteta a été mis à jour pour s'intégrer avec les nouvelles versions de Qt et de l'environnement de travail de KDE.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

[KMyMoney](https://kmymoney.org/), l'application de KDE qui vous aide à gérer vos finances a été mise à jour [pour corriger de nombreux bogues] (https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html). Une amélioration a été ajoutée pour prendre en charge les formulaires de vérification avec le protocole de séparation.

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Entrant

Keysmith est un générateur de code à deux facteurs pour Plasma Mobile et le bureau Plasma, en utilisant la boîte à outils « oath ». L'interface utilisateur est écrite dans le langage « Kirigami », ce qui le rend adapté à toutes les tailles d'écran. Les mises à jour sont attendues très bientôt.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) est un gestionnaire de paquets pour le système d'exploitation Windows. Chocolatey est conçu pour l'installation et la mise à jour de tous les logiciels de votre système et peut rendre la vie plus facile au utilisateurs sous Windows.

[KDE a un compte pour Chocolatey] (https://chocolatey.org/profiles/KDE). Ainsi, vous pouvez installer Digikam, Krita, KDevelop et Kate grâce à Chocolatey. L'installation est très simple. Tout ce que vous avez à faire est de lancer la commande « choco install kdevelop » et Chocolatey fera le reste. Les mainteneurs de Chocolatey s'imposent des exigences fortes pour vous garantir que les paquets sont testés et sécurisés.

Si vous êtes un mainteneur d'applications de KDE, vous pouvez considérer votre application comme acceptée et si vous un gestionnaire important de déploiement Windows, vous pouvez l'utiliser pour vos installations.

# Mises à jour des sites Internet

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}Our web team continues to update our online presence and has recently refreshed the [KMyMoney website](https://kmymoney.org).

# Mises à jour 19.12.2

Les mises à jour de certains projets se font selon leurs propres plannings et quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la version -19.12.2 a publié leurs mises à jour aujourd'hui. Celles-ci devraient être disponibles très bientôt dans les dépôts d'applications et dans les distributions. Veuillez consulter [la page des mises à jour 19.12.2] (https://www.kde.org/announcements/releases/19.12.2.php) pour plus de détails. Ce groupe était précédemment appelé les applications de KDE. Mais, ce terme a été abandonné pour devenir un service de mise à jour pour éviter la confusion avec les autres applications de KDE. La raison est qu'il y a des douzaines de logiciels différents, qui ne sont pas à considérer comme un ensemble unique.

Certaines de ces corrections incluses dans cette mise à jour sont :

* Le lecteur de musique [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) gère maintenant les fichiers ne contenant aucune méta-donnée.

* Les pièces jointes enregistrés dans le dossier « Brouillon » ne disparaissent plus lors de la relecture d'un message pour modification dans [KMail](https://kde.org/applications/internet/org.kde.kmail2)

* Un problème de temporisation a été corrigée dans l'afficheur de documents [Okular](https://kde.org/applications/office/org.kde.okular) qui conduisait à l'arrêt du rendu.

* Le modeleur UML, [Umbrello](https://umbrello.kde.org/) UML intègre maintenant une fonction d'import Java amélioré.

[Les notes pour la version 19.12.2]
(https://community.kde.org/Releases/19.12_Release_Notes) &bull; [La page de
tutoriel pour le téléchargement]
(https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [La
page d'information sur les sources pour la version 19.12.2]
(https://kde.org/info/releases-19.12.2) &bull; [La liste complète des
modifications pour la version 19.12.2] (https://kde.org/announcements/changelog-
releases.php?version=19.12.2)