---
layout: page
publishDate: 2020-07-09 12:00:00
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de julio de 2020"
type: announcement
---
# Nuevos lanzamientos

### KTorrent 5.2.0

La aplicación para compartir archivos [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) ha publicado la versión [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

La mejora de importación para compartir es la [tabla de hash distribuida](https://en.wikipedia.org/wiki/Mainline_DHT), que ahora arranca con nodos bien conocidos para que pueda disponer de las descargas de forma más rápida. Bajo el capó, se ha actualizado al reciente QtWebengine, que está basado en Chrome, en lugar de usar el más antiguo QtWebkit, basado en WebKit (todos están basados en el KHTML de KDE de antaño).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

KTorrent está disponible en su distribución Linux.

### Publicada KMyMoney 5.1.0

La aplicación de banca [KMyMoney](https://kmymoney.org/) [ha publicado la versión 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

En ella se ha añadido el uso del símbolo de la rupia india: ₹. También se ha añadido la opción para «revocar cargos y pagos» a la importación OFX y la vista de presupuestos muestra ahora todo tipo de cuentas.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[KMyMoney está disponible](https://kmymoney.org/download.html) en su distribución Linux, como descarga para Windows y para Mac, y ahora también en [Homebrew KDE](https://invent.kde.org/packaging/homebrew-kde).

### Notas de lanzamiento de KDiff3 1.8.3

La herramienta de comparación de archivos [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) ha lanzado la nueva versión 1.8.3, que contiene gran cantidad de correcciones de estabilidad.

El uso de KDiff3 como herramienta de diferencias para Git ya no provoca errores con los archivos que no existen. Los errores que ocurren durante la comparación de directorios se encolan correctamente para que solo se muestre un único mensaje. Se ha corregido la recarga en Windows. Se ha eliminado un bloqueo de la aplicación cuando el portapapeles no está disponible. Se ha revisado la conmutación a pantalla completa para evitar una problemática llamada a la API de Qt.

Puede [descargar KDiff3](https://download.kde.org/stable/kdiff3/) para Windows, Mac y para su distribución Linux.

# Tienda de aplicaciones

### Estadísticas de la Microsoft Store

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) nos ha proporcionado una actualización de la tienda de Microsoft. Kate y Okular se han actualizado y en el último mes se han instalado unas 4000 veces.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# Entrevista de la App Store: Flathub

Flatpak es uno de los nuevos formatos basados en contenedores que están cambiando el modo de obtener aplicaciones en Linux. Flatpak puede funcionar en cualquier máquina que quiera configurar una tienda, pero la tienda definitiva es [Flathub](https://flathub.org/home).

Recientemente, el ayudante de Flathub [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [pidió ayuda para que hubiera más aplicaciones de KDE en la tienda](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514).  Lo hemos entrevistado para conocer más detalles.

### Háblanos sobre ti: ¿de dónde eres?, ¿cómo te ganas la vida?, ¿desde cuándo te interesas por el código abierto y por Flatpak?

Me llamo Timothée Ravier y en la actualidad vivo en París, Francia. Soy ingeniero de sistemas Linux y actualmente trabajo para Red Hat en Red Hat CoreOS y en Fedora CoreOS.

Llevo metido en el código abierto desde la primera vez que instalé una distribución Linux en 2006 y no he parado desde entonces. La mayor parte de los proyectos de investigación que llevé a cabo durante mis estudios estaban relacionados con la seguridad de Linux, con los entornos de pruebas para aplicaciones y con la seguridad de la interfaz gráfica. De este modo, la introducción y el desarrollo de Flatpak despertaron mi interés.

En mi tiempo libre mantengo la [variante no oficial de KDE (apodada Kinoite)](https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) de Fedora Silverblue. En pocas palabras, Fedora Silverblue es un sistema operativo inmutable y la forma recomendada de instalar aplicaciones es mediante el uso de Flatpaks o de contenedores (a través de «podman»). Se puede consultar su documentación para más detalles.

### ¿Qué te hizo lanzar tu reciente solicitud de aplicaciones de KDE en Flathub?

En primer lugar quiero decir un enorme «¡Gracias!» a los encargados actuales que ya mantienen aplicaciones de KDE en Flathub, ya que están haciendo un gran trabajo.

He usado KDE durante mucho tiempo (empecé en 2006) y siempre he querido aportar mi parte. Las distribuciones ya disponen de equipos de mantenimiento bien establecidos y Flathub carece de un buen número de aplicaciones de KDE, por lo que me pareció un buen lugar para empezar.

También hice una solicitud de colaboración, ya que resultaría más fácil si dividíamos el trabajo. Además, eso también haría que más personas tuvieran conocimiento de Flatpak y de Flathub.

### Flatpak puede trabajar desde cualquier repositorio, ¿por qué necesitamos Flathub?

Esta pregunta resalta una de las ventajas de Flathub: puedes tener tu propio repositorio de aplicaciones en tu servidor y distribuirlas directamente a tus usuarios. No «necesitas» Flathub.

Pero, del mismo modo que no necesitas GitHub ni GitLab para tener un repositorio Git, resulta más fácil colaborar si tienes un único lugar al que dirigir a los usuarios y a los desarrolladores.

Flathub se ha convertido en el lugar más fácil para encontrar y probar con seguridad aplicaciones de Linux, tanto de código abierto como propietarias. Creo que esto resulta crítico si queremos mejorar el atractivo del ecosistema Linux como plataforma de escritorio.

### ¿Qué otras comunidades de código abierto han aceptado poner sus aplicaciones en Flathub?

Creo que muchas (tal vez la mayoría) de las aplicaciones de GNOME ya están disponibles en FlatHub.

### Ahora que los desarrolladores de aplicaciones pueden publicar nuestro software directamente en tiendas del tipo Flathub existen nuevas responsabilidades, como la seguridad y mantener el software actualizado. ¿Puedes decirnos cómo se gestiona esto en Flathub?

Con Flathub, las responsabilidades se comparten entre los mantenedores de la «plataforma» y los mantenedores de las aplicaciones.

La «plataforma» contiene la biblioteca principal, común para gran número de aplicaciones (existen las plataformas Freedesktop, GNOME y KDE) y se mantiene para preservar la compatibilidad binaria y para asegurar rápidas actualizaciones de seguridad.

Las actualizaciones del resto de bibliotecas que necesitan las aplicaciones y de las mismas aplicaciones son responsabilidad de quienes mantienen las aplicaciones.

### ¿Qué aplicaciones de KDE encuentras más útiles?

Uso diariamente Dolphin, Konsole, Yakuake, Okular y Ark, y realmente me gustan. También aprecio y uso de vez en cuando Gwenview, KCachegrind y Massif-Visualizer.

### Muchas de nuestras aplicaciones están empaquetadas como Flatpaks a través de nuestros [invent](https://invent.kde.org/packaging/flatpak-kde-applications) y [servidores binary-factory](https://binary-factory.kde.org/view/Flatpak/). ¿Trabajáis con estos dos procesos o de forma separada?

Los Flatpaks que se crean en la infraestructura de KDE tienen como objetivo ser compilaciones nocturnas para que las puedan probar los desarrolladores y los usuarios. Esto es un buen cúmulo de aplicaciones Flatpak con las que empezar, aunque algunas de ellas también necesitan actualizaciones. Mantener este repositorio al día nos ayudará con los desarrollos recientes que pueden necesitar cambios de empaquetado en Flathub. Todavía no he empezado a actualizarlas, pero trataré de hacerlo junto al envío de aplicaciones a Flathub.

### ¿Puedes divisar el momento en el que no existan RPM ni Apt y todas las distribuciones Linux usen paquetes contenedores?

No creo que esto llegue a ocurrir, ya que se valora el modo que tienen las distribuciones de empaquetar aplicaciones en la actualidad, incluso aunque tengan problemas. Pero creo que menos distribuciones se esforzarán en hacerlo. Por ejemplo, Fedora crea Flatpaks a partir de paquetes RPM y los pone a disposición de todo el mundo. En teoría se puede hacer lo mismo con los paquetes de Debian. La importancia aquí no está en el qué, sino en el quién: ¿te fías de esta distribución?, ¿de sus valores?, ¿de su compromiso con el software libre solo? En tal caso estarás seguro de que las aplicaciones que instales de su repositorio cumplirán los mismos requisitos que cualquier otro paquete. Flathub contiene tanto aplicaciones de código abierto como propietarias, y puede que eso no satisfaga a todos.

# Lanzamientos ya disponibles en kde.org/applications

Nuestra [página de aplicaciones](https://kde.org/applications) ha empezado a mostrar información de lanzamientos. Se esperan más próximamente. Si mantiene alguna aplicación, recuerde añadir la información sobre lanzamientos en los archivos Appstream.

{{< img class="text-center" src="krita-release.png" caption="Información sobre el lanzamiento" style="width: 800px">}}

# Lanzamiento de 20.04.3

Algunos de nuestros proyectos se publican según su propio calendario, mientras que otros se publican en masa. El paquete de proyectos 20.04.3 se ha publicado hoy y debería estar disponible próximamente en las tiendas de aplicaciones y en las distribuciones. Consulte la [página de lanzamientos 20.04.3](https://kde.org/info/releases-20.04.3) para más detalles.

Algunas de las correcciones de los lanzamientos de hoy:

* Se ha corregido el uso de rutas de iconos absolutas en las vistas previas de archivos de escritorio en Dolphin.

* Los elementos de tareas pendientes que se hayan completado se guardan correctamente en el diario de KOrganizer.

* El texto multilínea que se pega desde aplicaciones GTK en Konsole ya no contiene caracteres de «nueva línea» adicionales.

* Se ha corregido el comportamiento de maximización de Yakuake.

[Notas del lanzamiento de
20.04](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Página
wiki de descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Página de información principal sobre
20.04.3](https://kde.org/info/releases-20.04.3) &bull; [Registro completo de
cambios 20.04.3](https://kde.org/announcements/changelog-
releases.php?version=20.04.3)