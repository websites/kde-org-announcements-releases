---
layout: page
publishDate: 2019-12-12 13:01:00
summary: What Happened in KDE's Applications This Month
title: KDE's December 2019 Apps Update
type: announcement
---
# New versions of KDE applications landing in December

The release of new versions for KDE applications is part of KDE's continued effort to bring you a complete and up-to-date catalog of fully-featured, beautiful and useful programs for your system.

Available now are new versions of KDE's file browser Dolphin; Kdenlive, one of the most complete open source video editors; the document viewer Okular; KDE's image viewer, Gwenview; and all of your other favorite KDE apps and utilities. All of these applications have been improved, making them faster and more stable and they boast exciting new features. The new versions of KDE applications let you be productive and creative, while at the same time making use of KDE software easy and fun.

We hope you enjoy all the novel features and improvements worked into all of KDE's apps!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) Reloaded

[Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) is getting a big release in December. KDE's project planning and management tool has hit a huge milestone since the prior version was released almost two years ago.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Plan helps you manage small and large projects with multiple resources. In order for you to model your project, Plan offers different types of task dependencies and timing constraints. You can define your tasks, estimate the effort needed to perform each, allocate resources and then schedule the project according to your needs and the resources available.

One of Plan's strengths is its excellent support for Gantt charts. Gantt charts are charts made up of horizontal bars that provide a graphical illustration of a schedule and help plan, coordinate, and track specific tasks in a project. Using Gantt charts in Plan will help you better monitor your project's workflow.

## Pump up the Volume on [Kdenlive](https://kdenlive.org)

Developers of [Kdenlive](https://kdenlive.org) have been adding new features and squashing bugs at breakneck speed. This version alone comes with more than 200 commits.

A lot of work has gone into improving support for audio. In the "bug solved" department they have gotten rid of a huge memory consumption issue, inefficiency of audio thumbnails, and have sped up their storage.

But even more exciting is that Kdenlive now comes with a spectacular new sound mixer (see image). Developers have also added a new audio clip display in the clip monitor and the project bin so you can better synchronize your moving images with the soundtrack.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Kdenlive's audio mixer" >}}

In the "resolving paper-cuts" department, Kdenlive has acquired many performance and usability improvements, and developers have fixed many bugs in the Windows version of the video editor.

## [Dolphin](https://userbase.kde.org/Dolphin)'s Previews and Navigation

KDE's powerful file browser [Dolphin](https://userbase.kde.org/Dolphin) adds new features to help you find and reach what you are looking for in your file system. One such feature is the redesigned advanced search options and another is that now you can go backwards and forwards in the history of places you have already visited multiple times by long-pressing the arrow icon in the toolbar. The feature that lets you quickly reach recently saved or recently accessed files has been reworked and its glitches ironed out.

One of the main concerns of Dolphin's developers is to allow users to preview the contents of files before they open them. For example, in the new version of Dolphin you can preview GIFs by highlighting them and then hovering over the preview pane. You can also click and play videos and audio files in the preview pane.

If you are a fan of graphic novels, Dolphin can now display thumbnails for .cb7 comic books (check out the support for this comic book format also in Okular -- see below). And, talking about thumbnails, zooming in on thumbnails has been a thing in Dolphin for a long time now: hold down <kbd>Ctrl</kbd>, spin the wheel and the thumbnail will grow or shrink. A new functionality is that now you can reset the zoom to the default level by hitting <kbd>Ctrl</kbd> + <kbd>0</kbd>.

Another useful feature is that Dolphin shows you which programs are preventing a device from being unmounted.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): Let your Phone Control your Desktop

The latest version of [KDE Connect](https://community.kde.org/KDEConnect) comes stuffed with new features. One of the most noticeable is that there is a new SMS app that lets you read and write SMS with the full conversation history.

A new [Kirigami](https://kde.org/products/kirigami/)-based user interface means you can run KDE Connect not only on Android, but also on all those mobile Linux platforms we'll be seeing in upcoming devices like the PinePhone and the Librem 5. The Kirigami interface also provides new features for desktop-to-desktop users, such as media control, remote input, device ringing, file transfer and running commands.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="The new interface on desktop" >}}

You could already use KDE Connect to control the volume of media playing on your desktop, but now you can use it to also control your system's global volume from your phone -- super handy when using your phone as a remote. When giving a talk, you can also control your presentation using KDE Connect to flip forward and back through your slides.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Another feature that has been around for while is how you can use KDE Connect to send a file to your phone, but what's new in this version is that you can have your phone immediately open the file once it is received. KDE Itinerary uses this to send travel information to your phone from within KMail.

In a related note, you can now also send files from Thunar (Xfce's file manager) and Elementary applications such as Pantheon Files. Displaying progress of the copy of multiple files has been improved a lot and files received from Android now show their correct modification time.

When it comes to notifications, you can now trigger actions from Android notifications from the desktop and KDE Connect uses advanced features from Plasma's notification center to provide better notifications. The pairing confirmation notification does not time out so you can't miss it any more.

Finally, if you are using KDE Connect from the command line, *kdeconnect-cli* now features improved *zsh* autocompletion.

## Remote Images [Gwenview](https://userbase.kde.org/Gwenview)

[Gwenview](https://userbase.kde.org/Gwenview) now lets you adjust the JPEG compression level when you save images you have edited. Developers have also improved the performance for remote images and Gwenview can now import photos both to or from remote locations.

## [Okular](https://kde.org/applications/office/org.kde.okular) support .cb7 format

[Okular](https://kde.org/applications/office/org.kde.okular), the document viewer that lets you read, annotate and verify all sorts of documents, including PDFs, EPubs, ODTs and MDs, now also supports files using the .cb7 comic book format.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): Simple but Complete Music Player

[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) is a KDE music player that combines simplicity with an elegant and modern interface. In its latest version, Elisa has optimized its looks to adapt better to High DPI screens. It also integrates better with other KDE applications and has acquired support for KDE's global menu system.

Indexing music files has also improved and Elisa now supports web radios and ships with a few examples for you to try.

{{< img src="elisa.png" caption="Elisa updated interface" >}}

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) is Touchscreen-Ready

[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), KDE's default screen-grabbing program, is another KDE application that has evolved to acquire touchscreen capabilities: Its new touch-friendly drag handler makes it much easier to drag rectangles to capture the parts of the desktop you want on tactile-enabled devices.

Other improvements are the new autosave function, which comes in handy for rapidly taking multiple screenshots, and the animated progress bars, which make sure you can clearly see what Spectacle is doing.

{{<img style="max-width:500px" src="spectacle.png" caption="Spectacle new drag handler" >}}

## [Plasma Browser Integration](https://community.kde.org/Plasma/Browser_Integration) Improvements

The new release of [Plasma Browser Integration](https://community.kde.org/Plasma/Browser_Integration) now features a blacklist for the media controls functionality. This is useful for when you visit a site with a lot of media you can play, which can make it difficult to select what you want to control from your desktop. The blacklist lets you exclude these sites from the media controls.

The new version also lets you keep the origin URL in file metadata and adds support for the Web Share API. The Web Share API lets browsers share links, text and files with other apps in the same way KDE apps do, improving the integration of non-native browsers like Firefox, Chrome/Chromium and Vivaldi, with the rest of KDE's applications.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

Quite a few of KDE applications can be downloaded from the Microsoft Store. Krita and Okular have been there for a while, and they are joined now by [Kile](https://kde.org/applications/office/org.kde.kile), a user-friendly LaTeX editor.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## New

Which brings us to new arrivals: [SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), an application that lets you easily create subtitles for videos, is now in the KDE Incubator, on track to become a full member of KDE's applications family. Welcome!

Meanwhile, [plasma-nano](https://invent.kde.org/kde/plasma-nano), a minimal version of Plasma designed for embedded devices was moved into the Plasma repos ready for release with 5.18.

## Release 19.12

Some of our projects release on their own timescale and some get released en-masse. The 19.12 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12 releases page](https://www.kde.org/announcements/releases/19.12). This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because it is dozens of different products rather than a single whole.

