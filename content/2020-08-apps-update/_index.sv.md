---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i augusti 2020
type: announcement
---
Dussintals [KDE-program](https://kde.org/applications/) har fått nya utgåvor från KDE:s utgivningstjänst. Nya funktioner, användbarhetsförbättringar, omkonstruktioner och felrättningar bidrar alla till att höja din produktivitet och göra den här nya omgången program effektivare och angenäma att använda.

Här är vad du kan se fram mot:

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin visar förhandsgranskningar av många typer av filer." style="width: 800px" >}}

Dolphin är KDE:s filutforskare som hjälper dig att söka, kopiera och öppna filer och kataloger. Som sådan är en av dess huvudfunktioner att ge en klar bild av vad varje fil innehåller. Att visa miniatyrbilder är avgörande för det, och Dolphin har redan kunnat visa förhandsgranskningar av bilder, videoklipp, och till och med Blender 3D-filer under lång tid.

I den nya versionen lägger Dolphin till miniatyrbilder för 3D-tillverkningsformatfiler (3MF) i listan och du kan också förhandsgranska filer och kataloger på krypterade filsystem såsom Plasma valv. Det görs säkert genom att lagra miniatyrbilder på själva filsystemet, eller återgå till att generera dem men inte lagra versioner någonstans om det är nödvändigt. Hur som helst, har du fullständig kontroll över hur och när Dolphin visar innehållet i en fil, eftersom du kan oberoende ställa in filstorleksgränsen för visning av förhandsgranskningar för lokala filer och fjärrfiler.

Ett annat sätt att identifiera filer är enligt namn. Om filnamnet är för långt att visa, har utvecklarna förfinat avkortningsbeteendet för filnamn. Istället för att klippa bort mitten på ett långt fil- eller katalognamn som Dolphin gjorde i tidigare versioner, men den nya versionen klipper bort slutet och låter alltid filändelsen vara synlig efter uteslutningstecknet (om den finns). Det gör det mycket enklare att identifiera filer med långa namn.

Dolphin kommer nu ihåg och återställer platsen som du tittade på, samt de öppna flikarna och delade vyer som visades när det senast stängdes. Funktionen är normalt på, men kan stängas av på sidan "Start" i Dolphins fönster "Inställningar".

På tal om användbarhetsförbättringar, så har Dolphin alltid låtit dig montera och utforska delade fjärrkataloger, vare sig det är via FTP, SSH, eller andra protokoll. Men nu visar Dolphin fjärrmonteringar och FUSE-monteringar med användarvänliga visningsnamn istället för hela sökvägen. Ännu bättre är att du också kan montera ISO-avbilder med ett nytt menyalternativ i den sammanhangsberoende menyn. Det betyder att du kan ladda ner och utforska ett filsystem som du senare kommer att bränna på en disk eller USB-minne.

{{< video src="dolphin_ISO.mp4" caption="Dolphin låter dig montera ISO-avbilder." style="max-width: 900px" >}}

Dolphin har ett häpnadsväckande antal funktioner, men du kan lägga till ännu fler via  insticksprogram. Om du tidigare ville lägga till insticksprogrammen från menyn "Tjänst" (titta på <I>Inställningar</I> > <I>Anpassa Dolphin</I> > <I>Tjänster</I>) var du ofta tvungen att köra ett skript eller installera ett distributionspaket. Nu kan du installera dem direkt från fönstret "Ladda ner nya tjänster" utan några manuella mellanliggande steg. Andra ändringar av inställningssidan "Tjänster" är att den har ett helt nytt sökfält längst upp för att hjälpa till att snabbt hitta vad du letar efter, och tjänstlistan sorteras nu alfabetiskt.

Att flytta omkring saker är enklare med nya Dolphin, eftersom du nu har nya åtgärder för att snabbt flytta eller kopiera markerade filer i en ruta av en delad vy till katalogen i den andra rutan. För att klargöra vad som pågår när en fil dras, ändras nu pekaren normalt till en greppande hand istället för "kopieringspekaren" som gjordes i tidigare versioner.

Du kan också beräkna och visa katalogstorlekar på disk genom att använda vyn "Detaljer", och Dolphins informationspanel visar nu användbar information om papperskorgen. Det är också möjligt att navigera från Dolphins sökfält till resultatvyn genom att trycka på tangenten neråtpil.

Till sist, en ny användbar funktion är att Dolphin 20.08 levereras med ett nytt menyalternativ “Kopiera plats”...

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Terminal har fått några nya spännande funktioner." style="width: 800px" >}}

... precis som i KDE:s terminalprogram, Terminal. Det betyder att du kan högerklicka på ett objekt som visas i Terminal, välja "Kopiera plats" i den sammanhangsberoende menyn och sedan kopiera och klistra in filen eller katalogens fullständiga sökväg i ett dokument, en annan terminal, eller ett program.

Terminal levereras också med en ny funktion som visar en subtil markering för nya rader som dyker upp när terminalutmatningen snabbt rullar förbi, och visar normalt en förhandsgranskning med miniatyrbild för bildfiler när du håller pekaren över dem. Dessutom visar den sammanhangsberoende menyn "Öppna med" när du högerklickar på en understruken fil i Terminal, så att du kan öppna filen i ett godtyckligt program som du väljer.

Att kunna dela vyn i Terminal och lägga till flikar för att göra flera saker på samma gång har varit en funktion under lång tid, men nu kan huvuden för delade vyer inaktiveras, och avgränsarens bredd kan ökas. Du kan också övervaka en flik för att se när den aktiva processen är färdig och tilldela färger till flikar i Terminal.

{{< img class="text-center" src="konsole-monitor.webp" caption="Ny övervaka process i Terminal" style="width: 500px" >}}

På tal om flikar, har Terminals standardgenväg för "Koppla loss aktuell flik" tagits bort. Det visade sig att det var för lätt att koppla loss den aktuella fliken av misstag när det man ville var att rensa skärmen med Ctrl+Skift+K.

Den sista användbarhetsförbättringen i den här versionen är att Terminals I-formade markör nu följer teckenstorleken istället för att alltid ha samma storlek.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Ny Yakuake-ikon" style="width: 200px" >}}

På tal om terminaler, är Yakuake KDE:s uppdykande terminalemulator. Den viks ut från skärmens överkant varje gång du trycker på F12, och den har också fått ett antal förbättringar i den här utgåvan. Exempelvis låter den dig nu ställa in alla snabbtangenter som kommer från Terminal, och det finns ett nytt objekt i systembrickan som visar dig när Yakuake kör. Du kan förstås dölja det, men det gör det enklare att avgöra om Yakuake är aktivt och färdigt eller inte. Det tillhandahåller också ett grafiskt sätt att visa det.

Dessutom har vi också förbättrat Yakuake på Wayland. Huvudfönstret brukade dyka upp under toppaneler som visades, men det har nu rättats.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

Digikam är KDE:s professionella fotohanteringsprogram, och det råkar vara så att [gruppen just har givit ut en ny huvudversion, 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Ansiktsigenkänning i Digikam" style="width: 800px" >}}

En av höjdpunkterna i Digikam 7.0.0 är förbättringarna av ansiktsigenkänning som nu använder AI-algoritmer med djup inlärning. Du kan titta på föredraget på Akademy förra året för att se hur det gjordes, och det tillkommer mer arbete i framtida versioner för ansiktsigenkänning och automatisk detektering.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Ansiktsigenkänning i Digikam" style="max-width: 800px" controls="true" >}}

Det [huvudsakliga Flatpak-paketet för digiKam är tillgängligt via Flathub](https://flathub.org/apps/details/org.kde.digikam), samt ett instabilt nattligt bygge för testare. DigiKam är också tillgängligt för Linux visa AppImage och distributionens arkiv. Det finns också versioner av Digikam för Windows och Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

Vi har gjort några modifieringar av utseendet hos vår texteditor Kate. Menyn "Öppna senaste" visar nu dessutom dokument öppnade i Kate från kommandoraden och andra källor, inte bara de som öppnades med användning av fildialogrutan. En annan ändring är att Kates flikrad nu är visuellt förenlig med alla flikrader i andra KDE-program, och visar nya flikar till höger, som de flesta andra flikrader gör.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa är en lättviktig musikspelare för skrivbordet och mobilen." style="width: 800px" >}}

Elisa är en lättviktig musikspelare som fungerar på ditt skrivbord och i din mobiltelefon. Elisa låter dig nu visa genrer, artister eller album i sidoraden, under andra objekt. Spellistan visar också förloppet för låten som för närvarande spelas på plats, och överraden svarar nu på fönsterstorlek och enhetens formfaktor. Det betyder att den nu ser bra ut med ett fönster eller apparat med stående orientering (exempelvis på din telefon) och kan skalas ner att vara mycket liten.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="Kstars är KDE:s program för astronomientusiaster." style="width: 800px">}}

Slutligen har astronomiprogrammet Kstars lagt till viss kalibrering och fokusering i den [nya utgåvan](http://knroblogspot.com/2020/07/kstars-v343-is-released.html). [En Bahtinov-mask är en apparat som används för att noggrant fokusera små astronomiska teleskop.](https://en.wikipedia.org/wiki/Bahtinov_mask). Den är användbar för användare som inte har en motordriven fokuseringsenhet och föredrar att fokusera manuellt med hjälp av en mask. Efter att ha tagit en bild i fokuseringsmodulen med Bahtinov-mask algoritmen vald, analyserar Ekos bilderna och stjärnorna inom den. Om Ekos känner igen Bahtinov-stjärnmönstret ritas linjer över stjärnmönstret med cirklar i mitten och med en förskjutning för att indikera fokuseringen.

{{< img class="text-center" src="kstars-focus.webp" caption="Kstars nya fokuseringsalgoritm" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="Kstars kalibrering" style="width: 600px" >}}

Vi har lagt till en delflik med ett "Kalibreringsdiagram" till höger om "Avdriftsgrafiken". Det visar lagrade stativpositioner under kalibrering med kalibreringsverktyget vid intern guidning. Om allt går bra, ska det visa punkter i två linjer som är vinkelräta mot varandra: en då kalibreringen flyttar stativet framåt och bakåt längs RA-riktningen, och en då den gör samma sak längs DEK-riktningen. Inte så mycket information, men det kan vara användbart att se. Om de två linjerna har en 30-graders vinkel, är det något som inte går bra med kalibreringen. Ovan visas en bild av det i arbete med användning av simulatorn.

[KStars är tillgängligt för nerladdning](https://edu.kde.org/kstars/) för Android, från Snapcraft-butiken, för Windows, Mac, och förstås från Linux-distributionernas arkiv.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="KRDC med fjärrpekare" style="max-width: 900px" >}}

KRDC låter dig visa och kontrollera en annan dators skrivbordssession. Versionen som ges ut idag visar nu riktiga pekare från serversidan i VNC istället för en liten punkt med eftersläpande fjärrpekare.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

Okular är KDE:s dokumentvisare. Den låter dig läsa PDF-dokument, ePUB-böcker och många andra textbaserade filer. En rättning har placerat menyalternativen "Skriv ut" och "Förhandsgranskning av utskrift" intill varandra i menyn "Arkiv" igen.

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

Gwenview är ett bildvisningsprogram som levereras med några grundläggande redigeringsfunktioner, såsom storleksändring och beskärning. Den nya versionen sparar storleken på den senast använda beskärningsrutan, vilket betyder att du snabbt kan beskära flera bilder med samma storlek i snabb följd.

# Felrättningar

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* Att spara har flyttats till en tråd för att inte frysa gränssnittet när man sparar

* Implementerade ett D-Bus gränssnitt för genvägar och för att kontrollera inläsning

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

* Spectacles genvägar för tidtagningsfunktion, Skift-Print Screen (ta en skärmbild av hela skärmen) och Meta+Skift+Print Screen (ta skärmbild av rektangulärt område) fungerar nu på Wayland.

* Spectacle inkluderar normalt inte längre muspekaren i skärmbilder.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

* struct2osd använder nu castxml (användning av gccxml avråds från).

* Mindre användning av Qt-kod som avråds från, undvik loggade körtidsvarningar.

* Förbättrade översättningar.