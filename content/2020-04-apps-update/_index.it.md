---
layout: page
publishDate: 2020-04-23 13:00:00
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di aprile 2020
type: announcement
---
È arrivato un nuovo pacchetto di applicazioni KDE! In questi rilasci troverai più funzionalità, miglioramenti della stabilità e strumenti più facili da usare che ti aiuteranno a lavorare in modo più efficace.

Sono state fatte molte modifiche alle tue applicazioni preferite. Prendi Dolphin, per esempio: le condivisioni Windows Samba ora sono totalmente individuabili.

O nell'ambito della riproduzione musicale: il lettore Elisa ha aggiunto tante funzionalità. Questo rilascio contiene la nuova vista «Ora in riproduzione», di facile accesso tramite il vassoio di sistema, e un'opzione per minimizzare quando serve la scaletta. Grazie alla modalità di riordinamento visuale aggiunta di recente è ora molto più facile riordinare la musica nelle scalette.

Queste sono solo le principali novità di questo mese per le applicazioni KDE. Continua a leggere per scoprire tutto quello che abbiamo preparato per te.

## [Okular](https://okular.kde.org)

Okular è l'applicazione KDE che ti permette di leggere i PDF, documenti Markdown, fumetti e molti altri formati di documento. In questo rilascio Okular è stato migliorato nell'accessibilità sia per gli utenti desktop, sia per quelli che usano gli schermi tattili. Per i primi ora Okular implementa uno scorrimento più dolce sia con la rotella del mouse sia con la tastiera. Per gli utenti degli schermi tattili l'ultima versione di Okular offre lo scorrimento inerziale.

{{<video src="okular.mp4" caption="Scorrimento dolce in Okular. Illustrazione: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

Dolphin, il gestore file completo di KDE, aggiunge nuove funzionalità e ti consente d'interagire meglio con i sistemi di file remoti, tipo le condivisioni Samba e i server SSH. Ora puoi iniziare a guardare i film memorizzati in siti remoti senza doverli scaricare, e far partire lo streaming dal cloud direttamente al tuo lettore tramite Dolphin.

Allo stesso modo, la connessione con le condivisioni Windows di Samba e l'uso di tutti i servizi che esse offrono non sono state mai così semplici, dato che le condivisioni Samba sono ora individuabili tramite il protocollo WS-Discovery, utilizzato dalle moderne versioni di Windows. Questo ti permette di integrare senza problemi la tua macchina Linux all'interno di una rete Windows.

Novità dal reparto «usabilità»: non hai più bisogno del mouse per spostare il fuoco da e verso il pannello del terminale. Ora ti basta utilizzare la scorciatoia da tastiera <kbd>Ctrl</kbd>+<kbd>Maiusc</kbd>+<kbd>F4</kbd>. Un'altra funzionalità molto attesa è la ricerca dei file non solo per nome o contenuto ma anche per etichetta.

Il supporto per i file 7Zip ora è incorporato in Dolphin, e questo significa che puoi sfogliare questi file compressi come se fossero cartelle di file di sistema. Quando installi applicazioni esterne di ricerca, tipo KFind, Dolphin ti consente di accedere ad esse creando un collegamento.

Infine, una funzionalità apparentemente piccola che può fare risparmiare, però, molto tempo è la nuova funzione «Duplica». Seleziona uno o più file o cartelle, premi <kbd>Ctrl</kbd> + <kbd>D</kbd> (o fai clic destro e scegli «Duplica qui») e... bam! Una copia di ciascun elemento selezionato apparirà proprio a fianco agli originali.

![Dolphin duplicate feature](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

Lokalize è lo strumento per tradurre file gettext e xliff. Ora supporta il correttore grammaticale Language-Tool, che aiuta i traduttori evidenziando gli errori grammaticali nel testo.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

KMail è il client di posta elettronica di KDE più popolare e fa parte di Kontact, la suite di programmi che ti aiutano a gestire i messaggi, i contatti e le attività. In questo rilascio sono stati inclusi numerosi miglioramenti. Per i nuovi utilizzatori, i messaggi di posta elettronica sono ora facilmente esportabili in PDF, e i messaggi formattati con Markdown vengono visualizzati meglio. Anche l'aspetto della sicurezza ha ricevuto attenzioni. Più specificamente, ora KMail mostra un avviso quando il compositore del messaggio si apre dopo aver premuto un collegamento che ti chiede di allegare un file.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

Konsole è un emulatore di terminale utilizzato per eseguire istruzioni, processi e programmi tramite riga di comando. L'ultima versione di Konsole ti permette di usare il tasto <kbd>Alt</kbd> + un numero per passare direttamente a una qualsiasi delle prime nove schede aperte.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

Gwenview è il visore di immagini preferito di KDE, e in questa versione gli sviluppatori hanno risolto due problemi importanti: l'applicazione non si blocca più all'avvio quando gli appunti di sistema contengono testo proveniente da KDE Connect; l'importazione delle foto da (o l'esportazione verso) posizioni remote è stata sistemata.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

Elisa è un lettore musicale semplice e popolare e in questo rilascio presenta la vista «Ora in riproduzione» che lo rende più attraente.

Le modifiche non riguardano, però, la mera questione estetica. Elisa è anche più funzionale, in quanto ora puoi accedervi attraverso il vassoio di sistema e minimizzare la scaletta quando vuoi. Questo significa che puoi continuare ad ascoltare la tua musica preferita anche quando la finestra principale di Elisa è chiusa. La nuova modalità di riordinamento visuale rende più facile riordinare la musica nelle scalette e mostra il brano successivo da riprodurre.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

Kdenlive è l'applicazione avanzata di KDE per l'editing video, e questa nuova versione rende l'editing più veloce grazie alla risoluzione di anteprima configurabile. L'anteprima migliora anche la vista multitraccia, semplificando allo stesso tempo la scelta delle tracce da tutte quelle che stai modificando.

Scendendo alla linea temporale, la squadra di sviluppo ha migliorato l'aggancio delle clip limitando l'aggancio alle sole tracce attive. È stata corretta anche la funzionalità di aggancio di gruppo e aggiunta una scorciatoia per bypassare l'aggancio durante gli spostamenti. Un'altra operazione che ora si può fare è il rilascio diretto dei file (video, musica e immagini) dal navigatore dei file alla linea temporale. La nuova funzionalità di «ingrandimento sui fotogrammi chiave» sarà di aiuto per lavori più precisi. Gli editor video professionisti e gli utenti che provengono da altri programmi di editing sicuramente apprezzeranno, infine, la funzione di importazione/esportazione OpenTimelineIO.

Questo rilascio di Kdenlive contiene anche molte correzioni di errori, come quelle per il Tracciatore di movimento, che in alcuni sistemi risultava danneggiato. Ora ti permette di aggiungere un filtro di compensazione tono, come pure aggiungere un'azione «Sposta e ridimensiona» a una forma rotoscopio. È stato sistemato anche il crash della Procedura guidata DVD e un errore che minava la modifica del volume di registrazione dal mixer. La squadra di Kdenlive ha pure integrato nel contenitore del progetto le funzionalità di aggiunta etichette, valutazione e filtri.

La nuova versione Windows di Kdenlive contiene anche correzioni alle finestre della linea temporale, che prima non accettavano immissioni da tastiera. In più, ora in Windows puoi selezionare il motore audio.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

Yakuake è una finestra di terminale che puoi far comparire dalla parte superiore del desktop Plasma premendo <kbd>F12</kbd>. È molto utile quando devi eseguire istruzioni rapide nella riga di comando e poi rimuoverla.

In questa versione di Yakuake, quando apri nuove schede o dividi pannelli puoi avviarli nella stessa cartella della scheda/divisore attivo. Puoi anche ridimensionare la finestra di Yakuake in verticale trascinando la sua barra inferiore.

## Impostazioni di sistema

La pagina per l'integrazione degli account in linea nelle Impostazioni di sistema ti permette di configurare elementi quali l'account Google o l'accesso all'account Nextcloud. La pagina è stata completamente rivisitata e ora offre un aspetto moderno e pulito, con funzionalità molto più affidabili.

## [KDE Connect](https://kdeconnect.kde.org/)

KDE Connect è un'applicazione che ti aiuta a integrare il tuo telefono cellulare col desktop e viceversa. Una delle nuovissime funzionalità contenute in questo rilascio è la capacità di avviare nuove conversazioni con l'applicazione SMS.

Altre modifiche includono i lettori multimediali, che ora vengono mostrati nell'applet Lettore multimediale, un nuovo set di icone per lo stato della connessione e la gestione migliorata per le notifiche delle chiamate.

Dal reparto di «correzione errori» è stato risolto l'errore che causava il blocco del modulo delle chiamate da parte di quello della condivisione dei file. Sono stati risolti pure i problemi di trasferimento dei file di grandi dimensioni.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

Spectacle, il software cattura schermo di KDE, ha aggiunto migliorato l'usabilità e risolto alcune spigolosità. Una di queste impediva la copia di una schermata negli appunti quando Spectacle era aperto da riga di comando con l'opzione --nonotify. Sebbene questo fosse un problema molto specifico, è stato comunque risolto. Parimenti, le versioni più vecchie avevano problemi quando tentavi di trascinare una schermata da Spectacle allo schema di nomi dei file predefinito che includeva sotto-cartelle. Questi problemi sono stati risolti.

Un altro miglioramento utile in questa nuova versione sono i pulsanti «Valori predefiniti» e «Ripristina». Puoi utilizzarli per tornare a una configurazione precedente o addirittura rimuovere tutte le configurazioni personalizzate e ripristinare tutto ai valori predefiniti.

Spectacle non solo ricorda la tua configurazione predefinita ma anche l'ultima area utilizzata per la schermata. Questo ti aiuta ad essere coerente e ti permette di creare scatti uguali in forma e dimensione.

## [Krita](https://krita.org/en/) 4.2.9

Dietro questo rilascio ci sono diversi aggiornamenti tecnici ma anche contorni del pennello per la pittura più morbidi che non sfarfallano quando lo passi sopra la tela. Sono state aggiunte le opzioni «Aerografo» e «Ripetizione (aerografo)» al pennello Sfumino a colori, oltre a una nuova impostazione per le proporzioni che permette di creare la forma del pennello piatto utilizzando sensori differenti. La nuova funzionalità «Dividi livello in una maschera di selezione» ti consente di creare un nuovo livello per ciascun colore all'interno del livello attivo.

{{< youtube fyc8-qgxAww >}}

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

Il rilascio 3.0.4 di Smb4K corregge diversi problemi e crash, in special modo nel montatore. Rende inoltre Smb4K di nuovo compilabile con le versioni 5.9 e precedenti di Qt.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Questo rilascio principale dell'estensione, che integra Google Drive con le applicazioni KDE, aggiunge supporto per la funzionalità Dispositivi condivisi di Google Drive.

Include anche la nuova azione «Copia l'URL Google negli appunti» nel menu contestuale di Dolphin.

[note di rilascio 20.04](https://community.kde.org/Releases/20.04_Release_Notes)
• [Pagina wiki per lo scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) •
[Pagina informativa sui sorgenti 20.04](https://kde.org/info/releases-20.04.0) •
[Elenco completo delle modifiche 20.04](https://kde.org/announcements/changelog-
releases.php?version=20.04.0)