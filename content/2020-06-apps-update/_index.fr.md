---
layout: page
publishDate: 2020-06-11 12:00:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE.
title: "Mise \xE0 jour des applications de KDE de Juin 2020"
type: announcement
---
C'est toujours une joie de voir la famille KDE s'agrandir. Voila pourquoi, ce mois ci, nous sommes particulièrement heureux d'accueillir le gestionnaire de sauvegarde « kup » et un tout nouveau travail pour la création de paquets avec Homebrew.

# Nouvelles mises à jour

## Kup 0.8

[Kup](https://store.kde.org/p/1127689) est un outil de sauvegarde que vous pouvez utiliser pour conserver vos fichiers en sécurité.

Cela était antérieurement développé hors de KDE. Mais, il est passé le mois dernier dans le processus d'incubation et a rejoint la communauté, en devenant officiellement un projet KDE. Le responsable de développement, Simon Persson en a fait la promotion avec une nouvelle mise à jour.

Vous trouverez ici les modifications portées dans la nouvelle version :

* Changement de comment les sauvegardes de type « rsync » sont enregistrées quand il n'y a qu'un seul dossier source sélectionné. Cette modification tente de minimiser le risque de suppression de fichier pour un utilisateur sélectionnant un dossier non vide comme destination. Changement du code de migration pour détecter et déplacer vos fichiers à la première tentative et d'éviter de tout copier à nouveau en doublant le volume de stockage.

* Ajout d'une option avancée vous permettant de spécifier un fichier « Kup » contenant les motifs à exclure, par exemple, de dire à Kup de ne jamais enregistrer les fichiers « *.bak ».

* Paramètres par défaut modifiés, les rendant avec un peu de chance, bien meilleurs.

* Alarmes limitées concernant les fichiers non inclus, car cela conduisait à trop de fausses alarmes.

* Kup ne demande plus de mot de passe pour déverrouiller des volumes externes chiffrés, avec la possibilité d'afficher ce qu'il reste comme espace disponible.

* Correction de la déclaration d'échec lors l'enregistrement de la sauvegarde parce que les fichiers sont devenus manquants durant l'opération à la fois avec « rsync » et « bup ».

* Démarrage des vérifications d'intégrité des sauvegardes en cours et réparations en parallèle à partir du nombre de processeurs.

* Ajout de la prise en charge des métadonnées « bup » version 3, ajoutées dans la version de « bup » version 0.30.

* Un grand nombre de petites corrections pour l'interface utilisateur.

Kup peut réaliser des sauvegardes en utilisant « rsync » ou faire des sauvegardes versionnées avec l'outil Python « Bup ». Ce dernier ne fonctionne actuellement qu'avec Python 2, ce qui signifie que cette option ne sera pas disponible sur de nombreuses distributions. Mais, un portage vers Python 3 est en cours.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

Pour trouver plus d'informations à propose de Kup [Le site « Average Linux User » a réalisé un article](https://averagelinuxuser.com/kup-backup/) et récemment une vidéo concernant Kup.

{{< youtube y53IE0G8Brg >}}

# Krita sur les tablettes Android

Merci pour le travail important de Sharaf Zaman qui permet la mise à disposition de Krita sur la boutique « Google Play » pour les tablettes « Android » et les ordinateurs «  Chromebook» (mais pas pour les téléphones « Android »).

Cette version « bêta  », réalisée à partir de Krita 4.2.9 est une version complète pour bureau de Krita. Ainsi, elle ne possède pas d'interface utilisateur particulière tactile. Mais, elle est disponible et vous pouvez jouer avec elle.

Contrairement aux boutiques « Windows » et « Steam », la boutique de Krita ne vous demandera aucun payement depuis que cela la seule façon que vous pouvez installer Krita sur ces périphériques. Cependant, vous pouvez y acheter un badge de sympathisant pour soutenir le développement de Krita.

A installer

* Obtenir [Krita à partir de la logithèque « Google Play »](https://play.google.com/store/apps/details?id=org.krita)

* De façon alternative, dans la boutique « Play Store », passer dans l'onglet « Accès anticipé » et chercher pour « org.krita ». Veuillez consulter mes instructions officielles de Google pour l'accès anticipé. Jusqu'à ce nous ayons réalisé un nombre raisonnable de téléchargements, vous allez devoir faire défiler vers le bas juste un peu.

* Vous pouvez aussi [télécharger les fichiers « apk » par vous même](https://files.kde.org/krita/android/). 

* Voici tous les emplacements officiels. Veuillez ne pas installer Krita à partir d'autres sources dont la sécurité ne peut être garantie.

Notes

* Prend en charge les tablettes « Android » et les ordinateurs « Chromebook ». Les versions « Android » prises en charge sont Android 6 (Marshmallow) et ultérieur.

* Actuellement incompatible avec : téléphones « Android ».

* Si vous avez installé une des versions de « Sharaf » ou une version que vous avez signée vous même, vous devez les désinstaller tout d'abord pour tous les utilisateurs !

{{< img class="text-center" src="krita-android.jpg" caption="Krita sous Android" style="width: 600px">}}

# Entrant

[KIO Fuse](https://techbase.kde.org/Projects/KioFuse) a annoncé sa première [version beta ](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html) ce mois.

## Corrections de bogues

Les mises à jour correctives ont été publiées pour

* Gestionnaire de collections [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html) avec une boîte de dialogue de filtrage mise à jour, permettant la correspondance avec un texte vide.

* Le navigateur local de réseau [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) a été corrigé pour enregistrer les paramètres à la fermeture.

* Les développeurs « IDE » [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) ont proposé une mise à jour pour les dépôts de KDE ayant été déplacés.

# Magasin d'applications

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

Bien qu'avec Linux, l'installation d'applications individuelles à partir de boutiques se développent, l'inverse se produit dans le monde de « MacOS » et de « Windows ». Pour ces systèmes, les gestionnaires de paquets ont été intégrés pour ceux aimant avoir une source unique pour tout contrôler sur leurs systèmes.

Le dépôt majeur de paquets « Open source » pour MacOS est Homebrew, géré par une équipe d'élite de développeurs, y compris l'ancien développeur de KDE, Mike McQuaid.

Ce mois, le projet [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde) qui a été hébergé de façon externe à KDE pendant un moment, a été intégré dans KDE, comme une partie à part entière de notre communauté.

Vous pouvez ajouter le dépôt de KDE pour Homebrew pour MacOS et télécharger les sources de KDE compilés, prêts à être lancés.

Nous avons pris contact avec Yurii Kolesnykov, le responsable du développement, et lui avons demandé de parler de son projet.

### Dites nous quelque chose à propos de vous, votre nom, d'où vous venez, où se trouvent vos centres d'intérêt pour KDE et Mac, quel est votre parcours ?

Mon nom est Yurii Kolesnykov, je suis ukrainien. J'ai été passionné pour les logiciels libres dès que j'en ai entendu parler, à peu près à la fin de mes années de lycée. Je pense que KDE est le meilleur bureau pour Linux et les systèmes Unix avec des applications de grande qualité. Mon intérêt pour Mac provient de mon travail principal de développeur de logiciels pour iOS Mobile.

### Qu'est ce que Homebrew ?

Homebrew est le plus populaire des gestionnaires de paquets pour MacOS, comme « apt » ou « yum ». Depuis que MacOS est proche d'Unix et que Apple fournit un excellent compilateur et chaîne d'outils pour lui, certains ont décidé de créer des gestionnaires de paquets pour lui. Ainsi, vous pourrez installer plus de logiciels libres et « Open source » sur Mac. Homebrew possède aussi un sous-projet nommé « Homebrew Cask » vous permettant d'installer de nombreuses applications en binaire, c'est-à-dire, certaines interfaces graphiques ou certains logiciels propriétaires. Parce que les applications avec interfaces graphiques sont difficiles à intégrer avec le système si elles sont installées avec Homebrew.

### Quels paquets de KDE ont ils été produits pour Homebrew ?

J'ai lancé « grep » à votre demande et j'ai vu qu'il y avait en tout, 110 paquets, 67 d'entre eux sont des environnements de développement et approximativement 39 sont des applications. Il y a les applications les plus populaires, comme Kate, Dolphin et KDevelop, grâce aux demandes des utilisateurs.

### En tant qu'utilisateur « Mac », de quoi avez-vous besoin pour installer des applications ?

Tout d'abord, vous avez besoin de suivre le guide d'installation de Homebrew. Si vous ne l'avez pas encore, il est disponible à [brew.sh](https://brew.sh). Ensuite, vous avez besoin de contacter votre dépôt avec la commande suivante :

« brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git »

Malheureusement, beaucoup de paquets KDE ne fonctionnent pas de façon automatique. Mais, un script a été créé pour réaliser les adaptations nécessaires. Aussi, après la saisie, vous devez lancer la commande suivante : 

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### Savez-vous à quel point Homebrew est populaire, comme une façon d'installer des applications sur Mac ?

Bonne question. Malheureusement, aucune télémétrie n'est disponible. Elle va être ajoutée à ma liste des choses à faire. Mais, puisque Homebrew est le plus populaire des gestionnaires de paquets pour Mac, il vous est demandé, à vous, les utilisateurs de ne pas mélanger les installations sur le même Mac avec des projets similaires, à cause de conflits. Ainsi, oui, je pense qu'il est plutôt populaire.

### Quelle quantité de travail avez-vous besoin de réaliser pour faire fonctionner des applications de KDE dans Homebrew ?

Durant la création des paquets actuels, de nombreux problèmes communs ont été traités. Ainsi, l'arrivée de nouveaux logiciels est relativement facile. Je promets d'écrire un « Comment faire » pour ceci, car ceci est déjà une demande faite plusieurs fois par les utilisateurs.

### Actuellement, les paquets ont besoin d'être compilé localement. Auriez-vous des paquets précompilés disponible ?

Homebrew vous permet d'installer des logiciels grâce à « Bottles », c'est-à-dire, des paquets binaires précompilés. Mais, le processus pour la création de « bottles » est étroitement intégré avec l'infrastructure de Homebrew. C'est-à-dire qu'il est nécessaire de réaliser une intégration continue avec des tests sur chaque paquet avant de les rendre disponibles sous « Bottles ». Ainsi, il a été décidé d'intégrer autant de paquets que possible dans le dépôt principal de Homebrew pour limiter le poids de la maintenance.

### Y-a-t-il beaucoup d'autres logiciels de bureau disponibles dans Homebrew ?

Oui, en général, si une application est populaire et possède un canal de distribution en dehors de la boutique d'application « Mac AppStore », alors, il y a de fortes chances qu'elle soit déjà disponible pour l'installation grâce à « Brew Cask ».

### Comment les auteurs des applications de KDE peuvent aider pour intégrer leurs logiciels dans Homebrew ?

Le matériel Apple est très coûteux. Ainsi, l'achat d'un Mac pour tout développeur KDE n'est pas une bonne idée. Jusqu'à présent, celui-ci est encouragé à ouvrir une demande de fonctionnalité sur notre dépôt. Ensuite, les mainteneurs et les utilisateurs de Homebrew sous KDE peuvent signaler des bogues si quelque chose ne fonctionne pas comme il le devrait. Jusqu'à présent, de nombreux tickets concernant les applications de KDE restent en souffrance avec des bogues mineurs mais ennuyeux. J'espère une meilleur intégration avec l'infrastructure d KDE, c'est-à-dire de créer des liens avec les bogues dans notre dépôts avec les projets amont. La migration vers « KDE Invent » est déjà faite et la migration des bogues de KDE de Bugzilla vers « KDE Invent »sera bientôt réalisée.

### L'autre façon d'obtenir des applications de KDE adaptées pour Mac est d'utiliser Craft. Comment sont les applications produites avec Homebrew vis à vis de celles produites avec Craft ?

Je pense encore que Homebrew est plus agréable pour les utilisateurs finaux. Son processus d'installation est aussi facile que l'exécution d'une ligne de commandes. Pour ajouter votre dépôt et lancer l'installation d'applications à partir de celui-ci, vous avez besoin de lancer deux autres lignes de commandes.

### Merci pour votre temps, Yurii.

# Mises à jour 20.04.2

Les mises à jour de certains projets se font selon leurs propres plannings et quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la version 20.04.2 a publié leurs mises à jour aujourd'hui. Celles-ci devraient être disponibles très bientôt dans les dépôts d'applications et dans les distributions. Veuillez consulter [la page des mises à jour 20.04.2] (https://www.kde.org/info/releases-20.04.2.php) pour plus de détails.

Certaines de ces corrections de la mise à jour de ce jour :

* Les écritures sont réparties en de multiples requêtes aux serveurs « SFTP », limitant la taille des transferts

* Konsole met à jour la position du pointeur pour les méthodes d'entrées (comme IBus ou Fcitx) et ne se plante plus lors de la fermeture par le menu.

* KMail produit de meilleurs courriels au format « HTML » avec l'ajout de signature « HTML » aux courriels.

[Notes de la version 20.04]
(https://community.kde.org/Releases/20.04_Release_Notes) • [Page de tutoriel
pour le téléchargement de paquets]
(https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) • [Page
d'informations sur les sources de la version 20.04.02
(https://kde.org/info/releases-20.04.2). • [Liste complète des modifications
pour la version 20.04.2] (https://kde.org/announcements/changelog-
releases.php?version=20.04.2)