---
layout: page
publishDate: 2020-01-09 12:00:00
summary: "O que se Passou nas Aplica\xE7\xF5es do KDE Neste M\xEAs"
title: "Actualiza\xE7\xE3o de Janeiro 2020 das Aplica\xE7\xF5es do KDE"
type: announcement
---
O ano é 2020, estamos a viver no futuro, vamos ver o que as aplicações do KDE nos trouxeram no último mês!

## KTimeTracker migrado para as Plataformas do KDE 5

A versão mais modernizada do KTimeTracker, esperada já há algum tempo, finalmente foi lançada. A aplicação é um sistema de registo de horas pessoais que está agora disponível no Linux, FreeBSD e Windows. Ao longo de 2019, foi migrada para o Qt5 e as Plataformas do KDE, após estar sem manutenção desde 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

A nova versão está também arrumada e ligeiramente modernizada, sendo as novas funcionalidades mais notórias a nova janela de Edição do Tempo da Tarefa e a antevisão em directo na janela de Exportação, tal como aparece na imagem abaixo.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Janela de exportação no KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ Está disponível através da sua distribuição de Linux ou como um [programa de instalação no Windows](https://download.kde.org/stable/ktimetracker/5.0.1/win64/), bem  como algumas [versões para MacOS não testadas](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) que são compiladas todas as noites.

[A versão está anunciada no 'blog' do responsável de manutenção Alexander Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

O programa de astronomia [KStars recebeu novas funcionalidades no 3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

As imagens poderão ter pequenas alterações nas Sombras, Meios-Tons e Tons Claros, permitindo ver as estrelas mais ténues.

Constelações alternativas na Cultura do Céu Ocidental, a qual é fascinante [ler](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Constelações alternativas" alt="Western Sky Culture" >}}

[O KStars está disponível no](https://edu.kde.org/kstars/) Android, Windows, MacOS, loja do Snap e na sua distribuição de Linux.

## Plataformas Comuns - KNewStuff

Aqui na Actualização das Aplicações, focamo-nos mais nas aplicações do que nas bibliotecas de código. Contudo, as funcionalidades novas nas bibliotecas comuns significam funcionalidades novas em todas as suas aplicações :)

Este mês teve uma interface remodelada para o KNewStuff, a plataforma para obter extensões e módulos para as suas aplicações. A janela de navegação e transferência foi reorganizada e a secção de comentários poderá agora ser filtrada. Irá aparecer em todos os seus módulos de aplicações e configurações, a começar em breve com o módulo do Tema Global na Configuração do Sistema.

{{< img src="knewstuff-comments.png" caption="Filtros nos Comentários" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Janela de Navegação Remodelada" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Correcções de erros

A [actualização mensal de correcções de erros do KDevelop versão 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) corrigiu um problema antigo onde os ficheiros de inclusão em GPL e LGPL estavam misturados; obtenha-a a partir da sua distribuição ou AppImage para garantir que o seu licenciamento está correcto.

O [Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) corrigiu algumas funcionalidades com o Qt 5.14 e removeu alguns estoiros.

Os 'Plugins' do Dolphin 19.12.1 corrigiram a janela de Envio do SVN que tinha problemas.

Ocorreu uma melhor indexação de ficheiros no Elisa. Também corrigiu algumas questões de compilação no Android e sem o Baloo.

A nova versão do KPat foi declarada como não tendo [OARS](https://hughsie.github.io/oars/index.html) restrições etárias relevantes.

O Okular corrigiu um estoiro ao fechar a janela de antevisão da impressão.

A versão deste mês do Kdenlive teve um número impressionante de correcções, sendo a mais importante a actualização das [fotografias](https://kde.org/applications/multimedia/org.kde.kdenlive) usadas nos meta-dados. Também teve bastantes melhorias e correcções no tratamento da linha temporal e na antevisão.

Existe agora um novo suporte para JavaScript no cliente de [LSP](https://langserver.org/) do Kate.

## Desfrute do KDE na Loja do Flathub

O KDE está a aderir a todas as lojas de aplicações. Conseguimos agora entregar cada vez mais programas directamente a si, o utilizador. Uma das lojas de aplicações mais familiares é o [Flathub](https://flathub.org/home), que usa o formato FlatPak.

Já poderá ter o Flatpak e o Flathub configurados no seu sistema para usar no Discover ou noutros instaladores de aplicações. Por exemplo, o KDE Neon tem o mesmo configurado por omissão nas instalações já há um ano. Caso contrário, é um [processo rápido de configuração](https://flatpak.org/setup/) para a maioria das distribuições importantes.

Se estiver interessado nos detalhes técnicos, poderá navegar pelo [repositório de pacotes Flatpak do KDE](https://phabricator.kde.org/source/flatpak-kde-applications/) e ler o [guia de Flatpak do KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Mas o que provavelmente lhe irá interessar são as aplicações, por isso veja o que a [loja do Flathub tem para o KDE](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## O LabPlot agora no Chocolatey

O [Chocolatey](https://chocolatey.org/) é um gestor de pacotes para o Windows. Se quiser ter o controlo total sobre as aplicações que são instaladas na sua máquina de Windows ou num escritório de máquinas completo, então o Chocolatey dá-lhe um controlo simples, tal como estará habituado no Linux.

O [LabPlot](https://chocolatey.org/packages/labplot) é a aplicação do KDE para gerar gráficos e análises de dados científicos, estando agora disponível através do Chocolatey. Experimente-o!

[Blog do LabPlot](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Actualizações da Página Web

A Equipa de Web do KDE, recentemente revitalizada, fez uma actualização de grande parte dos nossos sites com temas mais antigos. A página Web do [KPhotoAlbum](https://www.kphotoalbum.org/), lançada há pouco tempo, é um bonito exemplo, estando actualizada e refrescada para a nossa aplicação de armazenamento e pesquisa de fotografias.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

E se quiser experimentar um leitor de música local, simples de usar mas poderoso, mas se sentia intimidado pela página Web com aspecto antigo, o [JuK](https://juk.kde.org/) também ficou com uma página Web actualizada.

## Versão 19.12.1

Alguns dos nossos projectos são lançados de acordo com as suas próprias agendas
e alguns são lançados em massa. A versão 19.12.1 dos projectos foi lançada hoje
e deverá estar disponível através das lojas de aplicações e distribuições em
breve. Consulte a [página da versão
19.12.1](https://www.kde.org/info/releases-19.12.1.php).  Este pacote foi
chamado anteriormente de Aplicações do KDE mas tem vindo a perder a sua marca
para se tornar um serviço de lançamento de versões, para evitar confusões com
todas as outras aplicações do Kde e porque consiste em dezenas de diferentes
produtos em vez de um único grupo.