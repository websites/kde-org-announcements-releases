---
layout: page
publishDate: 2020-01-09 12:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's January 2020 Apps Update
type: announcement
---
Наступил 2020 год, теперь мы живём в будущем и давайте посмотрим, что нового появилось в приложениях KDE за последний месяц.

## Выполнен перенос приложения для учёта времени KTimeTracker на KDE Frameworks 5

The long-awaited modernized version of KTimeTracker is finally released.The application is a personal time tracker for busy people which is nowavailable on Linux, FreeBSD and Windows. Over the course of 2019 it had beenported to Qt5 and KDE Frameworks after being unmaintained since around 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

The new version is also polished and slightly modernised with the mostnoticeable new features being the new Task Time Editing dialog andlive preview in the Export dialog as seen in the picture below.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Export dialog in KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/It is available through your Linux distro or as a [Windows installer](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) and there's even [untested MacOS builds](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) built nightly.

[Полный текст анонса этого выпуска на английском языке приведён в блоге Александра Поташова — нового сопровождающего этого проекта].(https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

Вышла [программа-планетарий KStars версии 3.3.9, дополненная новыми возможностями](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Добавлена возможность выполнять изменения изображений в тенях, полутонах и светлых участках, что позволяет увидеть самые слабые звезды.

Очаровательные альтернативные контуры созвездий, о которых [строит прочесть] https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternative constellations" alt="Western Sky Culture" >}}

Программа-планетарий [KStars доступна для](https://edu.kde.org/kstars/) Android, Windows, MacOS, в магазине приложений в формате пакета Snap и во многих дистрибутивах Linux.

## Основные наборы библиотек — KNewStuff

Here on the Apps Update we focus on the apps rather than coding libraries. But new features in the common libraries will mean new features in all your apps :)

This month saw a redesigned UI for KNewStuff, the framework to download addons for your applications. The browse and download dialog was redesigned and the comments section can now be filtered. It'll be appearing in all your apps and settings modules shortly starting with the Global Theme module in System Settings.

{{< img src="knewstuff-comments.png" caption="Filters on Comments" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Redesigned Browse Dialog" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Исправления ошибок

[Ежемесячный выпуск исправлений среды разработки KDevelop 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) содержит исправление давней ошибки, приводящей с смешиванию заголовков лицензий GPL и LGPL. Установите обновлённую версию из пакетов своего дистрибутива или в виде пакета Appimage.

В [Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) добавлены функции, предлагаемые библиотеками Qt 5.14 и исправлены несколько ошибок, приводивших к аварийному завершению.

В подключаемых модулях для диспетчера файла Dolphin восстановлена работа диалога фиксации изменений в SVN.

There was improved file indexing in Elisa. It also fixed some compilation issues on Android and without Baloo.

Новый выпуск карточных игр KPat заявлен как не имеющий возрастных ограничений по классификации [OARS](https://hughsie.github.io/oars/index.html).

В приложении для просмотра документов Okular исправлена ошибка, приводящая к аварийному завершению во время предварительного просмотра в диалоге печати.

В очередном ежемесячном выпуске редактора видео Kdenlive выполнено внушительное количество исправлений ошибок; наиболее важным было обновление [снимков экрана](https://kde.org/applications/multimedia/org.kde.kdenlive), используемых для метаданных. Также было внесено множество улучшений и исправлений в модулях монтажного стола и обработках предварительного просмотра.

В модуль работы с [LSP](https://langserver.org/) улучшенного текстового редактора Kate добавлена модуль поддержка JavaScript.

## KDE в магазине приложений Flathub

KDE is embracing all the app stores. We can now deliver more and more of our programs directly to you the user. One of the leading app stores on Linux is [Flathub](https://flathub.org/home) which uses the FlatPak format.

You may well already have Flatpak and Flathub configured on your system and ready to use in Discover or other app installers. For example KDE neon has set it up by default on installs for over a year now. If not it's a quick [setup process](https://flatpak.org/setup/) for all the major distros.

Технические подробности доступны в [репозитории KDE Flatpak](https://phabricator.kde.org/source/flatpak-kde-applications/) и в [Руководстве Flatpak для KDE](https: / /community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Приложения KDE в формате Flatpak размещены в [разделе KDE в магазине приложений Flathub](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## Приложение для построения графиков LabPlot теперь доступно в системе управления пакетами Chocolatey

[Chocolatey](https://chocolatey.org/) is a package manager for Windows. If you want full control over what software is installed on your Windows machine or whole office of machines then Chocolatey gives you easy control over that just like you are used to on Linux.

[LabPlot](https://chocolatey.org/packages/labplot) is KDE's app for interactive graphing and analysis of scientific data and it is now available through Chocolatey. Give it a try!

[Блог LabPlot](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Обновления веб-сайта

Недавно обновлённая команда KDE Web работает над обновлением старых тематических сайтов. Недавно обновленный веб-сайт [приложения для хранения и поиска фотографий KPhotoAlbum] (https://www.kphotoalbum.org/) является прекрасным тому примером.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

Чтобы поделиться информацией о простом в использовании, но полнофункциональным музыкальным проигрывателем, у [проигрывателя JuK] (https://juk.kde.org/) только что появился обновленный сайт.

## Выпуск 19.12.1

Some of our projects release on their own timescale and some get released en-masse. The 19.12.1 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12.1 releases page](https://www.kde.org/info/releases-19.12.1.php). This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because it is dozens of different products rather than a single whole.