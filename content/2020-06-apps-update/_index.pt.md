---
layout: page
publishDate: 2020-06-11 12:00:00
summary: "O que se Passou nas Aplica\xE7\xF5es do KDE Neste M\xEAs"
title: "Actualiza\xE7\xE3o de Junho 2020 das Aplica\xE7\xF5es do KDE"
type: announcement
---
É sempre um agrado quando a família do KDE cresce, e é por isso que este mês estamos especialmente felizes em dar as boas-vindas ao gestor de cópias de segurança Kup e um novo esforço de geração de pacotes: Homebrew.

# Novos lançamentos

## Kup 0.8

O [Kup](https://store.kde.org/p/1127689) é uma ferramenta de criação de cópias de segurança que poderá usar para manter em segurança os seus ficheiros.

Antigamente era desenvolvido fora do KDE, mas neste último mês passou o processo de Incubação e juntou-se à nossa comunidade, tornando-se oficialmente um projecto do KDE. O gestor de programação, Simon Persson, celebrou esse facto com uma nova versão.

Aqui estão as alterações que irá encontrar na nova versão:

* Foi alterada a forma como as cópias de segurança do tipo 'rsync' são guardadas quando só estiver seleccionada uma pasta de origem. Esta alteração tenta minimizar o risco de apagar os ficheiros de um utilizador que selecciona uma pasta não-vazia  como destino. Foi adicionado o código de migração para detectar e mover os seus ficheiros na primeira execução, evitando assim copiar tudo de novo e duplicar o armazenamento.

* Foi adicionada uma opção avançada que lhe permite indicar um ficheiro o qual o Kup irá ler os padrões de exclusão para, por exemplo, indicar ao Kup para nunca gravar os ficheiros *.bak.

* Mudança da configuração predefinida, tornando-a assim melhor.

* Redução de avisos sobre ficheiros não incluídos, dado que estava a gerar muitos falsos alarmes.

* O Kup não pede mais uma senha para desbloquear os discos externos encriptados, só porque se pretende mostrar o espaço disponível.

* Correcção do facto de não tratar como falhada uma cópia de segurança só porque alguns ficheiros estava em falta durante a operação, tanto no 'rsync' como no 'bup'.

* Início da execução de verificações e reparações da integridade das cópias de segurança em paralelo, baseando-se no número de CPU's.

* Adição do suporte para os meta-dados do 'bup' na versão 3, que foi adicionada na versão 0.30 do 'bup'.

* Diversas correcções pequenas na interface do utilizador.

O Kup consegue criar cópias de segurança com o 'rsync' ou criar cópias com controlo de versões, usando a ferramenta de Python Bup. O Bup de momento só funciona em Python 2, o que significa que esta opção não estará disponível em muitas distribuições, mas uma migração para o Python 3 está em curso.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

Para saber mais sobre o o Kup, [o Average Linux User fez uma avaliação](https://averagelinuxuser.com/kup-backup/) e um vídeo sobre o Kup há não muito tempo:

{{< youtube y53IE0G8Brg >}}

# O Krita nos 'Tablets' de Android

Graças ao trabalho árduo de Sharaf Zaman, o Krita está agora disponível na loja do Google Play para os 'tablets' Android e Chromebooks (mas não nos telefones Android).

Esta versão beta, baseada no Krita 4.2.9, é a versão completa do Krita para computadores, pelo que não têm uma interface especial para ecrãs tácteis. Mas está aí, e pode mexer nela à vontade.

Ao contrário da loja do Windows e a Steam, não pedem dinheiro para o Krita, dado que é a única forma de as pessoas poderem instalar o Krita nesses dispositivos. Contudo, poderá comprar um emblema de suporte no Krita para dar suporte ao desenvolvimento.

Para instalar

* Transfira o [Krita do Google Play](https://play.google.com/store/apps/details?id=org.krita)

* Em alternativa, na Play Store, mude para a página de "Acesso prévio" e procure pelo org.krita. (Veja as instruções sobre o Acesso Prévio da Google. Até que exista um número razoável de transferências, terá de deslocar o conteúdo da janela para baixo um bom bocado.)

* Também poderá [obter você mesmo os ficheiros APK](https://files.kde.org/krita/android/). NÃO peça ajuda na instalação destes ficheiros.

* Estes são todos os locais oficiais. Por favor não instale o Krita a partir de outras fontes. Não podemos garantir a sua segurança.

Notas

* Suporta os 'tablets' & Chromebooks em Android. Versões do Android suportadas: Android 6 (Marshmallow) e superiores.

* Não compatível de momento com: telemóveis Android.

* Se não tiver instalado nenhuma das versões do Sharaf ou uma versão que tenha sido assinada por si, terá de a desinstalar primeiro para todos os utilizadores!

{{< img class="text-center" src="krita-android.jpg" caption="O Krita no Android" style="width: 600px">}}

# Recebido

O [KIO Fuse](https://techbase.kde.org/Projects/KioFuse) teve a sua primeira [versão beta](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html) este mês.

## Correcções de erros

Foram lançadas versões com correcções de erros para

* O gestor de colecções [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html) com uma janela de filtragem actualizada que permite corresponder ao texto vazio.

* O navegador na rede local [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) corrigiu a gravação da configuração ao fechar.

* O IDE para programadores [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) teve uma alteração para os repositórios do KDE migrados.

# Loja de Aplicações

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

Enquanto no Linux nos estamos gradualmente a habituar À possibilidade de instalar aplicações individuais a partir de uma loja de aplicações, o inverso está a acontecer no mundo do macOS e do Windows. Para esses sistemas, os gestores de pacotes estão a ser introduzidos para aqueles gostam de ter uma única fonte para controlar tudo nos seus sistemas.

O repositório de pacotes de código aberto líder nos ambientes macOS é o Homebrew, gerido por uma grande equipa de programadores, os quais incluem o antigo programador do KDE Mike McQuaid.

Neste mês, o projecto [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde), que era executado de forma externa ao KDE, foi migrado para o KDE de forma a ser uma componente completa da nossa comunidade.

Poderá adicionar o repositório do KDE Homebrew para o macOS e obter o código-fonte do KDE compilado e pronto para você executar.

Estivemos a pôr a conversa em dia com o programador-chefe Yurii Kolesnykov e perguntámos-lhe sobre o projecto.

### Fale-nos sobre si, qual é o seu nome, onde nasceu, qual é o seu interesse no KDE e no mac e o que faz no dia-a-dia?

Chamo-me Yurii Kolesnykov, venho de Donetsk (reclamado pela Ucrânia e pela República Popular de Donetsk), mas saí de lá desde que se tornou um estado falhado. Tenho uma paixão por Software Livre desde que ouvi falar sobre o mesmo, o que acontece mais ou menos no fim do ensino secundário. Penso que o KDE é simplesmente o melhor ambiente de trabalho para os sistemas Linux e Unix com muitas aplicações excelentes. O meu interesse no Mac vem do meu emprego principal, onde desenvolvo aplicações móveis para iOS como modo de vida.

### O Que é o Homebrew?

O Homebrew é o gestor de pacotes mais conhecido para o macOS, funcionando mais ou menos como o APT ou o YUM. Dado que o macOS é um Unix e a Apple oferece um bom compilador e conjunto de ferramentas para o mesmo, as pessoas optaram por começar a criar gestores de pacotes para ele, pelo que poderá instalar mui aplicações gratuitas e em código aberto no Mac. O Homebrew também tem um sub-projecto chamado Homebrew Cask, que lhe permite instalar muitas aplicações binárias, i.e., proprietárias ou gráficas, dado que as aplicações gráficas são difíceis de integrara com o sistema caso sejam instaladas com o Homebrew.

### Quais os pacotes do KDE que criou para o Homebrew?

Acabei mesmo agora de executar um 'grep' no nosso 'tap', é possível ver que temos 110 pacotes no total, sendo 67 deles plataformas de desenvolvimento e aproximadamente 39 aplicações. Já incluímos as aplicações mais conhecidas, como o Kate, o Dolphin e o KDevelop, devido aos pedidos dos utilizadores.

### Como um utilizador de Mac, o que precisa ter para conseguir instalar as aplicações?

Em primeiro lugar, é necessário seguir o guia de instalação do Homebrew, caso ainda não o tenha feito, estando o mesmo disponível em [brew.sh](https://brew.sh). Depois é necessário aplicar o 'tap' no nosso repositório com o seguinte:

`brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git`

Infelizmente bastantes pacotes do KDE não funcionam por si só, mas foi criado um programa que aplica todos os truques necessários, pelo que após o 'tap' terá de executar o seguinte comando:

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### Tem a noção de quão popular se está a tornar o Homebrew como forma de obter aplicações para o Mac?

Boa pergunta. Infelizmente não temos configurado nenhum suporte analítico, sendo que será adicionado à minha lista de tarefas por por fazer. Contudo, dado que que o Homebrew é o gestor de pacotes mais conhecido no Mac, recomenda-se os utilizadores a não o misturar com outros projectos semelhantes para instalar aplicações no Mac, devido aos conflitos. Por isso, sim, penso que é bastante popular.

### Quanto trabalho precisava de fazer para ter as aplicações do KDE a funcionar no Homebrew?

Durante a criação dos pacotes actuais, já corrigimos bastantes problemas comuns, pelo que a migração de aplicações novas é relativamente simples. Prometo criar um manual HOWTO para isto, dado já ter sido pedido pelos utilizadores em várias ocasiões.

### De momento, os pacotes têm de ser compilados localmente; será que virá a ter disponíveis pacotes pré-compilados?

O Homebrew permite-lhe instalar as aplicações através de Bottles, i.e. pacotes binários pré-compilados. Contudo, o processo de criação de 'bottles' está intimamente integrado na infra-estrutura do Homebrew, i.e., é necessário executar o CI com os testes sobre todos os pacotes antes de ser criado o pacote. Como tal, optámos por integrar tantos pacotes quantos possíveis no repositório principal do Brew para eliminar o peso da manutenção.

### Existem aplicações para outros ambientes de trabalho disponíveis no Homebrew?

Sim. De um modo geral, se uma dada aplicação é popular e tem um canal de distribuição fora da Loja de Aplicações do Mac, então existe uma boa possibilidade de que já esteja disponível para instalação com uma Brew Cask.

### Como é que os autores de aplicações do KDE poderão ter as mesmas disponíveis no Homebrew?

O 'hardware' Apple é bastante caro, pelo que arranjar um Mac para cada programador do KDE não será uma boa ideia. Como tal, por agora, são bem-vindos para criar um Pedido de Funcionalidade no nosso repositório. Depois, os utilizadores ou responsáveis de manutenção do Homebrew KDE comunicam os erros caso algo não funcione como pretendido. Estamos a tentar fornecer tanta informação quanta possível a pedido dos programadores do KDE. Contudo, para já, temos um grande conjunto de pedidos pendentes para aplicações do KDE com muitos pequenos erros incómodos. Esperamos ficar muito mais integrados com a infra-estrutura do KDE, i.e., poderemos associar erros no nosso repositório aos projectos oficiais. Já migrámos para o  KDE Invent, pelo que esperamos que os erros do KDE sejam migrados do Bugzilla para o KDE Invent em breve.

### A outra forma de ter as suas aplicações do KDE compiladas para o Mac é com o Craft. Como é que o Homebrew compila as aplicações em comparação com as mesmas compiladas com o Craft?

Ainda continuo a pensar que o Homebrew é mais amigável para os utilizadores finais. O seu processo de instalação é tão simples como um comando numa única linha. Para adicionar o nosso repositório e começar a instalar aplicações a partir do mesmo, é necessário executar outras duas linhas.

### Obrigado pelo seu tempo Yurii.

# Versões 20.04.2

Alguns dos nossos projectos são lançados de acordo com as suas próprias agendas e alguns são lançados em massa. A versão 20.04.2 dos projectos foi lançada hoje e deverá estar disponível através das lojas de aplicações e distribuições em breve. Consulte mais detalhes na [página da versão 20.04.2](https://www.kde.org/info/releases-20.04.2.php).

Algumas das correcções incluídas nesta versão de hoje são:

* As gravações são repartidas por vários pedidos para os servidores SFTP que limitam o tamanho da transferência

* O Konsole actualiza a posição do cursor para os métodos de entrada (como o IBus ou o Fcitx) e não estoira mais ao fechar através do menu

* O KMail gera um melhor HTML ao adicionar uma assinatura em HTML às mensagens de e-mail

[Notas da versão 20.04](https://community.kde.org/Releases/20.04_Release_Notes)
• [Página de transferências de pacotes na
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Página de informação do código do
20.04.2](https://kde.org/info/releases-20.04.2) • [Registo de alterações
completo do 20.04.2](https://kde.org/announcements/changelog-
releases.php?version=20.04.2)