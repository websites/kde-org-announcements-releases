---
layout: page
publishDate: 2020-03-05 15:50:00
summary: What Happened in KDE's Applications This Month
title: KDE's March 2020 Apps Update
type: announcement
---
## Новые выпуски

### Клиент для ведения микроблогов Choqok 1.7

В феврале вышло долгожданное обновление клиента для ведения микроблогов [Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

В список важнейших изменений входят поддержка максимальной длины сообщения в сети Твиттер в 280 символов, возможность отключения учётных записей и поддержка расширенных твитов.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### Библиотека KPMcore и Диспетчер разделов KDE версии 4.1.0

Our disk formatting program Partition Manager got a new release with lots of work done to the library KPMCore which is also used by the Calamares distro installer. The app itself added support for Minix file systems.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partition Manager" >}}

### Фотоальбом KPhotoAlbum 5.6

При наличии сотен или даже тысяч изображений на жёстком диске, становится невозможным запомнить историю каждого отдельного изображения или имена людей, которые присутствуют на фотографиях. KPhotoAlbum был создан, чтобы помочь описать изображения, а затем быстро и эффективно находить из них нужную.

В этом выпуске значительно ускорено присвоение меток большим наборам фотографий, а также ускорено создание миниатюр. Robert Krawitz выполнил усовершенствование программного кода, нашёл места для оптимизации и удалил избыточные участки кода.

Кроме того, в этом выпуске добавлена поддержка модуля KDE Purpose.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Грядущие выпуски

Приложение Kid3 для редактирования метаданных MP3 ID перенесено в раздел kdereview, что означает первый шаг к подготовке новых выпусков.

And the Rocket.chat app Ruqola passed kdereview and was moved to be ready to release. However the word from the maintainer Laurent Montel is that a rewrite with Qt Widgets is planned so there's still a lot of work to be done.

## Каталоги приложений

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Пополнение в каталоге приложений Microsoft Store для Windows — музыкальный проигрыватель [Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

A modern and beautiful music player made with love by KDE. Browse, and play your music very easily! Elisa should be able to read almost any music files.Elisa is made to be accessible to everybody.

Кроме того, приложение для управление фотографиями Digikam также подготовлено к публикации в Microsoft Store.

## Обновления веб-сайта

Разработан новый веб-сайт для [KDE Connect](https://kdeconnect.kde.org/) — приложения для синхронизации ПК и мобильных устройств.

Для приложения для редактирования метаданных [Kid3](https://kid3.kde.org/) также разработан новый веб-сайт, содержащий новости и ссылки для загрузки из всех каталогов приложений, в которых размещено приложение. 

## Выпуск 19.12.3

Некоторые из наших проектов делают выпуск по собственным календарным графикам, а другие выпускаются массово. Сегодня был выпущен комплект проектов версии 19.12.3, скоро их новые версии будут доступны в магазинах приложений и через дистрибутивы. Подробную информацию смотрите на [странице выпусков 19.12.3](https://www.kde.org/info/releases-19.12.3.php). Этот комплект проектов ранее назывался «Приложения KDE» или «KDE Applications», но теперь этот бренд больше не используется, чтобы не путать с другими приложениями от KDE и поскольку «KDE Applications» на самом деле представляли собой десятки продуктов, а не один.

Некоторые из исправленных в этом выпуске ошибок:

* Улучшена работа с атрибутами файлов на общих ресурсах SMB, [несколько изменений кода](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12);

* Приложение для записи дисков K3b теперь обрабатывает файлы в формате WAV как звуковые файлы [Ошибка 399056](https://bugs.kde.org/show_bug.cgi?id=399056) [Изменение кода](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d);

* В приложение просмотра изображений Gwenview добавлена поддержка загрузки файлов по протоколу https [Изменение кода](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626).

[Примечания к выпуску
19.12](https://community.kde.org/Releases/19.12_Release_Notes) &bull; [Страница
сведений о получении приложений базы знаний
пользователей](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Ссылки на исходные коды приложений выпуска
19.12.2](https://kde.org/info/releases-19.12.3) &bull; [Полный список изменений
выпуска 19.12.3](https://kde.org/announcements/changelog-
releases.php?version=19.12.3)