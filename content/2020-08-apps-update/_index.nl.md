---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in augustus 2020
type: announcement
---
Tientallen [KDE toepassingen](https://kde.org/applications/) krijgen nieuwe uitgaven uit de uitgaveservice van KDE. Nieuwe functies, verbeteringen in gebruik, opnieuw ontwerpen en reparaties van bugs dragen allemaal bij om uw productiviteit te verhogen en deze nieuwe partij toepassingen efficiënter en prettiger in gebruik te maken.

Hier is waar u naar uit kunt kijken:

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin toont voorbeelden van vele typen bestanden." style="width: 800px" >}}

Dolphin is bestandenverkenner van KDE die u helpt bij zoeken, kopiëren en openen van bestanden en mappen. Op zichzelf is zijn hoofdfunctie u een helder idee te geven van wat elk bestand bevat. Tonen van miniaturen is hier cruciaal voor en Dolphin is al heel lang in staat om u voorbeelden van afbeeldingen, videoclips, zelfs Blender 3D-bestanden te tonen.

In deze nieuwe versie voegt Dolphin miniaturen toe voor 3D Manufacturing Format (3MF) bestanden aan de lijst en u kunt ook voorbeelden zien van bestanden en mappen op versleutelde bestandssystemen zoals Plasma Vaults. Dit wordt veilig gedaan door de miniaturen in de cache op het bestandssysteem zelf op te slaan of terug te vallen op ze generen maar niet geen versies ergens op te slaan indien nodig. In elk gevel hebt u steeds absolute controle over hoe en wanneer Dolphin u een bestandsinhoud toont, omdat u onafhankelijk de bestandsafsnijgrootte voor het tonen van voorbeelden voor lokale en bestanden op afstand.

Een andere manier om bestanden te identificeren is via hun namen. Als de bestandsnaam te lang is om te tonen, hebben ontwikkelaars het gedrag van verkorten van de bestandsnaam verfijnt. In plaats het midden van een lange bestandnaam of map weg te laten zoals Dolphin in eerdere versies deed, snijdt de nieuwe versie het einde af en behoudt altijd de bestandsextensie (indien aanwezi) zichtbaar achter de puntjes. Dit maakt het identificeren van bestanden met lange namen veel gemakkelijker.

Dolphin herinnert en herstelt nu de locatie die u bekeek, evenals de geopende tabbladen en gesplitste weergaven die u open had toen u het laatst sloot. Deze functie staat standaard aan, maar kan uitgeschakeld worden in de "Opstart"-pagina van het venster "Instellingen" van Dolphin.

Sprekend over verbeteringen in gebruik, Dolphin heeft altijd u shares op afstand laten aankoppelen en verkennen, zowel via FTP, SSH als andere protocollen. Maar nu toont Dolphin shows FUSE-aankondigingen met gebruikersvriendelijke getoonde namen in plaats van het volledige pad. Nog beter: u kunt ook ISO-images aankoppelen met een nieuw menu-item in het contextmenu. Dit betekent dat u een bestandssysteem kunt downloaden en verkennen dat u later op een dvd-schijf wilt branden of op USB-stick zetten.

{{< video src="dolphin_ISO.mp4" caption="Dolphin laat u ISO-images aankoppelen." style="max-width: 900px" >}}

Dolphin heeft een verbazingwekkend aantal mogelijkheden, maar u kunt er nog meer toevoegen met plug-ins. Als u eerder de menuplug-in "Service" wilde toevoegen (kijk onder <I>Instellingen</I> > <I>Dolphin configureren</I> > <I>Services</I>) moest u vaak een script uitvoeren of een pakket uit de opslagruimte. Nu kunt u ze installeren direct uit het venster "Nieuwe dingen" zonder handmatige intermediaire stappen. Andere wijzigingen aan de instellingenpagina "Services" is dat het een brandnieuw zoekveld bovenaan heeft u snel te helpen bij het vinden van waar u naar op zoek bent en de lijst met services is nu alfabetisch georganiseerd.

Zaken verplaatsen is gemakkelijker met de nieuwe Dolphin, omdat u nu nieuwe acties hebt om snel geselecteerde bestanden in een pane van een gesplitste weergave in de map in het andere paneel te verplaatsen of te kopiëren. Om duidelijk te maken wat er gaande is, bij verslepen van een bestand, wijzigt de cursor nu standaard in een grijphandje in plaats van de cursor "bezig met kopiëren" zoals het deed in eerdere versies.

U kunt ook mapgroottes op schijf berekenen en tonen met de weergave "Details" en het paneel "Informatie" van Dolphin dat nu nuttige informatie toont voor de Prullenbak. Het is ook mogelijk vanuit het zoekveld van Dolphin te navigeren naar de resultatenweergave door op de toets pijl omlaag te drukken.

Tenslotte, een nieuwe nuttige functie is dat Dolphin 20.08 komt met een nieuw menu-item "Locatie kopiëren"...

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Konsole heeft enige spannende nieuwe functies." style="width: 800px" >}}

… Net als in de terminaltoepassing van KDE, Konsole! Dit betekent dat u rechts kan klikken op een getoond item Konsole, kies "Locatie kopiëren" uit het pop-up-menu en kopieer en plak daarna het volledige pad van het bestand of map in een document, een andere terminal of toepassing.

Konsole komt ook met een nieuwe functie die een subtiele accentuering toont voor nieuw verschenen regels wanneer de terminaluitvoer snel voorbij schuift en standaard een miniatuur toont van een afbeeldingsbestand waarboven uw cursor zweeft. Bovendien, wanneer u rechts klikt op een onderstreept bestand in Konsole, zal het contextmenu een "Openen met"-menu tonen zodat u het bestand in elke toepassing die u kiest kunt openen.

In staat zijn om de weergave van Konsole te splitsen en tabbladen toevoegen om verschillende dingen tegelijkertijd te doen is al lang een mogelijkheid geweest, maar nu kunnen gesplitste weergavekoppen uitgeschakeld wordt en de dikte van de scheiding kan vergroot worden. U kunt ook een tabblad monitoren op het voltooien van het actieve proces en kleuren geven aan de Konsole tabbladen!

{{< img class="text-center" src="konsole-monitor.webp" caption="Nieuw monitorproces in Konsole" style="width: 500px" >}}

Sprekend over tabblad, de standaard sneltoets Ctrl+Shift+L van Konsole voor "Huidige tabblad loskoppelen" is verwijderd. Er bleek dat het te gemakkelijk was om per ongeluk het huidige tabblad los te koppelen wanneer u in werkelijkheid het scherm wilde wissen met Ctrl+Shift+K.

De laatste verbetering in gebruik in deze versie is dat de cursor in de vorm van een I van Konsole zich nu aanpast aan de grootte van het lettertype in plaatst van altijd dezelfde grootte te blijven.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Nieuw Yakuake-pictogram" style="width: 200px" >}}

Sprekend over terminals, Yakuake is de afrolterminalemulator van KDE. Het ontvouwt zich vanaf de bovenkant van uw scherm elke keer als u F12 indrukt en het heeft ook een hoop verbeteringen voor deze uitgave gekregen. Het laat u nu alle sneltoets configureren die uit Konsole komen en er is een nieuw systeemvakitem dat u toont wanneer Yakuake actief is. U kunt het natuurlijk verbergen maar het maakt het gemakkelijker te vertellen of Yakuake actief is en gereed of niet. Het biedt ook een grafische manier om het op te roepen.

Iets anders is dat we Yakuake in Wayland hebben verbeterd. Het hoofdvenster had de gewoonte onder toppanelen die u zichtbaar had te verschijnen, maar dat is nu gecorrigeerd.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

DigiKam is het professionele fotobeheerprogramma en het kwam zo uit dat [het team zojuist een nieuwe hoofdversie, 7.0, heeft vrijgegeven](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Gezichtsherkenning met Digikam" style="width: 800px" >}}

Een van de belangrijkste hoogtepunten van digiKam 7.0.0 is de verbetering van gezichtsherkenning die nu "deep learning AI algorithms" gebruikt. U kunt de Akademy talk van het laatste jaar bekijken om te zien hoe dit is gedaan en er zal meer opkomend werk in toekomstige versies voor gezichtsherkenning en automatische detectie zijn.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Gezichtsherkenning met Digikam" style="max-width: 800px" controls="true" >}}

Het [hoofd Flatpak pakket van digiKam is beschikbaar via Flathub](https://flathub.org/apps/details/org.kde.digikam), evenals een onstabiele nachtelijke bouw voor testers. DigiKam is ook beschikbaar voor Linux via AppImages en de opslagruimten van uw distributies. Er zijn ook versies van digiKam voor Windows en Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

We hebben een paar kleine wijzigingen gedaan aan het uiterlijk van onze tekstbewerker Kate. Het menu "Recent openen" toont nu documenten geopend in Kate vanaf de opdrachtregel en eveneens vanaf andere bronnen, niet alleen die geopend zijn met de bestandsdialoog. Een andere wijziging is dat de tabbladbalk van Kate nu visueel consistent is met alle tabbladbalken in andere KDE toepassingen en nieuwe tabbladen aan de rechterkant openen, zoals de meeste andere tabbladbalken doen.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa is een lichtgewicht muziekspeler voor bureaublad en mobiel." style="width: 800px" >}}

Elisa is een lichtgewicht muziekspeler die op uw bureaublad en uw mobiele telefoon werkt. Elisa laat u nu alle genres, artiesten of albums tonen in de zijbalk, onder andere items. De afspeellijst toont ook de voortgang van de nu spelende song en de bovenste balk reageert nu op de venstergrootte en de vormfactor van het apparaat. Dat betekent dat het er goed uitziet met een portretoriëntatie venster/apparaat (bijvoorbeeld, op uw telefoon) en omlaag kan schalen naar echt klein.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="KStars is de toepassing van KDE voor liefhebbers van astronomie." style="width: 800px">}}

Het astronomieprogramma KStars heeft enige calibratie en focus toegevoegd aan de [nieuwe uitgave](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [Het Bahtinov masker is een apparaat dat gebruikt wordt om accuraat de focus van kleine astronomische telescopen in te stellen](https://en.wikipedia.org/wiki/Bahtinov_mask). Het is nuttig voor gebruikers die geen gemotoriseerde instelling van de focus hebben en liever het focus handmatig met de hulp van het masker instellen. Na het nemen van een afbeelding in de focusmodule met het geselecteerde Bahtinov maskeralgoritme, zal Ekos de afbeeldingen en sterren erin analyseren. Als Ekos het Bahtinov sterpatroon herkent zal het lijnen over het sterrenpatroon in cirkels tekenen om het centrum en op een offset om het focus aan te geven.

{{< img class="text-center" src="kstars-focus.webp" caption="Nieuw focusalgoritme van KStars" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="Calibratie van KStars" style="width: 600px" >}}

We hebben een subtabblad "Calibratieplot" rechts van de "Driftplot". Het toont de aankoppelposities opgenomen tijdens interne-geleider-calibratie. Als alles goed gaat zou het stippen in twee lijnen moeten tonen die die een rechte hoek met elkaar vormen, een wanneer de calibratie de aankoppeling heen en weer drukt langs de RA-richting en dan wanneer het dat doet hetzelfde voor de DEC-richting. Niet erg veel informatie, maar kan nuttig zijn om te zien. Als de twee lijnen een 30-graden hoek maken is er iets niet in orde met uw calibratie! Hierboven is er een plaatje van in actie met de simulator.

[KStars is beschikbaar voor downloaden](https://edu.kde.org/kstars/) voor Android, uit de Snapcraft store, voor Windows, macOS en natuurlijk vanuit de opslagruimten van uw Linux distributie.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="KRDC met cursor op afstand" style="max-width: 900px" >}}

KRDC biedt u het bekijken en besturen van de bureaubladsessie op een andere machine. De vandaag uitgegeven versie toont nu op de juiste manier de cursors aan de kant van de server in VNC in plaats van een kleine stip met de cursor op afstand die achterblijft.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

Okular is de documentviewer van KDE. Het laat u PDF-documenten, ePUB boeken lezen en nog veel meer typen op tekst gebaseerde bestanden. Een reparatie heeft "Afdrukken" en "Voorbeeldafdruk" opnieuw naast elkaar gezet in het menu "Bestand".

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

Gwenview is een toepassing voor bekijken van afbeeldingen die enige basisfuncties heeft voor bewerken, zoals grootte wijzigen en afsnijden. De nieuwe versie slaat de grootte van de laatst gebruikte afsnijgrootte op, wat betekent dat u snel meerdere afbeeldingen snel achter elkaar naar dezelfde grootte kan brengen.

# Verbeterde bugs

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* Opslaan is verplaatst naar een thread om het interface niet te bevriezen tijdens het opslaan

* Geïmplementeerd als D-Bus interface voor sneltoetsen en besturing van scannen

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

* De timerfunctie van Spectacle en de sneltoetsen Shift + Printscreen (schermafdruk van volledig scherm) en Meta + Shift + Printscreen (schermafdruk van rechthoekig gebied) werken nu in Wayland.

* Spectacle neemt niet langer standaard de muiscursor mee in schermafdrukken.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

* struct2osd gebruikt nu castxml (gccxml is afgekeurd).

* Minder gebruik van afgekeurde Qt-code, waarmee gelogde waarschuwingen tijdens uitvoeren worden vermeden.

* Verbeterde vertalingen.