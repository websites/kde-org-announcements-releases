---
layout: page
publishDate: 2019-11-29 00:01:00
summary: Over 120 individual programs plus dozens of programmer libraries and feature
  plugins are released simultaneously as part of KDE's release service.
title: 19.12 RC Releases
type: announcement
---
2019년 11월 29일. KDE 릴리스와 함께 개별 프로그램 120여개, 프로그래머 라이브러리 수십개와 기능 플러그인이 출시되었습니다.

Today they all get release candidate sources meaning they are feature complete but need testing for final bugfixes.

Distro and app store packagers should update their pre-release channels to check for issues.

+ [19.12 release notes](https://community.kde.org/Releases/19.12_Release_Notes) for information on tarballs and known issues.

+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

+ [19.12 RC source info page](https://kde.org/info/applications-19.11.90)

## 보도 연락

더 많은 정보를 보려면 이메일을 보내 주십시오: [press@kde.org](mailto:press@kde.org).

