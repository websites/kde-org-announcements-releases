---
layout: page
publishDate: 2020-06-11 12:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's June 2020 Apps Update
type: announcement
---
It always a joy when the KDE family grows, that's why this month we are especially happy to welcome backup manager Kup and a whole new packaging effort: Homebrew.

# 새 릴리스

## Kup 0.8

[Kup](https://store.kde.org/p/1127689) is a backup tool you use to keep your files safe.

It was previously developed outside of KDE, but this last month is has passed the Incubation process and joined our community, becoming officially a KDE project. The lead developer, Simon Persson, has celebrated with a new release.

Here are the changes you will find in the new version:

* Changed how rsync type backups are stored when only one source folder is selected. This change tries to minimize risk of deleting files for a user who selects a non-empty folder as destination. Added migration code to detect and move your files on the first run, thus avoiding copying everything again and doubling the storage.

* Added advanced option that lets you specify a file Kup will read exclude patterns from, for example, letting you tell Kup to never save *.bak files.

* Changed default settings, hopefully making them better.

* Reduced warnings about files not being included, as it was raising too many false alarms.

* Kup no longer asks for a password to unlock encrypted external drives just for the sake of showing how much space is available.

* Fixed not treating a backup saving as failed just because files went missing during the operation, both for rsync and bup.

* Started running backup integrity checks and repairs in parallel based on the number of CPUs.

* Added support for bup metadata version 3, which was added in bup version 0.30.

* Lots of smaller fixes to user interface.

Kup can backup using rsync or do versioned backups with the Python tool Bup. Bup currently only works with Python 2 which means this option won't be available on many distros, but a port to Python 3 is in the works.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

To find out more about Kup, [Average Linux User did a review](https://averagelinuxuser.com/kup-backup/) and a video on Kup not long ago:

{{< youtube y53IE0G8Brg >}}

# Krita on Android Tablets

Thanks to the hard work of Sharaf Zaman, Krita is now available in the Google Play Store for Android tablets and Chromebooks (but not for Android phones).

This beta, based on Krita 4.2.9, is the full desktop version of Krita, so it doesn’t have a special touch user interface. But it's there, and you can play with it.

Unlike the Windows and Steam store, they don’t ask for money for Krita in the store, since it’s the only way people can install Krita on those devices. However, you can buy a supporter badge from within Krita to support development.

To install

* Get [Krita from Google Play](https://play.google.com/store/apps/details?id=org.krita)

* Alternatively, in the Play Store, switch to the "Early access" tab and search for org.krita. (See: Google’s official instructions on Early Access. Until we’ve got a reasonable number of downloads, you’ll have to scroll down a bit.)

* You can also [download the apk files yourself](https://files.kde.org/krita/android/). Do NOT ask for help installing those files.

* Those are all the official places. Please do not install Krita from other sources. We cannot guarantee their safety.

Notes

* Supports Android tablets & Chromebooks. Android versions supported: Android 6 (Marshmallow) and up.

* Currently not compatible with: Android phones.

* If you have installed one of Sharaf’s builds or a build you’ve signed yourself, you need to uninstall that first, for all users!

{{< img class="text-center" src="krita-android.jpg" caption="Krita on Android" style="width: 600px">}}

# 새로 추가됨

[KIO Fuse](https://techbase.kde.org/Projects/KioFuse) made its first [beta release](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html) this month.

## 버그 수정

Bugfix releases came out for

* Collection manager [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html) with an updated filter dialog to allow matching against empty text.

* Local network browser [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) fixed saving settings on close.

* Coders IDE [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) made an update for the moved KDE repositories.

# App Store

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

While in Linux we are gradually getting used to being able to install individual apps from an app store, the reverse is happening in the world of macOS and Windows. For these systems, package managers are being introduced for those who like one source to control everything on their systems.

The leading open source package repository for macOS is Homebrew, managed by a crack team of developers including former KDE dev Mike McQuaid.

This month the [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde) project, which has been running external to KDE for a while, moved into KDE to be a full part of our community.

You can add the KDE Homebrew repo for macOS and download KDE sources complied ready for you to run.

We caught up with lead dev Yurii Kolesnykov and asked him about the project.

### Tell us about yourself, what’s your name, where do you come from, what’s your interest in KDE and mac, what do you do for a living?

My name is Yurii Kolesnykov, I’m from Ukraine. I have a passion for Free Software since I first heard about it, approximately in the end of high school. I think KDE is simply the best DE for Linux and Unix systems with many great apps. My interest in Mac comes from my main job, I develop iOS Mobile Software for living.

### What Is Homebrew?

Homebrew is the most popular package manager for macOS, just like apt or yum. Since macOS is Unix and Apple provides good compiler and toolchain for it, people decided to create package managers for it, so you may install much free and open source software on Mac. Homebrew also has a subproject, called Homebrew Cask, which allows you to install many binary applications, i.e. proprietary or GUI ones. Because GUI apps are hard to integrate with the system if they are installed via Homebrew.

### What KDE packages have you made for Homebrew?

I just ran grep on our tap, and I see that we have 110 packages in total, 67 of them are frameworks, and approximately 39 apps. We already have most popular apps, like Kate, Dolphin and KDevelop, because of users request.

### As a Mac user what do you need to do to get apps installed?

At first, you need to follow Homebrew installation guide if you don’t have it yet, it’s available at [brew.sh](https://brew.sh). Then you need to tap our repo with the following:

`brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git`

Unfortunately a lot of KDE packages doesn’t work out-of-the-box, but we created a script that makes all the necessary hacks, so after tapping you need to run the following command:

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### Do you know how popular Homebrew is as a way of getting apps for Mac?

Good question. Unfortunately we haven’t setup any analytics yet, I will add it to my TODO list. But given the fact that Homebrew is the most popular package manager for Mac and it requires users not to mix it with other similar projects to install software on same Mac, due to conflicts. So, yes, I think it’s quite popular.

### How much work did you need to do to get KDE apps working in Homebrew?

During creation of current packages, we already addressed many common issues, so bringing new software is relatively easy. I promise to write a How to for this, users are already requested it many times.

### Currently, packages need to be compiled locally, will you have pre-compiled packages available?

Homebrew allows you to install software via Bottles, i.e. pre-compiled binary packages. But the process of creating bottles is tightly integrated with Homebrew infrastructure, i.e. we need to run CI with tests on every package before it get bottled. So we decided to integrate as many packages as possible into the main brew repo to eliminate maintenance burden.

### Is there much other desktop software available in Homebrew?

Yes. In general, if an app is popular and has a channel of distribution outside of Mac AppStore, then there is a very high chance that it’s already available to install via a Brew Cask.

### How can KDE app authors help get their software into Homebrew?

Apple hardware is very expensive, so getting a Mac for every KDE dev will be not a good idea. So as for now, they are welcome to create a Feature Request in our repo. Then maintainers or users of Homebrew KDE report bugs if something isn’t working as intended. And we are trying to provide as much information as possible upon request of KDE devs. But as for now we have a lot of pending tickets for KDE apps with small, but very annoying bugs. I hope that we will be more integrated with KDE infrastructure, i.e. we may link bugs in our repo with upstream projects. We had already migrated to KDE Invent and I hope KDE Bugs will be migrated from Bugzilla to KDE Invent soon.

### The other way to get your KDE apps built for Mac is with Craft. How does the Homebrew build apps compare to ones build with Craft?

I still think that Homebrew is more friendly to end users. Its install process is as easy as run one-liner. To add our repo and start installing apps from it, you need to run another two lines.

### Thanks for your time Yurii.

# Releases 20.04.2

Some of our projects release on their own timescale and some get released en-masse. The 20.04.2 bundle of projects was released today and will be available through app stores and distros soon. See the [20.04.2 releases page](https://www.kde.org/info/releases-20.04.2.php) for details.

Some of the fixes in today's releases:

* Writes are broken into multiple requests for SFTP servers that limit transfer size

* Konsole updates the cursor position for input methods (such as IBus or Fcitx) and no longer crashes when closing via menu

* KMail generates better HTML when adding a HTML signature to mails

[20.04 release notes](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [20.04.2 source info page](https://kde.org/info/releases-20.04.2) &bull; [20.04.2 full changelog](https://kde.org/announcements/changelog-releases.php?version=20.04.2)

