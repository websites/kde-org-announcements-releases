---
layout: page
publishDate: 2020-06-11 12:00:00
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in juni 2020
type: announcement
---
Het is altijd fijn wanneer de KDE-familie groeit, dat is waarom we deze maand speciaal blij zijn om backupbeheerder Kup te verwelkomen en een geheel nieuwe pakketmaker: Homebrew.

# Nieuwe uitgaven

## Kup 0.8

[Kup](https://store.kde.org/p/1127689) is een hulpmiddel voor het maken van reservekopieën dat u gebruikt om uw bestanden veilig te houden.

Het is eerder buiten KDE ontwikkeld, maar deze laatste maand is het door het incubatieproces gegaan en mee gaan doen in onze gemeenschap en een officieel KDE-project geworden. De hoofdontwikkelaar, Simon Persson, heeft dat gevierd met met een nieuwe uitgave.

Hier zijn de wijzigingen die u zult vinden in de nieuwe versie:

* Gewijzigd hoe backups van het type rsync opgeslagen worden wanneer slechts één bronmap is geselecteerd. Deze wijziging probeert het risico te minimaliseren van bestanden verwijderen voor een gebruiker die een niet-lege map als bestemming selecteert. Migratiecode toegevoegd die detecteert en uw bestanden verplaatst bij de eerste activering, waarmee alles opnieuw kopiëren en verdubbeling van de opslag wordt vermeden.

* Geavanceerde optie toegevoegd die u een bestand laat specificeren die Kup zal lezen waarmee patronen uitgesloten worden, bijvoorbeeld, u laten vertellen dat Kup nooit *.bak bestanden moet opslaan.

* Standaard instellingen gewijzigd, hopelijk zijn ze beter.

* Waarschuwingen over bestanden die niet zijn meegenomen verminderd, omdat het teveel valse alarmeringen deed rijzen.

* Kup vraagt niet langer om een wachtwoord om versleutelde externe stations te ontgrendelen alleen om te tonen hoeveel ruimte beschikbaar is.

* Gerepareerd het niet behandelen van een backup opslaan met mislukking alleen omdat bestanden gingen ontbreken gedurende de operatie, zowel voor rsync als bup.

* Backup-integriteitscontrole en reparaties parallel gestart gebaseerd op het aantal CPU's.

* Ondersteuning toegevoegd voor bup-metagegevens versie 3, die was toegevoegd in bup versie 0.30.

* Veel kleinere reparaties aan gebruikersinterface.

Kup kan backup maken met rsync of backups met versies met het Python hulpmiddel Bup. Bup werkt op dit moment alleen met Python 2 wat betekent dat deze optie niet beschikbaar zal zijn in vele distributies, maar aan het overzetten naar Python 3 wordt gewerkt.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

Om meer over Kup te weten te komen, [Gemiddelde Linux gebruiker deed een review](https://averagelinuxuser.com/kup-backup/) en een video over Kup niet te lang geleden:

{{< youtube y53IE0G8Brg >}}

# Krita op Android tablets

Dankzij het harde werk van Sharaf Zaman is Krita nu beschikbaar in de Google Play Store voor Android tablets en Chromebooks (maar niet voor Android telefoons).

Deze beta, gebaseerd op Krita 4.2.9, is de volledige bureaubladversie van Krita, het heeft dus geen speciaal aanraakgebruikersinterface. Maar het is er en u kunt er mee spelen.

Anders dan de Windows en Steam store, vragen ze niet om geld voor Krita, omdat het de enige manier voor de mensen people is om Krita te installeren op deze apparaten. U kunt echter een ondersteunerbadge kopen vanuit Krita om ontwikkeling te ondersteunen.

Om te installeren

* Haal op [Krita uit Google Play](https://play.google.com/store/apps/details?id=org.krita)

* Schakel ander in de Play Store om naar het tabblad "Early access" en zoek naar org.krita. (Zie: Google’s officiële instructies op Early Access. Totdat we een redelijk aantal downloads hebben, moet u een beetje omlaag schuiven).

* U kunt ook [zelf de apk bestanden downloaden](https://files.kde.org/krita/android/). Vraag GEEN hulp om deze bestanden te installeren.

* Dit zijn alle officiële plaatsen. Installeer Krita niet uit andere bronnen. We kunnen hun veiligheid niet garanderen.

Notities

* Ondersteunt Android tablets & Chromebooks. Ondersteunde Android versies: Android 6 (Marshmallow) en hoger.

* Op dit moment niet compatibel met: Android telefoons.

* Als u een van de builds van Sharaf hebt geïnstalleerd of een build die u zelf hebt ondertekenend, dan moet u die eerst verwijderen, voor alle gebruikers!

{{< img class="text-center" src="krita-android.jpg" caption="Krita op Android" style="width: 600px">}}

# Inkomend

[KIO Fuse](https://techbase.kde.org/Projects/KioFuse) kwam deze maand met de eerste [beta uitgave](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html).

## Verbeterde bugs

Uitgaven met reparaties voor bugs kwamen uit voor

* Beheerder van verzamelingen [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html) met een bijgewerkte filterdialoog om overeenkomen met lege tekst te bieden.

* Locale netwerkbrowser [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) kreeg reparaties voor opslaan van instellingen bij sluiten.

* Coders IDE [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) is bijgewerkt vanwege de verplaatste KDE opslagruimten.

# App Store

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

Terwijl we in Linux geleidelijk gewend raken aan het in staat stellen van het installeren van individuele toepassingen uit een app-store, gebeurt het omgekeerde in de wereld van macOS en Windows. Voor deze systemen worden pakketbeheerders geïntroduceerd voor hen die één bron willen hebben om als op hun systemen te beheren.

De leidende opslagruimte voor opensource pakketten voor macOS is Homebrew, beheerd door een kraakteam van ontwikkelaars inclusief voormalig KDE ontwikkelaar Mike McQuaid.

Deze maand het [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde) project, die al even buiten KDE actief is, is in KDE opgenomen om volledig onderdeel van onze gemeenschap te zijn.

U kunt de KDE Homebrew opslagruimte voor macOS toevoegen en KDE bronnen downloaden geheel gereed voor u om uit te voeren.

We hadden een ontmoeting met hoofdontwikkelaar Yurii Kolesnykov en vroegen hem naar het project.

### Vertel ons iets over uzelf, wat is uw naam, waar komt u vandaan, wat is uw interesse in KDE en mac, wat doet u voor de kost?

Mijn naam is Yurii Kolesnykov, Ik kom uit de Oekraïne. Ik heb een passie voor Vrije Software sinds ik er voor de eerste keer van hoorde, ongeveer aan het eind van de middelbare school. Ik dat KDE eenvoudig de beste bureaubladomgeving voor Linux en Unix systemen is met vele geweldige toepassingen. Mijn interesse in Mac komt van mijn hoofdjob, I ontwikkel iOS Mobile Software om van te leven.

### Wat is Homebrew?

Homebrew is de populairste pakketbeheerder voor macOS, zoals apt of yum. Omdat macOS Unix is en Apple een goede compiler en verdere hulpmiddelen ervoor levert, heeft men besloten om er pakketbeheerders voor te ontwikkelen, u kunt dus veel vrije en opensource software op een Mac installeren. Homebrew heeft ook een subproject, genaamd Homebrew Cask, die u in staat stelt om veel binaire toepassingen te installeren, d.w.z. niet vrije of met een GUI. Omdat toepassingen met een GUI moeilijk zijn te integreren met het system als ze geïnstalleerd zijn via Homebrew.

### Welke KDE pakketten hebt u gemaakt voor Homebrew?

Ik heb zojuist grep op ons tablet uitgevoerd en ik kan zien dat we 110 pakketten in totaal hebben, 67 ervan zijn frameworks en ongeveer 39 toepassingen. We hebben al de meest populaire toepassingen, zoals Kate, Dolphin en KDevelop, vanwege verzoeken van gebruikers.

### Als een Mac-gebruiker wat moet men doen om toepassingen geïnstalleerd te krijgen?

Ten eerste moet u de Homebrew installatiegids volgen als u die nog niet hebt, deze is beschikbaar op [brew.sh](https://brew.sh). Daarna moet u onze opslagruimte aftappen met het volgende:

`brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git`

Helaas werken veel pakketten van KDE niet zomaar, maar we hebben een script gemaakt dat alle noodzakelijke aanpassingen doet, dus na aftappen moet u het volgende commando uitvoeren:

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### Weet u hoe populair Homebrew is als een manier voor ophalen van toepassingen voor Mac?

Goeie vraag. Helaas hebben we nog geen enkele analyse gedaan, Ik zal het toevoegen aan mijn TEDOEN-lijst. Maar gegeven het feit dat Homebrew de meest populaire pakketbeheerder voor Mac is en het vereist dat gebruikers het niet mengen met andere gelijke projecten om software te installeren op dezelfde Mac, vanwege conflicten. Dus, ja, ik denk dat het behoorlijk populair is.

### Hoe veel werk had u nodig om KDE toepassingen te laten werken in Homebrew?

Tijdens het maken van de huidige pakketten hebben we al vele algemene problemen aangepakt, dus is het brengen van nieuwe software relatief gemakkelijk. Ik beloof een Hoe-te-doen hiervoor te schrijven, gebruikers hebben het al vele keren gevraagd.

### Op dit moment moeten pakketten lokaal gecompileerd worden, hebt u voor-gecompileerde pakketten beschikbaar?

Homebrew biedt u het installeren van software via Bottles, d.w.z. vooraf gecompileerde binaire pakketten. Maar het proces van het maken van bottles is nauw geïntegreerd met de Homebrew-infrastructuur, d.w.z. we moeten CI uitvoeren met testen op elk pakket voordat het in een bottle gaat. We hebben dus besloten om zo veel mogelijk pakketten te integreren in de hoofdbrew-opslagruimten om de onderhoudslast te elimineren.

### Is er veel andere bureaubladsoftware beschikbaar in Homebrew?

Ja. In het algemeen, als een toepassing populair is en een kanaal voor distributie heeft buiten de Mac AppStore, dan is er een zeer hoge kans dat het al beschikbaar is om te installeren via een Brew Cask.

### Hoe kunnen auteurs van KDE toepassingen helpen om hun software in Homebrew te krijgen?

Apple hardware is erg duur, dus het verkrijgen van een Mac voor elke KDE ontwikkelaar zal dus niet zo'n goed idee zijn. Voorlopig zijn ze welkom om een vezoek om een functie in onze opslagruimte aan te maken. Daarna rapporteren onderhouders of gebruikers van Homebrew KDE bugs als iets dat niet werkt zoals bedoeld. En we proberen zoveel mogelijk information te verstrekken op verzoek van ontwikkelaars van KDE. Maar op dit moment hebben we heel wat tickets voor KDE toepassingen met kleine, maar erg vervelende bugs. Ik hoop dat we meer geïntegreerd zullen zijn met de KDE-infrastructuur, d.w.z. we kunnen misschien bugs in onze opslagruimte koppelen met upstream projecten. We zijn al gemigreerd naar KDE Invent en ik hoop dat KDE Bugs spoedig gemigreerd zal worden van Bugzilla naar KDE Invent.

### De andere manier om uw KDE toepassingen gebouwd voor Mac is met Craft. Hoe kan men de Homebrew bouwtoepassing vergelijken met die gebouwd met Craft?

Ik denk nog steeds dat Homebrew vriendelijker is voor eindgebruikers. Zijn installatieproces is net zo gemakkelijk als een enkele regel. Om onze opslagruimte toe te voegen en beginnen met installeren van toepassingen eruit, moet u nog twee regels meer uitvoeren.

### Hartelijk dank voor uw tijd Yurii.

# Uitgaven 20.04.2

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en sommigen in massa vrijgegeven. De bundel 20.04.2 van projecten is vandaag vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor toepassingen en distributies. Zie de [20.04.2 uitgavepagina](https://www.kde.org/info/releases-20.04.2.php) voor details.

Enige van de reparaties in de uitgave van vandaag:

* Wegschrijven is gebroken bij meerdere verzoeken naar SFTP servers die grootte van overdracht beperken

* Konsole werkt de cursorpositie bij voor invoermethoden (zoals IBus of Fcitx) en crasht niet meer bij sluiten via menu

* KMail genereert betere HTML wanneer een HTML ondertekening wordt toegevoegd aan e-mailberichten

[20.04 uitgavenotities](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Wiki-pagina voor downloaden van
pakketten](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [20.04 informatiepagina van
broncode](https://kde.org/info/releases-20.04.0) &bull; [20.04 volledige log met
wijzigingen](https://kde.org/announcements/changelog-
releases.php?version=20.04.0)