---
title: KDE's September 2020 Apps Update
publishDate: "2020-09-03T12:00:00+00:00"
layout: page
summary: "What Happened in KDE's Applications This Month" # todo change it with social tweet content
type: announcement
draft: false
---

A quiet month ahead of our big conference this weekend Akademy.  Join us [online from Friday 4th](https://akademy.kde.org/2020/) to talk all things KDE.

# New releases

## PBI 1.7.8

[Plasma Browser Integration](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/) our addon for Chrome and Firefox got an update.

{{< youtube WGhrR1HsCOM >}}

This fixes it working with Vivaldi and Brave browsers.  It fixes video controls with iframes. Some websites set the album cover art through the Javascript Media Session API but for those which don't the video player’s poster is now used as an album cover.  And it fixes the detection of short "new message" notifications that do not need controls with those which just report their length wrongly.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

A new accessibility tool this month is Kontrast. It is a color contrast checker and tell you if your color combinations are accessible for people with color vision deficiencies.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="KPhotoAlbum Light and Dark" style="width: 600px">}}

[KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) got a new release with code tidying but also some new feature.

It added support for custom color schemes. KPhotoAlbum now also has a “Dark Mode”.  There are experimental options for image search tuning.   And it also added a command-line tool kpa-thumbnailtool to manage KPA’s thumbnail cache. 

# Bugfixes

## Tellico

Collection manager [Tellico](https://tellico-project.org/tellico-3-3-2-released/) got an update which improves access to [French cinema collection Allocine](http://www.allocine.fr/), [Russian Cinema collection Kinopoisk](https://www.kinopoisk.ru/), [Goodreads, the world’s largest site for readers and book recommendations](https://www.goodreads.com/) and our very own [KDE Store](https://store.kde.org/browse/cat/591/).

# Incoming

Added to KDE Review this month is plugin [SeExpr for Krita](https://invent.kde.org/graphics/seexpr), a Fork of Walt Disney Animation Studios' SeExpr, for Google Summer of Code 2020.

# App Store

There is an update on the [KDE is All About the Apps goal at Akademy](https://conf.kde.org/en/akademy2020/public/events/211) online conference, this Saturday 5 September at 09:30UTC.

A lightning talk on [Flatpak, Flathub and KDE](https://conf.kde.org/en/akademy2020/public/events/264) at 18:30UTC on Saturday.

Then [Saturday has tutorials](https://community.kde.org/Akademy/2020/Monday) on Snaps, Neon packaging, Building your first flatpak and Make your App an Appimage.

# Releases 20.08.1

Some of our projects release on their own timescale and some get released en-masse. The 20.08.1 bundle of projects was released today and will be available through app stores and distros soon. See the [20.08.1 releases page](https://www.kde.org/info/releases-20.08.1) for details.

Some of the fixes in today's releases:

 * In Kontact, QR codes now work in the preview of contacts.
 * The setting for disk space gauge in Dolphin's status bar is again applied correctly.
 * Creating alarms from templates in the KAlarm event reminder has been fixed.
 * Audio-only streams can now be inserted into Kdenlive's video timeline.

[20.08 release notes](https://community.kde.org/Releases/20.08_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;  [20.08.1 source info page](https://kde.org/info/releases-20.08.1) &bull; [20.08.1 full changelog](https://kde.org/announcements/changelog-releases.php?version=20.08.1)
