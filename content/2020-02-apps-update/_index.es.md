---
layout: page
publishDate: 2020-02-06 12:00:00
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de febrero de 2020"
type: announcement
---
Este mes contempla una plétora de lanzamientos de correcciones de errores. Aunque es posible que todos deseemos nuevas funcionalidades, también sabemos que a todo el mundo le gusta que los errores y los fallos se corrijan y se solucionen.

# Nuevos lanzamientos

## KDevelop 5.5

El gran lanzamiento de este mes viene de la mano de [KDevelop 5.5](https://www.kdevelop.org/news/kdevelop-550-released), el IDE que facilita la creación de programas en C++, Python y PHP. Hemos corregido gran cantidad de errores y arreglado mucho código de KDevelop. Hemos añadido capturas de inicialización de lambdas en C++, selecciones de conjuntos de comprobaciones predefinidas y configurables para Clazy-tidy y para Clang, y terminación automática predictiva más fiable. PHP obtiene las propiedades de tipos de PHP 7.4 y permite usar arrays de tipos y visibilidad de clases constantes. En Python se ha añadido compatibilidad con Python 3.8.

KDevelop está disponible a través de su distribución Linux o como AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) es nuestro gestor de listas de tareas pendientes que se integra con Kontact. Los lanzamientos más recientes de Kontact hicieron que Zanshin dejara de funcionar correctamente y que los usuarios dejaran de saber las siguientes tareas que debían realizar. Pero el lanzamiento de este mes hace que [vuelva a funcionar de nuevo](https://jriddell.org/2020/01/14/zanshin-0-5-71/). ¡Ya podemos saber qué tarea es la próxima que debemos realizar!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

El lanzamiento de [Okteta](https://kde.org/applications/utilities/org.kde.okteta), el editor hexadecimal de KDE, incluye una nueva funcionalidad: un algoritmo CRC-64 para la herramienta de sumas de verificación. Okteta también actualiza su código para las nuevas versiones de Qt y de KDE Frameworks.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

[KMyMoney](https://kmymoney.org/), la aplicación de KDE que le ayuda a gestionar sus ahorros, [incluye varias correcciones de errores](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) y una mejora que permite usar formularios de cheques con el protocolo de divisiones.

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Novedades

Keysmith es un generador de códigos de dos factores para Plasma Mobile y para el escritorio Plasma que usa el juego de herramientas «oath». La interfaz de usuario está escrita en Kirigami, haciendo que se pueda ajustar a cualquier tamaño de pantalla. Se espera su próximo lanzamiento.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) es un gestor de paquetes para el sistema operativo Windows. Chocolatey gestiona la instalación y la actualización de todo el software del sistema y puede facilitar la experiencia a los usuarios de Windows.

[KDE posee una cuenta en Chocolatey](https://chocolatey.org/profiles/KDE) con la que puede instalar Digikam, Krita, KDevelop y Kate. La instalación es muy sencilla: todo lo que tiene que hacer es «choco install kdevelop» para que Chocolatey haga el resto. Los encargados de Chocolatey tienen un alto nivel, por lo que puede estar seguro de que los paquetes están probados y son seguros.

Si usted mantiene alguna aplicación de KDE, considere añadirla; y si se dedica a la gestión de grandes lanzamientos para Windows, considere este sistema para sus instalaciones.

# Actualización del sitio web

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}Our web team continues to update our online presence and has recently refreshed the [KMyMoney website](https://kmymoney.org).

# Lanzamiento de 19.12.2

Algunos de nuestros proyectos se publican según su propio calendario, mientras que otros se publican en masa. El paquete de proyectos 19.12.2 se ha publicado hoy y debería estar disponible próximamente en las tiendas de aplicaciones y en las distribuciones. Consulte la [página de lanzamientos 19.12.2](https://www.kde.org/info/releases-19.12.2.php) para más detalles. Este paquete se llamaba anteriormente Aplicaciones de KDE, pero se ha simplificado su nombre para que se convierta en un servicio de lanzamiento, evitando así confusiones con la totalidad de aplicaciones creadas por KDE, ya que está compuesto de docenas de productos diferentes en lugar de su totalidad.

Estas son algunas de las correcciones de errores que incluye este lanzamiento:

* El reproductor de música [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) puede manejar archivos que no contienen metadatos.

* Los adjuntos de los mensajes guardados en la carpeta *Borradores* ya no desaparecen cuando se vuelve a abrir el mensaje que los contiene para editarlo en [KMail](https://kde.org/applications/internet/org.kde.kmail2).

* Se ha corregido un problema de tiempos en el visor de documentos [Okular](https://kde.org/applications/office/org.kde.okular) que podía hacer que se dejara de mostrar el contenido.

* El diseñador UML [Umbrello](https://umbrello.kde.org/) proporciona ahora una mejor importación de Java.

[Notas del lanzamiento de
19.12.2](https://community.kde.org/Releases/19.12_Release_Notes) &bull; [Página
wiki de descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Página de información principal sobre
19.12.2](https://kde.org/info/releases-19.12.2) &bull; [Registro completo de
cambios 19.12.2](https://kde.org/announcements/changelog-
releases.php?version=19.12.2)