---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: What Happened in KDE's Applications This Month
title: "Aktualiz\xE1cia aplik\xE1ci\xED KDE z Augusta 2020"
type: announcement
---
Dnes desiatky [KDE aplikácií] (https://kde.org/applications/) dostávajú nové vydania zo služby vydávania KDE. Nové funkcie, vylepšenia dizajnu a opravy chýb prispievajú k zvýšeniu vašej produktivity a zefektívneniu a spríjemneniu používania tejto novej dávky aplikácií.

Na čo sa môžete tešiť:

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin zobrazuje ukážky mnohých typov súborov." style="width: 800px" >}}

Dolphin je prehliadač súborov a adresárov KDE Pomáha vám vyhľadávať, kopírovať a otvárať súbory Jednou z jeho hlavných funkcií je preto poskytnúť vám jasnú predstavu o tom, čo každý súbor obsahuje. Zobrazovanie miniatúr je pre to rozhodujúce a Dolphin vám už dlhší čas dokáže zobraziť ukážky obrázkov, videoklipov, dokonca aj súborov Blender 3D.

V tejto novej verzii pridáva Dolphin možnosť zobrazovať miniatúry súborov 3D Manufacturing Format (3MF) a tiež si môžete prezrieť ukážky súborov a priečinkov v šifrovaných súborových systémoch, ako sú Plasma Vaults. Deje sa to bezpečne uložením miniatúr do vyrovnávacej pamäti do samotného súborového systému alebo vygenerovanie miniatúr bez ich ukladania. V oboch prípadoch máte stále úplnú kontrolu nad tým, ako a kedy vám Dolphin zobrazí obsah súborov, pretože nezávisle môžete nakonfigurovať obmedzenie veľkosti súboru na zobrazovanie ukážok miestnych a vzdialených súborov.

Ďalším spôsobom identifikácie súborov je ich meno. V prípade, že je názov súboru príliš dlhý na zobrazenie, vývojári vylepšili správanie pri skracovaní názvu súboru. Namiesto vystrihnutia uprostred dlhého názvu súboru a priečinka, ako to robil Dolphin v predchádzajúcich verziách, ale nová verzia odrezáva koniec a vždy zachováva príponu súboru (ak je k dispozícii) viditeľnú po elipsách. Týmto je identifikácia súborov s dlhými menami oveľa jednoduchšia.

Dolphin si teraz pamätá a obnoví miesto, ktoré ste si prezerali, ako aj otvorené karty a rozdelené zobrazenia, ktoré ste mali otvorené, keď ste ho naposledy zatvorili. Táto funkcia je predvolene zapnutá, ale dá sa vypnúť na stránke „Pri spustení“ v okne Dolphin nastavení.

Keď už hovoríme o vylepšeniach použiteľnosti, Dolphin vám vždy umožní pripojiť a preskúmať vzdialené zdieľania, či už prostredníctvom FTP, SSH alebo iných protokolov. Ale teraz Dolphin zobrazuje diaľkové a FUSE pripojenia s užívateľsky prívetivými zobrazovanými názvami ako úplnou cestou. Ešte lepšie: Obrazy ISO môžete pripojiť aj pomocou novej položky v kontextovej ponuke. To znamená, že si môžete stiahnuť a preskúmať súborový systém, ktorý neskôr napálite na disk alebo USB kľúč.

{{< video src="dolphin_ISO.mp4" caption="Dolphin vám umožňuje pripojiť obrazy ISO." style="max-width: 900px" >}}

Dolphin má ohromujúci počet funkcií, ale môžete ich pridať ešte pomocou doplnkov. Ak ste predtým chceli pridať do doplnkov ponuky Služby (pozrite si časť <I> Nastavenia</I>> <I>Nastaviť Dolphin</I>><I> Služby </I>) ste často museli spúšťať skript alebo inštalovať distro balík. Teraz ich môžete nainštalovať priamo z okna „Získať nové veci“ bez manuálnych prechodných krokov. Ďalšou zmenou na stránke nastavení „Služby“ je to, že má úplne nové vyhľadávacie pole v hornej časti, ktoré vám pomôže rýchlo nájsť to, čo hľadáte, a zoznam služieb je teraz usporiadaný podľa abecedy.

S novou verziou je ľahšie presúvať veci, pretože teraz máte nové akcie na rýchle presúvanie alebo kopírovanie vybratých súborov na jednom paneli rozdeleného zobrazenia do priečinka na druhom paneli. Aby sa objasnilo, čo sa deje, pri pretiahnutí súboru sa kurzor v predvolenom nastavení zmení na ruku namiesto kurzora „kopírujúci“ ako v predchádzajúcich verziách.

Veľkosti priečinkov na disku môžete vypočítať a zobraziť aj pomocou zobrazenia „Podrobnosti“ a na paneli „Informácie“ Dolphin sa teraz zobrazujú užitočné informácie o koši. Z vyhľadávacieho poľa Dolphin je tiež možné prejsť na zobrazenie výsledkov stlačením klávesu so šípkou nadol.

Nová užitočná funkcia je, že Dolphin 20.08 prichádza s novou položkou ponuky „Kopírovať umiestnenie“...

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Konsole dostáva niekoľko nových zaujímavých funkcií." style="width: 800px" >}}

... Rovnako ako v terminálovej aplikácii KDE, Konsole! To znamená, že môžete pravým tlačidlom myši kliknúť na položku zobrazenú v Konsole, v rozbaľovacej ponuke vybrať možnosť „Kopírovať umiestnenie“ a potom skopírovať a prilepiť celú cestu k súboru alebo adresáru do dokumentu alebo iného terminálu alebo do aplikácie.

Konsole prichádza aj s novou funkciou, ktorá zobrazuje jemné zvýraznenie pre nové riadky, ktoré sa zobrazujú, keď sa výstup z terminálu rýchlo posúva, a zobrazuje ukážku miniatúry pre obrazové súbory, na ktoré v predvolenom nastavení umiestnite kurzor. Keď kliknete pravým tlačidlom myši na podčiarknutý súbor v aplikácii Konsole, zobrazí sa kontextová ponuka a ponuka „Otvoriť pomocou“, aby ste mohli súbor otvoriť v ľubovoľnej aplikácii, ktorú si vyberiete.

Rozdelenie zobrazenia v Konsole a pridanie kariet, aby ste mohli robiť niekoľko vecí súčasne, je už dlhú dobu funkciou, ale teraz je možné zakázať hlavičky rozdelenia a nastaviť hrúbku oddeľovačov. Môžete tiež sledovať kartu pre aktívny proces a priraďovať farby ku kartám v Konsole!

{{< img class="text-center" src="konsole-monitor.webp" caption="Nový monitorovací proces v Konsole" style="width: 500px" >}}

Keď už hovoríme o kartách, východisková skratka Ctrl+Shift+L pre „Odpojiť aktuálnu kartu“ bola odstránená. Ukázalo sa, že je príliš ľahké náhodne odpojiť aktuálnu kartu, keď to, čo ste naozaj chceli, bolo vyčistiť displej pomocou klávesov Ctrl+Shift+K.

Posledným vylepšením použiteľnosti v tejto verzii je to, že kurzor I-lúča Konsole sa teraz prispôsobuje veľkosti písma namiesto toho, aby zostal vždy rovnaký.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Nová ikona Yakuake" style="width: 200px" >}}

Keď hovoríme o termináloch, Yakuake, emulátor terminálov KDE, ktorý sa rozvíja z hornej časti obrazovky zakaždým, keď stlačíte kláves F12, dostal aj veľa vylepšení Napríklad vám teraz umožňuje nakonfigurovať všetky klávesové skratky, ktoré skutočne pochádzajú z Konsole, a je tu nová položka na paneli úloh, ktorá sa zobrazuje, keď je spustený program Yakuake. Môžete to samozrejme skryť, ale uľahčuje vám to povedať, že Yakuake je skutočne aktívny a pripravený, a poskytuje grafický spôsob, ako ho zavolať.

Ďalšia vec je, že sme vylepšili Yakuake vo Waylande. Hlavné okno sa objavovalo pod hornými panelmi, ktoré ste mali zobrazené, ale to bolo teraz opravené.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

DigiKam is KDE's professional photo management application and it so happens that [the team have just released a major new version, 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Digikam rozpoznávanie tvárí" style="width: 800px" >}}

Jedným z hlavných vrcholov digiKam 7.0.0 je to, že sa vykonalo veľa práce na zlepšení rozpoznávania tváre pomocou algoritmov AI na strojové učenie. Môžete sledovať rozhovor Akademy z minulého roka, aby ste videli, ako sa to stalo, a v budúcich verziách sa bude pracovať viac na rozpoznávaní tváre a automatickej detekcii.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Digikam rozpoznávanie tvárí" style="max-width: 800px" controls="true" >}}

Flatpak balík[digiKam je k dispozícii na serveri Flathub] (https://flathuborg/apps/details/org.kde.digikam), ako aj nestabilné nočné zostavenie pre testerov. DigiKam je tiež k dispozícii pre Linux prostredníctvom AppImage a repozitárov vašej distribúcie. Existujú aj verzie digiKam pre Windows a Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

Vzhľad nášho textového editora Kate bol tiež vylepšený. V ponuke „Otvoriť posledné“ sa teraz zobrazujú dokumenty otvorené v Kate z príkazového riadku a tiež z iných zdrojov, nielen z tých, ktoré boli otvorené pomocou dialógového okna súboru. Ďalšou zmenou je, že lišta kariet Kate je teraz vizuálne konzistentná so všetkými lištami kariet v iných aplikáciách KDE a otvára nové karty napravo, rovnako ako väčšina ostatných panelov.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa je ľahký hudobný prehrávač pre stolné a mobilné zariadenia." style="width: 800px" >}}

Elisa je ľahký hudobný prehrávač, ktorý funguje na vašom počítači aj na vašom mobilnom telefóne. Elisa vám teraz umožňuje zobraziť všetky žánre, interpretov alebo albumy na bočnom paneli pod ostatnými položkami. Zoznam skladieb tiež zobrazuje priebeh práve prehrávanej skladby a horná lišta teraz reaguje na veľkosť okna a formu zariadenia. To znamená, že vyzerá dobre s oknom/zariadením s orientáciou na výšku (napríklad v telefóne) a môže byť zmenšený, aby bol skutočne malý.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="KStars je aplikácia KDE pre milovníkov astronómie." style="width: 800px">}}

Aplikácia pre astronómiu KStars nakoniec doplnila kalibráciu a zameriava sa na [nové vydanie] (http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [Bahtinovská maska je zariadenie používané na presné zameranie malých astronomických teleskopov] (https://en.wikipedia.org/wiki/Bahtinov_mask). Je to užitočné pre používateľov, ktorí nemajú motorizovaný zaostrovač a uprednostňujú manuálne zaostrovanie pomocou masky. Po nasnímaní obrázka v zaostrovacom module s vybratým algoritmom Bahtinovovej masky analyzuje Ekos obrázky a hviezdy v ňom. Ak Ekos rozpozná vzorec hviezdy v Bahtinove, nakreslí čiary nad vzorom hviezdy v kruhoch v strede a na ofsetu, čím označí zameranie.

{{< img class="text-center" src="kstars-focus.webp" caption="KStars nový algoritmus zaostrenia" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="KStars kalibrácia" style="width: 600px" >}}

Napravo od "Drift Plot" sme pridali kartu "Kalibračný graf". Zobrazuje montážne polohy zaznamenané počas kalibrácie interného navádzača. Ak sa situácia darí dobre, mala by zobrazovať bodky v dvoch líniách, ktoré sú navzájom v pravom uhle, jeden pri kalibrácii posúva držiak dozadu a dopredu v smere RA a potom to robí rovnako pre smer DEC. Nie je to veľa informácií, ale môže to byť užitočné vidieť. Ak sú dve čiary v 30-stupňovom uhle, niečo nie je v poriadku pri vašej kalibrácii! Hore je obrázok toho, čo sa deje pomocou simulátora.

[KStars je dostupné na stiahnutie](https://edu.kde.org/kstars/) pre Android, zo Snapcraft store, pre Windows, macOS a, samozrejme, pre vašu Linux distribúciu.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="KRDC s vzdialeným kurzorom" style="max-width: 900px" >}}

KRDC umožňuje zobraziť alebo dokonca ovládať reláciu pracovnej plochy na inom počítači. Verzia, ktorá bola dnes vydaná, teraz zobrazuje správny kurzor na strane servera vo VNC namiesto malej bodky so vzdialeným kurzorom za ňou.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

Okular je prehliadač dokumentov KDE. Umožňuje čítať dokumenty PDF, knihy ePUB a mnoho ďalších typov textových súborov. Oprava vložila "Tlačiť" a "Náhľad tlače" opäť vedľa seba v ponuke "Súbor".

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

Gwenview je aplikácia na prezeranie obrázkov, ktorá je dodávaná s niektorými základnými funkciami úpravy, ako je zmena veľkosti a orezanie. Nová verzia ukladá veľkosť naposledy použitého poľa orezania, čo znamená, že môžete rýchlo orezať viac obrázkov na rovnakú veľkosť v rýchlom slede.

# Opravy chýb

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* Ukladanie bolo presunuté do samostatného vlákna, aby počas ukladania nezamŕzalo rozhranie

* Rozhranie D-Bus pre klávesové skratky a kontrolu skenovania

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

* Funkcia časovača v Spectacle Shift + PrintScreen (snímka celej obrazovky) a skratky Meta + Shift + PrintScreen (snímka obrazovky obdĺžnikovej oblasti) teraz fungujú správne na Waylande.

* Spectacle už predvolene nezahŕňa kurzor myši na snímkach obrazovky.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

* struct2osd odteraz používa castxml (namiesto zastaraného gccxml).

* Menej zastaraných použití kódu Qt, čím sa vyhnete výstrahám vo výpisoch.

* Vylepšené preklady.