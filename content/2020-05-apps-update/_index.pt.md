---
layout: page
publishDate: 2020-05-15 13:00:00
summary: "O que se Passou nas Aplica\xE7\xF5es do KDE Neste M\xEAs"
title: "Actualiza\xE7\xE3o de Maio de 2020 das Aplica\xE7\xF5es do KDE"
type: announcement
---
# Novos lançamentos

## Kid3

O [Kid3](https://kid3.kde.org/) é um programa de marcação de ficheiros de música que lhe permite editar as marcas ID3 e outros formatos semelhantes nos ficheiros MP3 e outros ficheiros de música.

Neste mês passou a ser alojado pelo KDE e teve o seu primeiro lançamento como aplicação do KDE. A nota de lançamento refere:

"Para além das correcções de erros, esta versão oferece melhorias de usabilidade, atalhos de teclado adicionais e programas de acções do utilizador. Muito obrigado a várias pessoas no KDE, as quais traduzir a interface do utilizador e o manual para novas línguas."

O Kid3 está disponível no Linux, Windows, Mac e Android.  Poderá tentar obtê-lo a partir da página Web ou através da sua distribuição ou das lojas de aplicações Chocolatey, Homebrew e F-droid.

{{< img class="text-center" src="kid3-android.png" caption="O Kid3 no Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="O Kid3 no Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="O Kid3 no Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="O Kid3 está no FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="O Kid3 está no Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="O Kid3 está no Chocolatey" style="width: 175px">}}

## Calligra

O [Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) é um pacote de escritório pleno de funcionalidades e em conformidade com as normas. Tem estado sem versões lançadas há alguns anos, mas agora voltou em grande estilo.

A versão 3.2 inclui o Gemini, o pacote de escritório desenhado especialmente para os dispositivos 2-em-1, isto é, para os portáteis com ecrãs tácteis que poderão corresponder ao dobro dos 'tablets', graças à migração para a nossa bela plataforma Kirigami. O programa de desenho Karbon agora suporta documentos que contêm várias páginas. Por outro lado o Stage, o editor de apresentações do Calligra, agora suporta a transição automática de 'slides',

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Desenho do Karbon" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Apresentações do Stage" style="width: 400px">}}

O Calligra está disponível na sua distribuição de Linux.

## O Tellico 3.3 Actualiza as Fontes de Dados

O [Tellico](https://tellico-project.org/tellico-3-3-released/) é um gestor de colecções para manter o registo de tudo o que você possa coleccionar, como livros, bibliografias, vídeos, músicas, jogos de vídeo, moedas, selos, cromos, livros de banda desenhada e vinhos.

Para tornar isto mais simples, tem uma grande quantidade de motores de dados que simplificam a apresentação do que possui. A nova versão 3.3 que saiu no início deste mês adicionou uma fonte de dados para o [Colnect](https://colnect.com/), um catálogo abrangente de objectos coleccionáveis. Também actualiza uma grande quantidade de fontes de dados como a Amazon e o MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico com o modelo de dados do Colnect" >}}

# Próximos Projectos

Os novos projectos no KDE deste mês incluem o 'pvfviewer', um visualizador de padrões do PC Stitch, para que possa fazer bonitas tapeçarias. Finalmente, apareceu o Alligator, um leitor de RSS que usa a nossa bela plataforma Kirigami.

# AppImage do KDE

Em cada mês gostamos de nos focar num dos novos formatos de pacotes em contentores aplicacionais. O AppImage não é novo, sendo que já existe há cerca de uma década, mas vale a pena recordá-lo, dado que um conjunto de aplicações do KDE o usam.

## O que é uma AppImage?

A AppImage é um formato de pacotes que oferece uma forma de os programadores oficiais oferecem binários "nativos" para os utilizadores de GNU/Linux da mesma forma que seria feito para os outros sistemas operativos. Permite criar pacotes de aplicações para muitos sistemas operativos comuns baseados no Linux, p.ex., Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora etc. As AppImages vêm com todas as dependências que se assumem como não fazendo parte de todos os sistemas-alvo numa versão recente, sendo executadas na maioria das distribuições de Linux sem grandes modificações.

## O que é necessário instalar para as ter a executar melhor?

Para executar uma AppImage, é necessário o 'kernel' de Linux (> 2.6) e a `libfuse`. Estas dependências estão presentes na maioria das distribuições de GNU/Linux, para que não tenha necessidade de instalar nada em especial.

Infelizmente o suporte de AppImage na maioria dos ambientes de trabalho (KDE e Gnome) não está completo, pelo que poderá precisar de uma ferramenta adicional para criar um item de menu da sua aplicação. Nesses casos, dependendo da UX que preferir, poderá optar entre:

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) para uma janela de integração inicial ou

* [appimaged](https://github.com/AppImage/appimaged) para uma integração automatizada de todas as AppImage's instaladas na sua pasta pessoal.

Para actualizar as suas AppImages poderá usar o [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Está incorporado no AppImageLauncher pelo que, se o tiver já instalado, não terá de instalar nada adicional. Basta carregar com o botão direito sobre o ficheiro AppImage e escolher 'Actualizar'. Lembre-se que nem todos os criadores de pacotes incorporam a informação de actualização nos binários, pelo que poderão ocorrer alguns casos em que tenha de transferir manualmente a nova versão.



## Quais as aplicações do KDE que se executam com ela?

Existem diversas aplicações do KDE que já são distribuídas como AppImage's, sendo as mais relevantes:

* kdenlive

* krita

* kdevelop

## O que é o Appimagehub?

A forma recomendada de obter AppImages é a partir dos autores originais das aplicações, mas isso não é muito prático se não souber ainda qual é a aplicação que necessita. Aí é onde surge o [AppImageHub](appimagehub.com). É uma loja de aplicações dedicada apenas a AppImages. Aí poderá encontrar um catálogo com mais de 600 aplicações para as suas tarefas do dia-a-dia.

Esta página Web faz parte da plataforma [OpenDesktop.org](opendesktop.org) que oferece um ecossistema completo para utilizadores e programadores de aplicações gratuitas e em código aberto.

## Como se pode criar uma AppImage?

A criação de uma AppImage passa pelo agrupamento de todas as suas dependências da aplicação numa única pasta (AppDir). Depois é compactado numa imagem SquashFS e adicionada a um 'ambiente de execução' que permite isso mesmo.

Para cumprir esta tarefa, poderá usar as seguintes ferramentas:

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

No [Guia de Pacotes AppImage](https://docs.appimage.org/packaging-guide/index.html) poderá encontrar uma documentação detalhada sobre como usar essas ferramentas. Também se poderá associar ao [canal de IRC do AppImage](irc://irc.freenode.net/#AppImage).



# Versões 20.04.1

Alguns dos nossos projectos são lançados de acordo com as suas próprias agendas e alguns são lançados em massa. A versão 20.04.1 dos projectos foi lançada hoje e deverá estar disponível através das lojas de aplicações e distribuições em breve. Consulte mais detalhes na [página da versão 20.04.1](https://www.kde.org/info/releases-20.04.1.php).

Algumas das correcções incluídas nesta versão são:

* kio-fish: Só guardar a senha na KWallet se o utilizador o tiver solicitado.

* O desenhador de UML [Umbrello](https://umbrello.kde.org/) tem agora correcções na adição de suporte para comentários de C++ em várias linhas.

* O comportamento do deslocamento no visualizador de documentos [Okular](https://kde.org/applications/en/graphics/org.kde.okular) foi melhorado e é mais útil com rodas de rato com roda livre

* Foi corrigida uma regressão que fazia com que algumas vezes o leitor de música [JuK](https://kde.org/applications/en/multimedia/org.kde.juk) estoirasse no arranque

* O [editor de vídeos Kdenlive](https://kde.org/applications/en/multimedia/org.kde.kdenlive) recebeu muitas actualizações de estabilidade, incluindo uma correcção na criação de capítulos de DVD e uma correcção que melhora o tratamento de códigos temporais, o tratamento de 'clips' em falta, o desenho de uma imagem "fotográfica" nos 'clips' de imagens para diferenciar dos 'clips' de vídeo, assim como antevisões na linha temporal

* O [KMail](https://kde.org/applications/en/office/org.kde.kmail2) agora lida correctamente com as pastas 'maildir' existentes quando adicionar um novo perfil de 'maildir' e não estoira mais ao adicionar demasiados destinatários

* A importação e exportação da configuração do [Kontact](https://kde.org/applications/en/office/org.kde.kontact) foi melhorada para incluir mais dados

[Notas da versão 20.04](https://community.kde.org/Releases/20.04_Release_Notes)
• [Página de transferências de pacotes na
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Página de informação do código do
20.04.1](https://kde.org/info/releases-20.04.1) &bull; [Registo de alterações
completo do 20.04.1](https://kde.org/announcements/changelog-
releases.php?version=20.04.1)