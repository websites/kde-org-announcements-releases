---
layout: page
publishDate: 2020-04-23 13:00:00
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in april 2020
type: announcement
---
Een nieuwe bundel van KDE toepassingen is hier! In deze uitgaven kunt u verwachten meer functies, stabiliteitsverbeteringen en meer gebruikersvriendelijke hulpmiddelen te vinden die u zullen helpen meer effectief te werken.

Er zijn tientallen wijzigingen om naar uit te kijken in de meeste van uw favoriete toepassingen. Neem bijvoorbeeld Dolphin, Windows Samba-shares zijn nu volledig te vinden.

Bij het onderwerp van afspelen van muziek voegt de muziekspeler Elisa functies toe door springen en bindingen. Deze uitgave brengt een nieuwe weergave "Nu speelt", gemakkelijk toegankelijk via het systeemvak en een optie om de afspeellijst te minimaliseren wanneer u dat wilt. Dankzij de recent toegevoegde visuele modus wisselen, het is veel gemakkelijker om uw muziek in de afspeellijsten te herordenen.

Dit zijn alleen de hoogtepunten van wat er nieuw is in toepassingen van KDE in deze maand. Lees verder om alles te weten te komen over wat we voor u hebben bereid.

## [Okular](https://okular.kde.org)

Okular is de toepassing van KDE die u het lezen van PDF's, Markdown-documenten, stripboeken en vele andere documentformaten biedt. In deze uitgave ontving Okular enige verbeteringen aan toegankelijkheid voor gebruikers zowel op het bureaublad als met aanraakscherm. Voor de eerste implementeert Okular nu rustig schuiven zowel wanneer u het muiswiel als het toetsenbord gebruikt. Voor aanraakschermgebruikers komt de laatste versie van Okular met traag schuiven.

{{<video src="okular.mp4" caption="Okular glad schuiven. Illustratie: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

Aan de krachtige bestandsbeheerder Dolphin zijn nieuwe functies toegevoegd, waarmee u beter werkt met bestandssystemen op afstand zoals Samba-shares en SSH-servers. U kunt nu beginnen met films bekijken die opgeslagen zijn op sites op afstand zonder ze te hoeven downloaden en in plaats daarvan ze direct te streamen uit de cloud naar uw speler via Dolphin.

Op een gelijke manier is verbinden met Windows Samba-shares en gebruik maken van alle services die wordt geboden is nog nooit zo gemakkelijk geweest, omdat Samba-shares nu zijn te ontdekken via het WS-Discovery protocol gebruikt door moderne versies van Windows. Dit biedt u een naadloze integratie van uw Linuxbox in een Windows netwerk.

Wat betreft het nieuws uit de afdeling bruikbaarheid: u hebt uw muis niet meer nodig om focus te verplaatsen naar en van het terminalpaneel. U kunt nu gewoon de sneltoets <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F4</kbd> gebruiken. Een andere lang verwachte functie is zoeken naar bestanden niet alleen op hun namen of inhoud, maar ook op hun tags.

Ondersteuning voor 7Zip-bestanden is nu ingebouwd in Dolphin, wat betekent dat u kunt door deze gecomprimeerde bestanden kunt navigeren alsof ze systeemmappen zijn. Wanneer u externe toepassingen voor zoeken installeert zoals KFind, zal Dolphin u snel toegang tot hen geven door een koppeling aan te maken.

Een ogenschijnlijk kleine functie, tenslotte, die een grote tijdsbesparing kan geven is de nieuwe functie "Dupliceer". Selecteer een of meer bestanden of mappen, druk op <kbd>Ctrl</kbd> + <kbd>D</kbd> (of klik rechts en kies "Hier dupliceren ") en - bam! Er een kopie van elk geselecteerd item zal verschijnen direct naast de originelen.

![Dolphin dupliceerfunctie](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

Lokalize is een programma om te gebruiken gettext en xliff bestanden te vertalen. Het ondersteunt nu de grammaticale correctie met Language-Tool, die vertalers helpt door grammaticale fouten in de tekst te accentueren.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

KMail is de populairste e-mailclient van KDE en onderdeel van Kontact, een suite van programma's die u helpt uw e-mails, contactpersonen en taken te beheren. Er zijn heel wat verbeteringen in deze nieuwe uitgave gestopt. Voor beginners, e-mailberichten kunnen nu gemakkelijk naar PDF geëxporteerd worden en berichten geformatteerd met Markdown worden beter getoond. Het aspect beveiliging heeft ook enige aandacht gekregen. Meer specifiek, KMail toont een waarschuwing wanneer de berichtenmaker opent na het klikken op een koppeling die u vraagt om een bestand te openen.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

Konsole is een terminalemulator die u gebruikt om opdrachtregelinstructies, processen en programma's uit te voeren. De laatste versie van Konsole laat u <kbd>Alt</kbd>-toets + een cijfertoets gebruiken om direct naar elk van de eerste 9 tabbladen te springen.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

Gwenview is de favoriete afbeeldingsviewer van KDE en in deze versie, de ontwikkelaars hebben twee belangrijke problemen opgelost. De toepassing hangt niet langer bij opstarten wanneer het systeemklembord tekst bevat uit KDE Connect en het importeren van foto's naar of van locaties op afstand is gerepareerd.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

Elisa is een eenvoudige en populaire muziekspeler en deze uitgave bevat de weergave "Speelt nu" die het attractiever maakt.

Het is echter niet alleen het uiterlijk. Elisa is ook functioneler, omdat u nu toegang hebt via het systeemvak en afspeellijst kunt minimaliseren wanneer u wilt. Dit betekent dat u kunt blijven luisteren naar uw favoriete muziek zelfs wanneer het hoofdvenster van Elisa gesloten is. De nieuwe visuele wisselmodus maakt het gemakkelijker om uw muziek in de afspeellijst opnieuw te ordenen en toont u welke song als volgende zal worden gespeeld.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

Kdenlive is de geavanceerde toepassing voor videobewerking en de nieuwe versie maakt bewerken sneller dankzij zijn te configureren voorbeeldresolutie. Het voorbeeld verbetert ook het bekijken van meerdere tracks, wat het gemakkelijk maakt tracks te selecteren uit diegenen die u bewerkt, alles tegelijk.

Beneden op de tijdlijn heeft het team vastklikken van clips verbeterd door alleen vast te klikken op actieve tracks. Ze hebben ook de functie groepsklik gerepareerd en een sneltoets toegevoegd om vastklikken door verplaatsen te vermijden. Iets anders wat u nu kunt doen is uw bestanden direct te laten vallen (video's, muziek en afbeeldingen) vanuit uw bestandsbeheerder op de tijdlijn. De nieuwe functie "inzoomen op sleutelframes" zal helpen op fijner werk uit te voeren. Professionele videobewerkers en gebruikers die komen uit andere bewerkingshulpmiddelen zullen ongetwijfeld de functie im-/exporteren met OpenTimelineIO waarderen.

Deze uitgave van Kdenlive komt ook met veel reparaties van bugs, zoals die voor de Motion Tracker, die gebroken was op sommige systemen. Het laat u nu een Pitch compensatiefilter toevoegen, evenals "Verplaatsen en grootte wijzigen" naar een Rotoscoping-vorm. De crash van de DVD-assistent en een bug die het wijzigen van het opnamevolume plaagde vanuit de mixer zijn ook opgelost. Het team van Kdenlive heeft ook functionaliteit voor tags, waardering en filtering aan de projectplaatje toegevoegd.

De nieuwe Windows versie van Kdenlive komt ook met reparaties aan de tijdlijndialogen, die eerder geen invoer vanaf het toetsenbord accepteerde. Bovndien kunt u nu de audio-backend op Windows selecteren.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

Yakuake is een terminalvenster die u omlaag kunt trekken vanuit de bovenkant van het Plasma bureaublad door op <kbd>F12</kbd> te drukken. Het is handig om snel instructies op de opdrachtregel uit te voeren en het weer opzij te schuiven.

Wanneer u nieuwe tabbladen opent of panelen splitst in deze versie van Yakuake, dan kunt u ze nu starten in dezelfde map als het huidige tabblad/gesplitste. U kunt het Yakuake-venster verticaal van grootte veranderen door aan de onderste balk te trekken.

## Systeeminstellingen

De integratiepagina voor online accounts in Systeeminstellingen laat u zaken configureren zoals uw Google account of toegang tot uw Nextcloud account instellen. De pagina is volledig gewijzigd en bevat nu een helder, modern uiterlijk met een meer betrouwbare functionaliteit.

## [KDE Connect](https://kdeconnect.kde.org/)

KDE Connect is een toepassing die u helpt uw telefoon te integreren met uw bureaublad en vice versa. Een van de nieuwste functies in deze uitgave is de mogelijkheid nieuwe conversaties met de SMS app te beginnen.

Andere wijzigingen omvatten afspelers voor media op afstand die nu in de mediaspelerapplet verschijnen, een nieuwe set statuspictogrammen voor verbindingen en verbeterde behandeling van oproepmeldingen.

In de afdeling reparaties van bugs is de fout die veroorzaakte dat bestandsdelen de aanroepende toepassing blokkeerde opgelost en de problemen met overbrengen van echt grote bestanden zijn opgelost.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

Spectacle, de software voor schermafdrukken van KDE, heeft toegevoegde functies en enige opgeloste problemen. Een ervan verhinderde kopiëren van een schermafdruk naar het klembord wann u Spectacle opende vanuit de opdrachtregel met de optie --nonotify. Hoewel dit misschien een zeldzaam probleem was, is het punt dat u dit nu wel kunt doen. Op dezelfde manier hadden oudere versies problemen wanneer u een schermafdruk vanuit Spectacle naar het standaard bestandsnaampatroon wilde slepen die submappen bevatten. Die problemen zijn opgelost.

Een andere nuttige verbetering in de nieuwe versie van Spectacle komt in de vorm van de knoppen “Standaarden” en “Terugdraaien”. U kunt ze gebruiken om terug te gaan naar een eerdere configuratie of zelfs alle eerdere aanpassingen aan de configuratie verwijderen en alles te herstellen naar hun standaard waarden.

Spectacle zal niet alleen uw standaard configuratie onthouden, maar ook het laatst gebruikte schermafdrukgebied. Dit helpt u om consistent te blijven, waarmee het gemakkelijker is om afdrukken met dezelfde vorm en grootte te maken als uw eerdere.

## [Krita](https://krita.org/en/) 4.2.9

Er zijn een aantal technische opwaarderingen achter de schermen in deze uitgave maar ook gladdere omrandingen van de het penseel die niet flikkeren wanneer u zweeft boven het werkveld. “Airbrush” en “Airbrush Rate” is toegevoegd aan het penseel Kleursmeren en een nieuwe instelling Verhouding, ook voor het penseel Kleursmeren, die het maken van de vorm van het penseel vlakker met gebruik van de verschillende sensoren. Een nieuwe Laag splitsen in de functie Selectiemasker biedt u het maken van een nieuwe laag voor elke kleur in de actieve laag.

{{< youtube fyc8-qgxAww >}}

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

De 3.0.4 uitgave van Smb4K repareert verschillende problemen en crashes, speciaal in het aankoppelen. Het maakt ook Smb4K opnieuw te compileren met Qt versie 5.9 en lager.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Deze hoofduitgave van de plug-in die Google Drive integreert in toepassingen van KDE voegt ondersteuning toe voor de functie Gedeelde stations van Google Drive.

Het omvat ook een nieuwe actie "Google URL naar klembord kopiëren" in het context-menu van Dolphin.

[20.04 uitgavenotities](https://community.kde.org/Releases/20.04_Release_Notes)
• [Wiki-pagina voor downloaden van
pakketten](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) •
[20.04 informatiepagina van broncode](https://kde.org/info/releases-20.04.0) •
[20.04 volledige log met wijzigingen](https://kde.org/announcements/changelog-
releases.php?version=20.04.0)