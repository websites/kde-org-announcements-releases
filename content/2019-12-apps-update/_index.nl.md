---
layout: page
publishDate: 2019-12-12 13:01:00
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in december 2019
type: announcement
---
# Nieuwe versies van KDE toepassingen die landen in december

De uitgave van nieuwe versies van KDE toepassingen is onderdeel van de voortdurende inspanning van KDE om u een volledige en bij-de-tijdse catalogus van vol-mogelijkheden, mooie en nuttige programma's voor uw systeem te brengen.

Er zijn nu nieuwe versies van de bestandsbeheerder Dolphin; Kdenlive, een van de meest complete opensource videobewerkers; de documentviewer Okular; de afbeeldingenviewer, Gwenview; en alle  andere favoriete KDE toepassingen en hulpmiddelen beschikbaar. Al deze toepassingen zijn verbeterd, waarmee ze sneller en stabieler zijn gemaakt en ze bogen op opwindende nieuwe mogelijkheden. De nieuwe versies van KDE toepassingen laten u productiever en creatiever zijn, terwijl ze tegelijkertijd gebruik van KDE software gemakkelijker en plezieriger maken.

We hopen dat u plezier hebt van alle nieuwere mogelijkheden en verbeteringen ingebracht in alle toepassingen van KDE!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) Opnieuw geladen

[Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) krijgt een grote uitgave in december. Het hulpmiddel voor projectplanning en beheer van KDE heeft een geweldige mijlpaal bereikt sinds de vorige versie is vrijgegeven bijna twee jaar geleden.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Plan helpt u bij het beheren van kleine en grote projecten met meerdere hulpbronnen. Om uw project te modelleren biedt Plan verschillende typen taakafhankelijkheden en tijdbeperkingen. U kunt uw taken definiëren, de inspanning schatten nodig om elk uit te voeren, hulpbronnen alloceren en daarna het project plannen naar uw behoeften en de beschikbare hulpbronnen.

Een van de sterke punten van Plan is zijn excellente ondersteuning voor Gantt-grafieken. Gantt-grafieken zijn grafieken die bestaan uit horizontale balken die een grafische illustratie zijn van een planning en helpen bij het plannen, coördineren en volgen van specifieke taken in een project. Met gebruik van Gantt-grafieken in Plan wordt u beter geholpen bij het monitoren van de workflow van uw project.

## Pump up the Volume on [Kdenlive](https://kdenlive.org)

Ontwikkelaars van [Kdenlive](https://kdenlive.org) hebben nieuwe functies toegevoegd en bugs gerepareerd met rugbrekende snelheid. Deze versie alleen al komt met meer dan 200 commits.

Heel veel werk is gegaan in verbetering van ondersteuning voor audio. In d afdeling "bug opgelost" zijn ze een enorm probleem met geheugengebruik opgelost, inefficiëntie met audio-miniatuurvoorbeelden en versnelling van opslag aangepakt.

Maar nog opwindender is dat Kdenlive nu komt met een spectaculaire nieuwe soundmixer (zie afbeelding). Ontwikkelaars hebben ook een nieuwe audioclipweergave in de clipmonitor en de projectbin toegevoegd zodat u beter uw filmbeelden kunt synchroniseren met de soundtrack.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Audiomixer van kdenlive" >}}

In de afdeling "resolving paper-cuts" heeft Kdenlive vele verbeteringen op het gebied van prestatie en bruikbaarheid ontvangen en ontwikkelaars hebben vele bugs in de Windowsversie van de video-editor gerepareerd.

## [Dolphin](https://userbase.kde.org/Dolphin)'s Voorbeelden en navigatie

De krachtige bestandssysteembrowser [Dolphin](https://userbase.kde.org/Dolphin) heeft nieuwe functies toegevoegd om u te helpen bij het zoeken en bereiken van waar u naar op zoek was in uw bestandssysteem. Een zo'n functie zijn de opnieuw ontworpen geavanceerde zoekopties en een andere is dat u nu terug en vooruit kunt gaan in de geschiedenis van plaatsen die u al meerdere keren hebt bezocht door lang het pijltjespictogram in de werkbalk in te drukken. De functie die u snel recent opgeslagen of recent bezochte bestanden laat bereiken is herzien en zijn onvolkomenheden zijn gladgestreken.

Een van de hoofdbrekens van de ontwikkelaars van Dolphin is gebruikers toestaan de inhoud van bestanden te zien alvorens ze te openen. In de nieuwe versie van Dolphin kunt u nu GIF's bekijken door ze te accentueren en dan te zweven boven het voorbeeldpaneel. U kunt ook op video's en geluidsbestanden klikken en deze afspelen in het voorbeeldpaneel.

Als u een fan bent van grafische verhalen, dan kan Dolphin nu miniaturen voor .cb7 stripboeken tonen (controleer de ondersteuning voor dit stripboekformaat ook in Okular -- zie onderstaand). En, pratend over miniaturen, inzoomen op miniaturen is al heel lang een punt geweest in Dolphin: houd <kbd>Ctrl</kbd> ingedrukt, draai het wieltje en de miniatuur zal groeien of krimpen. Een nieuwe functie is dat u nu de zoom kan resetten naar het standaard niveau door <kbd>Ctrl</kbd> + <kbd>0</kbd> in te drukken.

Een andere nuttige functie is dat Dolphin u laat zien welke programma's voorkomen dat een apparaat niet kan worden afgekoppeld.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): Laat uw telefoon uw bureaublad besturen

De laatste versie van [KDE Connect](https://community.kde.org/KDEConnect) komt volgestopt met nieuwe mogelijkheden. Een van de meest opmerkelijke is dat er een nieuwe SMS-app is die u SMSjes laat lezen en schrijven met de volledige conversatiegeschiedenis.

Een nieuwe op [Kirigami](https://kde.org/products/kirigami/) gebaseerde gebruikersinterface betekent dat u KDE Connect niet alleen op Android kunt uitvoeren, maar ook op alle soortgelijke mobiele Linux platforms die we zullen zien in opkomende apparaten zoals de PinePhone en de Librem 5. Het Kirigami-interface biedt ook nieuwe functies voor bureaublad-naar-bureaublad gebruikers, zoals mediabesturing, invoer op afstand, laten rinkelen van apparatuur, bestandsoverdracht en uitvoeren van commando's.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="het nieuwe interface op het bureaublad" >}}

U kon KDE Connect al gebruiken om het volume van media afspelen te besturen op uw bureaublad, maar nu kunt u het ook gebruiken om het globale volume van uw telefoon te besturen -- super handig wanneer u uw telefoon als een afstandsbediening gebruikt. Bij het geven van een lezing kunt u ook uw presentatie besturen met KDE Connect om voorwaarts en achterwaarts door uw dia's te gaan.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Een andere functie die al een poosje beschikbaar is is hoe u KDE Connect kunt gebruiken om een bestand naar uw telefoon te sturen, maar wat nieuw is in deze versie is dat u uw telefoon onmiddellijk het bestand kan laten openen nadat het is ontvangen. KDE Itinerary gebruikt dat om reisinformatie naar uw telefoon te sturen vanuit KMail.

In een gerelateerde notitie kunt u nu bestanden uit Thunar (bestandsbeheerder van Xfce) versturen en elementaire toepassingen zoals Pantheon Files. Het tonen van de voortgang van het kopiëren van meerdere bestanden is heel wat verbeterd en bestanden ontvangen uit Android tonen nu hun juiste tijd van wijziging.

Over meldingen gesproken, u kunt nu acties starten uit Android meldingen vanaf het bureaublad en KDE Connect gebruikt geavanceerde functies uit het meldingencentrum van Plasma om betere meldingen te leveren. Het paren van confirmatiemeldingen verloopt niet meer in de tijd en dus u het niet meer missen.

Tenslotte, als u KDE Connect gaat gebruiken vanaf de opdrachtregel, *kdeconnect-cli* heeft nu verbeterde *zsh* automatisch aanvullen.

## Afbeeldingen op afstand [Gwenview](https://userbase.kde.org/Gwenview)

[Gwenview](https://userbase.kde.org/Gwenview) laat u nu het compressieniveau van JPEG aanpassen wanneer u afbeeldingen opslaat die u bewerkt hebt. Ontwikkelaars hebben ook de prestatie voor afbeeldingen op afstand verbeterd en Gwenview kan nu import foto's importeren zowel naar als van locaties op afstand.

## [Okular](https://kde.org/applications/office/org.kde.okular) ondersteuning van formaat .cb7

[Okular](https://kde.org/applications/office/org.kde.okular), de documentviewer die u allerlei soorten documenten laat lezen, annoteren en verifiëren, inclusief PDF's, EPub's, ODT's en MD's, ondersteunt no ook bestanden die het .cb7 stripboekformaat gebruiken.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): Eenvoudige maar complete muziekspeler

[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) is een muziekspeler van KDE die eenvoud combineert met een elegant en modern interface. In zijn laatste versie heeft Elisa zijn uiterlijk geoptimaliseerd om zich beter aan te passen aan hoge DPI schermen. Het integreert beter met andere KDE toepassingen en heeft ondersteuning gekregen voor het globale menusysteem van KDE.

Indexering van muziekbestanden is ook verbeterd en Elisa ondersteunt nu webradio's en wordt geleverd met een paar voorbeelden voor u om te proberen.

{{< img src="elisa.png" caption="Bijgewerkt interface van elisa" >}}

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) is gereed voor het aanraakscherm

[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), het standaard schermafdruksysteem van KDE, is een andere KDE toepassing die geëvolueerd is om mogelijkheden voor aanraakschermen te verkrijgen: Zijn nieuwe aanraakvriendelijke sleepbehandelaar maakt het veel gemakkelijker om rechthoeken te verslepen en delen van het bureaublad te vangen die u wilt op aanraakgevoelige apparaten.

Andere verbindingen is de nieuwe functie voor automatisch opslaan, die handig is voor het snel maken van meerdere schermafdrukken en de geanimeerde voortgangsbalken, die verzekeren dat u helder kan zien wat Spectacle aan het doen is.

{{<img style="max-width:500px" src="spectacle.png" caption="Nieuw sleephandel van Spectacle" >}}

## [Plasma Browser Integration](https://community.kde.org/Plasma/Browser_Integration) Verbeteringen

De nieuwe uitgave van [Plasma Browser Integration](https://community.kde.org/Plasma/Browser_Integration) bevat nu functies voor een zwarte lijst voor de functionaliteit mediabesturing. Dit is nuttig voor wanneer u een site bezoekt met heel wat media om af te spelen, wat het moeilijk kan maken om te selecteren waar u controle over wilt hebben vanaf uw bureaublad. De zwarte lijst laat u deze sites uitsluiten van de mediabesturing.

De nieuwe versie laat u ook de originele URL behouden in de bestandsmetagegevens en voegt ondersteuning toe voor de "Web Share API". De "Web Share API" laat browsers koppelingen tekst en bestanden delen met andere toepassingen op dezelfde manier als KDE-toepassingen doen, waarmee de integratie met niet-inheemse browsers zoals Firefox, Chrome/Chromium en Vivaldi, met de overige van KDE's toepassingen, wordt verbeterd.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

Heel wat KDE toepassingen kunnen gedownload worden uit de Microsoft Store. Krita en Okular zijn daar al een poosje en krijgen nu gezelschap van [Kile](https://kde.org/applications/office/org.kde.kile), een gebruikersvriendelijke LaTeX editor.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Nieuw

Wat ons brengt bij nieuwe aanwinsten: [SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), een toepassing die u gemakkelijk ondertitels laat maken voor video's, is nu in de KDE Incubator, op weg om een volledig lid te worden van de toepassingenfamilie van KDE. Welkom!

Intussen is [plasma-nano](https://invent.kde.org/kde/plasma-nano), een minimale versie van Plasma ontworpen voor ingebedde apparaten, verplaatst naar de Plasma-opslagruimten, gereed voor uitgave met 5.18.

## Uitgave 19.12

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en
sommigen in massa vrijgegeven. De bundel 19.12 van projecten is vandaag
vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor
toepassingen en distributies. Zie de [19.12
uitgavepagina](https://www.kde.org/announcements/releases/19.12). Deze bundel
was eerder genaamd KDE Applications maar heeft geen naam meer om een
uitgaveservice te worden om verwarring te voorkomen met alle andere toepassingen
door KDE en omdat het tientallen verschillende producten zijn in plaats van een
enkel geheel.