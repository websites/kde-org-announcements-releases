---
layout: page
publishDate: 2020-03-05 15:50:00
summary: What Happened in KDE's Applications This Month
title: KDE's March 2020 Apps Update
type: announcement
---
## Uued väljalasked

### Choqok 1.7

Veebruar algas meie mikroblogimise rakenduse [Choqok] (https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html) ammuoodatud uuendusega.

Tähtsamate paranduste hulka kuuluvad 230 märgi piiranguta Twitteris, kontode keelamise lubamine ja laiendatud säutsude toetus.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore ja KDE partitsioonihaldur 4.1.0

Our disk formatting program Partition Manager got a new release with lots of work done to the library KPMCore which is also used by the Calamares distro installer. The app itself added support for Minix file systems.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partition Manager" >}}

### KPhotoAlbum 5.6

Kui sul on kõvakettal sadu või isegi tuhandeid pilte, on peaaegu võimatu mäletada nende kõikidega seotud lugusid või pildile jäänud inimeste nimesid. KPhotoAlbum ongi loodud selleks. et saaksid oma pilte kirjeldada ning hiljem ka vajalikke pilte väga suurest pildikogust väga kiiresti ja hõlpsalt üles leida.

Selles väljalaskes on erilist rõhku pandud jõudluse tuntavale suurendamisele väga suure arvu piltide sildistamisel, samuti on parandatud jõudlust pisipildivaates. Suur tänu Robert Krawitzile vapra pea ees sukeldumise eest koodisügavustesse ja seal kohtade leidmine, mida optimeerida ja liiasust kõrvaldada!

Lisaks toetab see väljalase KDE Purpose plugina raamistikku.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Peale tulemas

MP3-siltide redaktor liigutati kdereview alla, mis kujutab endast esimest sammu uue väljalaske suunas.

And the Rocket.chat app Ruqola passed kdereview and was moved to be ready to release. However the word from the maintainer Laurent Montel is that a rewrite with Qt Widgets is planned so there's still a lot of work to be done.

## Rakendusepood

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Uus rakendus Windowsile Microsofti poes on [Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

A modern and beautiful music player made with love by KDE. Browse, and play your music very easily! Elisa should be able to read almost any music files.Elisa is made to be accessible to everybody.

Samal ajal Digikam ootab veel pääsu Microsofti poodi.

## Veebilehekülje uuendused

Meie vahva telefoni ja töölaua lõimimise tööriist [KDE Connect](https://kdeconnect.kde.org/) sai uue veebilehekülje.

[Kid3](https://kid3.kde.org/) sai uue särava veebilehe, kus leiab kõik uudised ja lingid allalaadimiseks kõigist kohtadest, kust rakendust saada võib.

## Väljalasked 19.12.3

Mõned meie rakendused lasevad uusi versioone välja oma ajakava järgi, mõned ilmuvad paljukesi korraga. Täna nägi ilmavalgust projektikimp 19.12.3, mis varsti peaks jõudma ka rakenduste hoidlatesse ja distributsioonidesse. Vt  [19.12.3 väljalaskelehekülge](https://www.kde.org/info/releases-19.12.3.php). Varem nimetati seda kimpu üldnimetusega KDE rakendused, aga see muudeti väljalasketeenuseks, et vältida segaduse tekkimist kõigi teiste KDE rakendustega, aga ka seepärast, et see pole tegelikult üks tervik, vaid koosneb kümnetest eri rakendustest.

Mõned sellekuise väljalaske parandused:

* SMB ressursside failiatribuutide toetust on täiustatud [vt mitmeid sissekandeid](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)

* K3b tunnistab nüüd WAV-failid helifailideks [VEATEADE 399056](https://bugs.kde.org/show_bug.cgi?id=399056) [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)

* Gwenview võib nüüd laadida võrgupilte protokolliga https [Sissekanne](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [19.12
väljalaskemärkmed](https://community.kde.org/Releases/19.12_Release_Notes)
teabega pakendatud tarkvara ja teadaolevate probleemide kohta. 💻 [Tarkvara
allalaadimise wiki-
lehekülg](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻
[19.12.3 lähtekoodi teabelehekülg](https://kde.org/info/releases-19.12.3) 💻
[19.12 täielik muudatuste logi](https://kde.org/announcements/changelog-
releases.php?version=19.12.3) 💻