---
layout: page
publishDate: 2019-11-29 00:01:00
summary: Over 120 individual programs plus dozens of programmer libraries and feature
  plugins are released simultaneously as part of KDE's release service.
title: 19.12 RC Releases
type: announcement
---
29 ноября 2019 года. Более 120 отдельных программ и десятки программных библиотек и модулей одновременно выпущены силами Службы выпусков KDE.

Сегодня для всех них опубликован предварительный выпуск исходного кода, что означает финальный набор возможностей, но в то же время необходимость дополнительного тестирования.

Сопровождающим пакетов в дистрибутивах и каталогах приложений следует обновить каналы ранних сборок, чтобы обнаружить потенциальные проблемы.

* [Примечания к выпуску 19.12](https://community.kde.org/Releases/19.12_Release_Notes) содержат информацию о пакетах и об известных проблемах.

* [Вики-страница об установке пакетов](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

* [Архивы с исходным кодом 19.12 RC](https://kde.org/info/applications-19.11.90)

## Контакты для прессы

Для получения дополнительной информации свяжитесь с нами по электронной почте:
[press@kde.org](mailto:press@kde.org).