---
draft: false
layout: page
publishDate: '2020-09-10T12:00:00+00:00'
summary: "O que se Passou nas Aplica\xE7\xF5es do KDE Neste M\xEAs"
title: "Actualiza\xE7\xE3o de Outubro 2020 das Aplica\xE7\xF5es do KDE"
type: announcement
---
Depois do Akademy, chegou um mês pleno de novas actualizações de aplicações e correcções de erros nas aplicações do KDE.

# Novos lançamentos

## digiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="digiKam" style="width: 600px">}}

O mês começou com o lançamento da nossa famosa [aplicação de gestão de fotografias, o digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

Melhor suporte para os meta-dados da Canon CR3, os formatos de imagens RAW das máquinas fotográficas estão normalmente mal documentados e são difíceis de processar. Os programadores do digiKam 7.1.0 criaram uma interface de meta-dados baseada na 'libraw' para o CR3, sendo que a aplicação agora é capaz de ler uma boa parte das marcas EXIF, incluindo a informação de GPS, o perfil de cores e, obviamente, os contentores-padrão de IPTC e XMP.

Foi introduzido um novo 'plugin' de Gestor de Fila em Lote que lhe permite aplicar texturas sobre as imagens e outro que corrige automaticamente os pixels queimados, isto é, os “pixels errados” que obtém quando usa velocidades de obturação baixas ou trabalha com imagens à noite.

O [Modelo de Intercâmbio de Informações de IPTC](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) é o formato antigo de meta-dados, mas ainda comum, que é usado para fotografias, sendo que esta versão melhora o suporte de Unicode para o mesmo.

O digiKam está disponível na sua distribuição de Linux como um pacote com o código-fonte, imagens AppImage para Linux a 32/64 bits, um pacote para o macOS e instaladores para o Windows a 32/64 bits. [Obtenha o digiKam 7.1.0 aqui](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

O LabPlot é uma aplicação do KDE para gráficos interactivos e análise de dados científicos. O LabPlot oferece uma forma simples de criar, gerir e editar gráficos e de efectuar algumas análises de dados. O [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) foi lançado ao fim de um ano pleno de trabalho.

Na 2.8, agora é mais simples aceder a muitos recursos 'online' que fornecem conjuntos de dados para fins educativos. Existem agora cinco colecções com cerca de 2 000 conjuntos de dados documentados. Veja este vídeo para obter mais informações:

{{< youtube pyP-5J3seQ8 >}}

Esta versão vem com dois novos objectos da folha de trabalho: linhas de referência e elementos de imagens. As linhas de referência podem ser colocadas à vontade no gráfico para realçar certos valores nos dados visualizados. Os elementos de imagem permitem-lhe adicionar uma imagem ao gráfico ou à folha de trabalho.

A folha de cálculo fornece estatísticas descritivas sobre os conjuntos de dados. Alargámos esta informação e adicionámos o cálculo dos quartis, da média interna e do modo estatístico.

Graças ao trabalho efectuado sobre o Cantor nos últimos meses, os programadores conseguiram adicionar a compatibilidade com os ficheiros de projectos do Jupyter nesta versão do LabPlot.

Foi aplicado mais esforço na versão para macOS do LabPlot. Para além de diversas correcções pequenas e específicas do macOS, foi também adicionado o suporte para a barra táctil que está disponível nos modelos mais recentes do MacBook Pro.

[Pode obter o Labplot](https://labplot.kde.org/download/) para o Linux, Windows e Mac.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

O nosso IDE de desenvolvimento de aplicações, o [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released), ao fim de cerca de meio ano de trabalho, focou-se principalmente na estabilidade, na performance e na capacidade de manutenção no futuro. Adiciona a funcionalidade útil para [mostrar opcionalmente as notas incorporadas dos problemas no fim da linha](https://blog.david-redondo.de/kde/2020/02/28/problems.html).

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

Já saiu a aplicação móvel vocacionada para calendários [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html)! Ainda que algumas versões tenham também sido assinaladas, esta é a primeira versão estável do Calindori como uma aplicação do KDE.

O Calindori está disponível no KDE Neon para o Plasma Móvel, assim como no postmartketOS. Também está disponível nas versões diárias do repositório de Flatpak 'kdeapps' para o ARM e x84_64. Finalmente, podê-lo-á também compilar a partir do código-fonte no seu computador pessoal com Linux. Poderá obter o código-fonte e as assinaturas em [download.kde.org](https://download.kde.org/stable/calindori/).

## Kid3 3.8.4

O [Kid3](https://invent.kde.org/websites/kid3-kde-org) melhorou a nossa aplicação de marcação de ficheiros de música.

Para além das correcções de erros, esta versão oferece melhorias de usabilidade, como mais atalhos de navegação e a possibilidade de os configurar. A detecção do tipo de ficheiro é mais tolerante.

Poderá obter o [Pacote do Chocolatey para Windows](https://chocolatey.org/packages/kid3/), o [Pacote do Homebrew para macOS](https://formulae.brew.sh/cask/kid3), o [Pacote do F-Droid para Android](https://f-droid.org/en/packages/net.sourceforge.kid3/), o [Flatpak](https://flathub.org/apps/details/org.kde.kid3), o [executável para macOS](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download), o [executável para Windows](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download)

# Correcções de erros

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

Para os programadores por aí, foi lançada uma [versão com correcções de erros do Heaptrack](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0). O Heaptrack regista todas as alocações de memória e anota esses eventos com registos de chamadas. Algumas ferramentas dedicadas de análise então permitir-lhe-ão interpretar o perfil da memória de dados

Existem melhorias na estabilidade do Heaptrack que o tornam mais resistente a erros na gravação dos dados. Para além disso, foram feitos alguns pequenos ajustes na ferramenta de análise gráfica que o tornam mais eficiente no seu uso. A performance do passo de análise foi também ligeiramente melhorado. Finalmente, foi corrigido um erro importante no algoritmo de diferenças de dados, o que deverá tornar essa funcionalidade mais útil de novo.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

O KDiff3 permite-lhe comparar até três ficheiros de texto de uma vez.

[Esta actualização](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html) corrige erros ortográficos, o recarregamento de ficheiros no Windows e a cópia na rede.

Poderá [obter o KDiff3](https://download.kde.org/stable/kdiff3/) para o Windows, Mac e Linux.

## Tellico 3.3.3

O gestor de colecções [Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) melhorou a fonte de dados Entrez (Pubmed) para usar uma chave de API quando estiver disponível e para respeitar a taxa de chamadas. Os programadores também melhoraram a fonte de dados ISDBdb para procurar por vários valores de ISBN.

## Konversation 1.7.6

A aplicação de conversas em IRC [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) recebeu uma correcção de erro onde é possível compilar com mais versões do Qt e corrige o carregamento do tema de ícones da alcunha.

## 'Plugins' de Visualização de Markdown

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="XPTO" style="width: 600px">}}

Foram actualizados alguns 'plugins' neste mês. O [KPart de Visualização de Markdown](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) e a [KMarkdownWebView](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) receberam ambos novas versões que actualizam as suas traduções e que corrigem problemas na compilação com versões mais antigas do Qt. Estes 'plugins' fornecem ambos antevisões dos ficheiros formatados em Markdown no Kate e nas outras aplicações do KDE.

# Recebido

O KTechLab, um IDE para microcontroladores e electrónica, é uma aplicação mais antiga que não foi lançada durante muito tempo pelo facto de estar a usar tecnologia antiga. O código de base do KTechLab foi actualizado e a [versão 0.50.0](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) está agora disponível para testes.

# Versão 20.08.2

Alguns dos nossos projectos são lançados de acordo com as suas próprias agendas e alguns são lançados em massa. A versão 20.08.2 dos projectos foi lançada hoje e deverá estar disponível através das lojas de aplicações e distribuições em breve. Consulte mais detalhes na [página da versão 20.08.2](https://www.kde.org/info/releases-20.08.2.php).

Algumas das correcções incluídas nesta versão de hoje são:

* Foi corrigido um erro que fazia com que o editor de texto Kate escondesse elementos da interface após fechar uma página

* A transferência de documentos do Office através da integração do GDrive com o Dolphin já não tem problemas em lidar com o redireccionamento obrigatório do servidor

* Foi corrigido o deslocamento na diagonal (p.ex. com ratos tácteis) na área de terminal do Konsole

* O visualizador de documentos Okular agora permite carregar em ligações que estejam cobertas por anotações

* No Kontact, a configuração do arquivo automático do correio ou o encaminhamento de uma mensagem incorporada já não provoca nenhum estoiro

* O corte automático de partes da cena no editor de vídeo Kdenlive agora corta de facto o vídeo, assim como a abertura de projectos com 'clips' de vídeo em falta agora é mais robusta

* A importação de imagens no Gwenview já não bloqueia após o fecho

* A ferramenta de partilha do ecrã remoto Krfb agora lida correctamente com ecrãs com escala

* As ligações telefónicas com o KDE Connect tornaram-se mais robustas após uma revisão de segurança

[Notas da versão 20.08](https://community.kde.org/Releases/20.08_Release_Notes)
• [Página de transferências de pacotes na
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Página de informação do código do
20.08.2](https://kde.org/info/releases-20.08.2) &bull; [Registo de alterações
completo do 20.08.2](https://kde.org/announcements/changelog-
releases.php?version=20.08.2)