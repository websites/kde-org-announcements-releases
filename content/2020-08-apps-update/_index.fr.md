---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: Quoi de neuf ce mois-ci avec les applications de KDE.
title: "Mise \xE0 jour des applications de KDE de Ao\xFBt 2020"
type: announcement
---
Des douzaines [d'applications de KDE](https://kde.org/applications/) ont reçues de nouvelles mises à jour à partie du service de mise à jour de KDE. De nouvelles fonctionnalités, des améliorations d'ergonomie, de nouvelles conceptions et des corrections de bogues contribuent tous à augmenter votre productivité et rendre l'utilisation plus efficace et plaisante de ce nouvel ensemble d'applications.

Voici ce que vous pouvez regarder en plus :

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin affiche les vignettes de nombreux types de fichiers." style="width: 800px" >}}

Dolphin est un explorateur de fichiers pour KDE, vous aidant à la recherche, la copie et l'ouverture de fichiers et de dossiers. Comme tel, une de ces principales fonctions est de vous donner une idée claire du contenu de chaque fichier. L'affichage des vignettes est essentiel pour cela. Dolphin a été capable d'afficher vos vignettes d'images, de clips vidéo et même de fichiers « 3D » maintenant de puis longtemps.

Dans cette nouvelle version, Dolphin ajoute à la liste des vignettes pour les fichiers « 3MF » (3D Manufacturing Format). Vous pouvez aussi maintenant les vignettes de fichiers et de dossiers pour des systèmes de fichiers chiffrés comme les portefeuilles de Plasma. Ceci est fait de façon sécurisée par l'enregistrement des vignettes en cache dans le système de fichiers lui-même ou alternativement en les produisant mais sans enregistrer les versions de cache n'importe si nécessaire. En conclusion, vous avez toujours un contrôle absolu sur comment et quand Dolphin affiche votre contenu de fichier, puisque vous pouvez configurer indépendamment la limite de la taille du fichier pour l'affichage de vignettes, pour des fichiers locaux ou distants.

Une autre façon d'identifier les fichiers est par leurs noms. Dans le cas où le nom du fichier est trop long pour l'affichage, les développeurs ont affiné le comportement pour raccourcir le nom de fichier. Plutôt que de couper dans le milieu d'un long fichier et du nom de dossier comme le faisait Dolphin dans les versions précédentes, la nouvelle version coupe à la fin et conserve toujours visible l'extension du fichier (si présente) après les points de suspension. Cela rend l'identification des fichiers avec de longs noms plus facile.

Dolphin se souvient maintenant et restaure l'emplacement que vous explorez, tout autant que les onglets ouverts. Il découpe les affichages que vous aviez ouverts au moment de la dernière fermeture. Cette fonctionnalité est activée par défaut mais peut être désactivée dans la page « Démarrage » dans la fenêtre de Dolphin de paramétrage.

Concernant les améliorations d'ergonomie, Dolphin vous a toujours permis de monter et d'explorer les partages distants, qu'ils soient sous FTP, SSH ou sous d'autres protocoles. Mais, Dolphin affiche aussi les montages distants et « Fuse » avec des noms d'affichage compréhensibles, plutôt que les emplacements complets. Encore mieux, vous pouvez monter les images « ISO » par utilisation d'un nouveau élément de menu dans un menu contextuel. Ceci signifie que vous pouvez télécharger et explorer un système de fichiers que vous allez ensuite graver sur un disque ou enregistrer sur une clé USB.

{{< video src="dolphin_ISO.mp4" caption="Dolphin vous permet maintenant de monter des images « ISO »." style="max-width: 900px" >}}

Dolphin a une quantité incroyable de fonctionnalités. Mais, vous pouvez en ajouter plus grâce à des modules externes. Précédemment, si vous vouliez ajouter des modules externes de menu « Service » (regarder <I>Configuration</I> > <I>Configurer Dolphin</I> > <I>Services</I>), vous aviez souvent besoin de lancer un script ou d'installer un paquet de distribution. Maintenant, vous pouvez les installer directement à partir de la fenêtre « Obtenir un nouvel élément » sans aucune étape intermédiaire manuelle. Une autre modification de la page de paramétrage de « Services » est que le nouveau champ de recherche en haut pour vous aidez à trouver rapidement ce que vous recherchez et que la liste des services est maintenant triée de façon alphabétique.

Le déplacement d'objets est plus facile avec le nouveau Dolphin, puisque que vous disposez maintenant de nouvelles actions pour déplacer ou copier rapidement des fichiers d'un panneau de l'affichage scindé dans un dossier d'un autre panneau. Pour clarifier ce qui va se passer, lors d'un glisser un fichier, le curseur se change maintenant en une main par défaut, plutôt qu'un curseur « de copie », comme fait dans les versions antérieures.

Vous pouvez aussi calculer et afficher les tailles des dossiers du disque en utilisant l'affichage « Détails » et le panneau de Dolphin « Informations » affiche maintenant des informations utiles pour la corbeille. Ceci rend possible la navigation à partir du champ de recherche de Dolphin à partir de l'affichage des résultats en appuyant sur la touche de flèche vers le bas.

Enfin, une nouvelle fonctionnalité utile pour Dolphin 20.08 est l'ajout d'un nouvel élément de menu « Copier l'emplacement »...

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Konsole arrive avec quelques nouvelles fonctionnalités attrayantes." style="width: 800px" >}}

... Exactement comme dans l'application de terminal de KDE, Konsole ! Cela signifie que vous pouvez faire un clic droit sur un élément affiché dans Konsole, sélectionnez « Copier l'emplacement » à partir du menu contextuel et ensuite faire un copier / coller l'emplacement complet d'un fichier ou d'un dossier dans un document, un autre terminal ou une application.

Konsole arrive aussi avec une nouvelle fonctionnalité, affichant un surbrillance légère pour les nouvelles lignes arrivant dans l'affichage quand la sortie du terminal est en défilement rapide et vous affiche un aperçu de vignette pour les fichiers d'image, que vous pouvez survoler avec le point de souris par défaut. Quoi de plus, quand vous faites un clic droit sur un fichier souligné dans Konsole, le menu contextuel s'affiche et un menu « Ouvrir avec ». Ainsi, vous pouvez ouvrir le fichier dans tout application que vous avez sélectionné.

La capacité de scinder l'affichage de Konsole et d'ajouter des onglets pour faire plusieurs choses en même temps est une fonctionnalité depuis longtemps. Mais, maintenant, les en-têtes scindées des affichage peuvent être désactivée et l'épaisseur du séparateur peut être augmentée. Vous pouvez aussi surveiller un onglet pour le processus actif pour terminer et assigner des couleurs aux onglets de Konsole !

{{< img class="text-center" src="konsole-monitor.webp" caption="Nouveau processus de surveillance de Konsole" style="width: 500px" >}}

En parlant d'onglet, le raccourci par défaut de Konsole, « Crtl » + « Maj » + « L » pour « Détacher l'onglet courant » a été supprimé. Il a été admis qu'il était trop facile de détacher par accident l'onglet courant quand ce que vous voulez vraiment faire est d'effacer l'affichage en utilisant « Crtl » + « Maj » + « K ».

La dernière amélioration d'ergonomie dans cette version est le pointeur de Konsole « I-beam », qui s'adapte maintenant à la taille de la police, plutôt que toujours rester à la même taille.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Nouvelle icône pour Yakuake" style="width: 200px" >}}

En parlant de terminaux, Yakuake est un émulateur de terminal en menus de KDE. Il se déroule du haut de votre écran à chaque fois que vous appuyez sur « F12 ». Il a reçu aussi un ensemble d'améliorations pour cette version. Par exemple, il vous laisse maintenant configurer tous les raccourcis de clavier venant de Konsole. Il y a un élément nouveau dans la boîte à miniatures, qui vous affiche que Yakuake est lancé. Vous pouvez bien sûr le masquer mais il vous renseigne plus facilement si Yakuake est actif et prêt ou non. Il vous fournit aussi un moyen graphique pour le lancer.

Une autre chose est l'amélioration de Yakuake dans Wayland. La fenêtre principale est utilisée pour apparaître sous les panneaux supérieurs que vous affichez. Mais, cela a été corrigé maintenant.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

DigiKam est l'application professionnelle pour la gestion de photos de KDE. Elle vient de recevoir de la part de l'équipe de développement, une nouvelle version majeure 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Reconnaissance faciale pour Digikam" style="width: 800px" >}}

Un des éléments principaux de digiKam 7.0.0 est l'amélioration de la reconnaissance faciale, utilisant maintenant les algorithmes d'intelligence artificielle « Deep Learning ». Vous pouvez consulter la présentation de Akademy de l'année dernière, pou voir comment cela a été fait. Il y aura plus de travaux à venir dans les versions ultérieures pour la reconnaissance faciale et la détection automatique.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Reconnaissance faciale pour Digikam" style="max-width: 800px" controls="true" >}}

Le [paquet « Flatpak»  principal de digiKam est disponible sous Flathub](https://flathub.org/apps/details/org.kde.digikam), ainsi qu'une compilation de nuit instable pour les testeurs. DigiKam est aussi disponible pour Linux grâce à des « AppImages » et des dépôts de distributions. Il y a aussi des versions de digiKam pour Windows et Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

Un ensemble de réglages a été fait pour l'apparence de votre éditeur de texte Kate. Le menu « Ouvrir les plus récents » affiche maintenant les documents ouverts dans Kate à partir de la ligne de commandes et d'autres sources, c'est-à-dire, pas seulement ceux ouverts avec la boîte de dialogue de fichiers. Un autre changement est que la barre d'onglets de Kate est maintenant cohérente visuellement avec toutes les barres d'onglets dans les autres applications de KDE. Celle-ci ouvre de nouveaux onglets sur le côté droit, de la même façon que les autres barres d'onglets.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa est un lecteur audio léger pour environnements de bureau et plate-forme mobile." style="width: 800px" >}}

Elisa est un lecteur de musique léger, fonctionnant sur votre bureau et sur votre téléphone portable. Elisa vous laisse afficher tous les genres, les artistes ou les albums dans la barre latérale, sous les autres éléments. La liste de lecture affiche aussi la progression dans la liste intégrée des morceaux en cours de lecture. La barre en haut est maintenant plus réactive pour la taille de la fenêtre et le facteur de forme pour le périphérique. Cela signifie que cela semble mieux avec une fenêtre ou un périphérique en format paysage (par exemple votre téléphone) et que cela peut être réduit pour être réellement petit.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="KStars est une application de KDE pour les utilisateurs avancés d'astronomie." style="width: 800px">}}

Enfin, l'application d'astronomie KStars a reçu certaines calibrations et certains focus dans la [nouvelle version](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [Le masque de « Bahtinov » est un périphérique utilisé pour le focus plus précis des petits télescopes astronomiques](https://en.wikipedia.org/wiki/Bahtinov_mask). Il est utile pour les utilisateurs qui ne possèdent pas de systèmes d'autofocus motorisés et qui préfèrent de faire un focus manuellement avec l'aide du masque. Après une capture d'image dans le module de focus avec l'algorithme de masque de « Bahtinov » activé, Ekos analysera les images et les étoiles grâce à lui. Si Ekos reconnaît le profil « Bahtinov » d'une étoile, il dessinera des lignes au dessus du profil de l'étoile en cercles au centre et avec un décalage pour préciser le focus.

{{< img class="text-center" src="kstars-focus.webp" caption="Nouvel algorithme de focus pour KStars" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="Calibration de KStard" style="width: 600px" >}}

Un onglet secondaire « Tracé de calibration » a été ajouté sur la droite de « Tracé de dérive ». Il affiche les positions du montage, enregistrés durant la calibration par guidage interne. Si les choses se passent bien, il devrait afficher des points sur deux lignes, à angle droit l'une de l'autre, une quand la calibration déplace le montage en arrière et en avant le long de la direction « RA » et alors quand il fait la même chose pour la direction « DEC ». Aucune information pléthorique mais elle peut être utile à consulter. Si les deux lignes font un angle de 30 degrés, quelque chose s'est mal passé avec votre calibration ! L'image ci-dessous montre comment il fonctionne en utilisant le simulateur.

[L'application KStars est disponible pour téléchargement]  (https://edu.kde.org/kstars/) pour Android, sur la boutique « Snap »,pour Windows, pour MacOS, et bien sûr, dans les dépôts des distributions Linux.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="KRDC avec un curseur à distance" style="max-width: 900px" >}}

KRDC vous permet l'affichage et le contrôle de la session de bureau sur une autre machine. La version a été publiée aujourd'hui et affiche maintenant des curseurs correcte coté serveur dans VNC au lieu d'un petit point avec le curseur traînant derrière.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

Okular est l'afficheur de documents de KDE. Il vous permet de lire des documents « PDF », des livres « ePUB » et encore plus de types de fichiers reposant sur du texte. Une correction a été faite pour proposer « Imprimer » et « Imprimer une vignette » une nouvelle fois proche l'un de l'autre dans le menu « Fichier ».

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

Gwenview est une application d'affichage d'images, fourni avec certaines fonctionnalités de modifications, comme le redimensionnement et le rognage. La nouvelle version enregistre la taille du dernier découpage, signifiant que vous pouvez rapidement rogner de nombreuses images avec la même taille à la file. 

# Corrections de bogues

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* L'enregistrement a été déplacé dans un processus pour ne pas bloquer l'interface durant son exécution.

* Implémentation d'une interface « D-Bus » pour les raccourcis clavier et le contrôle de la numérisation

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

* Les raccourcis pour la fonctionnalité de chronomètre de Spectacle, la copie d'écran par « Maj » + « ImpEcran » (prise d'une copie d'écran en plein écran) et la copie d'écran d'une section rectangulaire par « Meta » + « Maj » + « ImpEcran » fonctionnent maintenant sous Wayland.

* Spectacle ne prend plus en compte le pointeur de souris par défaut dans les copies d'écran.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

* « struct2osd » utilise maintenant « castxml » (« gccxml » est maintenant déconseillé).

* Moins d'utilisation de code Qt déconseillé, évitant la journalisation d'alarmes à l'exécution. 

* Traductions améliorées.