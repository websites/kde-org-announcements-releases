---
layout: page
publishDate: 2019-12-12 13:01:00
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i december 2019
type: announcement
---
# Nya versioner av KDE-program som kommer i december

Utgivning av nya versioner av KDE-program är en del av KDE:s fortsatta arbete för att ge dig en fullständig och uppdaterad katalog av vackra och användbara program med alla funktioner för ditt system.

Nu tillgängliga är nya versioner av KDE:s filbläddrare Dolphin; Kdenlive, en av de mest kompletta videoeditorer med öppen källkod; dokumentvisaren Okular; KDE:s bildvisare, Gwenview; och alla andra favoritprogram och verktyg i KDE. Alla dessa program har förbättrats, vilket gör dem snabbare och stabilare, och de ståtar med spännande nya funktioner. De nya versionerna av KDE-program gör att man kan vara produktiv och kreativ, medan det samtidigt är enkelt och roligt att använda KDE-programvara.

Vi hoppas att du tycker om alla nya funktioner och förbättringar som har arbetats in i alla KDE:s program!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) Omgjord

[Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) har en stor utgåva i december. KDE:s projektplanerings- och hanteringsverktyg har nått en stor milsten sedan den tidigare versionen gavs ut för nästan två år sedan.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Plan låter dig hantera små och stora projekt med flera resurser. För att du ska kunna modellera ditt projekt, erbjuder Plan olika typer av aktivitetsberoenden och tidsbegränsningar. Du kan definiera aktiviteter, uppskatta nödvändigt arbete för att utföra var och en, tilldela resurser och därefter schemalägga projektet enligt dina behov och tillgängliga resurser.

En av Plans styrkor är det utmärkta stödet för Gantt-diagram. Gantt-diagram är diagram bestående av horisontella staplar som tillhandahåller en grafisk illustration av tidplanen och hjälper till att planera, koordinera och följa specifika aktiviteter i ett projekt. Användning av Gantt-diagram i Plan hjälper dig att bättre övervaka projektets arbetsflöde.

## Öka volymen med [Kdenlive](https://kdenlive.org)

Utvecklare av [Kdenlive](https://kdenlive.org) har lagt till nya funktioner och eliminerat fel med halsbrytande fart. Enbart den här versionen levereras med fler än 200 ändringar.

En hel del arbete har gjorts för att förbättra ljudstöd. I avdelningen "lösta fel" har de gjort sig av med ett stort minnesanvändningsproblem, ineffektiva ljudminiatyrer, och har snabbat upp lagring.

Men ännu mer spännande är att Kdenlive nu levereras med en spektakulär ny ljudmixer (se bilden). Utvecklarna har också lagt till en ny visning av ljudklipp i klippövervakaren och projektkorgen så att du kan synkronisera rörliga bilder bättre med ljudspåret.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Kdenlives ljudmixer" >}}

Under avdelningen "lösa irritationsmoment", har Kdenlive fått många prestanda- och användbarhetsförbättringar, och utvecklarna har rättat många fel i Windows-versionen av videoeditorn.

## [Dolphin](https://userbase.kde.org/Dolphin) förhandsgranskning och navigering

KDE:s kraftfulla filbläddrare [Dolphin](https://userbase.kde.org/Dolphin) har lagt till nya funktioner för att hjälpa dig hitta och nå vad du letar efter i filsystemet. En sådan funktion är de omkonstruerade avancerade sökalternativen och en annan är att du nu kan gå bakåt och framåt i historiken för platser som du redan har besökt flera gånger genom att hålla nere pilikonen på verktygsraden länge. Funktionen som låter dig snabbt nå senast sparade eller senast använda filer har omarbetats och dess problem undanröjts.

En av huvudangelägenheterna för Dolphins utvecklare är att låta användare förhandsgranska filernas innehåll innan de öppnar dem. Exempelvis kan du förhandsgranska GIF-filer i den nya versionen av Dolphin genom att markera dem och därefter hålla musen ovanför förhandsgranskningspanelen. Du kan också klicka och spela videor och ljudfiler i förhandsgranskningspanelen.

Om du gillar serieromaner, kan Dolphin nu visa miniatyrbilder för .cb7 serieböcker (ta också en titt på stödet för det här serieboksformatet i Okular, se nedan). Och, på tal om miniatyrbilder, har inzoomning av miniatyrbilder varit något som nu har funnits länge i Dolphin: håll nere <kbd>Ctrl</kbd>, snurra mushjulet så växer eller krymper miniatyrbilden. En ny funktion är att du nu kan återställa zoomningen till den normala nivån genom att trycka <kbd>Ctrl</kbd> + <kbd>0</kbd>.

En annan användbar funktion är att Dolphin visar dig vilka program som förhindrar att en enhet avmonteras.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): Låt din telefon styra ditt skrivbord

Den senaste version av [KDE-anslut](https://community.kde.org/KDEConnect) är fullproppad med nya funktioner. En av de mest märkbara är ett nytt SMS-program som låter dig läsa och skriva SMS med fullständig konversationshistorik.

Ett nytt [Kirigami](https://kde.org/products/kirigami/)-baserat användargränssnitt gör att du kan inte bara köra KDE-anslut på Android, utan också på alla de mobila Linux-plattformarna vi kommer att se i framtida apparater som PinePhone och Librem 5. Kirigami-gränssnittet tillhandahåller också nya funktioner för användare mellan skrivbord, såsom mediakontroll, fjärrinmatning, ringande apparater, filöverföring och köra kommandon.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="Skrivbordets nya gränssnitt" >}}

Du kunde redan använda KDE-anslut för att kontrollera volymen för media som spelas på skrivbordet, men nu kan du också använda det för att styra systemets globala volym från telefonen - mycket praktiskt när telefonen används som fjärrkontroll. När du håller ett föredrag kan du också kontrollera presentationen med användning av KDE-anslut för att bläddra framåt och bakåt i bilderna.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

En annan funktion som har funnits ett tag är att du kan använda KDE-anslut för att skicka en fil till telefonen, men vad som är nytt i den här versionen är att du kan låta telefonen omedelbart öppna filen när den väl har tagits emot. KDE:s resplan använder det för att skicka reseinformation till telefonen inifrån Kmail.

En relaterad anmärkning är att du nu också kan skicka filer från Thunar (Xfce:s filhanterare) och elementära program som Pantheon Files. Visning av kopieringsförloppet för flera filer har förbättrats en hel del, och filer mottagna från Android visar nu sin korrekta ändringstid.

När det gäller underrättelser, kan du nu utföra åtgärder från Android underrättelser från skrivbordet och KDE-anslut använder avancerade funktioner från Plasmas underrättelsecentral för att tillhandahålla bättre underrättelser. Underrättelsen om bekräftelse av hopkoppling har ingen tidsgräns, så du kan inte längre missa den.

Till sist, om du använder KDE-anslut via kommandoraden, har nu *kdeconnect-cli* funktioner för bättre automatisk komplettering för *zsh*.

## Fjärrbilder [Gwenview](https://userbase.kde.org/Gwenview)

[Gwenview](https://userbase.kde.org/Gwenview) låter dig nu justera JPEG komprimeringsnivå när du sparar bilder som du har redigerat. Utvecklare har också förbättrat prestanda för fjärrbilder, och Gwenview kan nu importera foton till och från fjärrplatser.

## [Okular](https://kde.org/applications/office/org.kde.okular) stöder  formatet .cb7

[Okular](https://kde.org/applications/office/org.kde.okular), dokumentvisaren som låter dig läsa, kommentera och verifiera alla sorters dokument, inklusive PDF, EPub, ODT och MD, och stöder nu också serieboksformatet .cb7.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): Enkel men fullständig musikspelare

[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) är en musikspelare i KDE som kombinerar enkelhet med ett elegant och modernt gränssnitt. I den senaste versionen har Elisa optimerat utseendet för att anpassas bättre till skärmar med hög upplösning. Den är också bättre integrerad med andra KDE-program och har fått stöd för KDE:s globala menysystem.

Indexering av musikfiler har också förbättrats, och Elisa stöder nu webbradio och levereras med några exempel som du kan prova.

{{< img src="elisa.png" caption="Uppdaterat gränssnitt i Elisa" >}}

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) är redo för pekskärmar

[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), KDE:s standardprogram för skärmbilder är ett annat KDE-program som har utvecklats till att ha pekskärmsstöd: Den nya pekvänliga draghanteraren gör det mycket enklare att dra rektanglar för att lagra de delar av skrivbordet som du vill på pekkänsliga enheter.

Andra förbättringar är den nya funktionen för att spara automatiskt, vilken är praktisk för att snabbt ta flera skärmbilder, och de animerade förloppsraderna, som säkerställer att du klart kan se vad Spectacle gör.

{{<img style="max-width:500px" src="spectacle.png" caption="Ny draghanterare i Spectacle" >}}

## [Plasma Browser Integration](https://community.kde.org/Plasma/Browser_Integration) Förbättringar

Den nya utgåvan av [Plasma webbläsarintegrering](https://community.kde.org/Plasma/Browser_Integration) har nu en svartlista för mediakontrollfunktionen. Det är användbart när du besöker en plats med många media som du kan spela, vilket kan göra det svårt att välja vad du vill styra från skrivbordet. Svartlistan låter dig undanta dessa platser från mediakontrollerna.

Den nya versionen låter dig också behålla den ursprungliga webbadressen i filens metadata och lägger till stöd för programmeringsgränssnittet för webbdelning. Programmeringsgränssnittet för webbdelning låter webbläsare dela länkar, text och filer med andra program på samma sätt som KDE-program gör, vilket förbättrar integreringen av icke-inbyggda webbläsare som Firefox, Chrome/Chromium och Vivaldi med resten av KDE:s program.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

En hel del av KDE:s program kan laddas ner från Microsoft Store. Krita och Okular har funnits där ett tag, och de får nu sällskap av [Kile](https://kde.org/applications/office/org.kde.kile), en användarvänlig Latex-editor.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Ny

Vilket tar oss till nyanlända: [SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), ett program som låter dig enkelt skapa textning för videor, finns nu i KDE:s inkubator, på väg att bli en fullvärdig medlem av KDE:s programfamilj. Välkommen!

Under tiden har [plasma-nano](https://invent.kde.org/kde/plasma-nano), en minimal version av Plasma konstruerad för inbäddade enheter, flyttats till Plasma-arkivet redo för utgivning med 5.18.

## Utgåva 19.12

Några av våra projekt ges ut med egna tidplaner och vissa ges ut tillsammans.
Projekten i 19.12-paketet gavs ut idag och bör snart vara tillgängliga via
programbutiker och distributioner. Se [utgivningssidan för
19.12](https://www.kde.org/announcements/releases/19.12). Paketet kallades
tidigare KDE Applications, men märkningen har tagits bort så att det nu är en
utgivningstjänst för att undvika sammanblandning med alla andra program av KDE,
och eftersom det är dussintals olika produkter istället för en helhet.