---
draft: false
layout: page
publishDate: '2020-09-03T12:00:00+00:00'
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di settembre 2020
type: announcement
---
Un mese tranquillo prima della nostra grande conferenza Akademy di questo fine settimana. Unisciti a noi [online da venerdì 4](https://akademy.kde.org/2020/) per discutere di tutto ciò che riguarda KDE.

# Nuovi rilasci

## PBI 1.7.8

[Integrazione browser di Plasma](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/) il nostro componente aggiuntivo per Chrome e Firefox è stato aggiornato.

{{< youtube WGhrR1HsCOM >}}

Risolve il suo funzionamento con i browser Vivaldi e Brave. Corregge i controlli video con iframes. Alcuni siti web impostano la copertina dell'album tramite l'API Javascript Media Session, ma quelli che non lo fanno utilizzano come copertina dell'album il poster del riproduttore video. E risolve il rilevamento delle notifiche brevi «nuovo messaggio» che non necessitano di controlli con quelle che restituiscono una lunghezza scorretta.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

Questo mese presentiamo Kontrast, un nuovo strumento per l'accesso facilitato. È uno strumento di controllo per il contrasto dei colori che segnala se le tue combinazioni di colore sono accessibili a quelle persone con problemi nella visione dei colori.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="KPhotoAlbum chiaro e scuro" style="width: 600px">}}

Il nuovo rilascio di [KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) contiene la sistemazione del codice e alcune nuove funzionalità.

È stato aggiunto il supporto per gli schemi di colori personalizzati. KPhotoAlbum ora possiede anche la «Modalità scura». Sono presenti opzioni sperimentali di regolazione della ricerca delle immagini, ed è stato aggiunto anche lo strumento a riga di comando kpa-thumbnailtool per la gestione della cache delle miniature KPA.

# Correzioni di errori

## Tellico

Il gestore di raccolte [Tellico](https://tellico-project.org/tellico-3-3-2-released/) ha ricevuto un aggiornamento che che migliora l'accesso alla [raccolta di cinema francese Allocine](http://www.allocine.fr/), la [raccolta di cinema russo Kinopoisk](https://www.kinopoisk.ru/), [Goodreads, il più grande sito al mondo di raccomandazioni lettori e libri](https://www.goodreads.com/) e il nostro personale [KDE Store](https://store.kde.org/browse/cat/591/).

# In arrivo

L'aggiunta di questo mese a KDE Review è l'estensione [SeExpr per Krita](https://invent.kde.org/graphics/seexpr), un fork di SeExpr degli Walt Disney Animation Studios, per il Google Summer of Code 2020.

# App Store

C'è un aggiornamento riguardante il [goal KDE is All About the Apps durante la conferenza online Akademy](https://conf.kde.org/en/akademy2020/public/events/211), questo sabato, 5 settembre, alle ore 09:30 UTC.

Una presentazione lampo su [Flatpak, Flathub e KDE](https://conf.kde.org/en/akademy2020/public/events/264) sabato alle 18:30 UTC.

[Sabato, poi, ci saranno esercitazioni](https://community.kde.org/Akademy/2020/Monday) su Snap, Neon packaging, Building your first flatpak e Make your App an Appimage.

# Rilasci 20.08.1

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri vengono rilasciati in massa. Il gruppo 20.08.1 dei progetti è stato rilasciato oggi e sarà presto disponibile negli app store nelle distribuzioni. Consulta la [pagina dei rilasci 20.08.1](https://www.kde.org/info/releases-20.08.1) per i dettagli.

Alcune delle correzioni nel rilascio odierno:

* In Kontact ora i codici QR funzionano nell'anteprima dei contatti.

* L'impostazione per la stima dello spazio su disco nella barra di stato di Dolphin è riapplicata correttamente.

* È stata corretta la creazione degli avvisi dai modelli nel promemoria degli eventi di KAlarm.

* In Kdenlive i flussi solo audio ora possono essere inseriti nella linea temporale dei video.

[note di rilascio 20.08](https://community.kde.org/Releases/20.08_Release_Notes)
&bull; [Pagina wiki per lo scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Pagina informativa sui sorgenti
20.08.1](https://kde.org/info/releases-20.08.1) &bull; [Elenco completo delle
modifiche 20.08.1](https://kde.org/announcements/changelog-
releases.php?version=20.08.1)