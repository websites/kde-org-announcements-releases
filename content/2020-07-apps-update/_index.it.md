---
layout: page
publishDate: 2020-07-09 12:00:00
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di luglio 2020
type: announcement
---
# Nuovi rilasci

### KTorrent 5.2.0

L'applicazione per la condivisione dei file [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) ha visto un nuovo rilascio [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

Le migliorie di importazione delle condivisioni consistono nei miglioramenti della [Distributed Hash Table](https://en.wikipedia.org/wiki/Mainline_DHT) che ora si inizializzano con dei nodi conosciuti in modo da poter iniziare a scaricare più rapidamente. Dietro le quinte troviamo il più moderno QtWebengine, basato su Chrome, al posto del più datato QtWebkit basato su WebKit (in origine tutti basati su KHTML di KDE).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

KTorrent è disponibile nella tua distribuzione Linux.

### Rilasciato KMyMoney 5.1.0

L'applicazione per banca [KMyMoney](https://kmymoney.org/) [è stata rilasciata in versione 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

Aggiunge supporto per il simbolo delle Rupia indiana: ₹. È stata aggiunta anche l'opzione di importazione OFX a «Inverti spese e pagamenti» e la vista budget ora mostra tutti i tipi di conti.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[KMyMoney è disponibile](https://kmymoney.org/download.html) nella tua distribuzione Linux, in Windows, in Mac e ora anche in [Homebrew KDE](https://invent.kde.org/packaging/homebrew-kde).

### Note di rilascio di KDiff3 1.8.3

Lo strumento di confronto file [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) è stato rilasciato nella nuova versione 1.8.3, con molte correzioni di stabilità.

L'uso di KDiff3 come strumento per le differenze in Git non crea più errori sui file non coerenti. Gli errori durante il confronto delle directory sono accodati correttamente e appare solo un messaggio. È stato corretto il ricaricamento su Windows. È stato rimosso un crash quando gli appunti non sono disponibili. L'attivazione dello schermo intero è stata rivista al fine di evitare l'uso di una API problematica di Qt.

Puoi [scaricare KDiff3](https://download.kde.org/stable/kdiff3/) per Windows, Mac e la tua distribuzione Linux.

# App Store

### Statistiche di Microsoft Store

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) ha fornito un aggiornamento sullo store di Microsoft. Kate e Okular sono stati aggiornati e nell'ultimo mese sono stati installati più di 4000 volte.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# Intervista sugli App Store: Flathub

Flatpak è uno dei nuovi formati basati su contenitori che cambia il modo con cui otteniamo il software in Linux. Flatpak è in grado di funzionare con qualsiasi host voglia configurare uno store, ma [Flathub](https://flathub.org/home) è da considerare lo store più autorevole.

Di recente [Timothée Ravier](https://discourse.flathub.org/u/Siosm), un collaboratore Flathub, [ha chiesto aiuto per aggiungere allo store più applicazioni KDE](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514). L'abbiamo intervistato per saperne di più.

### Parlaci di te, da dove vieni, cosa fai per vivere, come sei arrivato all'open source e a Flatpaks?

Mi chiamo Timothée Ravier e attualmente vivo a Parigi, Francia. Sono un ingegnere di sistemi Linux e attualmente lavoro in Red Hat e mi occupo di Red Hat CoreOS e Fedora CoreOS.

Ho conosciuto l'open source nel 2006, quando ho installato la mia prima distribuzione Linux, e da allora non ho più smesso. La maggior parte dei progetti di ricerca intrapresa durante i miei studi furono relativi alla sicurezza di Linux, il sandbox delle applicazioni e la sicurezza delle interfacce grafiche. L'introduzione e lo sviluppo di Flatpak ha poi catturato la mia attenzione.

Nel mio tempo libero sono responsabile della [variante KDE (nome in codice Kinoite)](https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) non ufficiale di Fedora Silverblue. In breve, Fedora Silverblue è un sistema operativo immutabile e il metodo raccomandato per installarvi le applicazioni è utilizzare Flatpak o contenitori (tramite podman). Per maggiori dettagli consultate la sua documentazione.

### Come mai di recente hai lanciato una call per le applicazioni KDE in Flathub?

Prima di tutto voglio ringraziare tantissimo gli attuali responsabili delle applicazioni KDE in Flathub perché stanno facendo un grandissimo lavoro!

Sono da lungo tempo un utente KDE (ho iniziato nel 2006) e voglio sempre dare indietro il mio contributo. Le distribuzioni possiedono già dei team di responsabili stabili mentre a Flathub fa difetto un buon gruppo di applicazioni KDE, dunque mi è sembrato un buon posto dove iniziare.

Ho creato la call anche perché sarebbe più facile se dividessimo il lavoro, e magari questo amplierebbe il bacino di utenti di Flatpak e Flathub.

### Flatpak può funzionare con qualsiasi repository, che necessità abbiamo di Flathub?

Questa domanda sottolinea uno dei vantaggi di Flathub: puoi ospitare il tuo deposito personale di applicazioni nel tuo server e distribuirle direttamente ai tuoi utenti. Non «hai bisogno» di Flathub.

Ma proprio come non hai bisogno di GitHub o GitLab, ecc., per ospitare un repository Git, è molto più facile collaborare se concentri utenti e sviluppatori in un unico spazio.

Flathub è diventato il posto più facile per trovare e provare le applicazioni Linux, sia open source, sia proprietarie. Credo che questo sia un punto critico se vogliamo migliorare l'attrattiva dell'ecosistema Linux come piattaforma desktop.

### Cosa hanno guadagnato le altre comunità open source mettendo le loro applicazioni in Flathub?

Credo che molte (probabilmente la maggior parte) delle applicazioni GNOME sono ora disponibili in FlatHub.

### Ora che gli sviluppatori di app possono inserire il loro software direttamente negli store tipo Flathub, sorgono nuove responsabilità come la sicurezza e l'aggiornamento del software. Ci puoi dire in che modo vengono gestiti questi aspetti in Flathub?

Con Flathub le responsabilità vengono condivise tra i responsabili della piattaforma e i responsabili delle applicazioni.

Le piattaforme contengono le librerie degli elementi fondamentali comuni a molte applicazioni (esistono piattaforme Freedesktop, GNOME e KDE) e vengono gestite per assicurare sia la compatibilità ABI, sia gli aggiornamenti di sicurezza tempestivi.

Gli aggiornamenti alle restanti librerie richieste da un'applicazione e dalla stessa applicazione sono a carico del responsabile dell'applicazione.

### Quale o quali applicazioni KDE trovi più utili?

Io uso quotidianamente Dolphin, Konsole, Yakuake, Okular e Ark e mi piacciono davvero molto. Mi piacciono e utilizzo ogni tanto anche Gwenview, KCachegrind e Massif-Visualizer.

### Molte nostre applicazioni sono impacchettate come Flatpak tramite i nostri server [invent](https://invent.kde.org/packaging/flatpak-kde-applications) e [binary-factory](https://binary-factory.kde.org/view/Flatpak/). Lavori di concerto con questi processi oppure per conto tuo?

I Flatpak che sono costruiti nell'infrastruttura KDE sono concepiti per essere nightly build di prova per gli sviluppatori e gli utenti. Questo è un buon gruppo di applicazioni Flatpak per iniziare ma alcuni hanno necessità di essere aggiornati. Mantenere questo repository aggiornato ci aiuterà per gli sviluppi nuovi che potrebbero richiedere modifiche dei pacchetti in Flathub. Non ho ancora iniziato ad aggiornarli ma proverò a farlo durante l'invio delle applicazioni a Flathub.

### Intravvedi un giorno in cui non esisteranno più RPM e Apt e tutte le distribuzioni Linux utilizzeranno i pacchetti contenitori?

Non credo che questo avverrà mai, in quanto esiste ci sono dei vantaggi nel modo in cui le distribuzioni attualmente impacchettano le applicazioni, anche se ciò crea anche dei problemi. Ma penso che sempre meno distribuzioni si impegneranno in questa direzione. Fedora, per esempio, compila i Flatpak da pacchetti RPM e li rende disponibili a tutti. Lo si potrebbe fare potenzialmente anche con i pacchetti Debian. Il valore qui non è rappresentato da cosa ma da chi: ai fiducia in questa distribuzione? Nei suoi valori? Si impegna solo nel software libero? In questo caso sei sicuro che le applicazioni che installi dai loro repository avranno gli stessi requisiti di qualsiasi altro pacchetto. Flathub contiene applicazioni sia open source, sia proprietarie, e potrebbe non andar bene a tutti.

# Rilasci attuali in kde.org/applications

Il nostro [sito secondario delle applicazioni](https://kde.org/applications) è stato aggiornato con le informazioni sui rilasci, e attenditi ulteriori novità. Se sei il responsabile di un'applicazione, ricorda di aggiungere le informazioni sul rilascio nei file appstream.

{{< img class="text-center" src="krita-release.png" caption="Informazioni sui rilasci" style="width: 800px">}}

# Rilasci 20.04.3

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri vengono rilasciati in massa. Il gruppo 20.04.3 dei progetti è stato rilasciato oggi e sarà presto disponibile negli app store e nelle distribuzioni. Consulta la [pagina dei rilasci 20.04.3](https://kde.org/info/releases-20.04.3) per i dettagli.

Alcune delle correzioni nel rilascio odierno:

* Sono state risolte le anteprime dei file desktop in Dolphin per i percorsi assoluti delle icone

* Gli elementi da fare completati ora vengono registrati correttamente nel diario di KOrganizer

* Il testo multi riga incollato dalle applicazioni GTK in Konsole non contiene più caratteri extra di «nuova riga»

* È stato corretto il comportamento di massimizzazione finestra in Yakuake

[note di rilascio 20.04](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Pagina wiki per lo scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Pagina informativa sui sorgenti
20.04.3](https://kde.org/info/releases-20.04.3) &bull; [Elenco completo delle
modifiche 20.04.3](https://kde.org/announcements/changelog-
releases.php?version=20.04.3)