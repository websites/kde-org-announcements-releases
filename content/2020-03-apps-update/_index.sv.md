---
layout: page
publishDate: 2020-03-05 15:50:00
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i mars 2020
type: announcement
---
## Nya utgåvor

### Choqok 1.7

Februari började med en efterlängtad uppdatering av vårt mikrobloggprogram [Choqok](https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html).

Viktiga rättningar inkluderar 280-teckenbegränsningen för Twitter, vilket tillåter att inaktivera konton och stöder utökade inlägg.

{{< img class="text-center" src="quick-post.png" link="https://choqok.kde.org/" caption="Choqok" >}}

### KPMcore och KDE:s partitionshanterare 4.1.0

Vårt program för diskformatering, Partition Manager, har en ny utgåva med mycket arbete utfört på biblioteket KPMCore, som också används av Calamares distributionsinstallationsverktyget. Programmet själv har fått stöd för filsystemet Minix.

{{< img class="text-center" src="kdepartitionmanager.png" link="https://kde.org/applications/system/org.kde.partitionmanager" caption="Partitionshanterare" >}}

### Kfotoalbum 5.6

Om du har hundratals eller till och med tusentals bilder på din hårddisk, blir det omöjligt att komma ihåg historien bakom varje enskild bild eller namnen på personerna som fotograferades. Kfotoalbum skapades för att hjälpa till att beskriva dina bilder, och sedan snabbt och effektivt söka igenom den stora högen med bilder.

I synnerhet ger den här utgåvan omfattande prestandaförbättringar vid etikettering av ett stort antal bilder och prestandaförbättringar för miniatyrbildsvisningen. Stort tack till Robert Krawitz för att återigen ha dykt ner i koden och hittat platser att optimera och ta bort redundans.

Dessutom lägger den här utgåvan till stöd för KDE:s insticksramverk purpose.

[KPhotoAlbum](https://www.kphotoalbum.org/2020/02/08/0099/)

## Inkommande

MP3 id-taggeditorn Kid3 har flyttats till kdereview, första steget för att skapa nya utgåvor.

Och Rocket.chat-programmet Ruqola passerade kdereview och flyttades för att vara redo för utgivning. Dock meddelar underhållsansvarige Laurent Montel att en omskrivning med Qt Widgets är planerad, så det finns fortfarande en hel del arbete som återstår.

## Programbutik

{{< img class="text-center" src="elisa.jpeg" link="https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab" caption="Elisa" >}}

Nytt på Microsoft Store för Windows är [Elisa](https://www.microsoft.com/en-gb/p/elisa/9pb5md7zh8tl?activetab=pivot:overviewtab).

En modern och snygg musikspelare tillverkad med kärlek av KDE. Bläddra i och spela upp din musik med enkelhet! Elisa ska kunna läsa nästan vilken musikfil som helst. Elisa är gjord för att vara tillgänglig för alla.

Under tiden är Digikam redo att skickas in till Microsoft Store.

## Webbplatsuppdateringar

[KDE Connect](https://kdeconnect.kde.org/), vår tjusiga telefon- och skrivbordsintegreringsverktyg, har en ny webbsida.

[Kid3](https://kid3.kde.org/) har en glänsande ny webbsida med nyheter och nerladdningslänkar till alla butiker som det är tillgängligt på.

## Utgåvor 19.12.3

Några av våra projekt ges ut med egna tidplaner och vissa ges ut tillsammans. Projekten i 19.12.3-paketet gavs ut idag och bör snart vara tillgängliga via programbutiker och distributioner. Se [utgivningssidan för 19.12.3](https://www.kde.org/info/releases-19.12.3.php) för detaljerad information. Paketet kallades tidigare KDE Applications, men märkningen har tagits bort så att det nu är en utgivningstjänst för att undvika sammanblandning med alla andra program av KDE, och eftersom det finns dussintals olika produkter istället för en helhet.

Några av felrättningarna som ingår i den här utgåvan är:

* Stöd för filattribut på delade SMB-filer har förbättrats, [diverse incheckningar](https://cgit.kde.org/kio-extras.git/log/?h=release/19.12)

* K3b accepterar nu WAV-filer som ljudfiler [Fel 399056](https://bugs.kde.org/show_bug.cgi?id=399056) [Commit](https://cgit.kde.org/k3b.git/commit/?h=release/19.12&id=2491cc70f9ffb129cf49633cbdc4f0d77789677d)

* Gwenview kan nu läsa in fjärrbilder via https [Incheckning](https://cgit.kde.org/gwenview.git/commit/?h=release/19.12&id=9973a097d30a91dd50a4d23388b0b2b00eeeb626)

💻 [19.12 versionsfakta](https://community.kde.org/Releases/19.12_Release_Notes)
för information om arkiv och kända problem. 💻 [Wiki-sida för paketnerladdning](h
ttps://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) 💻 [19.12.3
informationssida om källkod](https://kde.org/info/releases-19.12.3) 💻 [19.12
fullständig ändringslogg](https://kde.org/announcements/changelog-
releases.php?version=19.12.3) 💻