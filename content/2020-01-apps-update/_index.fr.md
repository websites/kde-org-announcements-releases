---
layout: page
publishDate: 2020-01-09 12:00:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE.
title: "Mise \xE0 jour des applications de KDE de Janvier 2020 "
type: announcement
---
Nous voilà en 2020 et nous vivons dans le futur. Voyons ce que les applications de KDE nous ont amené dans ce dernier mois !

## KTimeTracker mis à niveau avec la version 5 de l'environnement KDE

La version modernisée de KTimeTracker, si longtemps attendue est enfin disponible. L'application est un chronomètre pour les personnes occupées, disponible maintenant sous Linux, FreeBSD et Windows. Au cours de l'année 2019, elle a été portée vers Qt5 et l'environnement de développement de KDE, après avoir été abandonnée en 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

La nouvelle version est ainsi affinée et plutôt modernisée avec des nouvelles fonctionnalités très remarquables, comme la boîte de dialogue pour la modification des heures des tâches et un aperçu de développement de la boîte de dialogue d'exportation comme proposée dans l'image ci-dessous.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Boîte de dialogue dans KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/. L'application est disponible pour votre distribution Linux ou comme un [installateur Windows] (https://download.kde.org/stable/ktimetracker/5.0.1/win64/). Il y a même une [version de développement pour MacOS](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/).

[La version est annoncée dans le blog du mainteneur Alexander Potashev] (https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

Le programme d'astronomie [KStars a reçu de nouvelles fonctionnalités dans sa version 3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Les images peuvent recevoir des modifications très légères avec les modes ombré, mi-teinte et sur-brillance permettant de voir les étoiles les plus faibles.

Des constellations alternatives venant de la culture du ciel occidental, ce qui est fascinant à [lire] (https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Constellations alternatives" alt="Western Sky Culture" >}}

[L'application KStars est disponible pour] (https://edu.kde.org/kstars/) Android, Windows, MacOS, boutique Snap et pour votre distribution Linux.

## Environnements communs de développement - KNewStuff

Concernant la mise à jour des applications, la priorité a porté sur les applications plutôt que sur les bibliothèques. Mais de nouvelles fonctionnalités dans les bibliothèques communes signifieront de nouvelles fonctionnalités dans les applications  : )

Ce mois, une nouvelle interface utilisateur est disponible pour KNewStuff, l'environnement de développement pour le téléchargement de modules externes pour vos applications. Les boîtes de dialogue pour la navigation et le téléchargement ont été revues et la section de commentaires peut maintenant être filtrée. Elle apparaîtra brièvement dans toutes vos applications et modules de paramétrage, avec le démarrage du module de thème global dans la configuration du système.

{{< img src="knewstuff-comments.png" caption="Filtres sur les commentaires" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Nouvelle conception pour la boîte de dialogue de navigation " alt="Redesigned Browse Dialog" style="width: 800px">}}

## Corrections de bogues

La [mise à jour mensuelle 5.4.6 des corrections de bogues de KDevelop ](https://www.kdevelop.org/news/kdevelop-546-released) résout un problème ouvert depuis longtemps, le mélange des en-têtes des licences « GPL » et « LGPL ». Veuillez regarder pour votre distribution ou « Appimage » que vos conditions de licence sont correctes.

[La version 0.9.7 de Latte Dock ](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) a corrigé certaines fonctionnalités liées à Qt 5.14 et certains plantages.

Modules externes de Dolphin 19.12.1 avec une correction d'une erreur « SVN Commit ».

L'indexation de fichiers dans Elisa a été améliorée. Quelques soucis de compilation sous Android et sans Baloo ont été corrigés.

Une nouvelle version de KPat a confirmé n'avoir aucune restriction d'âge [OARS](https://hughsie.github.io/oars/index.html).

Un plantage d'Okular lors de la fermeture de la boîte de dialogue d'aperçu d'affichage avant impression a été corrigé.

La mise à jour mensuelle de l'éditeur vidéo Kdenlive intègre un très grand nombre de corrections. La plus importante est la mise à jour des [copies d'écran] (https://kde.org/applications/multimedia/org.kde.kdenlive) utilisés dans les méta-données. Il faut aussi noter des douzaines d'améliorations et corrections and la gestion de préversions.

Ajout de la prise en compte de JavaScript dans le client Kate [LSP](https://langserver.org/).

## Savourer KDE sur la boutique « Flathub »

KDE accueille toutes les boutiques d'applications. De plus en plus de nos programmes sont maintenant distribués directement aux utilisateurs finaux. Une des plus importantes boutiques d'applications pour Linux est [Flathub](https://flathub.org/home), utilisant le format « FlatPak ».

Votre système devrait déjà être configuré pour « Flatpak » et « Flathub » et prêt à utiliser Discover et les autres installateurs d'applications. Par exemple, KDE Neon est déjà configuré par défaut depuis un an maintenant, pour ces installations. Si tel n'était pas le cas, il existe un [paramétrage rapide](https://flatpak.org/setup/) pour chaque distribution majeure.

Si vous êtes intéressé par les détails techniques, vous pouvez explorer [le dépôt de paquets Flatpak de KDE] (https://phabricator.kde.org/source/flatpak-kde-applications/) et lire [le guide Flatpak de KDE] (https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Mais, vous devriez plutôt être intéressé sur le contenu des applications. Ainsi, regardez ce que la [boutique Flathub store propose pour KDE](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## LabPlot est maintenant sur Chocolatey

[Chocolatey](https://chocolatey.org/) est un gestionnaire de paquets pour Windows. Si vous voulez avoir un contrôle complet sur ce que vos logiciels installent sur votre machine Windows ou sur un réseau de machines. Alors Chocolatey vous donne cette possibilité de la même façon que sous Linux.

[LabPlot](https://chocolatey.org/packages/labplot) est l'application de KDE pour les tracés interactifs et l'analyse de données scientifiques. Cette application est maintenant disponible via Chocolatey. Essayez la !

[blog de LabPlot ](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Mises à jour des sites Internet

L'équipe de KDE Web vient de se reconstituer. Elle a mis à jour un ensemble de sites thématiques un peu anciens. Le site de [KPhotoAlbum](https://www.kphotoalbum.org/) en est un exemple magnifique, mis à jour et rafraîchi pour servir de stockage de vos photos et de recherche d'applications.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

Et, si vous voulez découvrir un lecteur de musique, simple à utiliser mais rempli de nombreuses fonctionnalités locales mais un peu rebuté par l'aspect vieillot du site Internet, alors [JuK] (https://juk.kde.org) a aussi remis à jour son site.

## Mises à jour 19.12.1

Les mises à jour de certains projets se font selon leurs propres plannings et
quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la
version -19.12.1.1 a publié leurs mises à jour aujourd'hui. Celles-ci devraient
être disponibles très bientôt dans les dépôts d'applications et dans les
distributions. Veuillez consulter [la page des mises à jour 19.12.1]
(https://www.kde.org/info/releases-19.12.1.php). Ce groupe était précédemment
appelé les applications de KDE. Mais, ce terme a été abandonné pour devenir un
service de mise à jour pour éviter la confusion avec les autres applications de
KDE. La raison est qu'il y a des douzaines de logiciels différents, qui ne sont
pas à considérer comme un ensemble unique.