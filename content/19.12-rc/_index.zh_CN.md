---
layout: page
publishDate: 2019-11-29 00:01:00
summary: Over 120 individual programs plus dozens of programmer libraries and feature
  plugins are released simultaneously as part of KDE's release service.
title: 19.12 RC Releases
type: announcement
---
2019 年十二月 29 日，作为 KDE 发行服务的一部分，超过 120 独立程序以及数十个程序库和功能插件同时发布。

Today they all get release candidate sources meaning they are feature complete but need testing for final bugfixes.

Distro and app store packagers should update their pre-release channels to check for issues.

+ [19.12 release notes](https://community.kde.org/Releases/19.12_Release_Notes) for information on tarballs and known issues.

+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

+ [19.12 RC source info page](https://kde.org/info/applications-19.11.90)

## Press Contacts

For more information send us an email: [press@kde.org](mailto:press@kde.org).

