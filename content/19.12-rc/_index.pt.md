---
layout: page
publishDate: 2019-11-29 00:01:00
summary: "Cerca de 120 programas individuais, para al\xE9m de dezenas de bibliotecas\
  \ de programa\xE7\xE3o e 'plugins' de funcionalidades s\xE3o lan\xE7ados em simult\xE2\
  neo como parte do servi\xE7o de lan\xE7amento de vers\xF5es do KDE."
title: "Vers\xF5es 19.12 RC"
type: announcement
---
29 de Novembro de 2019. Cerca de 120 programas individuais, para além de dezenas de bibliotecas de programação e 'plugins' de funcionalidades são lançados em simultâneo como parte do serviço de lançamento de versões do KDE.

Hoje irão receber todos versões candidatas para lançamento, o que significa que estão completos a nível de funcionalidades, mas precisam de testes para correcções de erros.

Os criadores de pacotes das distribuições e lojas de aplicações deverão actualizar os seus canais de versões prévias para procurar problemas.

* [Notas de lançamento da 19.12](https://community.kde.org/Releases/19.12_Release_Notes) para obter informações sobre os pacotes e problemas conhecidos.

* [Página de transferência de pacotes na Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

* [Página de informação do código do 19.12 RC](https://kde.org/info/applications-19.11.90)

## Contactos de Imprensa

Para mais informações, envie-nos um e-mail:
[press@kde.org](mailto:press@kde.org).