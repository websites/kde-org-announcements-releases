---
layout: page
publishDate: 2020-01-09 12:00:00
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i januari 2020
type: announcement
---
Året är 2020, och vi lever i framtiden. Låt oss se vad KDE:s program har gett oss den senaste månaden.

## Ktimetracker konverterad till KDE Ramverk 5

Den länge efterlängtade moderniserade versionen av Ktimetracker är äntligen utgiven. Programmet är en personlig tidmätare för upptagna personer som nu är tillgängligt på Linux, FreeBSD och Windows. Under loppet av 2019 har det konverterats till Qt5 och KDE Ramverk efter att ha varit utan underhåll sedan omkring 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="Ktimetracker" >}}

Den nya versionen är också polerad och något moderniserad, där de mest märkbara nya funktionerna är den nya dialogrutan för redigering av aktivitetstid och direkt förhandsgranskning i exportdialogrutan som visas på bilden nedan.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Exportdialogruta i Ktimetracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ Det är tillgängligt via din Linux-distribution eller som [Windows installationsprogram](https://download.kde.org/stable/ktimetracker/5.0.1/win64/), och det finns till och med [otestade byggen för MacOS](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) built nightly.

[Utgåvan annonserades på underhållsansvarige Alexander Potashevs blogg](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## Kstars 3.3.9

Astronomiprogrammet [Kstars har nya funktioner i 3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Bilder kan ha små förändringar i skuggor, mellantoner och dagrar vilket möjliggör att de allra svagaste stjärnorna blir synliga.

Alternativa stjärnbilder från den västerländska himmelskulturen, som är fascinerande att [läsa om](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternativa stjärnbilder" alt="Western Sky Culture" >}}

[KStars är tillgängligt för](https://edu.kde.org/kstars/) Android, Windows, MacOS, Snap och från din Linux-distribution.

## Gemensamma ramverk - Heta nyheter

Här på programuppdateringarna fokuserar vi på programmen istället för kodningsbiblioteken, men nya funktioner i de gemensamma biblioteken betyder nya funktioner i alla dina program.

Den här månaden fick vi ett omkonstruerat användargränssnitt för Heta nyheter, ramverket för att ladda ner tillägg för program. Bläddrings- och nerladdningsdialogrutan konstruerades om och kommentarsektionen kan nu filtreras. Det kommer snart att dyka upp i alla dina program och inställningsmoduler, med början på den globala temamodulen i systeminställningarna.

{{< img src="knewstuff-comments.png" caption="Kommentarfilter" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Omkonstruerad bläddringsdialogruta" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Felrättningar

[KDevelops månatliga felrättningsuppdatering 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) rättade ett långvarigt problem där GPL- och LGPL-huvuden blandades ihop. Hämta den från din distribution eller Appimage, för att säkerställa att din licenshantering är riktig.

[Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) rättade några funktioner med Qt 5.14 och gjorde sig av med några krascher.

Dolphin insticksprogram 19.12.1 rättade en felaktig SVN incheckningsdialogruta.

Filindexering i Elisa har förbättrats. Några kompileringsproblem på Android utan Baloo har också rättats.

Den nya utgåvan av Patiens förklarades att inte ha några åldersbegränsningar relevanta för [OARS](https://hughsie.github.io/oars/index.html).

Okular rättade en krasch när dialogrutan förhandsgranskning av utskrift stängdes.

Den här månadens utgåva av videoeditorn Kdenlive har ett imponerande antal rättningar, där det bästa av allt var uppdatering av  [skärmbilderna](https://kde.org/applications/multimedia/org.kde.kdenlive) använda i metainformationen. Den har också dussintals förbättringar och rättningar i tidslinje- och förhandsgranskningshantering.

Nytt stöd för Javascript finns nu i Kates  [LSP-klient](https://langserver.org/).

## Njut av KDE i Flathub-butiken

KDE håller på att omfatta alla programbutiker. Vi kan nu leverera fler och fler av våra program direkt till dig som användare. En av de ledande programbutikerna på Linux är [Flathub](https://flathub.org/home), som använder formatet FlatPak.

Du kanske redan har ställt in Flatpak och Flathub på systemet och är redo att använda det i Discover eller andra programinstallationsverktyg. Exempelvis har KDE neon det normalt inställt för installation över ett år nu. Om inte, är det en snabb [inställningsprocess](https://flatpak.org/setup/) för alla större distributioner.

Om du är intresserade av de tekniska detaljerna kan du bläddra i [KDE:s Flatpak paketeringsarkiv](https://phabricator.kde.org/source/flatpak-kde-applications/) och läsa [KDE Flatpak guide](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Men det du troligen är intresserad av är programmen, så ta en titt på vad [Flathub butiken har under KDE](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## LabPlot finns nu på Chocolatey

[Chocolatey](https://chocolatey.org/) är en pakethanterare för Windows. Om du vill ha fullständig kontroll över vilken programvara som är installerad på din Windows-dator eller alla kontorets datorer ger Chocolatey dig enkelt kontroll på det, precis som du är van vid på Linux.

[LabPlot](https://chocolatey.org/packages/labplot) är KDE:s program för interaktiv diagramritning och analys av vetenskaplig data, och är nu tillgängligt via Chocolatey. Prova det gärna!

[LabPlot blog](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Webbplatsuppdateringar

Den nyligen återuppväckta KDE:s webbgrupp har uppdaterat en mängd av våra platser med äldre teman. Den nystartade webbplatsen för [Kfotoalbum](https://www.kphotoalbum.org/) är ett vackert exempel, uppdaterad och förnyad för vårt fotolagrings- och sökprogram.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

Och om du vill briljera med en lättanvänd lokal musikspelare med alla funktioner, men skämdes över den ålderdomliga webbplatsen, har [JuK](https://juk.kde.org/) också just uppdaterats.

## Utgåvor 19.12.1

Några av våra projekt ges ut med egna tidplaner och vissa ges ut tillsammans.
Projekten i 19.12.1-paketet gavs ut idag och bör snart vara tillgängliga via
programbutiker och distributioner. Se [utgivningssidan för
19.12.1](https://www.kde.org/info/releases-19.12.1.php). Paketet kallades
tidigare KDE Applications, men märkningen har tagits bort så att det nu är en
utgivningstjänst för att undvika sammanblandning med alla andra program av KDE,
och eftersom det är dussintals olika produkter istället för en helhet.