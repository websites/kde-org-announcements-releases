---
layout: page
publishDate: 2020-01-09 12:00:00
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in januari 2020
type: announcement
---
Het jaar is 2020, we leven in de toekomst, laten we eens kijken wat KDE toepassingen ons in de laatste maand hebben gebracht!

## KTimeTracker overgezet naar KDE Frameworks 5

De langverwachte gemoderniseerde versie van KTimeTracker is eindelijk uitgegeven.  De toepassing is een persoonlijke tijdvolger voor bezige mensen die nu beschikbaar is op Linux, FreeBSD en Windows. In de loop van 2019 is deze overgebracht naar Qt5 en KDE Frameworks na niet onderhouden te zijn sinds ongeveer 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

De nieuwe versie is ook gepolijst en licht gemoderniseerd met de meest opmerkelijke nieuwe functies zoals de nieuwe dialoog Taaktijdbewerking en live bekijken in de dialoog Exporteren zoals te zien in het onderstaande plaatje.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Exportdialoog in KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ Het is beschikbaar via uw Linux distributie of als een [Windows installer](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) en er zijn zelfs [untested MacOS builds](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) nachtelijke bouw.

[De uitgave is geanonceerd in de blog van de onderhouder Alexander Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

Astronomieprogramma [KStars heeft nieuwe functies gekregen in 3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Afbeeldingen kunnen verfijnde wijzigingen hebben in schaduwen, midden tonen en accentueringen waarmee de zwakste sterren gezien kan worden.

Alternatieve samenstelling uit de Westerse hemelcultuur die fascinerend is om [lezen over](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternatieve samenstellingen" alt="Western Sky Culture" >}}

[KStars is beschikbaar voor](https://edu.kde.org/kstars/) Android, Windows, MacOS, Snap store en uit uw Linux distributie.

## Gezamenlijke Frameworks - KNewStuff

Hier bij de "Apps Update" is de focus op de toepassingen in plaats van coderen van bibliotheken. Maar nieuwe functies in de gemeenschappelijke bibliotheken zal nieuwe functies betekenen in al uw toepassingen :)

Deze maand zag een opnieuw ontworpen UI voor KNewStuff, het framework om add-ons te downloaden voor uw toepassingen. De blader- en downloaddialoog is opnieuw ontworpen en de sectie commentaar kan nu gefilterd worden. Het zal verschijnen in al uw toepassingen- en instellingenmodules kort na begin van de Globale themamodule in Systeeminstellingen.

{{< img src="knewstuff-comments.png" caption="Filters op toelichting" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Opnieuw ontworpen bladerdialoog" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Verbeterde bugs

[De maandelijkse uitgave met reparaties van bugs van KDevelop 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) repareert een lang bestaand problem waar de GPL en LGPL headers verwisseld waren, pak deze uit uw distributie of Appimage om er zeker van te zijn dat uw licenties juist zijn.

[Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) heeft enige functies met Qt 5.14 gerepareerd en enige crashes verwijderd.

Dolphin Plugins 19.12.1 heeft een gebroken SVN Commit dialoog gerepareerd.

Er is verbeterde indexering van bestanden in Elisa. Het heeft ook enige problemen met compilatie gerepareerd op Android en zonder Baloo.

De nieuw uitgave van KPat is verklaard geen [OARS](https://hughsie.github.io/oars/index.html) relevante leeftijdrestricties te hebben.

Okular heeft een crash gerepareerd bij sluiten van de dialoog voor het afdrukvoorbeeld.

De uitgave van de videobewerker Kdenlive van deze maand heeft een indrukwekkend aantal reparaties waarvan de beste was het bijwerken van de [schermafdrukken](https://kde.org/applications/multimedia/org.kde.kdenlive) gebruikt in de meta-informatie. Het heeft ook tientallen verbeteringen en reparaties in de behandeling van de tijdlijn en voorvertoning.

Nieuwe ondersteuning van JavaScript is nu in [LSP](https://langserver.org/)-client van Kate.

## Geniet ban KDE op de Flathub winkel

KDE omarmt alle app-stores. We kunnen nu meer en meer van onze programma's direct aan u de gebruiker leveren. Een van de leidende app-stores op Linux is [Flathub](https://flathub.org/home) die het FlatPak-formaat gebruikt.

U zou best wel eens Flatpak en Flathub hebben geconfigureerd op uw systeem en klaar zijn om te worden gebruikt in Discover of andere installatieprogramma's van toepassingen. KDE neon, bijvoorbeeld, heeft het nu al een jaar lang standaard ingesteld bij installaties. Indien niet het is een snel [instelproces](https://flatpak.org/setup/) voor alle belangrijke distributies.

Als u geïnteresseerd bent in de technische details dan kun u bladeren in [KDE's Flatpak pakkettenopslagruimte](https://phabricator.kde.org/source/flatpak-kde-applications/) en de [KDE Flatpak guide](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak) lezen.

Maar u bent waarschijnlijk meer geïnteresseerd de toepassingen, neem dus een kijkje bij wat de [Flathub-store heeft onder KDE](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## LabPlot nu op Chocolatey

[Chocolatey](https://chocolatey.org/) is een pakketbeheerder voor Windows. Als u volledige controle wilt over welke software is geïnstalleerd op uw Windows machine of gehele kantoor met machines dan geeft Chocolatey u er gemakkelijke controle over die net als u ook gebruikt wordt op Linux.

[LabPlot](https://chocolatey.org/packages/labplot) is de toepassing van KDE voor interactieve grafieken maken en analyse van wetenschappelijke gegevens en het is nu beschikbaar via Chocolatey. Probeer het eens!

[LabPlot blog](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Bijgewerkte website

Het recent opnieuw in leven geroepen KDE Web Team heeft een aantal van onze sites met oudere thema's bijgewerkt. De opnieuw gelanceerde [KPhotoAlbum](https://www.kphotoalbum.org/) website is een heerlijk voorbeeld, bijgewerkt en opgefrist voor onze toepassing voor foto-opslag en zoeken.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

En als u een eenvoudig te gebruiken maar volledig voorziene lokale muziekspeler wilt tonen maar zich schaamde voor de oud uitziende website, [JuK](https://juk.kde.org/) heeft nu net ook een bijgewerkte website.

## Uitgave van 19.12.1

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en
sommigen in massa vrijgegeven. De bundel 19.12.1 van projecten is vandaag
vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor
toepassingen en distributies. Zie de [19.12.1
uitgavepagina](https://www.kde.org/info/releases-19.12.1.php).  Deze bundel was
eerder genaamd KDE Applications maar heeft geen naam meer om een uitgaveservice
te worden om verwarring te voorkomen met alle andere toepassingen door KDE en
omdat het tientallen verschillende producten zijn in plaats van een enkel
geheel.