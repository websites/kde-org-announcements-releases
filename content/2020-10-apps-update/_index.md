---
title: KDE's October 2020 Apps Update
publishDate: "2020-09-10T12:00:00+00:00"
layout: page
summary: "What Happened in KDE's Applications This Month"
type: announcement
draft: false
---

After Akademy, a bumper month of new software updates and bugfixes has arrived in KDE's apps.

# New releases

## digiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="digiKam" style="width: 600px">}}

The month started with the release of our popular [photo management app, digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

Better Canon CR3 Metadata Support, RAW image formats from cameras are often undocumented and hard to process. The developers of digiKam 7.1.0 have written a metadata interface based on libraw for the CR3, and the application is now able to read a large portion of Exif tags, including GPS information, color profile, and, of course, standard IPTC and XMP containers.

A new Batch Queue Manager plugin has been introduced that lets you apply textures over images and another one fixes Hot Pixels automatically, that is, the “bad pixels” you get when using slow shutter speed or working with night images.

[IPTC Information Interchange Model](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) is the old, but still common metadata format used for photos and this release improves Unicode support for it.

digiKam is available in your Linux distro as a source code tarball, Linux 32/64 bits AppImage bundles, macOS package and Windows 32/64 bit installers. [Download digiKam 7.1.0 here](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

LabPlot is a KDE application for interactive graphing and analysis of scientific data. LabPlot provides an easy way to create, manage and edit plots and to perform data analysis. [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) was released after a year's worth of work.

In 2.8, it's now easier to access many online resources that provide data sets for educational purposes. There's now five collections with over 2000 documented data sets. Check out this video for more information:

{{< youtube pyP-5J3seQ8 >}}

This release comes with two new worksheet objects: reference lines and image elements. Reference lines can be freely positioned on the plot to highlight certain values in the visualized data. Image elements let you add an image to the plot or worksheet. 

The spreadsheet provides descriptive statistics for the data sets. We extended this information and added the calculation of quartiles, trimean and of the statistical mode.

Thanks to the work carried out on Cantor during the last months, developers have been able to add compatibility with Jupyter project files in this release of LabPlot.

More effort has been put into the macOS version of LabPlot. Besides many small macOS specific fixes, we have also added support for the touch bar that is available on newer MacBook Pro models. 

[Labplot can be downloaded](https://labplot.kde.org/download/) for Linux, Windows and Mac.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

Our software development IDE, [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released), brings half a year of work, focused mainly on stability, performance, and future maintainability. It adds the useful feature of [optionally displaying inline notes for problems at the end of the line](https://blog.david-redondo.de/kde/2020/02/28/problems.html).

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

Mobile based calendar app [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html) is out! Although a couple of versions have also been tagged, this is the first stable release of Calindori as a KDE application.

Calindori is available on KDE Neon for Plasma Mobile as well as on postmartketOS. It’s also available in the flatpak nightlies kdeapps repository for ARM and x84_64. Finally, you can also build it from source on your Linux desktop workstation. The source code and signatures can be downloaded from [download.kde.org](https://download.kde.org/stable/calindori/).

## Kid3 3.8.4

[Kid3](https://invent.kde.org/websites/kid3-kde-org) improves our music file tagging app.

Besides bug fixes, this release provides usability improvements, such as more navigation keyboard shortcuts and the possibility to customize them. The file type detection is more tolerant.

Get it from [Windows Chocolatey Package](https://chocolatey.org/packages/kid3/), [macOS Homebrew Package](https://formulae.brew.sh/cask/kid3), [Android F-Droid Package](https://f-droid.org/en/packages/net.sourceforge.kid3/), [Flatpak](https://flathub.org/apps/details/org.kde.kid3), [macOS binary](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download), [Windows binary](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download)

# Bugfixes

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

For the developers out there, we had a [bugfix release of Heaptrack](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0). Heaptrack traces all memory allocations and annotates these events with stack traces. Dedicated analysis tools then allow you to interpret the heap memory profile

There are improvements to the stability of Heaptrack which make it more error resilient while recording data. Additionally, some minor tweaks to the graphical analysis tool make it more efficient to use. The performance of the analysis step was slightly improved too. Finally, an important bug in the data-diffing algorithm has been fixed, which should make that feature more useful again.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

KDiff3 lets you compare up to three text files at one time.

[This update](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html) fixes typos, reloading files on Windows and network copy.

KDiff3 can be [downloaded](https://download.kde.org/stable/kdiff3/) for Linux, Mac and Windows.

## Tellico 3.3.3

Collection manager [Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) improved the Entrez (Pubmed) data source to use an API key when available and to honor the rate limit. Developers also improved the ISDBdb data source to search for multiple ISBN values.

## Konversation 1.7.6

IRC chat app [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) got a bugfix which builds against more Qt versions and fixes loading of nick icon theme.

## Markdown Viewing Plugins

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="Foo" style="width: 600px">}}

A pair of plugins were updated this month. [Markdown Viewer KPart](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) and [KMarkdownWebView](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) both got new releases updating their translations and fixing build with older Qt. These plugins both give previews of Markdown formatted files in Kate and other KDE apps.

# Incoming

KTechLab, an IDE for microcontrollers and electronics, is an older app which hasn't been released in some time due to the fact it was using old tech. The codebase of KTechLab has been updated and [version 0.50.0](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) is out for testing now.

# Releases 20.08.2

Some of our projects release on their own timescale and some get released en-masse. The 20.08.2 bundle of projects was released today with dozens of bugfixes and will be available through app stores and distros soon. See the [20.08.2 releases page](https://www.kde.org/info/releases-20.08.2) for details.

Some of the fixes in today's releases:

 * A bug that caused the Kate text editor to hide UI elements after closing a tab was fixed
 * Downloading of office documents via Dolphin's GDrive integration no longer fails to handle the required server redirection
 * Diagonal scrolling (e.g. with touchpads) in Konsole's terminal view was fixed
 * The Okular document viewer now allows to click on hyper links that are covered by annotations
 * In Kontact, configuring the automatic mail archiving or forwarding a message inline no longer causes a crash
 * Automatic cutting of scene splits in the Kdenlive video editor now actually cuts the video, and opening projects with missing video clips was made more robust
 * The image importer in Gwenview no longer hangs after closing
 * The remote desktop sharing tool Krfb now handles scaled screens correctly
 * Phone connections with KDE Connect got more robust after a security review

[20.08 release notes](https://community.kde.org/Releases/20.08_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [20.08.2 source info page](https://kde.org/info/releases-20.08.2) &bull; [20.08.2 full changelog](https://kde.org/announcements/changelog-releases.php?version=20.08.2)
