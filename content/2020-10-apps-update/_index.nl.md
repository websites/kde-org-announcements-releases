---
draft: false
layout: page
publishDate: '2020-09-10T12:00:00+00:00'
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in oktober 2020
type: announcement
---
Na Akademy is een ophoogmaand met nieuwe bijgewerkte software en reparaties van bugs gearriveerd in de toepassingen van KDE.

# Nieuwe uitgaven

## digiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="digiKam" style="width: 600px">}}

De maand is gestart met de uitgave van ons populaire [fotobeheerprogramma, digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

Betere ondersteuning voor Canon CR3 Metadata, RAW afbeeldingsformaten uit camera's zijn vaak niet gedocumenteerd en moeilijk te verwerken. De ontwikkelaars van digiKam 7.1.0 hebben een metadata-interface gebaseerd op libraw voor de CR3, en het programma is nu in staat een groot gedeelte van Exif-tags te lezen, inclusief GPS informatie, kleurprofiel en, natuurlijk, standaard IPTC en XMP containers.

Een nieuwe plug-in met takenwachtrijbeheer is geïntroduceerd die u texturen laat toepassen op afbeeldingen en een andere repareert hot pixels automatisch, dat wil zeggen, de “slechte pixels” die u krijgt bij gebruik van lange sluitertijden of werken met nachtafbeeldingen.

[IPTC Information Interchange Model](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) is de oude, maar nog steeds het algemene gebruikte metagegevensformaat voor foto's en deze uitgave verbetert de ondersteuning voor Unicode ervan.

digiKam is beschikbaar in uw Linux distributie als een broncode-tarball, Linux 32/64 bits AppImage bundels, macOS pakket en Windows 32/64 bit installatieprogramma's. [Download digiKam 7.1.0 hier](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

LabPlot is een interactieve grafische toepassing van KDE voor het maken van plots en de analyse van wetenschappelijke gegevens. LabPlot biedt een gemakkelijke manier om plots te maken, te beheren en te bewerken en om gegevensanalyse te doen. [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) is uitgegeven met een waarde van een jaar werk.

In 2.8 is het nu gemakkelijker om toegang te krijgen tot vele online hulpbronnen die gegevenssets bieden voor onderwijsdoeleinden. Er zijn nu vijf verzamelingen met meer dan 2000 gedocumenteerde gegevenssets. Bekijk deze video voor meer informatie:

{{< youtube pyP-5J3seQ8 >}}

Deze uitgave komt met twee nieuwe werkvelobjecten: referentielijnen en afbeeldingselementen. Referentielijnen kunnen vrij gepositioneerd worden op de plot om bepaalde waarden in de gevisualiseerde gegevens te accentueren. Afbeeldingselementen laten u een afbeelding toevoegen aan de plot of werkvellen.

Het spreadsheet biedt beschrijvingen voor statistieken voor de gegevenssets. We hebben deze informatie uitgebreid en de berekening van quartiles, trimean en van de statistische modus toegevoegd.

Dankzij het werk uitgevoerd op Cantor gedurende de laatste maanden zijn ontwikkelaars in staat om compatibiliteit met Jupyter-projectbestanden in deze uitgave van LabPlot toe te voegen.

Er is meer inspanning gestopt in de macOS versie van LabPlot. Naast vele kleine specifieke reparaties voor macOS, hebben we ook ondersteuning voor de toetsbalk die beschikbaar is op nieuwere MacBook Pro modellen toegevoegd.

[Labplot kan gedownload worden](https://labplot.kde.org/download/) voor Linux, Windows en Mac.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

Onze IDE voor het ontwikkelen van software, [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released), brengt een half jaar werk, hoofdzakelijk gericht op stabiliteit, prestatie en toekomstige onderhoudbaarheid. Het voegt de nuttige functie toe van [optioneel inline notities tonen voor problemen aan het eind van de regel](https://blog.david-redondo.de/kde/2020/02/28/problems.html).

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

De op het mobieltje gebaseerde agendatoepassing [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html) is uit! Hoewel een paar versies ook een tag hebben gekregen, is dit de eerste stabiele uitgave van Calindori als een KDE toepassing.

Calindori is beschikbaar op KDE Neon voor Plasma Mobile evenals op postmartketOS. Het is ook beschikbaar in de flatpak nightlies kdeapps opslagruimten voor ARM en x84_64. Tenslotte kunt u het ook uit broncode bouwen op uw Linux bureaubladwerkstation. De broncode en ondertekening kan gedownload worden van [download.kde.org](https://download.kde.org/stable/calindori/).

## Kid3 3.8.4

[Kid3](https://invent.kde.org/websites/kid3-kde-org) verbetert onze toepassing voor het geven van tags aan muziekbestanden.

Naast reparaties van bugs, biedt deze uitgave verbeteringen voor gebruik, zoals meer sneltoetsen voor navigatie en de mogelijkheid ze aan te passen. De detectie van het bestandstype is toleranter.

Verkrijg het uit [Windows Chocolatey Package](https://chocolatey.org/packages/kid3/), [macOS Homebrew Package](https://formulae.brew.sh/cask/kid3), [Android F-Droid Package](https://f-droid.org/en/packages/net.sourceforge.kid3/), [Flatpak](https://flathub.org/apps/details/org.kde.kid3), [macOS binary](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download), [Windows binary](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download)

# Verbeterde bugs

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

Voor de ontwikkelaars buiten ons, we hadden een [uitgave met reparaties van bugs van Heaptrack](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0). Heaptrack traceert alle toewijzingen van geheugen en annoteert deze gebeurtenissen met stacktraces. Specifieke hulpmiddelen voor analyse bieden u de mogelijkheid om het geheugenprofiel van de heap te interpreteren

Er zijn verbeteringen aan de stabiliteit van Heaptrack die het meer bestand maken tegen fouten bij het vastleggen van gegevens. Bovendien hebben enkele slimmigheidjes aan het grafische analysehulpmiddel het efficiënter in gebruik gemaakt. De prestatie van de analysestap is ook enigszins verbeterd. Tenslotte is een belangrijke bug in het algoritme data-diffing gerepareerd, die deze functie opnieuw nuttiger zou moeten maken.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

KDiff3 laat u tot drie tekstbestanden tegelijk vergelijken.

[Deze update](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html) repareert typefouten, opnieuw laden van bestanden op Windows en kopiëren via het netwerk.

KDiff3 kan [gedownload worden](https://download.kde.org/stable/kdiff3/) voor Linux, Mac en Windows.

## Tellico 3.3.3

Beheerder van verzamelingen [Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) heeft de gegevensbron Entrez (Pubmed) verbetert door een API-sleutel te gebruiken indien beschikbaar en door de snelheidslimiet te gehoorzamen. Ontwikkelaars hebben ook de gegevensbron SDBdb verbeterd om te zoeken naar meerdere ISBN waarden.

## Konversation 1.7.6

IRC chat toepassing [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) kreeg een reparatie van een bug die bouwt tegen meer Qt versies en repareert het laden van pictogramthema nick.

## Plug-ins voor het bekijken van Markdown

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="Foo" style="width: 600px">}}

Er zijn een paar plug-ins deze maand bijgewerkt. [Markdown Viewer KPart](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) en [KMarkdownWebView](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) beiden kregen nieuwe uitgaven met bijwerken van hun vertalingen en reparaties voor bouwen met oudere Qt. Deze beide plug-ins geven voorbeelden van met Markdown geformatteerde bestanden in Kate en andere KDE toepassingen.

# Inkomend

KTechLab, een IDE voor microcontrollers en elektronica, is een oudere toepassing die al enige tijd niet is uitgegeven vanwege het feit dat het oude techniek gebruikte. De codebasis van KTechLab is bijgewerkt en [versie 0.50.0](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) is nu uit voor testen.

# Releases 20.08.2

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en sommigen in massa vrijgegeven. De bundel 20.08.2 van projecten is vandaag vrijgegeven met tientallen reparaties van bugs en zou spoedig beschikbaar moeten zijn via opslagruimten voor toepassingen en distributies. Zie de [20.08.2 uitgavepagina](https://www.kde.org/info/releases-20.08.2.php) voor details.

Enige van de reparaties in de uitgave van vandaag:

* Een bug die in de Kate tekstverwerker zorgde voor het verbergen van UI-elementen na sluiten van een tabblad is gerepareerd

* Downloaden van kantoordocumenten via de GDrive-integratie van Dolphin mislukt niet langer om de vereiste serververwijzing te behandelen

* Diagonaal schuiven (bijv. met touchpads) in de terminalweergave van Konsole is gerepareerd

* De Okular documentviewer biedt nu het klikken op hyperlinks die bedekt zijn door annotaties

* In Kontact volgt het configureren van de automatische e-mailarchivering of inline doorsturen van een bericht niet langer een crash

* Automatisch snijden van scenesplitsing in de videobewerker Kdenlive snijdt nu echt de video en het openen van projecten met ontbrekende videoclips is robuuster gemaakt

* Het importprogramma van afbeeldingen in Gwenview hangt niet langer na sluiten

* Het hulpmiddel Krfb voor delen van het bureaublad behandelt nu geschaalde schermen op de juiste manier

* Telefoonverbindingen met KDE Connect zijn robuuster geworden na een herziening op beveiliging

[20.08 uitgavenotities](https://community.kde.org/Releases/20.08_Release_Notes)
&bull; [Wiki-pagina voor downloaden van
pakketten](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [20.08.2 informatiepagina van
broncode](https://kde.org/info/releases-20.08.2) &bull; [20.08.2 volledige log
met wijzigingen](https://kde.org/announcements/changelog-
releases.php?version=20.08.2)