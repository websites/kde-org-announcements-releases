---
layout: page
publishDate: 2019-11-29 00:01:00
summary: Over 120 individual programs plus dozens of programmer libraries and feature
  plugins are released simultaneously as part of KDE's release service.
title: 19.12 RC Releases
type: announcement
---
29. november 2019. KDE väljalasketeenus laskis ühekorraga välja üle 120 rakenduse ning kümneid teeke ja omadusi lisavaid pluginaid.

Täna saavad nad kõik väljalaskekandidaadi staatuse, mis tähendab, et need on omaduste poolest lõppvariandid, aga vajavad vigade parandamiseks veel testimist.

Distributsioonide ja rakenduste hoidlate pakendajad peaksid probleemide kontrollimiseks uuendama oma väljalaske-eelseid kanaleid.

* [19.12 väljalaskemärkmed](https://community.kde.org/Releases/19.12_Release_Notes) tarkvarapakettide ja teadaolevate vigade kohta.

* [Pakettide allalaadimise wiki-lehekülg](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

+ [19.12 RC source info page](https://kde.org/info/applications-19.11.90)

## Pressikontaktid

Täpsema teabe saamiseks saatke meile e-kiri:
[press@kde.org](mailto:press@kde.org).