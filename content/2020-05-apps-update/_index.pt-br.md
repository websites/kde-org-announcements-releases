---
layout: page
publishDate: 2020-05-15 13:00:00
summary: "O que aconteceu com o KDE Applications este m\xEAs"
title: "Atualiza\xE7\xE3o de maio de 2020 dos aplicativos da KDE"
type: announcement
---
# Novos lançamentos

## Kid3

O [Kid3](https://kid3.kde.org/) é um programa conveniente e poderoso que permite editar tags ID3 e formatos similares em MP3 e outros arquivos de música.

Neste mês, o aplicativo foi movido para ser hosteado pela KDE e teve seu primeiro lançamento como aplicativo da KDE. As notas de lançamento dizem:

"Além de correções de erros, este lançamento fornece melhorias de usabilidade, atalhos de teclado adicionais e scripts de ações de usuário. Estão de parabéns as várias pessoas da KDE que traduziram a interface e o manual para mais línguas."

O Kid3 está disponível para Linux, Windows, Mac e Android. Você pode baixá-lo direto do site ou por sua distro ou lojas como Chocolatey, Homebrew e F-droid.

{{< img class="text-center" src="kid3-android.png" caption="Kid3 no Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="Kid3 no Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="Kid3 no Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="Kid3 no FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="Kid3 no Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="Kid3 agora no Chocolatey" style="width: 175px">}}

## Calligra

O [Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) é uma suíte de escritório completa e em conformidade com normas internacionais. Ele passou alguns anos sem um novo lançamento, mas acabou de retornar - e com estilo!

A versão 3.2 inclui o Gemini, a suíte de escritório feita especificamente para dispositivos 2-em-1 (ou seja, laptops com telas de toque que também podem funcionar como tablets), foi portado para a nossa framework Kirigami. O programa de desenho, o Karbon, agora tem suporte a documentos de múltiplas páginas, enquanto que o Stage, o editor de apresentações, agora tem suporte a transição automática de slides.

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Desenhando no Karbon" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Apresentações do Stage" style="width: 400px">}}

O Calliga está disponível na sua distribuição Linux.

## Atualização de fontes de dados do Tellico 3.3

O [Tellico](https://tellico-project.org/tellico-3-3-released/) é um gerenciador de coleções para organizar praticamente tudo que você gosta de colecionar, como livros, bibliografias, vídeos, música, videogames, moedas, selos, cartas colecionáveis, quadrinhos ou vinhos.

Para tornar tudo isso mais fácil, ele possui alguns mecanismos de dados para exibir o que você possui. A nova versão 3.3 que saiu no começo do mês inclui fonte de dados do [Colnect](https://colnect.com/), um catálogo de coleções extenso. Várias fontes de dados como Amazon e MobyGames também foram atualizadas.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico com o modelo de dados Colnect" >}}

# Projetos que estão por vir

Os novos projetos deste mês na KDE incluem o pvfviewer, um visualizador de padrões de ponto-cruz de fotos para você fazer lindos tapetes, e o Alligator, um programa para ler feeds RSS que usa a nossa framework Kirigami.

# KDE AppImage

Gostamos de destacar todo mês um novo formato de pacote conteinerizado. O AppImage não é novo, já existe há mais de uma década, mas é bom relembrar já que um bom número de apps da KDE estão disponíveis assim.

## O que é um AppImage?

O AppImage é um formato de pacote que fornece um jeito para que desenvolvedores upstream possam fornecer arquivos binários "nativos" para usuários do GNU/Linux de modo similar ao que pode ser feito em outros sistemas operacionais. Ele permite empacotar aplicativos para qualquer sistema operacional baseado no Linux, como Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora, etc. O AppImage vem com todas as dependências que assume-se não serem parte do sistema do usuário para fornecer uma versão mais recente que roda na maioria das distribuições Linux sem quaisquer modificações.

## O que você precisa instalar para fazê-los rodar da melhor maneira?

Para executar um AppImage, você precisa ter o kernel Linux (mais atual que o 2,6) e o `libfuse`. Essas dependências estão presentes na maioria das distribuições, então você não precisa instalar nada em particular.

Infelizmente o suporte ao AppImage na maioria dos ambientes de desktop (KDE e GNOME) não é completo, então pode ser que você precise de uma ferramenta a mais para criar uma entrada para o seu app no menu. Nesses casos, a depender da experiência de usuário preferida, você pode escolher entre:

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) para ver um pop-up ao rodar pela primeira vez, ou

* [appimaged](https://github.com/AppImage/appimaged) para uma integração automatizada de cada AppImage usado na sua pasta home.

Para atualizar seus AppImages, você pode usar o [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Ele já vem embutido no AppImageLauncher, então se você já tiver ele instalado, não é necessário instalar mais nada. Apenas dê um clique direito no arquivo AppImage e selecione atualizar. Note que nem todos os empacotadores incluem informações de atualização nos binários, então podem haver casos em que você terá que atualizar manualmente para a nova versão.



## Que aplicativos da KDE rodam com eles?

Há vários aplicativos da KDE que já estão sendo distribuídos como AppImage, os mais relevantes são:

* kdenlive

* krita

* kdevelop

## O que é o appimagehub?

O modo recomendado de conseguir AppImages é pelos autores originais do aplicativo, mas isso pode não ser prático se você não souber ainda de que aplicativo você precisa. É aí que o [AppImageHub](appimagehub.com) entra. Ele é uma loja de software especificamente para AppImages. Lá é possível ver um catálogo com mais de 600 aplicativos para suas tarefas diárias.

Esse site é parte da plataforma [OpenDesktop.org](opendesktop.org) que fornece um ecosistema completo para usuários e desenvolvedores de aplicativos livres e de código aberto.

## Como criar um AppImage

Criar um AppImage tem tudo a ver com colocar as dependências do seu aplicativo em um único diretório (AppDir). Ele então é compactado em uma imagem squashfs e inclusa num 'runtime' que permite sua execução.

Para realizar esta tarefa você precisa das seguintes ferramentas:

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

No [Guia de empacotamento AppImage](https://docs.appimage.org/packaging-guide/index.html) você encontra documentação extensa sobre como usar essas ferramentas. Você também pode entrar no [Canal IRC do AppImage](irc://irc.freenode.net/#AppImage).



# Lançamentos 20.04.1

Alguns dos nossos projetos lançam a seu próprio tempo e alguns são lançados em lotes. O conjunto de projetos do 20.04.1 foi lançado hoje e deve ficar disponível nas lojas de aplicativos e distribuições em breve. Dê uma olhada na [página de lançamentos do 20.04.1](https://www.kde.org/info/releases/20.04.1.php) para os detalhes.

Algumas das correções inclusas neste lançamento são:

* kio-fish: Apenas armazene a senha no KWallet se o usuário solicitar.

* O [Umbrello](https://umbrello.kde.org/) Com correções para inclusão de suporte a comentários multilinha do C++.

* O comportamento de rolagem no visualizador de documentos [Okular](https://kde.org/applications/en/graphics/org.kde.okular) foi melhorado e se tornou mais usável com rodas de mouse que funcionam em todas as direções

* Uma regressão que ocasionalmente fazia o reprodutor de música [JuK](https://kde.org/applications/en/multimedia/org.kde.juk) crashar ao ser iniciado foi arrumado

* O [editor de vídeo Kdenlive](https://kde.org/applications/en/multimedia/org.kde.kdenlive) recebeu várias atualizações de estabilidade, incluindo uma correção na criação de capítulos de DVD e uma melhoria na manipulação de códigos de tempo, manejo de clipes ausentes otimizado, desenho de quadros "fotográficos" em clipes de imagem para diferenciação de clipes de vídeo, e pré-visualizações na linha do tempo

* O [KMail](https://kde.org/applications/en/office/org.kde.kmail2) agora lida corretamente com pastas maildir ao adicionar um novo perfil maildir e não tem mais crash ao incluir destinatários demais

* A importação e exportação nas configurações do [Kontact](https://kde.org/applications/en/office/org.kde.kontact) foi melhorada para incluir mais dados

[Notas de lançamento do
20.04](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Página da
wiki para baixar os
pacotes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Página com informações do código-fonte do
20.04.1](https://kde.org/info/releases-20.04.1) &bull; [Registro completo de
alterações do 20.04.1](https://kde.org/announcements/changelog-
releases.php?version=20.04.1)