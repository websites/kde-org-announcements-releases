# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-License-Identifier: LGPL-2.0-or-later

import frontmatter
import os
import subprocess
import shutil
import argparse
import gettext
import copy
import polib
import typing as tp
import re
import textwrap

from yaml import safe_load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

def webpage_list():
    """
    Generate a list of webpage that need to be translated
    """
    return [
            'content/19.12-rc/_index.md',
            'content/2019-12-apps-update/_index.md',
            'content/2020-01-apps-update/_index.md',
            'content/2020-02-apps-update/_index.md',
            'content/2020-03-apps-update/_index.md',
            'content/2020-04-apps-update/_index.md',
            'content/2020-05-apps-update/_index.md',
            'content/2020-06-apps-update/_index.md',
            'content/2020-07-apps-update/_index.md',
            'content/2020-08-apps-update/_index.md',
            'content/2020-09-apps-update/_index.md',
            'content/2020-10-apps-update/_index.md',
            ]


def import_frontmatter(data, _):
    """
    Import translation from po files. This function is recursive and support
    list and dict.
    """
    if isinstance(data, list):
        for index, item in data.enumerate():
            if isinstance(item, str):
                data[index] = _(item)
            else:
                import_frontmatter(item, _)
    elif isinstance(data, dict):
        for key in data:
            if isinstance(data[key], str):
                data[key] = _(data[key])
            else:
                import_frontmatter(data[key], _)


def extract_frontmatter(data, pot, filename):
    """
    Export translation to po files. This function is recursive and support
    list and dict.
    """
    if isinstance(data, str):
        entry = polib.POEntry(
            msgid=data,
            msgstr=u'',
            occurrences=[(filename, '0')]
        )
        try:
            pot.append(entry)
        except Exception:
            pass
    elif isinstance(data, list):
        for item in data:
            extract_frontmatter(item, pot, filename)
    elif isinstance(data, dict):
        for key in data:
            if key not in ['layout', 'date', 'type', 'publishDate']:
                extract_frontmatter(data[key], pot, filename)


def process_line(line: str) -> tp.List[tp.Tuple[str, str, str]]:
    """
    Process a line. Give tuples containing the simplified line, a comment
    and a format string to recreate the old line.
    """
    if line.startswith('#'):
        heading_level = len(line) - len(line.lstrip('#'))
        if line[heading_level] != ' ':
            print('WARNING: naughty heading we have here!')
        line = line[heading_level + 1:]
        return [(line, 'type: Title ' + (heading_level * '#'), '#' * heading_level + ' %s')]

    if len(line) > 2 and line[0] in ('+', '-', '*') and line[1] == ' ':
        line = line[2:]
        return [(line, 'type: List item', '* %s')]

    if line.startswith('{{<'):
        prog = re.compile('caption="([^"]*)"')
        result = prog.findall(line)
        r = []
        if result:
            r.append((result[0], 'type: image caption', re.sub('caption="([^"]*)"', 'caption="%s"', line)))
        return r

    return [(line, 'type: Plain Text', '%s')]

def extract_content(post, pot, filename: str):
    """
    Extract main content of a page.

    Seperate it into multiples logicial string (e.g. split strings by
    paragraphs, list items, ...).
    """
    sectionContent = ""
    parsing_list_element = False
    line_number = 4 # approximation
    for line in post.content.splitlines():
        line = ' '.join((line + " ").split())
        if len(line) == 0 or (parsing_list_element and (line[0] in ('+', '-', '*') and line[1] == ' ')):
            strings = process_line(sectionContent)
            for string in filter(bool, strings):
                if string[0] != "":
                    entry = polib.POEntry(
                        msgid=string[0],
                        msgstr=u'',
                        occurrences=[(filename, line_number)],
                        comment=string[1]
                    )
                    try:
                        pot.append(entry)
                    except Exception:
                        pass
            sectionContent = ""
            parsing_list_element = False
            if line:
                sectionContent += line
        else:
            sectionContent += line

        if len(line) > 2 and line[0] in ('+', '-', '*') and line[1] == ' ':
            parsing_list_element = True
        line_number += 1

    strings = process_line(sectionContent)
    for string in filter(bool, strings):
        entry = polib.POEntry(
            msgid=string[0],
            msgstr=u'',
            occurrences=[(filename, line_number)],
            comment=string[1]
        )
        try:
            pot.append(entry)
        except Exception:
            pass

def import_content(post, _) -> tp.Tuple[str, float]:
    """
    Import translation for main content.

    Similar to export_content in the way it works.
    """
    sectionContent = ""
    parsing_list_element = False
    translated = ""
    totalTranslated = 0
    totalString = 0
    for line in post.content.splitlines():
        line = ' '.join((line + " ").split())
        if len(line) == 0 or (parsing_list_element and (line[0] in ('+', '-', '*') and line[1] == ' ')):
            strings = process_line(sectionContent)
            for string in filter(bool, strings):
                if len(string[0]) > 0:
                    if _(string[0]) != string[0]:
                        translated += string[2] % _(string[0]) + '\n\n'
                        totalString += 1
                        totalTranslated += 1
                    else:
                        translated += sectionContent + '\n\n'
                        totalString += 1
                else:
                    translated += '\n\n'

            if not strings:
                translated += sectionContent + '\n\n'

            sectionContent = ""
            parsing_list_element = False
            if len(line) != 0:
                sectionContent += line
        else:
            sectionContent += line

        if len(line) > 2 and line[0] in ('+', '-', '*') and line[1] == ' ':
            parsing_list_element = True

    strings = process_line(sectionContent)
    for string in filter(bool, strings):
        if _(string[0]) != string[0]:
            translated += string[2] % '\n'.join(textwrap.wrap(_(string[0]), 80)) + '\n\n'
        else:
            translated += sectionContent + '\n\n'

    return translated, totalTranslated / totalString


def extract(args):
    """
    First parameter will be the path of the pot file we have to create
    """
    pot_file = args.pot
    pot = polib.POFile(check_for_duplicates=True)
    pot.metadata = {
        'Project-Id-Version': '1.0',
        'Report-Msgid-Bugs-To': 'kde-www@kde.org',
        'Last-Translator': 'you <you@example.com>',
        'Language-Team': 'English <yourteam@example.com>',
        'MIME-Version': '1.0',
        'Content-Type': 'text/plain; charset=utf-8',
        'Content-Transfer-Encoding': '8bit',
    }
    for webpage in webpage_list():
        post = frontmatter.load(webpage)

        extract_frontmatter(post.metadata, pot, webpage)
        extract_content(post, pot, webpage)

    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)

        for key, string in en_string_trans.items():
            entry = polib.POEntry(
                msgid=string['other'],
                msgstr=u'',
                occurrences=[('i18n/en.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

    with open("config.yaml", "r") as stream:
        config = safe_load(stream)

        for menu_item in config['menu']['main']:
            entry = polib.POEntry(
                msgid=menu_item['name'],
                msgstr=u'',
                occurrences=[('config.yaml', '0')]
            )
            try:
                pot.append(entry)
            except:
                pass

        entry = polib.POEntry(
            msgid=config['title'],
            msgstr=u'',
            occurrences=[('config.yaml', '0')]
        )
        try:
            pot.append(entry)
        except:
            pass

    pot.save(pot_file)


def import_po(args):
    """
    First parameter will be a path that will contain several .po files with the format LANG.po
    """
    directory = args.directory
    for translation in os.listdir(directory):
        lang = os.path.splitext(translation)[0]
        if lang == "ca@valencia":
            lang = "ca-valencia"
        if lang == "pt_BR":
            lang = "pt-br"

        os.makedirs("locale/" + lang + "/LC_MESSAGES/")
        po_file = "locale/" + lang + "/LC_MESSAGES/announcement"
        shutil.copyfile(directory + "/" + translation, po_file + ".po")

        command = "msgfmt " + po_file + ".po -o " + po_file + ".mo"
        subprocess.run(command, shell=True, check=True)
        print("Translations files for " + translation + " imported")


def generate_translations(args):
    """
    Assume translation located at `locale/$LANG/LC_MESSAGES/`
    """
    for translations in os.listdir('locale'):
        os.environ["LANGUAGE"] = translations
        gettext.bindtextdomain('announcement', os.path.abspath('locale'))
        gettext.textdomain('announcement')
        _ = gettext.gettext
        for webpage in webpage_list():
            extension = os.path.splitext(webpage)[1]
            basename = os.path.splitext(webpage)[0]
            translated = basename + "." + translations + extension
            post = frontmatter.load(webpage)
            import_frontmatter(post.metadata, _)
            content, percent = import_content(post, _)

            if percent > 0.5:
                with open(translated, 'w+') as translation_file:
                    translation_file.write('---\n')
                    translation_file.write(dump(post.metadata, default_flow_style=False))
                    translation_file.write('---\n')
                    translation_file.write(content)
                with open(translated) as f_input:
                    data = f_input.read().rstrip('\n')

                with open(translated, 'w') as f_output:
                    f_output.write(data)

    with open("i18n/en.yaml", 'r') as stream:
        en_string_trans = safe_load(stream)

    with open("config.yaml", 'r') as config_file:
        config_content = safe_load(config_file)

    for translations in os.listdir('locale'):
        os.environ["LANGUAGE"] = translations
        gettext.bindtextdomain('announcement', os.path.abspath('locale'))
        gettext.textdomain('announcement')
        _ = gettext.gettext

        trans_content = dict()
        for key, string in en_string_trans.items():
            if _(string['other']) is not string['other']:
                trans_content[key] = dict()
                trans_content[key]['other'] = _(string['other'])

        if len(trans_content):
            with open('i18n/' + translations + '.yaml', 'w+') as trans_file:
                trans_file.write(dump(trans_content, default_flow_style=False))

        if not translations in config_content['languages']:
            config_content['languages'][translations] = dict()
            config_content['languages'][translations]['weight'] =  2

        config_content['languages'][translations]['menu'] = dict()
        config_content['languages'][translations]['menu']['main'] = list()

        for menu_item in config_content['menu']['main']:
            menu = copy.deepcopy(menu_item)
            menu['name'] = _(menu['name'])
            config_content['languages'][translations]['menu']['main'].append(menu)

    with open('config.yaml', 'w+') as conf_file:
        conf_file.write(dump(config_content, default_flow_style=False))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='sub-command help')

    extract_cmd = subparsers.add_parser('extract', help='extract strings for translations')
    extract_cmd.add_argument('pot')
    extract_cmd.set_defaults(func=extract)

    import_po_cmd = subparsers.add_parser('import', help='import translated strings')
    import_po_cmd.add_argument('directory')
    import_po_cmd.set_defaults(func=import_po)

    generate_translations_cmd = subparsers.add_parser('generate-translations', help='generate translated content')
    generate_translations_cmd.set_defaults(func=generate_translations)

    args = parser.parse_args()
    args.func(args)
