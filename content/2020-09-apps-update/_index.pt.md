---
draft: false
layout: page
publishDate: '2020-09-03T12:00:00+00:00'
summary: "O que se Passou nas Aplica\xE7\xF5es do KDE Neste M\xEAs"
title: "Actualiza\xE7\xE3o de Setembro 2020 das Aplica\xE7\xF5es do KDE"
type: announcement
---
Um mês sossegado após a nossa grande conferência deste fim-de-semana, a Akademy. Junte-se a nós [online na Sexta-Feira 4](https://akademy.kde.org/2020/) para falar sobre tudo do KDE.

# Novos lançamentos

## PBI 1.7.8

[Integração de Navegadores do Plasma](https://blog.broulik.de/2020/08/plasma-browser-integration-1-7-6/) - A nossa extensão para o Chrome e o Firefox teve uma actualização.

{{< youtube WGhrR1HsCOM >}}

Isto corrige o funcionamento deles com os navegadores Vivaldi e Brave. Corrige os controlos de vídeo com 'iframes'. Algumas páginas Web definem as imagens de capas dos álbuns através da API de Sessão Multimédia em JavaScript, mas para os que não suportam, é usado o publicador do leitor de vídeo como capa do álbum. Corrige a detecção de notificações de "mensagens novas" que não necessitam de controlos, face às que apenas comunicam o seu tamanho de forma errada.

## Kontrast

{{< img class="text-center" src="kontrast.png" caption="Kontrast" style="width: 600px">}}

{{< img class="text-center" src="kontrast2.png" caption="Kontrast" style="width: 600px">}}

Uma nova ferramenta de acessibilidade deste mês é o Kontrast. É uma ferramenta de verificação de contraste de cores e diz-lhe se as suas combinações de cores são acessíveis para pessoas com deficiências de visualização das cores.

## KPhotoAlbum 5.7

{{< img class="text-center" src="kpa-5.7-homescreen-light+dark.png" caption="KPhotoAlbum Claro e Escuro" style="width: 600px">}}

O [KPhotoAlbum 5.7](https://www.kphotoalbum.org/2020/08/09/0102/) teve uma nova versão com arrumações de código mas também com algumas funcionalidades novas.

Adicionou o suporte para os esquemas de cores personalizados. O KPhotoAlbum tem agora também um “Modo Escuro”. Existem algumas opções experimentais para o afinamento da pesquisa de imagens. Também adicionou uma ferramenta da linha de comandos, o 'kpa-thumbnailtool', para gerir a 'cache' de miniaturas do KPA.

# Correcções de erros

## Tellico

O gestor de colecções [Tellico](https://tellico-project.org/tellico-3-3-2-released/) teve uma actualização que melhora o acesso à [colecção de cinema francês Allocine](http://www.allocine.fr/), à [colecção de Cinema Russo Kinopoisk](https://www.kinopoisk.ru/), à [Goodreads, o maior portal mundial para os leitores e recomendações de livros](https://www.goodreads.com/) e a nossa própria [Loja do KDE](https://store.kde.org/browse/cat/591/).

# Recebido

Foi adicionado este mês ao KDE Review o 'plugin' de [SeExpr para o Krita](https://invent.kde.org/graphics/seexpr), uma versão alternativa do SeExpr da 'Walt Disney Animation Studios', com base no Google Summer of Code 2020.

# Loja de Aplicações

Existe uma actualização na conferência 'online' [KDE is All About the Apps no Akademy](https://conf.kde.org/en/akademy2020/public/events/211) neste Sábado, 5 de Setembro, às 09:30UTC.

Uma conversa-relâmpago sobre o [Flatpak, Flathub e o KDE](https://conf.kde.org/en/akademy2020/public/events/264) às 18:30UTC no Sábado.

Depois [no Sábado terá tutoriais](https://community.kde.org/Akademy/2020/Monday) sobre os Snaps, pacotes do Neon, a compilação do seu primeiro Flatpak e a criação da sua primeira aplicação no AppImage.

# Versão 20.08.1

Alguns dos nossos projectos são lançados de acordo com as suas próprias agendas e alguns são lançados em massa. A versão 20.08.1 dos projectos foi lançada hoje e deverá estar disponível através das lojas de aplicações e distribuições em breve. Consulte mais detalhes na [página da versão 20.08.1](https://www.kde.org/info/releases-20.08.1.php).

Algumas das correcções incluídas nesta versão de hoje são:

* No Kontact, os códigos QR agora funcionam na antevisão dos contactos.

* A configuração do gráfico do espaço em disco na barra de estado do Dolphin agora é aplicada correctamente de novo.

* A criação de alarmes a partir de modelos na chamada de atenção de eventos do KAlarm foi corrigida.

* As transmissões apenas de áudio podem agora ser inseridas na linha temporal de vídeo do Kdenlive.

[Notas da versão 20.08](https://community.kde.org/Releases/20.08_Release_Notes)
• [Página de transferências de pacotes na
Wiki](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Página de informação do código do
20.08.1](https://kde.org/info/releases-20.08.1) &bull; [Registo de alterações
completo do 20.04.1](https://kde.org/announcements/changelog-
releases.php?version=20.08.1)