---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: "O que se Passou nas Aplica\xE7\xF5es do KDE Neste M\xEAs"
title: "Actualiza\xE7\xE3o de Agosto de 2020 das Aplica\xE7\xF5es do KDE"
type: announcement
---
Dezenas de [aplicações do KDE](https://kde.org/applications/) têm versões novas lançadas pelo serviço de versões do KDE. Novas funcionalidades, melhorias de usabilidade, remodelações e correcções de erros, tudo isto contribuir para ajudar a acelerar a sua produtividade e a tornar este novo lote de aplicações mais eficiente e agradável de usar.

Isto é o que poderá investigar mais à frente:

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="O Dolphin mostra antevisões de vários tipos de ficheiros." style="width: 800px" >}}

O Dolphin é o explorador de ficheiros e pastas do KDE que o ajuda a procurar, copiar e abrir os ficheiros e pastas. Como tal, uma das funcionalidades principais é dar-lhe uma ideia clara do que contém cada ficheiro. A apresentação de miniaturas é crucial para esse fim, e o Dolphin tem sido capaz de lhe apresentar antevisões de imagens, 'clips' de vídeo e até ficheiros 3D do Blender já há muito tempo.

Nesta nova versão, o Dolphin adiciona miniaturas para os ficheiros no Formato de Fabrico 3D (3MF) na lista e poderá também ver antevisões dos ficheiros e pastas nos sistemas de ficheiros encriptados, como os Cofres do Plasma. Isto é feito de forma segura, guardando as miniaturas em 'cache' no próprio sistema de ficheiros, ou então gerando-as sem armazenar versões em 'cache', caso seja necessário. Em qualquer dos casos, tem o controlo absoluto de como e quando o Dolphin lhe mostra o conteúdo de um ficheiro, dado que poderá configurar de forma independente o valor-limite do tamanho dos ficheiros para mostrar as antevisões dos ficheiros locais e remotos.

Outra forma de identificar os ficheiros é através dos seus nomes. No caso de o nome do ficheiro ser demasiado grande para apresentar, os programadores ajustaram o comportamento de redução do nome do ficheiro. Em vez de cortar a parte do meio do nome do ficheiro ou pasta como acontecia nas versões anteriores do Dolphin, a nova versão corta a parte final e mantém sempre a extensão do ficheiro (se estiver disponível) visível a seguir às reticências. Isto torna mais simples a identificação dos ficheiros com nomes compridos.

O Dolphin agora recorda e repõe a localização que estava a ver, as páginas abertas e as áreas divididas que tinha quando as fechou da última vez. Esta funcionalidade está activa por omissão, mas poderá ser desactivada na página de "Arranque" na janela de "Configuração" do Dolphin.

Por falar em melhorias de usabilidade, o Dolphin tem-lhe sempre permitido montar e explorar as partilhas remotas, sejam elas por FTP, SSH ou outros protocolos. Mas agora o Dolphin mostra as montagens remotas e do FUSE com nomes visíveis amigáveis em vez da localização completa. Melhor ainda: Também poderá montar imagens ISO com um novo item do menu de contexto. Isto significa que poderá transferir e explorar um sistema de ficheiros que irá gravar posteriormente para um disco ou dispositivo USB.

{{< video src="dolphin_ISO.mp4" caption="O Dolphin permite-lhe montar imagens ISO." style="max-width: 900px" >}}

O Dolphin recebeu um número espantoso de funcionalidades, mas poderá adicionar ainda mais através dos 'plugins'. Anteriormente, se queria adicionar os 'plugins' ao menu "Serviço" (procure em <I>Configuração</I> > <I>Configurar o Dolphin</I> > <I>Serviços</I>) tinha normalmente de executar um programa ou instalar um pacote da distribuição. Agora consegue instalá-los directamente a partir da janela para "Obter coisas novas" sem passos manuais intermédios. Outras alterações na página de configuração dos "Serviços" é que possui agora um novo campo de pesquisa no topo que o ajuda a encontrar rapidamente o que procura e a lista de serviços está agora ordenado alfabeticamente.

Mudar as coisas de sítio é mais fácil com o novo Dolphin, já que tem acções novas para mover ou copiar rapidamente os ficheiros seleccionados numa área dividida para uma pasta na outra área. Para explicar melhor o que se está a passar, ao arrastar um ficheiro, o cursor agora muda para um mão a pegar em vez do cursor de "cópia" que usava nas versões anteriores.

Também poderá calcular e apresentar os tamanhos das pastas no disco com a área de "Detalhes" e o painel de "Informações" do Dolphin agora mostra informações úteis sobre o Lixo. Também é possível navegar entre o campo de pesquisa do Dolphin  e os resultados com a tecla de cursor para Baixo.

Finalmente, uma nova funcionalidade útil é que o Dolphin 20.08 vem com um novo item de menu "Copiar a Localização"...

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="O Konsole recebeu algumas funcionalidades novas e excitantes." style="width: 800px" >}}

… Tal como na aplicação de terminal do KDE, o Konsole! Isto significa que poderá carregar com o botão direito sobre um item visível no Konsole, escolher a opção "Copiar a Localização" do menu de contexto e depois copiar e colar a localização completa do ficheiro ou pasta para um documento, outro terminal ou uma aplicação.

O Konsole também vem com uma nova funcionalidade que mostra um realce subtil das novas linhas que vão aparecendo quando o resultado do terminal está em deslocamento rápido, e mostra-lhe uma antevisão em miniatura dos ficheiros de imagens quando passar o seu cursor sobre eles por omissão. Mais ainda, quando carregar com o botão direito sobre um ficheiro sublinhado no Konsole, o menu de contexto aparece e apresenta um menu "Abrir com" que poderá usar para abrir o ficheiro com qualquer aplicação à sua escolha.

Ser capaz de dividir a janela do Konsole e adicionar páginas separadas para fazer várias coisas ao mesmo tempo é algo que já existe há bastante tempo, mas agora os cabeçalhos das áreas divididas poderão ser desactivados, assim como é possível aumentar a espessura do separador. Poderá também vigiar uma página pela finalização do processo activo e a atribuição de cores às páginas do Konsole!

{{< img class="text-center" src="konsole-monitor.webp" caption="Novo processo de monitorização no Konsole" style="width: 500px" >}}

Falando em páginas, o atalho predefinido do Konsole (Ctrl+Shift+L) para “Dissociar a página actual” foi removido, para que não seja mais fácil de dissociar a página actual por engano, quando o que queria era limpar o ecrã com o Ctrl+Shift+K.

A melhoria de usabilidade final nesta versão é que o cursor em I do Konsole agora segue o tamanho do texto em vez de ter sempre o mesmo tamanho.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Novo ícone do Yakuake" style="width: 200px" >}}

Falando em terminais, o Yakuake é o emulador de terminal suspenso do KDE. Ele desdobra-se do topo do ecrã sempre que carregar em F12 e também recebeu um conjunto de melhorias para esta versão. Por exemplo, agora permite-lhe configurar todos os atalhos de teclado que vêm do Konsole e agora existe um novo item da bandeja do sistema que lhe mostra quando o Yakuake está em execução. Podẽ-lo-á esconder, como é óbvio, mas fica mais fácil de perceber se o Yakuake está activo e pronto ou não. Também oferece uma forma gráfica de o invocar.

Outra coisa que fizemos foi melhorar o Yakuake para o Wayland. A janela principal do Yakuake não aparece mais por cima de nenhuns painéis de topo que tenha visíveis.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

O DigiKam é a aplicação de gestão profissional de fotografias do KDE e o que se passou é que [a equipa acabou de lançar uma nova versão principal, a 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Reconhecimento de Caras do Digikam" style="width: 800px" >}}

Um dos destaque principais do digiKam 7.0.0 é a melhoria do reconhecimento de caras com a utilização de algoritmos de IA por aprendizagem. Poderá ver a discussão no Akademy no ano passado para perceber como é que isto foi feito. Existirá mais trabalho em breve, nas próximas versões, para o reconhecimento de caras e outros reconhecimentos automáticos.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Reconhecimento de Caras do Digikam" style="max-width: 800px" controls="true" >}}

O [pacote Flatpak foi agora também disponibilizado no Flathub](https://flathub.org/apps/details/org.kde.digikam), assim como uma nova versão diária instável para os colaboradores de testes. O DigiKam também está disponível para o Linux através de AppImages e dos repositórios das suas distribuições. Existem também versões do digiKam para o Windows e o Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

Fizemos alguns ajustes na interface do nosso editor de texto Kate. O menu “Abrir um Recente” agora mostra os documentos abertos no Kate a partir da linha de comandos e também de outras fontes, e não apenas as que foram abertas na janela de ficheiros. Outra alteração é que a barra de páginas do Kate agora está visualmente consistente com todas as barras de páginas nas outras aplicações do KDE e abre as páginas novas à direita, como acontece nas outras barras.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="O Elisa é um leitor de música leve para os computadores e para dispositivos móveis." style="width: 800px" >}}

O Elisa é um leitor de música leve que funciona no seu computador ou no seu dispositivo móvel. O Elisa agora permite-lhe mostrar o progresso da música actualmente a tocar de forma incorporada, e agora a barra superior adapta-se ao tamanho da janela e ao formato de dispositivo. Isto significa que tem bom aspecto numa orientação em retrato (por exemplo, no seu telefone) e consegue-se ajustar até ficar realmente pequena.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="O KStars é a aplicação do KDE para os aficionados de astronomia." style="width: 800px">}}

Finalmente, a aplicação de astronomia KStars adicionou algumas funcionalidades de calibração e focagem na [nova versão](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html). [A máscara de Bahtinov é um dispositivo usado para focar pequenos telescópios astronómicos com precisão.](https://en.wikipedia.org/wiki/Bahtinov_mask). É útil para os utilizadores que não têm um sistema de focagem a motor e preferem focar manualmente com a ajuda da máscara. Depois de capturar uma imagem no módulo de focagem com o algoritmo da máscara de Bahtinov seleccionado, o Ekos irá analisar as imagens e as estrelas dentro dele. Se o Ekos reconhecer o padrão de estrelas de Bahtinov, irá desenhar linhas ao longo do padrão de estrelas em círculos no centro e com um deslocamento a indicar a focagem.

{{< img class="text-center" src="kstars-focus.webp" caption="Novo algoritmo de focagem do KStars" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="Calibração do KStars" style="width: 600px" >}}

Foi adicionado um "Gráfico de Calibração" ao lado direito do "Gráfico de Desvio". Mostra as posições da montagem registadas durante a calibração da guia interna. Se as coisas estiverem a funcionar bem, deverá mostrar pontos em duas linhas que formam ângulos rectos entre si - uma quando a calibração empurra a montagem para trás e para frente ao longo da direcção da AR, e depois quando faz o mesmo para a direcção da DEC. Não tem muita informação, mas poderá ser útil de ver. Se as duas linhas formarem um ângulo de 30 graus entre si, algo de errado se está a passar com a sua calibração! Em cima aparece uma imagem da mesma em acção com o simulador.

[O KStars está disponível para transferência](https://edu.kde.org/kstars/) no Android, na loja Snapcraft, no Windows, no macOS e, como é óbvio, nos repositórios da sua distribuição de Linux.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="O KRDC com o cursor remoto" style="max-width: 900px" >}}

O KRDC permite-lhe ver ou até controlar sessões de ambientes de trabalho noutras máquinas. A versão lançada hoje agora mostra cursores do lado do servidor adequados no VNC em vez de um simples ponto com o cursor remoto a arrastar-se atrás dele.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

O Okular é o visualizador de documentos do KDE. Permite-lhe ler os documentos PDF, os livros ePUB e muitos outros tipos de ficheiros baseados em texto. Uma das correcções colocou as opções "Imprimir" e "Antevisão da Impressão" juntas de novo no menu "Ficheiro".

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

O Gwenview é uma aplicação de visualização de imagens que vem com algumas funcionalidades básicas de edição, como o dimensionamento e o recorte. A nova versão agora guarda o tamanho da última área de recorte usada. Isto significa que poderá recortar rapidamente várias imagens do mesmo tamanho em sucessão rápida.

# Correcções de erros

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* A gravação passou para uma tarefa separada para não bloquear a interface durante a gravação

* Interface de D-Bus para as combinações de teclas e para controlar a digitalização

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

* Os atalhos da funcionalidade de temporização do Spectacle, o Shift + Printscreen (tirar uma fotografia de todo o ecrã) e o Meta + Shift + Printscreen (tirar uma fotografia de uma região rectangular) agora funcionam no Wayland.

* O Spectacle não inclui mais o cursor do rato nas capturas por omissão.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

* O 'struct2osd' usa agora o 'castxml' (o 'gccxml' foi descontinuado).

* Menos utilizações de código obsoleto do Qt, evitando a apresentação de avisos durante a execução.

* Melhorias nas traduções.