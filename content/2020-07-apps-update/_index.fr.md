---
layout: page
publishDate: 2020-07-09 12:00:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE.
title: "Mise \xE0 jour des applications de KDE de Juillet 2020"
type: announcement
---
# Nouvelles mises à jour

### KTorrent 5.2.0

L'application de partage de fichiers [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) propose une nouvelle version [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

L'amélioration de l'importation pour le partage concerne l'amélioration de [DHT, « Distributed Hash Table »](https://en.wikipedia.org/wiki/Mainline_DHT), qui démarre maintenant avec les nœuds bien connus. Ainsi, vous pouvez réaliser vos téléchargements plus rapidement. Sous le capot, il est mis à jour avec le nouveau moteur « QtWebengine », reposant sur Chrome et bien loin de la vieille version de « QtWebkit » reposant sur «  » (chacun d'entre eux reposaient autrefois sur « KHTML » de KDE ). 

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

KTorrent est disponible pour votre distribution Linux.

### KMyMoney 5.1.0 est disponible

Application de gestion de comptes [KMyMoney](https://kmymoney.org/) [released 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

Il ajoute la prise en charge du symbole de la roupie indienne  : ₹. Ils ont aussi ajouté l'option pour « Revenir aux frais et payements » pour l'importation « OFX ». L'affichage du budget propose maintenant tous les types de comptes.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[KMyMoney est disponible](https://kmymoney.org/download.html) pour votre distribution Linux, comme un téléchargement Windows, Mac et maintenant [Homebrew KDE](https://invent.kde.org/packaging/homebrew-kde).

### Notes de mise à jour de KDiff3 1.8.3

L'outil de comparaison de fichiers [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) a été mis à jour dans une nouvelle version 1.8.3, avec un ensemble de corrections de stabilité.

L'utilisation de KDiff3 comme un outil de comparaison pour Git ne déclenchera plus d'erreurs pour des fichiers non existants. Les erreurs lors d'une comparaison entre dossiers sont correctement mis en file. Ainsi, un seul message apparaîtra. KDiff3 corrige le rechargement sous Windows. Il supprime un plantage quand le clavier n'est pas disponible. Le basculement en plein écran a été re-travaillé pour éviter un appel problématique à l'« Qt API ».

Vous pouvez [télécharger KDiff3](https://download.kde.org/stable/kdiff3/) pour Windows, Mac et votre distribution Linux.

# Magasin d'applications

### Statistiques pour la boutique Microsoft

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) a fourni une mise à jour du magasin Microsoft. Kate et Okular ont été mis à jour et durant le dernier mois, ont été installés plus de 4000 fois. 

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# Interview sur les boutiques d'applications : flathub

Flatpak est l'un des nouveaux conteneurs reposant sur les modifications de formats dans lesquels se trouvent les applications sous Linux. Il peut fonctionner avec tout hôte voulant mettre en place un magasin. Mais, le magasin le plus complet est [Flathub](https://flathub.org/home).

Récemment, l'assistant de « Flathub » [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [a demandé de l'aide pour installer plus d'applications dans le magasin](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514). Son interview en dit plus.

### Dis nous en plus sur vous, d'où vous êtes, ce que vous faites, comment vous êtes vous intéressés aux logiciels libres et à flatpak ?

Je m'appelle Timothée Ravier. Je vis actuellement à Paris, France. Je suis un ingénieur système Linux. Je travaille actuellement pour RedHat pour les noyaux RedHat et Fedora.

Je suis arrivé dans les logiciels libres quand j'ai installé pour la première fois une distribution Linux en 2006 et je n'ai jamais arrêté depuis. La plus part des projets de recherche auxquels j'ai participés durant mes études concernaient la sécurité de Linux et le fonctionnement isolé des applications « Bac à sable » et la sécurité des interfaces graphiques. Ainsi, l'introduction et le développement de « Flatpak » ont décuplé mon intérêt.

Durant mes loisirs, je maintiens la [version alternative avec KDE (nommé Kinoite)](https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) de Fedora SilverBlue. De façon synthétique, Fedora SilverBlue est une système d'exploitation robuste. La façon qui est recommandée d'installer des applications est l'utilisation de « Flatpak » ou des conteneurs (via «  »). Veuillez consulter la documentation pour plus de détails.

### Qu'est ce qui a motivé votre appel récent pour déposer les applications KDE dans « Flathub » ?

D'abord, je voudrais remercier tout particulièrement les mainteneurs actuels, ayant déjà en charge la maintenance des applications dans « Flathub », pour l'énorme travail qu'ils réalisent !

Je suis depuis très longtemps un utilisateur de KDE (j'ai démarré en 2006) et j'ai toujours voulu contribuer. Les distributions ont toujours eu des équipes de mainteneurs confirmés. « Flathub » ne possédait pas un bon ensemble d'applications KDE. Ansi, j'ai senti que c'était un bon sujet pour commencer.

J'ai aussi lancé un appel pour rendre plus facile si tout le travail était partagé et peut être aussi, que cela pourrait rendre les personnes plus attentives à « Flatpak » et « Flathub ».

### Flatpak peut fonctionner à partir de tout dépôt, pour avoir besoin alors de Flathub ?

Cette question met en avant un des avantages de « Flathub ». Vous pouvez héberger votre propre dépôt d'applications sur votre serveur personnel et les distribuer directement vers les utilisateurs. Vous n'avez pas « besoin » de « Flathub ».

Mais, comme vous n'avez pas besoin de « GitHub » ou « GitLab » ou etc., pour héberger un dépôt « git », il est bien plus facile de collaborer si vous avez une emplacement unique vers lequel se tourner les utilisateurs et les développeurs.

Flathub est devenu l'emplacement le plus facile pour trouver et tester sans risque les applications Linux, quelles soient libres ou propriétaires. Je pense que cela est critique pour l'amélioration de l'attractivité de l'éco-système Linux sur des plate-formes de bureau.

### Quelles sont les autres communautés des logiciels libres qui ont accepté de placer leurs applications sous « Flathub » ?

Je pense que beaucoup (voire la plupartà des applications GNOME sont maintenant disponibles sous « FlatHub ».

### Maintenant que les développeurs d'applications peuvent placés leurs logiciels sur des magasins comme « Flathub », de nouvelles responsabilités apparaissent comme la sécurité et le maintien des logiciels à jours. Pourriez-vous nous dire comme cela est géré dans « Flathub » ?

Avec « Flathub », les responsabilités sont partagées entre les mainteneurs de la plate-forme et les mainteneurs d'applications.

Les plate-formes contiennent la bibliothèque centrale, commune à un grand nombre d'applications (il y a les plate-formes Freedesktop, GNOME et KDE). Elles sont maintenues pour conserver la compatibilité « ABI » et aussi pour s'assurer de mises à jour rapides de sécurité.

Les mises à jour des bibliothèques restantes requises par une application et l'application elle-même sont de la responsabilité du mainteneur de l'application.

### Quelles applications KDE trouvez-vous les plus utiles ?

J'utilise Dolphin, Konsole, Yakuake, Okular et Ark, de façon quotidienne et je les apprécie grandement. J'apprécie et j'utilise aussi de temps en temps, Gwenview, KCachegrind et Massif-Visualizer.

### Plusieurs de nos applications sont proposées en paquets « Flatpaks » via les sites [invent](https://invent.kde.org/packaging/flatpak-kde-applications) et [binary-factory servers](https://binary-factory.kde.org/view/Flatpak/). Travaillez-vous avec ces processus ou indépendamment ?

Les paquets « Flatpak » qui sont construits à partir de l'infrastructure de KDE, sont supposés être des compilations de nuit pour les développeurs et les utilisateurs pour des tests. Cela représente un bon ensemble d'applications « Flatpak » pour démarrer mais certaines d'entre elles ont aussi besoin d'être mises à jour. Le maintien à jour de ce dépôt nous aidera pour les développements récents, nécessitant des modifications de mise en paquets sous « Flathub ». Je n'ai pas encore commencé à les mettre à jour. Mais, j'essaierais de le faire en même temps que le passage des applications sous « Flathub ».

### Voyez-vous un jour où les paquets « rpm » ou « apt » ne seront plus utilisés et que toutes les distributions Linux utiliseront des paquets de conteneurs ?

Je ne pense pas que cela arrive un jour puisque il y a de l'intérêt sur la façon dont les distributions créent actuellement des paquets d'applications, même si il y a aussi des problèmes. Par exemple, Fedora créé des « Flatpak » à partir des paquets « RPM » et les rendre disponible pour tout le monde. Vous pouvez dire la même chose avec les paquets Debian. L'intérêt n'est pas comment mais avec qui. Est-ce que vous faites confiance à cette distribution ? Ces valeurs ? Son engagement envers uniquement les logiciels libres ? Ensuite, voulez-vous vraiment que les applications que vous installez à partir de leurs dépôts, appliquent les mêmes exigences que tout autre paquet ? Flathub propose tout aussi bien des applications libres et propriétaires et même si cela n'est pas adapté pour tout le monde.

# Publication maintenant sur kde.org/applications

Les [sites secondaires d'applications sub-site](https://kde.org/applications) ont commencé à afficher leurs mises à jour. Il faut attendre plus bientôt. Si vous êtes un mainteneur d'applications, n'oubliez pas d'ajouter les informations de mise à jour dans les fichiers amont des applications.

{{< img class="text-center" src="krita-release.png" caption="Informations concernant les mises à jour" style="width: 800px">}}

# Mises à jour 20.04.3

Les mises à jour de certains projets se font selon leurs propres plannings et quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la version 20.04.3 a publié leurs mises à jour aujourd'hui. Celles-ci devraient être disponibles très bientôt dans les dépôts d'applications et dans les distributions. Veuillez consulter [la page des mises à jour 20.04.3] (https://www.kde.org/info/releases-20.04.3) pour plus de détails.

Certaines de ces corrections de la mise à jour de ce jour :

* Les vignettes des fichiers de bureau dans Dolphin ont été corrigées pour des emplacements absolus d'icônes.

* Les éléments terminés de la liste « A Faire » sont maintenant correctement enregistrés dans le journal de KOrganizer

* Le collage de texte multi-ligne d'applications « GTK » dans le logiciel « Konsole » ne se fait plus sans caractère superflu de retour à la ligne.

* Le comportement lors de la maximisation de Yakuake a été corrigé.

[Notes de la version 20.04]
(https://community.kde.org/Releases/20.04_Release_Notes) • [Page de tutoriel
pour le téléchargement de paquets]
(https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) • [Page
d'informations sur les sources de la version 20.04.3]
(https://kde.org/info/releases-20.04.3) • [Liste complète des modifications pour
la version 20.04.3] (https://kde.org/announcements/changelog-
releases.php?version=20.04.3)