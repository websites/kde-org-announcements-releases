---
layout: page
publishDate: 2020-04-23 13:00:00
summary: Quoi de neuf ce mois-ci avec les applications de KDE.
title: "Mise \xE0 jour des applications de KDE de Avril 2020"
type: announcement
---
Un nouveau groupe d'applications est arrivé ! Dans ces mises à jour, vous pouvez vous attendre de trouver plus de fonctionnalités, des améliorations de stabilité, et des outils plus conviviaux pour vous aider à travailler plus efficacement.

Il y a des douzaines de modifications pour préparer l'avenir dans la plupart de vos applications favorites, comme Dolphin par exemple. Les partages Windows Samba sont maintenant totalement découvrables.

Concernant la lecture de musique : le lecteur de musique Elisa a reçu des fonctionnalités à pas de géant. Cette mise à jour apporte un nouvel affichage « Lecture en cours », une accessibilité améliorée via la boîte à miniatures et une option pour réduire la liste de lecture quand vous le souhaitez. Grâce à l'ajout récent d'un affichage du mode « Aléatoire », il est plus facile de ré-arranger votre musique dans des listes de lecture.

Il y a juste les points importants concernant les nouveautés de ce mois pour les applications de KDE. Veuillez découvrir tout ce que nous avons préparé pour vous.

## [Okular](https://okular.kde.org)

Okular est l'application de KDE permettant de lire les fichiers de type « pdf », les documents « Markdown », les Comic Books et bien d'autres formats de documents. Dans cette mise à jour, Okular a reçu certaines améliorations d'accessibilité similaires pour les utilisateurs sur ordinateur ou tablette tactile. Par rapport à la version antérieur, Okular intègre maintenant un défilement doux que vous utilisiez la roulette de souris ou le clavier. Pour les utilisateurs d'écrans tactiles, la dernière version d'Okular prend en compte d'un défilement amorti.

{{<video src="okular.mp4" caption="Défilement fluide sous Okular. Illustration : David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

Le puissant gestionnaire de fichiers de KDE, Dolphin, reçoit de nouvelles fonctionnalités, vous permettant de mieux interagir avec les systèmes de fichiers distants comme les partages « Samba » ou les serveurs « SSH ». Maintenant, vous pouvez commencer de regarder des films, enregistrés sur des sites distants sans avoir à les télécharger, mais en dirigeant directement le flux vidéo du cloud vers votre lecteur via Dolphin.

Dans le même esprit, il n'a jamais été aussi facile de se connecter aux partages « Windows Samba » et de tirer avantage de tous les services. Cela est possible car les partages « Samba » sont maintenant découvrables via le protocole « WS-Discovery », utilisé par les versions modernes de Windows. Cela vous permet d'intégrer harmonieusement votre lecteur Linux dans un réseau Windows.

Selon les nouvelles du département d'ergonomie : vous n'avez plus besoin de souris pour déplacer le focus vers et à partir d'un panneau de terminal. Maintenant, vous n'avez qu'à utiliser le raccourci clavier<kbd>Ctrl</kbd> + <kbd>Maj</kbd> + <kbd>F4</kbd>. Une autre fonctionnalité longtemps attendue est la recherche de fichier, non pas par leurs noms ou contenus mais aussi par leurs étiquettes.

La prise en charge des fichiers « 7zip » est maintenant effective dans Dolphin. Cela veut dire que vous pouvez naviguer dans ces fichiers compressés comme s'ils faisaient partie des dossiers du système de fichiers. Quand vous installez une application de recherche externe comme KFind, Dolphin vous permet d'y accéder rapidement par la création d'un lien.

Enfin, une petite fonctionnalité qui apparemment peut être source de grand gain de temps est la fonction « Dupliquer ». Sélectionner un ou plusieurs fichiers ou dossiers, appuyez sur <kbd>Ctrl</kbd> + <kbd>D</kbd> (ou faites un clic droit ou choisissez « Dupliquer ici » et hop ! Une copie de chaque élément sélectionné apparaîtra juste à coté des originaux.

 ! [Fonctionnalité de duplication de Dolphin](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

L'utilitaire Lokalize vous permet de traduire des fichiers « gettext » et « xliff ». Il prend en charge la correction grammaticale « Language-Tool », aidant les traducteurs en montrant les erreurs grammaticales dans le texte.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

KMail est le client de messagerie le plus populaire de KDE et membre de Kontact, la suite de logiciels qui vous aide à gérer vos adresses de courriels, vos contacts et vos tâches. Un nombre plutôt important d'améliorations sont incluses dans cette nouvelle version. Pour commencer, les messages de courriels peuvent être facilement exportés au format « pdf » et l'affichage des messages au format « Markdown » est amélioré. Les aspects de sécurité ont été traités avec attention. Plus spécifiquement, KMail affiche une alarme lorsque le composeur de message ouvre suite à un clic, un lien vous demandant le joindre un fichier.

 ! [KMail] (kmail-mailto.png)

## [Konsole] (https://kde.org/applications/system/org.kde.konsole)

Konsole est l'émulateur de terminal utilisé pour lancer en ligne de commandes, les instructions, les processus et les programmes. La dernière version de Konsole vous permet d'utiliser la suite de touches <kbd>Alt</kbd> + un nombre, pour passer directement à l'un des 9 onglets.

## [Gwenview] (https://kde.org/applications/graphics/org.kde.gwenview)

Gwenview est l'afficheur d'images favori de KDE. Dans cette version, les développeurs ont résolu deux problèmes majeurs. L'application ne se plante plus au démarrage quand le presse-papier du système contient du texte provenant de KDE Connect et lors de l'importation de photos de ou vers des emplacements distants.

## [Elisa] (https://kde.org/applications/multimedia/org.kde.elisa)

Elisa est un lecteur de musique simple et populaire. Cette mise à jour propose une fonctionnalité nouvelle d'affichage de « En cours de lecture », ce qui le rend plus attractif.

Il n'y a pas que l'apparence. Elisa est aussi encore plus fonctionnel puisque vous pouvez maintenant y accéder via la boîte à miniatures et réduire la liste de lecture lorsque vous le souhaitez. Cela signifie que vous pouvez rester en écoute de votre musique favorite, même si la fenêtre d'Elisa est fermée. Le nouveau mode visuel de ré-arrangement facilite la modification de vos morceaux dans les listes de lecture et montre quel morceau sera joué après.

 ! [Elisa] (elisa.png)

## [Kdenlive] (https://kdenlive.org)

Kdenlive est une application avancée de KDE pour le montage vidéo. La nouvelle version rend le montage plus rapide grâce à sa résolution configurable de l'aperçu. L'aperçu améliore l'affichage de pistes multiples, ce qui le rend plus facile pour sélectionner les pistes parmi celles que vous modifiez en même temps.

Pour la chronologie, l'équipe a amélioré la synchronisation vidéo juste en s'alignant sur les pistes actives. Ils ont aussi corrigé la fonctionnalité d'alignement de groupe et ajouté un raccourci pour ignorer l'alignement en lecture. Une autre chose que vous pouvez faire est de glisser vos fichiers ( vidéos, musique, images) de votre explorateur de fichiers directement sur la ligne de temps. La nouvelle fonctionnalité « Zoomer sur les trames clé » vous aidera à réaliser un travail plus précis. Les professionnels de montage vidéo et les utilisateurs utilisant d'autres outils de montage apprécieront sans aucun doute la fonctionnalité d'importation / exportation « OpenTimelineIO ».

Cette mise à jour de Kdenlive arrivent aussi avec de nombreuses corrections de bogues, comme celles apportées au logiciel « Motion Tracker » qui a été défectueux sur certains systèmes. Elle vous permet d'ajouter un filtre de compensation « Pitch » ainsi que d'ajouter un « Déplacer et redimensionner » à une forme de rotoscopie. Le plantage de l'assistant DVD et un bug affectant le changement de volume d'enregistrement d'un mixeur ont aussi été corrigés. L'équipe de Kdenlive a aussi ajouté les fonctionnalités d'étiquetage, de notation et de filtrage à la projection binaire.

La nouvelle version Kdenlive pour Windows reçoit aussi des corrections pour les boîtes de dialogue pour la ligne de temps, qui n'acceptaient pas les entrées du clavier. En plus, vous pouvez maintenant sélectionner le moteur audio dans Windows.

 ! [Kdenlive] (kdenlive.png)

## [Yakuake] (https://kde.org/applications/system/org.kde.yakuake)

Yakuake est une fenêtre de terminal que vous pouvez ouvrir en haut du bureau Plasma en appuyant sur la touche <kbd>F12</kbd>. C'est pratique pour exécuter des instructions rapides à partir de la ligne de commandes et de passer à autre chose.

Quand vous ouvrez de nouveaux onglets ou partager des panneaux dans cette version de Yakuake, vous pouvez maintenant les démarrer dans le mème dossier comme l'onglet ou le partage actuel. Vous pouvez aussi re-dimensionner la fenêtre de Yakuake verticalement en faisant glisser sa barre de dessous.

## Configuration du système

La page d'intégration des comptes en ligne dans la configuration du système vous permet de configurer ou de régler l'accès au compte « Nextcloud ». La page a été complètement remaniée et maintenant offre une apparence moderne et simple avec une fonctionnalité plus fiable. 

## [KDE Connect] (https://kdeconnect.kde.org/)

KDE Connect est une application qui vous aide à intégrer votre téléphone avec votre bureau et vice-versa. Une des nouvelles fonctionnalité fournie avec cette version est la capacité de démarrer de nouvelles conversations avec l'application de SMS.

Les autres modifications concernent l'affichage des lecteurs de morceaux distants dans l'icône du lecteur, un nouvel ensemble d'icônes pour l'état de connexion et la gestion améliorée des notifications d'appels.

Au département de corrections des bogues, le bogue qui bloquait l'application appelante suite au partage d'un fichier a été corrigé. Les problèmes liés aux transferts de fichiers vraiment très volumineux ont été résolus.

## [Spectacle] (https://kde.org/applications/utilities/org.kde.spectacle)

Spectacle, le logiciel de capture d'écran de KDE a reçu des fonctionnalités plus ergonomiques et quelques corrections. Une d'entre elles vous empêchait de transférer une copie d'écran vers le presse-papier quand vous ouvriez Spectable à partir de la ligne de commandes avec l'option « -nonotifiy ». Même si cela était un problème mineur, maintenant, vous pouvez faire exactement cela. De même, les anciennes versions rencontraient des problèmes quand vous tentiez de glisser une copie d'écran de Spectable vers un modèle par défaut de nom de fichiers incluant les sous-dossiers. Ces problèmes sont maintenant résolus.

Une autre amélioration utilise dans le nouvelle version de Spectacle vient du formulaire pour les boutons « Défaut » et « Rétablir ». Vous pouvez les utiliser pour revenir à la configuration précédente ou même supprimer toutes les configurations personnalisées antérieures et tout rétablir à ses valeurs par défaut.

Spectacle ne se contentera pas de se souvenir de votre configuration par défaut, mais aussi la dernière zone de copie d'écran utilisée. Cela vous aide à être cohérent, rendant plus facilement la création de copies avec les mêmes formes et tailles que les précédentes.

## [Krita] (https://krita.org/en/) 4.2.9

Il y a un certain nombre de mises à jour techniques derrière le décor dans cette mise à jour. Il y a aussi aussi les contours des brosses colorés plus doucement sans scintillement quand vous survolez la zone de travail. La brosse d'étalement de couleurs a reçu deux fonctionnalités avec le mode « Aérographe » et « Taux Aérographe ». Elle intègre un nouveau paramètre de ratio permettant d'aplatir la forme de brosse en utilisant différents moyens. Un nouvelle fonction de partage de calque dans le menu de sélection du masque vous permet de créer de nouveau claque de chaque couleur dans le calque actif.

{{< youtube fyc8-qgxAww >}}

## [Smb4K] (https://kde.org/applications/utilities/org.kde.smb4k)

La mise à jour 3.0.4 de Smb4K corrige plusieurs problèmes et plantages, en particulier avec le processus de montage. De plus, il rend smb4K de nouveau compilable avec les versions de Qt 5.0 et inférieures.

## [KIO GDrive] (https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Cette mise à jour majeur du module externe intégrant Google Drive avec les applications de KDE prend en charge la fonctionnalité de disques partagés de Google Drive.

Il intègre aussi dans le menu contextuel de Dolphin, une nouvelle action « Copier l'URL de Google vers le presse-papier ».

[Notes de la version 20.04]
(https://community.kde.org/Releases/20.04_Release_Notes) • [Page de tutoriel
pour le téléchargement de paquets]
(https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) • [Page
d'informations sur les sources de la version 20.04]
(https://kde.org/info/releases-20.04.0) • [Liste complète des modifications pour
la version 20.04] (https://kde.org/announcements/changelog-
releases.php?version=20.04.0)