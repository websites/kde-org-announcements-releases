---
layout: page
publishDate: 2020-01-09 12:00:00
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de enero de 2020"
type: announcement
---
Es el año 2020 y estamos viviendo en el futuro. Veamos lo que nos han traído las aplicaciones de KDE durante el último mes.

## KTimeTracker se ha portado a KDE Frameworks 5

La largamente esperada versión modernizada de KTimeTracker ya se ha publicado. La aplicación permite hacer un seguimiento del tiempo personal de las personas atareadas que está disponible para Linux, FreeBSD y Windows. A lo largo de 2019 ha sido portada a Qt5 y a KDE Frameworks tras haber estado sin mantenimiento desde alrededores de 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

La nueva versión también se ha pulido y se ha modernizado ligeramente, siendo sus funcionalidades más visibles el nuevo diálogo de edición de tiempo y la vista previa instantánea del diálogo de exportación, como se puede observar en la imagen inferior.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Diálogo de exportación de KTimeTracker" >}}

Su código fuente está disponible en [https://download.kde.org/stable/ktimetracker/5.0.1/src/](https://download.kde.org/stable/ktimetracker/5.0.1/src/). Está disponible a través de su distribución Linux o como [instalador para Windows](https://download.kde.org/stable/ktimetracker/5.0.1/win64/), e incluso como [compilaciones no probadas para MacOS](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) que se crean cada noche.

[El lanzamiento está anunciado en el blog de su encargado, Alexander Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html).

## KStars 3.3.9

El programa de astronomía [KStars incorpora nuevas funcionalidades en 3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Las imágenes pueden tener cambios refinados en las sombras, en los tonos medios y en el resaltado para permitirle ver las estrellas más débiles.

Se han incorporado constelaciones alternativas de la cultura celeste occidental sobre las que resulta fascinante [leer](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Constelaciones alternativas" alt="Western Sky Culture" >}}

[KStars está disponible para](https://edu.kde.org/kstars/) Android, Windows, MacOS, la tienda Snap y para su distribución Linux.

## Infraestructuras comunes: KNewStuff

En la actualización de las aplicaciones nos centramos más en las aplicaciones que en las bibliotecas de programación. Pero nuevas funcionalidades en las bibliotecas comunes significan nuevas funcionalidades en todas las aplicaciones :)

Este mes se ha producido un rediseño de la interfaz gráfica de KNewStuff, la infraestructura para descargar complementos de las aplicaciones. El diálogo de navegación y de descarga se ha rediseñado y ahora permite filtrado en la sección de comentarios. Aparecerá en todas las aplicaciones y en los módulos de preferencias en breve junto con el módulo de temas globales de las preferencias del sistema.

{{< img src="knewstuff-comments.png" caption="Filtros de comentarios" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Diálogo de navegación rediseñado" alt="Redesigned Browse Dialog" style="width: 800px">}}

## Corrección de errores

La [actualización de corrección de errores mensual de KDevelop 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) ha corregido un antiguo problema que mezclaba las cabeceras GPL y LGPL. Obténgala de su distribución o use la Appimage  para asegurarse de que las licencias sean correctas.

[Latte Dock 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html) corrige varios problemas con Qt 5.14 y ha solucionado varios fallos de la aplicación.

Los complementos de Dolphin 19.12.1 corrigen un diálogo inservible de envío de modificaciones de SVN.

Se ha mejorado la indexación de archivos en Elisa. También se han corregido varios problemas de compilación en Android y cuando se usa sin Baloo.

El nuevo lanzamiento de KPat anuncia que no tiene restricciones de edad [OARS](https://hughsie.github.io/oars/index.html) relevantes.

En Okular se ha corregido un fallo de la aplicación que se producía al cerrar el diálogo de vista previa de la impresión.

El lanzamiento de este mes del editor de vídeo Kdenlive contiene un extenso número de correcciones, siendo la mejor de ellas la actualización de las [capturas de pantalla](https://kde.org/applications/multimedia/org.kde.kdenlive) que se usan en la información de metadatos del programa. También contiene docenas de mejoras y correcciones en el manejo de la línea de tiempo y de las vistas previas.

El cliente [LSP](https://langserver.org/) de Kate dispone de una nueva implementación de JavaScript.

## Disfrute de KDE en la Flathub Store

KDE está llegando a todas las tiendas de aplicaciones. Ahora podemos entregar un número cada vez mayor de nuestros programas directamente al usuario. Una de las tiendas más importantes para Linux es [Flathub](https://flathub.org/home), que usa el formato Flatpak.

Es posible que ya tenga Flatpak y Flathub configurado en su sistema y listo para usar en Discover o en otros instaladores de aplicaciones. Por ejemplo, KDE Neon lo configura de forma predeterminada en la instalación desde hace más de un año. En caso contrario, es un [proceso de configuración](https://flatpak.org/setup/) rápido para todas las distribuciones principales.

Si le interesan los detalles técnicos, puede explorar el [repositorio de empaquetado Flatpak de KDE](https://phabricator.kde.org/source/flatpak-kde-applications/) y leer la [guía de KDE para Flatpak]](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak).

Aunque probablemente le interese más echar un vistazo a las aplicaciones existentes en la [tienda Flathub para KDE](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## LabPlot ya está en Chocolatey

[Chocolatey](https://chocolatey.org/) es un gestor de paquetes para Windows. Si desea tener un control total sobre el software instalado en su máquina Windows o en todas las máquinas de la oficina, Chocolatey le proporciona un control sencillo sobre él de forma similar a como se hace en Linux.

[LabPlot](https://chocolatey.org/packages/labplot) es la aplicación de KDE para gráficos interactivos y análisis de datos científicos, y ahora está disponible a través de Chocolatey. ¡Pruébela!

Consulte el [blog de LabPlot](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/).

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## Actualización del sitio web

El recién reanimado equipo web de KDE ha estado actualizando un buen número de nuestros antiguos sitios web temáticos. El flamante sitio web de [KPhotoAlbum](https://www.kphotoalbum.org/) es una buena muestra de ello, actualizado y revitalizado para nuestra aplicación de almacenamiento y búsqueda de fotos.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

Y si lo que quiere es presumir de un reproductor de música fácil de usar a la vez que completamente multifunción, aunque estaba avergonzado del aspecto de su antiguo sitio web, [JuK](https://juk.kde.org/) también dispone de un sitio web actualizado.

## Lanzamiento de 19.12.1

Algunos de nuestros proyectos se publican según su propio calendario, mientras
que otros se publican en masa. El paquete de proyectos 19.12.1 se ha publicado
hoy y debería estar disponible próximamente en las tiendas de aplicaciones y en
las distribuciones. Consulte la [página de lanzamientos
19.12.1](https://www.kde.org/info/releases-19.12.1.php). Este paquete se llamaba
anteriormente Aplicaciones de KDE, pero se ha simplificado su nombre para que se
convierta en un servicio de lanzamiento, evitando así confusiones con la
totalidad de aplicaciones creadas por KDE, ya que está compuesto de docenas de
productos diferentes en lugar de su totalidad.