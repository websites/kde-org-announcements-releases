---
draft: false
layout: page
publishDate: '2020-09-10T12:00:00+00:00'
summary: Hilabete honetan KDEren aplikazioekin gertatutakoa
title: KDE aplikazioen 2020ko urriko eguneratzea
type: announcement
---
Akademy igarota, hilabeteko software eguneraketa eta akats konponketa oparoa iritsi da KDEren aplikazioetara

# Argitalpen berriak

## digiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="digiKam" style="width: 600px">}}

Hilabetea gure [argazkiak kudeatzeko aplikazio ezagunaren, digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/) argitalpenarekin hasi zen.

Canon CR3 metadatuen euskarri hobea, kameren RAW irudi formatuak dokumentatu gabe egon eta prozesatzeko zailak izan ohi dira. digiKam 7.1.0ren garatzaileek CR3rentzako libraw liburutegian oinarritutako metadatu interfaze bat idatzi dute, eta aplikazioa, orain, «Exif» etiketen zati handi bat irakurtzeko gai da, GPS informazioa, kolore-profilak, eta, noski, IPTC eta XMP edukitzaile estandarrak barne.

Agindu-sorten ilara kudeatzaile plugin berri bat sartu da, irudien gainean ehundurak aplikatzeko aukera ematen duena. Beste batek «pixel beroak» automatikoki konpontzen ditu,  hau da, obturadorearen abiadura motela erabiltzen duzunean edo gaueko irudiekin lan egitean lortzen dituzun «pixel okerrak».

[Informazioa trukatzeko IPTC eredua](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) argazkiekin erabiltzen den metadatu formatu zahar baina oraindik erabilera arruntekoa da, eta bertsio honek Unicode euskarria hobetzen dio.

digiKam eskuragarri dago zure Linux banaketan sorburu-kode «tarball», 32/64 biteko AppImage fardel, macOS pakete eta Windows 32/64 biteko instalatzaile gisa. [Zama-jaitsi digiKam 7.1.0 here](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

LabPlot datu zientifikoen grafikatze elkarreragile eta analisirako KDEren aplikazio bat da. LabPlot-ek trazuak sortzeko, kudeatzeko eta editatzeko eta datuen analisia egiteko modu erraza ematen du. [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) urtebeteko lanaren ondoren kaleratu da.

2.8 bertsioan, orain errazagoa da hezkuntza-helburuetarako datu-multzoak eskaintzen dituzten lerroko baliabide ugari eskuratzea. Orain bost bilduma daude dokumentatutako 2000tik gora datu-multzo dituztenak. Ikusi bideo hau informazio gehiago izateko:

{{< youtube pyP-5J3seQ8 >}}

Bertsio hau kalkulu-orrietarako bi objektu berrirekin dator: erreferentzia lerroak eta irudi elementuak. Erreferentzia lerroak askatasunez koka daitezke trazuan, bistaratutako datuetan zenbait balio nabarmentzeko. Irudi elementuek trazuari edo kalkulu-orriari irudi bat gehitzeko aukera eskaintzen dizute.

Kalkulu-orriak datu-multzoen estatistika deskribatzaileak eskaintzen ditu. Informazio hau hedatu dugu eta gehitu ditugu kuartila, hiruko batezbestekoa eta estatistika moduaren kalkuluak.

Azken hilabeteetan Cantor-ren egindako lanari esker, garatzaileak LabPlot-en bertsio honetan Jupyter proiektuko fitxategiekin bateragarritasuna gehitzeko gai izan dira.

Ahalegin handiagoa ipini da macOS-ren LabPlot bertsioan. MacOS-ren berariazko konponketa txiki ugariez gain, MacBook Pro eredu berriagoetan eskuragarri dagoen ukimen-barrarako euskarria ere gehitu dugu.

[Labplot zama-jaitsi daiteke](https://labplot.kde.org/download/) Linux, Windows eta Mac-erako.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

Gure software garapenerako IDEak, [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released), urte erdiko lana dakar, batez ere egonkortasunera, errendimendura, eta etorkizuneko mantentze-gaitasunera bideratua. Aukeran, lerroaren amaieran lerro barruko arazoei buruzko oharrak azaltzeko ezaugarri baliagarria gehitzen du.

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

Mugikorrean oinarritutako [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html) aplikazioa kalean da! Bertsio pare bat ere etiketatu diren arren, hau da Calindori KDE aplikazio gisa kaleratzen den aurreneko bertsio egonkorra.

Calindori erabilgarri dago Plasma Mugikorreko KDE Neon-en, baita postmartketOS-en ere. Flatpak «nightlies kdeapps» ARM eta x84_64-erako gordetegietan ere erabilgarri dago. Azkenik, sorburuetatik ere eraiki dezakezu zure mahaigaineko Linux ordenagailuan. Sorburu-kodea eta sinadurak [download.kde.org](https://download.kde.org/stable/calindori/)-etik jaitsi ditzakezu.

## Kid3 3.8.4

[Kid3](https://invent.kde.org/websites/kid3-kde-org) musika fitxategiak etiketatzeko gure aplikazioa hobetzen du.

Akatsak zuzentzeaz gain, bertsio honek erabilgarritasun hobekuntzak ekartzen ditu, hala nola nabigatzeko teklatuko lasterbide gehiago eta horiek norbere nahietara egokitzeko aukera. Fitxategi-mota hautematea toleranteagoa da.

Lor ezazu [Windows Chocolatey paketeetatik](https://chocolatey.org/packages/kid3/), [macOS Homebrew paketeetatik](https://formulae.brew.sh/cask/kid3), [Android F-Droid paketeetatik](https://f-droid.org/en/packages/net.sourceforge.kid3/), [Flatpak-etik](https://flathub.org/apps/details/org.kde.kid3), [macOS bitarretatik](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download), [Windows bitarretatik](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download)

# Akats zuzenketak

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

Hor kanpoan dauden garatzaileentzat, [Heaptrack-en akatsak konpontzeko bertsio](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0) bat dugu. Heaptrack-ek memoriaren esleipen guztien aztarnak jarraitzen ditu eta gertakari hauek pilen aztarnekin idazten ditu. Ondoren, analisi tresna espezializatuek pilen memoria-profila interpretatzeko aukera ematen dizute.

Heaptrack-en egonkortasunean hobekuntzak daude, datuak grabatzean izandako erroreekiko jasankorragoa egiten dutena. Gainera, analisi grafikoko tresnari egindako doikuntza txikiek erabiltzeko eraginkorragoa egiten dute. Analisi-urratsaren errendimendua ere zertxobait hobetu da. Azkenik, datuen ezberdintasunak bilatzeko algoritmoko akats garrantzitsu bat konpondu da, eta horrek ezaugarri hori berriz erabilgarriagoa egin beharko luke.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

KDiff3-k aldi berean hiru testu-fitxategi arte alderatzen uzten dizu.

[Eguneratze honek](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html) idatzizko akatsak, Windows-en fitxategiak birzamatzea eta sarean kopiatzea konpontzen ditu.

KDiff3 Linux, Mac eta Windows-erako [zama-jaitsi](https://download.kde.org/stable/kdiff3/) daiteke.

## Tellico 3.3.3

[Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) bilduma-kudeatzaileak «Entrez» (Pubmed) datu sorburua hobetu du erabilgarri dagoenean API gako bat erabiltzeko eta emari-muga betetzeko. Garatzaileek ISDBdb datu sorburua ere hobetu dute ISBN balio anizkoitzak bilatzeko.

## Konversation 1.7.6

IRC berriketa-aplikazioak [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) akats-konponketa bat jaso du, Qt bertsio gehiagorekin eraikitzeko aukera eman eta goitizen ikono gaia zamatzea konpontzen duena.

## Markdown ikusteko pluginak

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="Foo" style="width: 600px">}}

Plugin pare bat eguneratu dira hilabete honetan, [Markdown Viewer KPart](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) eta [KMarkdownWebView](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) biek jaso dituzte bertsio berriak, haien itzulpenak eguneratu eta Qt zaharragoarekin eraikitzean zituzten arazoak konponduz. Plugin hauek, biek, Markdown formatuko fitxategien aurreikuspegiak ematen dituzte Kate eta beste KDE aplikazio batzuetan.

# Sarrerakoa

KTechLab, mikrokontrolatzaileetarako eta elektronikarako IDE bat, aplikazio zaharragoa da, teknologia zaharra erabiltzen zuelako denbora batez kaleratu ez dena. KTechLabs-en kode-oinarria eguneratu egin da eta [0.50.0 bertsioa](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) kalean da orain probatzeko prest.

# 20.04.2 argitaratzen du

Gure proiektuetako batzuk beren epe propioetan argitaratzen dira, beste batzuk berriz batera argitaratzen dira. 20.08.2ko proiektu-sorta gaur argitaratu da hamabika akats-konponketarekin eta laster egongo dira erabilgarri aplikazio saltoki/biltegietan eta banaketen bidez. Begiratu [20.08.2 argitalpen orria](https://www.kde.org/info/releases-20.08.2) xehetasun bila.

Some of the fixes in today's releases:

* Kate testu-editoreko fitxa bat ixtean erabiltzaile-interfazearen elementuak ezkutatzea eragiten zuen akatsa konpondu da

* Bulegotika dokumentuak Dolphin-en GDrive integrazioaren bidez zama-jaistea ez du jada beharrezkoa den zerbitzari-birbideratzea maneiatzean huts egiten

* Konsole-ren terminal-ikuspegiko kiribiltze diagonala (ukimen-saguekin adib.) konpondu egin da

* Okular dokumentu erakusleak, orain, idatz-oharrekin estalita dauden hiperestekei klik egiten uzten du

* Kontact-en, posta artxibatze automatikoa konfiguratzeak edo mezu bat lerro barruan birbidaltzeak jada ez du klaskatzea eragiten

* Kdenlive bideo-editorean eszena zatien ebakitze automatikoak, orain, bideoa benetan ebakitzen du, eta bideoklipak faltan dituzten proiektuak irekitzea sendotu egin da

* Gwenview-ko irudi-inportatzailea jada ez da eskegita gelditzen itxi ondoren

* Urrunetik mahaigaina partekatzeko Krfb tresnak, orain, eskalatutako pantailak behar bezala maneiatzen ditu

* KDE Connect bidezko telefono-konexioak sendoago egin dira segurtasun-berrikuspen baten ondoren

[20.08 argitalpen orria](https://community.kde.org/Releases/20.08_Release_Notes)
&bull; [Paketeak zama-jaisteko wiki
orria](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[20.08.2 sorburuen informazio orria](https://kde.org/info/releases-20.08.2)
&bull; [20.08.2 aldaketen egunkari
osoa](https://kde.org/announcements/changelog-releases.php?version=20.08.2)