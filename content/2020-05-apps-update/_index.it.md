---
layout: page
publishDate: 2020-05-15 13:00:00
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di maggio 2020
type: announcement
---
# Nuovi rilasci

## Kid3

[Kid3](https://kid3.kde.org/) è un programma di gestione dei tag musicali completo ma facile da usare, che ti permette di modificare i tag ID3 e simili nei file musicali MP3 e non solo.

Questo mese è stato spostato nell'infrastruttura di KDE e ha conosciuto il primo rilascio come applicazione KDE. Le note di rilascio recitano:

«Correzioni di errori a parte, questo rilascio fornisce miglioramenti nell'usabilità, scorciatoie da tastiera aggiuntive e script per azioni utente. Un grazie speciale a tutte quelle persone in KDE che hanno tradotto l'interfaccia utente e il manuale in altre lingue.»

Kid3 è disponibile per Linux, Windows, Mac e Android. Puoi scaricarlo dal sito web o mediante la tua distribuzione e gli store di Chocolatey, Homebrew e F-droid.

{{< img class="text-center" src="kid3-android.png" caption="Kid3 in Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="Kid3 in Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="Kid3 in Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="Kid3 è in FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="Kid3 è in Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="Kid3 è in Chocolatey" style="width: 175px">}}

## Calligra

[Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) è una suite per ufficio dalle funzionalità complete e conforme agli standard. Il suo ultimo rilascio risale a un paio di anni fa ma ora è tornata con stile.

La versione 3.2 contiene Gemini, la suite per ufficio progettata in particolare per i dispositivi 2 in 1, ossia per portatili con schermo tattile che possono trasformarsi in tablet convertiti nel nostro bel framework Kirigami. Il programma per disegno Karbon ora supporta i documenti che contengono più pagine. E Stage, l'editor delle presentazioni di Calligra, ora supporta la transizione automatica delle diapositive,

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Disegno Karbon" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Presentazioni Stage" style="width: 400px">}}

Calligra è disponibile tramite la tua distribuzione Linux.

## Tellico 3.3 aggiorna le sorgenti dati

[Tellico](https://tellico-project.org/tellico-3-3-released/) è un gestore di raccolte che tiene traccia di tutto ciò che collezioni: libri, bibliografie, video, musica, videogiochi, monete, francobolli, figurine, fumetti e vini.

Per semplificarne l'uso, contiene un sacco di motori per la gestione dei dati per rendere semplice la visione degli oggetti da te posseduti. La nuova versione 3.3, uscita i primi di questo mese, aggiunge una sorgente dati per [Colnect](https://colnect.com/) un catalogo per oggetti da collezione completo. Aggiorna inoltre diverse sorgenti dati, tra cui Amazon e MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico col modello di dati Colnect" >}}

# Progetti in arrivo

Nuovi progetti in KDE questo mese includono pvfviewer, un visualizzatore per modelli di disegno per PC per creare bellissimi arazzi. E Alligator, un lettore di fonti RSS che utilizza il nostro ottimo framework Kirigami.

# KDE AppImage

Ogni mese ci piace descrivere in dettaglio uno dei nuovi formati contenitore dei pacchetti. AppImage non è nuovo ed esiste da circa dieci anni, ma vale la pena ricordarci di esso, dato il quantitativo di applicazioni KDE che lo utilizza.

## Che cos'è un pacchetto AppImage?

AppImage è un formato di impacchettamento che fornisce agli sviluppatori upstream un modo per fornire binari «nativi» agli utenti GNU/Linux proprio con lo stesso metodo che farebbero per gli altri sistemi operativi. Permette l'impacchettamento di applicazioni per qualsiasi sistema operativo comune basato su Linux, per esempio Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora, ecc. Le AppImage offrono tutte le dipendenze che non possono essere presunte essere parte di ciascun sistema di destinazione, in versione abbastanza recente, e funzioneranno sulla maggior parte delle distribuzioni Linux senza ulteriori modifiche.

## Cosa devi installare affinché funzionino al meglio?

Per avviare una AppImage hai bisogno del kernel Linux (> 2.6) e `libfuse`. Queste dipendenze sono presenti nella maggioranza delle distribuzioni GNU/Linux, dunque non devi installare nulla di speciale.

Purtroppo il supporto AppImage negli ambienti desktop principali (KDE Plasma e Gnome) non è completo, dunque potrebbe essere richiesto uno strumento aggiuntivo per creare una voce di menu per la tua applicazione. In questi casi, a seconda delle tue preferenze grafiche, puoi scegliere tra:

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) per una finestra a comparsa di integrazione al primo avvio, oppure

* [appimaged](https://github.com/AppImage/appimaged) per un'integrazione automatizzata di qualsiasi AppImage presente nella tua cartella Home.

Per aggiornare i tuoi pacchetti AppImage usa [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Questa funzione è incorporata in AppImageLauncher, dunque se l'hai già installato non devi installare nient'altro. Basta solo fare clic col destro sul file AppImage e scegliere Aggiorna. Nota che non tutti i creatori di pacchetti incorporano le informazioni di aggiornamento nei binari, dunque in alcuni casi potresti dover scaricare manualmente la nuova versione.



## Quali applicazioni KDE ci girano?

Esistono diverse applicazioni KDE giù distribuite come AppImage, le più degne di nota sono:

* kdenlive

* krita

* kdevelop

## Che cos'è Appimagehub?

Il metodo consigliato per ottenere AppImage è ottenerle dagli autori dell'applicazione originale, ma questo metodo non è molto pratico se non sai ancora di quale applicazione hai bisogno. Qui entra in gioco [AppImageHub](appimagehub.com). È uno store di software dedicato solo ad AppImage. Qui trovi un catalogo con più di 600 applicazioni per il tuo lavoro quotidiano.

Questo sito web è parte della piattaforma [OpenDesktop.org](opendesktop.org), che fornisce un ecosistema completo per gli utenti e sviluppatori di applicazioni gratuite e open source.

## Come creare una AppImage?

Creare una AppImage significa mettere assieme tutte le dipendenze della tua applicazione all'interno di una singola directory (AppDir). Viene poi incorporata all'interno di un'immagine sqaushfs e allegata a un «runtime» che ne consente l'esecuzione.

Per eseguire questa operazione puoi utilizzare gli strumenti seguenti:

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

Nella [AppImage Package Guide](https://docs.appimage.org/packaging-guide/index.html) trovi la documentazione dettagliata su come utilizzare tali strumenti. Puoi anche unirti al [canale IRC di AppImage](irc://irc.freenode.net/#AppImage).



# Rilasci 20.04.1

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri vengono rilasciati in massa. Il gruppo 20.04.1 dei progetti è stato rilasciato oggi e sarà presto disponibile negli app store nelle distribuzioni. Consulta la [pagina dei rilasci 20.04.1](https://www.kde.org/announcements/releases/20.04.1.php) per dettagli.

Alcune delle correzioni incluse in questo rilascio sono:

* kio-fish: memorizza password in KWallet solo se l'utente lo richiede.

* [Umbrello](https://umbrello.kde.org/) Correzioni per l'aggiunta del supporto dei commenti c++ multi-linea.

* È stato migliorato il comportamento dello scorrimento nel visore di documenti [Okular](https://kde.org/applications/office/org.kde.okular) e ora è più utilizzabile con rotelle del mouse a rotazione libera

* È stata risolta una regressione che talvolta causava il crash di [JuK](https://kde.org/applications/en/multimedia/org.kde.juk), il riproduttore musicale

* L'[editor video Kdenlive](https://kde.org/applications/en/multimedia/org.kde.kdenlive) è stato oggetto di molti aggiornamenti di stabilità, inclusa una correzione nella creazione dei capitoli DVD e un'altra che migliora la gestione dei codici temporali. È stata inoltre migliorata la gestione delle clip mancanti, il disegno di frame «fotografici» sulle clip immagini per differenziarli dalle clip video e le anteprime nella linea temporale

* [KMail](https://kde.org/applications/en/office/org.kde.kmail2) ora gestisce correttamente le cartelle maildir esistenti quando viene aggiunto un nuovo profilo maildir e non si blocca più quando sono aggiunti troppi destinatari

* Le impostazioni di importazione ed esportazione di [Kontact](https://kde.org/applications/en/office/org.kde.kontact) sono state migliorate per includere più dati

[note di rilascio 20.04](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Pagina wiki per lo scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Pagina informativa sui sorgenti
20.04.1](https://kde.org/info/releases-20.04.1) &bull; [Elenco completo delle
modifiche 20.04.1](https://kde.org/announcements/changelog-
releases.php?version=20.04.1)