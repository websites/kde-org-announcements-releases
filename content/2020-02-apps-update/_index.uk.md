---
layout: page
publishDate: 2020-02-06 12:00:00
summary: "\u041D\u043E\u0432\u0438\u043D\u0438 \u043F\u0440\u043E\u0433\u0440\u0430\
  \u043C KDE \u0446\u044C\u043E\u0433\u043E \u043C\u0456\u0441\u044F\u0446\u044F"
title: "\u041B\u044E\u0442\u043D\u0435\u0432\u0435 \u043E\u043D\u043E\u0432\u043B\u0435\
  \u043D\u043D\u044F \u043F\u0440\u043E\u0433\u0440\u0430\u043C KDE \u0443 2020"
type: announcement
---
Цього місяця ви зможете скористатися багатьма випусками програм, у яких виправлено помічені вади. Хоча нам усім хочеться побільше нових можливостей, ми знаємо, що корисними є також виправлення помилок та усування аварійних завершень роботи!

# Нові випуски

## KDevelop 5.5

Цього місяця випущено етапну версію [KDevelop 5.5](https://www.kdevelop.org/news/kdevelop-550-released), комплексного середовища розробки, яке полегшує написання коду мовами C++, Python та PHP. Нами виправлено низку вад та удосконалено код KDevelop. Додано можливість визначення захоплених змінних у лямбда-функціях C++, придатні до налаштовування набори перевірок Clazy-tidy і Clang та складніші алгоритми випереджального доповнення коду. У модулі PHP реалізовано типізовані властивості PHP 7.4 та підтримку масивів типів та видимість сталих класів. У модулі Python додано підтримку Python 3.8.

KDevelop можна встановити зі сховищ пакунків вашого дистрибутива або за допомогою пакунка AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) — наша програма стеження за списком завдань, яку інтегровано до Kontact. У останніх випусках Kontact взаємодію із Zanshin було поламано, і користувачі програми лишилися без зручного засобу визначення своїх планів. Втім, цього місяця було випущено нову версію, у якій [помилку виправлено](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Тепер вам буде простіше визначитися із власними першочерговими планами!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Панель Латте 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Панель Латте" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

Новий випуск [Okteta](https://kde.org/applications/utilities/org.kde.okteta), шістнадцяткового редактора KDE, містить виправлення вад та нову можливість: інструмент для обчислення контрольних сум CRC-64. Крім того, код Okteta оновлено з метою забезпечення сумісності із новими бібліотеками Qt та KDE.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

Нова версія [KMyMoney](https://kmymoney.org/), програми KDE, яка допоможе вам стежити за власним фінансовим станом, [містить виправлення декількох вад](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) та удосконалення, серед яких підтримка форм чеків із поділом витрат.

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# Нове

Keysmith — нова програма для створення двофакторних кодів розпізнавання на основі oath-toolkit для мобільної Плазми та Плазми для комп'ютерів. Інтерфейс користувача програми створено за допомогою Kirigami, що робить його придатним до екранів будь-яких розмірів. Невдовзі ми очікуємо на перший випуск цієї програми.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) — програма для керування пакунками у операційній системі Windows. Chocolatey може встановлювати і оновлювати усі програми у системі. Ця програма спростить життя користувачам Windows.

[KDE має обліковий запис у Chocolatey](https://chocolatey.org/profiles/KDE), отже, за допомогою цієї програми ви зможете встановити digiKam, Krita, KDevelop і Kate. Процедура встановлення є дуже простою: вам достатньо віддати, наприклад, команду «choco install kdevelop», і Chocolatey виконає решту роботи. Супровідники Chocolatey намагаються підтримувати найвищі стандарти у роботі, тому ви можете бути певні щодо того, що встановлені пакунки добре перевірено, а також щодо того, що вони є безпечними.

Якщо ви є супровідником програми KDE, вам варто замислитися над додаванням до списку вашої програми. А якщо ви є адміністратором великих користувацьких мереж Windows, зверніть увагу на наші програми і додайте їх до вашого списку встановлення.

# Оновлення сайтів

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}Our web team continues to update our online presence and has recently refreshed the [KMyMoney website](https://kmymoney.org).

# Випуск 19.12.2

Деякі з наших проєктів мають власний розклад випусків, а деякі ми випускаємо у наборі. Сьогодні було випущено набір програм 19.12.2. Пакунки із програмами невдовзі мають з'явитися у крамницях програмного забезпечення та сховищах дистрибутивів. Докладніше про це на [сторінці випуску 19.12.2](https://www.kde.org/info/releases-19.12.2.php). Раніше ми називали такі набори Програмами KDE, але тепер вирішено, що це лише поточний випуск певного набору, щоб не плутати його із іншими програмами KDE. Крім того, насправді, це випуск десятка різних продуктів, а не випуск певного монолітного продукту.

Ось деякі зі виправлень, які включено до цього випуску:

* Нова версія музичного програвача [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) може працювати із файлами, у яких не міститься метаданих

* Долучення, які збережено до теки «Чернетки» більше не зникають при повторному відкритті повідомлення для редагування у [KMail](https://kde.org/applications/internet/org.kde.kmail2)

* Виправлено ваду, пов'язану із конкурентною обробкою даних, яка могла призводити до припинення показу документа у [Okular](https://kde.org/applications/office/org.kde.okular).

* Поліпшено підтримку імпортування коду мовою Java до [Umbrello](https://umbrello.kde.org/), програми для роботи зі схемами UML.

[Нотатки щодо випуску
19.12.2](https://community.kde.org/Releases/19.12_Release_Notes) &bull;
[Сторінка вікі із посиланнями на
пакунки](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[Сторінка відомостей щодо коду 19.12.2](https://kde.org/info/releases-19.12.2)
&bull; [Повний список змін у 19.12.2](https://kde.org/announcements/changelog-
releases.php?version=19.12.2)