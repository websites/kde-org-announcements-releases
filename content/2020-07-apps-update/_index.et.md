---
layout: page
publishDate: 2020-07-09 12:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's July 2020 Apps Update
type: announcement
---
# Uued väljalasked

### KTorrent 5.2.0

File sharing app [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) had a new release [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

The import improvement for sharing is [Distributed Hash Table](https://en.wikipedia.org/wiki/Mainline_DHT) improvements which now bootstraps with well-known nodes so you can get your downloads faster. Under the hood it updates to the newer QtWebengine which is based on Chrome away from the older QtWebkit based on WebKit (all of them are based on KDE's KHTML back in the day).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

KTorrent is available in your Linux distro.

### KMyMoney 5.1.0 released

Banking app [KMyMoney](https://kmymoney.org/) [released 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

It adds support for the Indian Rupee symbol: ₹. They also added the option to “Reverse charges and payments” to OFX import and the budget view now displays all account types.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[KMyMoney is available](https://kmymoney.org/download.html) in your Linux distro, as a Windows download, a Mac download and now in [Homebrew KDE](https://invent.kde.org/packaging/homebrew-kde).

### KDiff3 1.8.3 Release Notes

File comparison tool [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) released a new version 1.8.3 was released with a bunch of stability fixes.

Using KDiff3 as a difftool for Git will no longer trigger errors on non-existent files. Errors during directory comparison are properly queued so only one messagewill appear. Fixes reload on Windows. Removed a crash when clipboard is not available. Full screen toggle has been reworked to avoid a problematic Qt API call.

You can [download KDiff3](https://download.kde.org/stable/kdiff3/) for Windows, Mac and your Linux distro.

# Rakendusepood

### Microsoft Store Stats

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) gave us an update on the Microsoft store. Kate and Okular have been updated and in the last month have both had over 4000 installs.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# App Store Interview: Flathub

Flatpak is one of the new container based formats changing how we get our apps on Linux. Flatpak can work with any host who wants to set up a store but the definitive store is [Flathub](https://flathub.org/home).

Recently Flathub helper [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [asked for help putting more KDE apps in the store](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514). We interviewed him to find out more.

### Tell us about yourself, where are you from, what do you do for a living, how did you get into open source and Flatpaks?

My name is Timothée Ravier and I am currently living in Paris, France. I am a Linux system engineer and I currently work at Red Hat on Red Hat CoreOS and Fedora CoreOS.

I got into open source when I first installed a Linux distribution in 2006 and never stopped since. Most of the research projects I undertook during my studies were related to the security of Linux, application sandboxing and graphical interface security. Thus the Flatpak introduction and development piqued my interest.

In my spare time I maintain the unofficial [KDE variant (nicknamed Kinoite)](https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) of Fedora Silverblue. In short, Fedora Silverblue is an immutable operating system and the recommended way to install applications is to use Flatpaks or containers (via podman). See the documentation for more details.

### What made you put out your recent call for KDE apps in Flathub?

First I want to say a big “Thank you” to the current maintainers that already maintain KDE Apps on Flathub as they are doing a great job!

I have been a long time KDE user (I started in 2006) and I always wanted to contribute back. Distributions already have teams of established maintainers and Flathub was missing a good bunch of KDE Apps so it felt like a good place to start.

I also made a call as it will be easier if we split the work and maybe that will also make more people aware of Flatpaks and Flathub.

### Flatpak can work from any repository, why the need for Flathub?

This question highlights one of the advantages of Flathub: you can host your own repository of applications on your own server and distribute them directly to your users. You do not “need” Flathub.

But just like you do not need GitHub or GitLab, etc. to host a Git repository, it is much easier to collaborate if you have a single place to point users and developers at.

Flathub has become the easiest place to find and safely try Linux applications, both open source and proprietary. I think this is critical if we want to improve the attractiveness of the Linux ecosystem as a desktop platform.

### What other open source communities have embraced putting their apps on Flathub?

I think that a lot (maybe most) of the GNOME applications are now available on FlatHub.

### Now that the app developers can put out our software directly on stores like Flathub there are new responsibilities like security and keeping software up to date. Can you say how well these are handled into Flathub?

With Flathub, the responsibilities are shared between the Platform maintainers and the application maintainers.

The Platforms contain the core library common to a lot of applications (there are Freedesktop, GNOME and KDE platforms) and are maintained to preserve both ABI compatibility and ensure prompt security updates.

Updates to the remaining libraries required by an application and the application itself are the responsibility of the application maintainer.

### Which KDE apps do you find most useful?

I use Dolphin, Konsole, Yakuake, Okular and Ark daily and I really like them. I also appreciate and use Gwenview, KCachegrind and Massif-Visualizer from time to time.

### Many of our apps are packaged as Flatpaks through our [invent](https://invent.kde.org/packaging/flatpak-kde-applications) and [binary-factory servers](https://binary-factory.kde.org/view/Flatpak/) are you working with these processes or separately?

The Flatpaks that are built on the KDE infrastructure are intended to be nightly builds for developers and users to try out. This is a good pool of Flatpak applications to get started but some of them also need to be updated. Keeping this repository updated will help us with recent developments that may require packaging changes on Flathub. I have not yet started updating them but I will try to do it along the applications submission to Flathub.

### Can you see a time when RPMs and Apt are no more and Linux distros all use container packages?

I don’t think this will ever happen as there is value in how distributions currently package apps even though it also has issues. But I think that less distribution will put in the effort to do it. For example, Fedora builds Flatpaks from RPM packages and makes them available for everyone. You could also potentially do the same with Debian packages. The value here isn’t in the what but the who: do you trust this distribution? Its values? Its commitment to free software only? Then you are sure that the applications that you install from their repo will have the same requirements that every other package. Flathub has both open source and proprietary apps and that may not be for everybody.

# Releases Now on kde.org/applications

Our [Applications sub-site](https://kde.org/applications) has started showing release info on it. Expect more to come soon. If you are an app maintainer remember to add the release info to the Appstream files.

{{< img class="text-center" src="krita-release.png" caption="Release Info" style="width: 800px">}}

# Releases 20.04.3

Some of our projects release on their own timescale and some get released en-masse. The 20.04.3 bundle of projects was released today and will be available through app stores and distros soon. See the [20.04.3 releases page](https://kde.org/info/releases-20.04.3) for details.

Some of the fixes in today's releases:

* Previews of desktop files in Dolphin have been fixed for absolute icon paths

* Completed To-Do items are now correctly recorded in KOrganizer's journal

* Multi-line text pasted from GTK applications into Konsole no longer has extra "new line" characters

* Yakuake's maximization behavior has been fixed

[20.04 release notes](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [20.04.3 source info page](https://kde.org/info/releases-20.04.3) &bull; [20.04.3 full changelog](https://kde.org/announcements/changelog-releases.php?version=20.04.3)

