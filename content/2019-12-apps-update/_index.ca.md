---
layout: page
publishDate: 2019-12-12 13:01:00
summary: "Qu\xE8 ha passat a les aplicacions del KDE aquest mes"
title: "Actualitzaci\xF3 de les aplicacions del KDE de desembre de 2019"
type: announcement
---
# Versions noves de les aplicacions del KDE que aterren el desembre

La publicació de noves versions de les aplicacions del KDE és part de l'esforç continu del KDE per a portar-vos un catàleg complet i actualitzat de programes plens de funcionalitats, agradables i útils per al vostre sistema.

Ara hi ha disponibles versions noves del navegador de fitxers del KDE, el Dolphin. El Kdenlive, un dels editors de vídeo de codi font obert més complets. El visor de documents Okular; el visor d'imatges del KDE, el Gwenview. I totes les altres aplicacions i utilitats preferides del KDE. S'han millorat totes aquestes aplicacions, fent-les més ràpides i més estables i aporten funcionalitats noves i excitants. Les versions noves de les aplicacions del KDE permeten que sigueu més productiu i creatiu, al mateix temps que feu ús del programari fàcil i divertit del KDE.

Esperem que gaudiu de totes les funcionalitats noves i millores que trobareu a totes les aplicacions del KDE!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) recarregat

El [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) farà un gran llançament el desembre. L'eina de planificació i gestió de projectes del KDE ha arribat a una gran fita des de la versió anterior que es va publicar fa gairebé dos anys.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

El Plan us ajuda a gestionar projectes petits i grans amb recursos múltiples. Per tal de modelar el projecte, el Plan ofereix tipus diferents de dependències de tasques i restriccions de temps. Podeu definir les tasques, estimar l'esforç necessari per a portar-les a terme, assignar recursos i després planificar el projecte segons les necessitats i els recursos disponibles.

Una de les fortaleses del Plan és el seu excel·lent suport dels diagrames de Gantt. Els diagrames de Gantt són diagrames fets amb barres horitzontals que proporcionen una il·lustració gràfica d'una planificació i ajuden a planificar, coordinar i seguir tasques específiques d'un projecte. Usant els diagrames de Gantt al Plan us ajudarà a controlar millor el flux de treball del projecte.

## Augmenta el volum amb el [Kdenlive](https://kdenlive.org)

Els desenvolupadors del [Kdenlive](https://kdenlive.org) han afegit funcionalitats noves i eliminat fallades a tota velocitat. Només aquesta versió acumula més de 200 «commits».

S'ha dedicat molta feina a millorar el funcionament de l'àudio. El departament d'«errors corregits» s'ha desembarassat d'un problema d'alt consum de memòria, d'ineficiència de les miniatures d'àudio, i s'ha accelerat el seu emmagatzematge.

Però el que encara és més excitant és que el Kdenlive ara té un espectacular mesclador de so nou (vegeu la imatge). Els desenvolupadors també han afegit una pantalla nova de clips d'àudio al monitor de clips i a la safata del projecte, de manera que es poden sincronitzar millor les imatges en moviment i la pista de so.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Mesclador d'àudio del Kdenlive" >}}

Al departament de «resolució de defectes», el Kdenlive ha obtingut moltes millores de rendiment i usabilitat, i els desenvolupadors han corregit molts errors a la versió per a Windows de l'editor de vídeo.

## Vistes prèvies i navegació del [Dolphin](https://userbase.kde.org/Dolphin)

El potent navegador de fitxers del KDE [Dolphin](https://userbase.kde.org/Dolphin) afegeix noves funcionalitats per a ajudar-vos a cercar i trobar allò que esteu cercant al sistema de fitxers. Una d'aquestes funcionalitats és el redisseny de les opcions de cerca avançada i una altra és que ara es pot anar enrere i endavant a l'historial de llocs que ja hàgiu visitat diverses vegades mitjançant un clic llarg a la icona de la fletxa de la barra d'eines. La funcionalitat que permet arribar ràpidament als fitxers accedits recentment o desats recentment s'ha refet i els defectes s'han polit.

Una de les principals preocupacions dels desenvolupadors del Dolphin és permetre que els usuaris previsualitzin el contingut dels fitxers abans d'obrir-los. Per exemple, a la versió nova del Dolphin es poden previsualitzar els GIF ressaltant-los i després passant-hi per sobre de la subfinestra de vista prèvia. També es pot fer clic i reproduir fitxer de vídeo i àudio a la subfinestra de vista prèvia.

Si sou un aficionat a les novel·les gràfiques, ara el Dolphin pot mostrar miniatures dels llibres de còmic «.cb7» (comproveu el suport per a aquest format de llibres de còmic també a l'Okular - vegeu a sota). I parlant de miniatures, el zoom a les miniatures ha estat força temps en el Dolphin: manteniu premuda <kbd>Ctrl</kbd>, gireu la roda del ratolí i la miniatura creixerà o minvarà. Ara hi ha una funcionalitat nova per a reiniciar el zoom al nivell predeterminat prement <kbd>Ctrl</kbd> + <kbd>0</kbd>.

Una altra funcionalitat útil és que el Dolphin mostra els programes que eviten que es desmunti un dispositiu.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): permet que el telèfon controli l'escriptori

La darrera versió del [KDE Connect](https://community.kde.org/KDEConnect) ve farcida amb funcionalitats noves. Una de les més destacades és que hi ha una aplicació nova de SMS que permet llegir i escriure SMS amb l'historial de conversacions complet.

Una nova interfície d'usuari basada en el [Kirigami](https://kde.org/products/kirigami/) vol dir que es pot executar el KDE Connect no només a l'Android, sinó també a totes les plataformes Linux mòbils que es veuran en els propers dispositius com el PinePhone i el Librem 5. La interfície del Kirigami també proporciona funcionalitats noves per als usuaris d'escriptori a escriptori, com a controlador de mitjans, entrada remota, trucades a dispositiu, transferència de fitxers i execució d'ordres.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="La interfície nova a l'escriptori" >}}

Ja podíeu usar el KDE Connect per a controlar el volum de la reproducció de mitjans a l'escriptori, però ara podeu usar-lo per a controlar el volum global del sistema des del telèfon -molt útil per a usar el telèfon com a remot. En donar una xerrada, també podeu controlar la presentació usant el KDE Connect per a fer avançar i retrocedir les dispositives.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Una altra funcionalitat que ja ha estat implementada des de fa temps és que podeu usar el KDE Connect per a enviar un fitxer al telèfon, però el que és nou en aquesta versió és que podeu obrir el fitxer immediatament al telèfon una vegada s'ha rebut el fitxer. El KDE Itinerary usa això per a enviar la informació del viatge al telèfon des del KMail.

De manera relacionada, ara podeu enviar fitxers des del Thunar (el gestor de fitxers del Xfce) i les aplicacions de l'Elementary com el Pantheon Files. S'ha millorat la visualització del progrès de la còpia de fitxers múltiples i ara els fitxers rebuts des de l'Android mostren la seva hora de modificació correcta.

Quan es parla de les notificacions, ara es poden activar accions des de les notificacions de l'Android a l'escriptori, i el KDE Connect usa funcionalitats avançades des del centre de notificacions del Plasma per a proporcionar millors notificacions. La notificació de confirmació d'aparellament no venç, així que no la tornareu a perdre.

Finalment, si utilitzeu el KDE Connect des de la línia d'ordres, el *kdeconnect-cli* ara té una autocompleció millorada del *zsh*.

## Imatges remotes [Gwenview](https://userbase.kde.org/Gwenview)

Ara el [Gwenview](https://userbase.kde.org/Gwenview) permet ajustar el nivell de compressió del JPEG en desar imatges que hàgiu editat. Els desenvolupadors també han millorat el rendiment de les imatges remotes i ara el Gwenview pot importar fotos a o des d'ubicacions remotes.

## L'[Okular](https://kde.org/applications/office/org.kde.okular) admet el format «.cb7»

L'[Okular](https://kde.org/applications/office/org.kde.okular), el visor de documents que permet llegir, anotar i verificar tota classe de documents, inclosos PDF, EPub, ODT i MD, ara també permet els fitxers que usen el format de llibre de còmic «.cb7».

## L'[Elisa](https://kde.org/applications/multimedia/org.kde.elisa): un reproductor musical senzill però complet

L'[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) és un reproductor musical del KDE que combina la simplicitat amb una interfície elegant i moderna. A la seva darrera versió, l'Elisa ha optimitzat el seu aspecte per a adaptar-se millor a les pantalles de ppp alt. També s'integra millor amb altres aplicacions del KDE i ha incorporat la compatibilitat per al sistema de menú global del KDE.

També s'ha millorat la indexació dels fitxers de música i ara l'Elisa permet ràdios web i es distribueix amb varis exemples per a provar-ho.

{{< img src="elisa.png" caption="Interfície actualitzada de l'Elisa" >}}

## L'[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle) està preparat per a les pantalles tàctils

L'[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), el programa de captura de pantalla predeterminat del KDE, és una altra aplicació del KDE que ha evolucionat per a incorporar capacitats de pantalla tàctil: la seva nova maneta d'arrossegament tàctil fa més fàcil arrossegar rectangles per a capturar les parts de l'escriptori que voleu en els dispositius tàctils.

Altres millores són la funció de desament automàtic, la qual és útil per a prendre ràpidament múltiples captures de pantalla, i les barres de progrés animades, les quals fan més segur que veureu el que està fent l'Spectacle.

{{<img style="max-width:500px" src="spectacle.png" caption="La maneta nova d'arrossegament de l'Spectacle" >}}

## Millores a la [Integració del navegador del Plasma](https://community.kde.org/Plasma/Browser_Integration)

El nou llançament de la [Integració del navegador del Plasma](https://community.kde.org/Plasma/Browser_Integration) ara permet una llista negra de la funcionalitat dels controls multimèdia. Això és útil quan visiteu un lloc amb molts medis que podeu reproduir, i que fa difícil seleccionar el que voleu controlar des de l'escriptori. La llista negra permet excloure aquests llocs dels controls multimèdia.

La nova versió també permet guardar l'URL d'origen a les metadades del fitxer i afegeix la implementació per a l'API Web Share. L'API Web Share permet que els navegadors comparteixin enllaços, text i fitxers amb altres aplicacions de la mateixa manera que ho fan les aplicacions del KDE, millorant la integració dels navegadors no natius com el Firefox, el Chrome/Chromium i el Vivaldi, amb la resta de les aplicacions del KDE.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

Algunes aplicacions del KDE es poden baixar des de la Microsoft Store. El Krita i l'Okular hi han estat durant un temps, i ara s'ha afegit el [Kile](https://kde.org/applications/office/org.kde.kile), un editor de LaTeX fàcil d'usar.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Nou

El que ens porten les novetats: [SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), una aplicació que permet crear amb facilitat subtítols per a vídeos, ara es troba al KDE Incubator, a la ruta per a esdevenir un membre complet de la família d'aplicacions del KDE. Benvinguda!

Mentrestant, el [plasma-nano](https://invent.kde.org/kde/plasma-nano), una versió mínima del Plasma dissenyada per a dispositius incrustats s'ha mogut als repositoris del Plasma en preparació per al llançament de la 5.18.

## Llançament 19.12

Diversos projectes publiquen en la seva pròpia escala de temps i altres es
publiquen conjuntament. El paquet de projectes 19.12 s'ha publicat avui i aviat
hauria de ser disponible a través de les botigues d'aplicacions i de les
distribucions. Vegeu la [pàgina del llançament
19.12](https://www.kde.org/announcements/releases/19.12). Aquest paquet s'havia
anomenat prèviament Aplicacions del KDE però s'ha descatalogat per a esdevenir
un servei de llançaments per a evitar confusions amb totes les altres
aplicacions del KDE i perquè són una dotzena de productes diferents en lloc d'un
únic grup.