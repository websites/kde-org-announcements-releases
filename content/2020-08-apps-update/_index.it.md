---
draft: false
layout: page
publishDate: '2020-08-13T14:00:00+02:00'
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di agosto 2020
type: announcement
---
Decine di [applicazioni KDE](https://kde.org/applications/) sono state oggetto di nuovi rilasci dal servizio dei rilasci di KDE. Le nuove funzionalità, i miglioramenti nell'utilizzo, la riprogettazione e la correzione di errori contribuiscono nel complesso ad aumentare la produttività e rendere questo nuovo lotto di applicazioni più efficiente e facile da usare.

Ecco cosa puoi aspettarti di trovare:

## [Dolphin](https://kde.org/applications/en/system/org.kde.dolphin)

{{< img class="text-center" src="dolphin_preview.webp" caption="Dolphin mostra le anteprime di molti tipi di file." style="width: 800px" >}}

Dolphin è il navigatore di file KDE che ti aiuta a cercare, copiare e aprire file e cartelle. Una delle sue funzioni principali, dunque, è fornirti un'idea chiara di cosa contiene ciascun file. L'anteprima delle miniature è cruciale per questo, e Dolphin è in grado di mostrarti, da molto tempo ormai, le anteprime delle immagini, delle clic video clips e persino dei file 3D di Blender.

In questa nuova versione Dolphin aggiunge all'elenco le miniature per i file 3D Manufacturing Format (3MF) e ti permette di visualizzare le anteprime di file e cartelle nei sistemi di file cifrati, come il Caveau di Plasma. Ciò viene eseguito in sicurezza memorizzando le miniature in cache sullo stesso file system o tornando indietro per generarle e non memorizzando le versioni in cache, se necessario. Con entrambi i metodi hai comunque sempre il controllo assoluto di come e quando Dolphin mostrerà il contenuto dei tuoi file, perché puoi configurare indipendentemente il limite di dimensione del file per la visualizzazione delle anteprime per i file sia remoti, sia locali.

Un altro metodo di identificazione dei file è quello per nome. Nel caso in cui il nome del file sia troppo lungo da visualizzare, gli sviluppatori hanno perfezionato il modo in cui viene accorciato il nome. Anziché tagliare a metà il nome lungo di un file o di una cartella, come faceva Dolphin nelle versioni precedenti, la nuova versione taglia la fine e mantiene sempre visibile l'estensione del file, se presente, dopo i puntini. Questo metodo rende i file con nome lungo molto più facili da individuare.

Dolphin ora ricorda e ripristina sia la posizione in cui stavi visualizzando, sia le schede aperte, sia le viste divise aperte dall'ultima chiusura del programma. Questa funzionalità è attiva in modo predefinito ma può essere disattivata nella pagina «Avvio» della finestra delle «Impostazioni» di Dolphin.

Spostandoci ai miglioramenti nell'utilizzo, Dolphin ti ha sempre permesso di montare e navigare le condivisioni remote tramite FTP, SSH o altri protocolli. In aggiunta, ora Dolphin mostra i montaggi remoti e FUSE con nomi intuitivi invece di usare l'intero percorso. Ancora meglio: puoi montare le immagini ISO utilizzando una nuova voce di menu presente nel menu contestuale. Ciò significa che puoi scaricare ed esplorare un file system da masterizzare successivamente su un disco o una penna USB.

{{< video src="dolphin_ISO.mp4" caption="Dolphin ti permette di montare le immagini ISO." style="max-width: 900px" >}}

Dolphin possiede un'impressionante numero di funzioni, ma puoi aggiungerne altre ancora mediante le estensioni. Prima, se volevi aggiungerne una tramite le estensioni del menu «Servizi» (guarda in <I>Impostazioni</I> > <I>Configura Dolphin</I> > <I>Servizi</I>) spesso dovevi eseguire uno script o installare un pacchetto della distribuzione. Ora puoi installarle direttamente dalla finestra «Scarica nuovi servizi», senza passaggi intermedi. Sempre nella pagina delle impostazioni «Servizi» è presente in alto il nuovissimo campo di ricerca che ti aiuta a trovare rapidamente quello che stai cercando, e l'elenco dei servizi è ora disposto in ordine alfabetico.

Spostare gli elementi è più facile nel nuovo Dolphin, dato che sono presenti nuove azioni per spostare o copiare rapidamente i file selezionati in un panello della vista divisa all'interno della cartella visualizzata nell'altro pannello. Per rendere più chiara l'operazione, quando trascini un file il puntatore ora diventa in modo predefinito una mano che trascina, anziché essere il puntatore «copia in corso» presente nelle versioni precedenti.

Puoi anche calcolare e visualizzare le dimensioni su disco delle cartelle utilizzando la vista Dettagli, mentre il pannello «Informazioni» di Dolphin ora mostra informazioni utili per il Cestino. È anche possibile navigare dal campo di ricerca di Dolphin fino alla vista dei risultati premendo il tasto Freccia giù.

Per finire, una nuova e utile funzionalità di Dolphin 20.08 è il nuovo elemento di menu «Copia posizione»...

## [Konsole](https://kde.org/applications/en/system/org.kde.konsole)

{{< img class="text-center" src="konsolecolors.webp" caption="Konsole possiede nuove emozionanti funzionalità." style="width: 800px" >}}

… proprio come nel terminale di KDE, Konsole! Questo significa che puoi fare clic col destro su un elemento mostrato in Konsole, fare clic su "Copia posizione" dal menu a comparsa, poi copiare e incollare l'intero percorso del file o della cartella all'interno di un documento, un altro terminale o un'applicazione.

Konsole possiede la nuova funzionalità di evidenziazione delle nuove righe che sono visualizzate quando scorri rapidamente l'output del terminale e, per impostazione predefinita, ti mostra un'anteprima per i file immagine su cui passi sopra il puntatore del mouse. Inoltre, quando fai clic col destro su un file sottolineato, il menu contestuale visualizza il comando «Apri con» per permetterti di aprire il file nell'applicazione di tua scelta.

La capacità di dividere la vista di Konsole e aggiundere schede per lavorare contemporaneamente su più fronti è una funzionalità di vecchia data che ora può essere disabilitata e lo spessore del separatore aumentato. Puoi anche monitorare una scheda in cui è presente un processo attivo da completare e assegnare colori alle schede!

{{< img class="text-center" src="konsole-monitor.webp" caption="Nuovo monitoraggio in Konsole" style="width: 500px" >}}

Relativamente alle schede, è stata rimossa la scorciatoia predefinita di Konsole «Ctrl+Maiusc+L» per il comando «Stacca la scheda attuale». È stata eliminata in quanto è stato accertato che era troppo facile staccare accidentalmente la scheda quando in realtà l'operazione richiesta era ripulire lo schermo utilizzando la scorciatoia Ctrl+Maiusc+K.

Un'ultima miglioria, in questa versione, relativa all'usabilità è l'adattamento dinamico alla dimensione del carattere del cursore I-beam di Konsole.

## [Yakuake](https://kde.org/applications/en/system/org.kde.yakuake)

{{< img class="text-center" src="yakuake.webp" caption="Nuova icona Yakuake" style="width: 200px" >}}

Sempre con riferimento ai terminali, Yakuake è l'emulatore di terminale a comparsa di KDE. Si srotola a ogni pressione del tasto F12 e in questo rilascio è stato notevolmente migliorato. Per esempio, ora ti permette di configurare tutte le scorciatoie da tastiera di Konsole ed è presente un nuovo elemento nel vassoio di sistema che indica quando Yakuake è in esecuzione. Ovviamente puoi nasconderlo, ma permette di segnalare in modo più facile la sua presenza e prontezza. Fornisce inoltre anche un metodo grafico per richiamarlo.

Abbiamo anche migliorato Yakuake in Wayland. La finestra principale compariva sotto i pannelli superiori visulizzati, ma questo è stato corretto.

## [Digikam](https://kde.org/applications/en/graphics/org.kde.digikam) 7.0.0

DigiKam è l'applicazione KDE per la gestione professionale delle fotografie e [il suo team ha appena rilasciato la nuova versione principale 7.0](https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4).

{{< img class="text-center" src="digikam-face.webp" caption="Riconoscimento facciale di DigiKam" style="width: 800px" >}}

Una delle novità principali di digiKam 7.0.0 è il miglioramento del riconoscimento facciale, che ora utilizza algoritmi IA con apprendimento avanzato. Puoi visionare un talk dell'Akademy dello scorso anno per capire come è stato concepito, e ci saranno ulteriori miglioramenti al riconoscimento facciale e il rilevamento automatico nelle versioni future.

{{< video src="https://files.kde.org/akademy/2019/127-AI_Face_Recognition_with_OpenCV_in_digiKam.mp4" caption="Riconoscimento facciale di DigiKam" style="max-width: 800px" controls="true" >}}

Il [pacchetto Flatpak principale di digiKam è disponibile in Flathub](https://flathub.org/apps/details/org.kde.digikam), e pure nightly build non stabili per i tester. DigiKam è disponibile pure per Linux tramite AppImages e nei repository della tua distribuzione. Ci sono anche versioni digiKam per Windows e Mac.

## [Kate](https://kde.org/applications/en/utilities/org.kde.kate)

Abbiamo fatto un paio di ritocchi all'aspetto del nostro editor di testo, Kate. Il menu «Apri recente» ora mostra i documenti aperti in Kate dalla riga di comando e da altre fonti, e non solo quelli aperti tramite la finestra di dialogo. Un'altra modifica è relativa alla barra delle schede, che ora è visivamente coerente con tutte le barre delle schede delle altre applicazioni KDE e ora apre le nuove schede sulla destra, come normalmente fa la maggior parte delle barre.

## [Elisa](https://kde.org/applications/en/multimedia/org.kde.elisa)

{{< img class="text-center" src="elisa.webp" caption="Elisa è un riproduttore musicale leggero per desktop e dispositivi mobili." style="width: 800px" >}}

Elisa è un riproduttore musicale leggere che funziona col tuo desktop e il tuo dispositivo mobile. Elisa ora ti permette di visualizzare tutti i generi, gli artisti o gli album nella barra laterale, sotto gli altri elementi. La scaletta inoltre mostra l'avanzamento del brano in riproduzione e la barra superiore ora è dinamica in relazione alla dimensione della finestra e al fattore di forma del dispositivo. Questo significa che appare bene con un'orientazione verticale della finestra, o del dispositivo (per esempio, sul tuo telefono), e si ridimensiona in scala davvero piccola.

## [KStars](https://kde.org/applications/en/education/org.kde.kstars) 3.4.3

{{< img class="text-center" src="kstars-main-screen.webp" caption="KStars è l'applicazione KDE per gli appassionati di astronomia." style="width: 800px">}}

Infine, l'appliczione di astronomia KStars ha aggiunto nel [nuovo rilascio](http://knro.blogspot.com/2020/07/kstars-v343-is-released.html) fuoco e calibrazione. [La maschera Bahtinov è un dispositivo utlizzato per mettere accuratamente a fuoco i telescopi astronomici piccoli](https://en.wikipedia.org/wiki/Bahtinov_mask). È utile a quegli utenti che non hanno una messa a fuoco motorizzata e preferiscono mettere a fuoco con metodo manuale e con l'aiuto della maschera. Dopo aver scattato un'immagine nel modulo fuoco e con l'algoritmo per la maschera selezionato, Ekos analizzerà le immagini e le stelle al suo interno. Se Ekos riconosce il modello di stelle Bahtinov, disegnerà le linee sul modello di stelle con cerchi al centro e con scostamento per indicare il fuoco.

{{< img class="text-center" src="kstars-focus.webp" caption="Nuovo algoritmo per il fuoco di KStars" style="width: 600px">}}

{{< img class="text-center" src="kstars-calibration.webp" caption="Calibrazione KStars" style="width: 600px" >}}

È stata aggiunta la scheda secondaria «Tracciato calibrazione» alla destra del «Tracciato deriva». Mostra le posizioni di montaggio registrate durante la calibrazione della guida interna. Se tutto è ok, dovrebbero essere visualizzati punti in due linee ad angolo retto l'un l'altro, uno quando la calibrazione spinge indietro e in avanti lungo la direzione AR, poi quando esegue la stessa operazione per la direzione DEC. Non molte informazioni, ma utili per visualizzare. Se le due linee hanno un angolo di 30 gradi, qualcosa nella calibrazione non ha funzionato! Sopra un'immagine delle calibrazione in azione utilizzando il simulatore.

[KStars è disponibile per lo scaricamento](https://edu.kde.org/kstars/) per Android, dallo store Snapcraft, per Windows, macOS e, naturalmente, dai repository della tua distribuzione Linux.

## [KRDC](https://kde.org/applications/en/internet/org.kde.krdc)

{{< video src="krdc.mp4" caption="KRDC con puntatore remoto" style="max-width: 900px" >}}

KRDC ti permette di visualizzare e controllare la sessione desktop di un'altra macchina. La versione rilasciata oggi ora visualizza correttamente i cursori lato server in VNC anziché un puntino col puntatore remoto in ritardo dietro di esso.

## [Okular](https://kde.org/applications/en/graphics/org.kde.okular)

Okular è il visore di documenti di KDE. Ti permette di leggere documenti PDF, libri ePUB e molti altri tipi di file basati su testo. È stata introdotta una correzione che ha riportato i comandi «Stampa» e «Anteprima di stampa» di nuovo uno vicino all'altro nel menu «File».

## [Gwenview](https://kde.org/applications/en/graphics/org.kde.gwenview)

Gwenview è un'applicazione per la visualizzazione delle immagini con funzionalità di modifica, quali il ridimensionamento e il ritaglio. La nuova versione salva la dimensione dell'ultimo riquadro di ritaglio, il che significa che puoi ritagliare rapidamente in successione più immagini alla stessa dimensione.

# Correzioni di errori

## [Skanlite](https://kde.org/applications/en/graphics/org.kde.skanlite) [2.2.0](http://download.kde.org/stable/skanlite/2.2/skanlite-2.2.0.tar.xz.mirrorlist)

* Il salvataggio è stato spostato in un thread in modo da evitare che l'interfaccia si blocchi durante il salvataggio

* È stata implementata un'interfaccia D-Bus per i tasti di scelta rapida e il controllo delle scansioni

## [Spectacle](https://kde.org/applications/en/utilities/org.kde.spectacle)

* La funzione timer di Spectacle e le combinazioni di tasti Maiusc + Stamp (per la schermata a schermo intero) e Meta + Maiusc + Stamp (per la schermata di regione rettangolare) ora funzionano in Wayland.

* Spectacle non include più, per impostazione predefinita, il puntatore del mouse nelle schermate.

## [Okteta](https://kde.org/applications/en/utilities/org.kde.okteta) 0.26.4

* struct2osd usa ora castxml (gccxml è deprecato).

* Viene fatto meno uso di codice Qt deprecato, evitando avvisi a tempo di esecuzione.

* Traduzioni migliorate.