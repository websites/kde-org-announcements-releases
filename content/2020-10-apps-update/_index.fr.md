---
draft: false
layout: page
publishDate: '2020-09-10T12:00:00+00:00'
summary: Quoi de neuf ce mois-ci avec les applications de KDE.
title: "Mise \xE0 jour des applications de KDE de Octobre 2020"
type: announcement
---
Après Akademy, un mois exceptionnel de nouvelles mises à jour de logiciels et de corrections de bogues a suivi pour les applications de KDE.

# Nouvelles mises à jour

## DigiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="DigiKam" style="width: 600px">}}

Le mois a démarré avec la mise à jour de notre populaire [application de gestion de photos, digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

Une meilleure prise en charge des métadonnées « Canon CR3 », des formats d'images « RAW » venant des appareils photos sont souvent non documentés et difficiles à traiter. Les développeurs de digiKam 7.1.0 ont écrit une interface de métadonnées reposant sur « libraw » pour le « CR3 ». L'application est maintenant capable de lire une large partie des étiquettes « Exif », incluant les informations « GPS », le profil de couleurs et, bien sûr, les conteneurs « IPTC » et « XMP ».

Un nouveau module externe de gestion des files de lots de travaux a été introduit vous permettant d'appliquer des textures sur des images. Une autre amélioration corrige automatiquement les pixels « chauds », c'est-à-dire les « mauvais pixels » que vous obtenez lors de l'utilisation d'une vitesse d'obturation lente ou lors de travaux en images de nuit.

Le [modèle d'information d'échange « IPTC » Information Interchange Model](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) est l'ancien mais toujours utilisé format commun de métadonnées pour les photos. Cette version améliore la prise en charge « Unicode » pour lui.

digiKam est disponible pour votre distribution Linux comme une archive compressée des sources, des paquets « AppImage » en 32 / 64 bits, des paquets « MacOS » et des installateurs « Windows » 32 / 64 bits. [digiKam 7.1.0 peut être téléchargé à l'adresse ](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

LabPlot est une application KDE pour le tracé interactif et l'analyse de données scientifiques. Labplot fournit une façon facile de créer, de gérer et de modifier les tracés ainsi que de réaliser des analyses de données. [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) a été publiée après une année de travail.

Dans la version 2.8, il est maintenant facile d'accéder à de nombreuses ressources en ligne, fournissant des exemples pour des besoins éducatifs. Il y a maintenant cinq collection avec plus de 2000 exemples documentés de données. Veuillez regarder cette vidéo pour plus d'informations :

{{< youtube pyP-5J3seQ8 >}}

Cette mise à jour arrive avec deux nouveaux objets de feuilles de travail : des lignes de référence et des éléments d'images. Les lignes de références peut être librement positionnées sur le tracé pour mettre en valeur certaines valeurs dans les données affichées. Les éléments d'images vous permettent d'ajouter une image sur le tracé ou sur la feuille de travail.

La feuille de calcul fournit des statistiques détaillées pour les ensembles de données. Cette information est étendue et ajoutée pour les calculs de quartile et dans le mode de statistiques.

En remerciement du travail effectué par Cantor durant ces derniers mois, les développeurs ont été capables d'ajouter la compatibilité avec les fichiers des projets « Jupyter » dans cette version de LabPlot.

Plus d'effort a été porté dans la version « MacOS » de Labplot. A coté de nombreux petites corrections spécifiques à « MacOs », la prise en charge de la barre tactile a aussi été ajoutée, maintenant disponible sur les tout nouveaux modèles « MacBook Pro ».

[Labplot peut être téléchargé à l'adresse ](https://labplot.kde.org/download/) for Linux, Windows et Mac.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

Notre environnement de développement intégré « IDE » [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released), arrive avec six mois de développement, avec un objectif centré sur la stabilité, la performances et la maintenance évolutive. Il acquière une fonctionnalité utile [d'affichage en ligne de notes pour des problèmes en fin de ligne](https://blog.david-redondo.de/kde/2020/02/28/problems.html).

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

L'application d'agenda pour périphériques mobiles [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html) est publiée. Ainsi, un ensemble de version a aussi été défini, ceci est la première version stable de Calindori comme application de KDE.

Calindori est disponible sur KDE Neon pour Plasma Mobile, comme aussi sous postmartketOS. Il est aussi disponible dans les dépôts expérimentaux « flatpak » de « kdeapps » pour « ARM » et « x84_64 ». Enfin, vous pouvez aussi le compiler à partir des sources sur une station de travail sous bureau Linux. Le code source et les signatures peuvent être téléchargés à partir de [download.kde.org](https://download.kde.org/stable/calindori/).

## Kid3 3.8.4

[Kid3](https://invent.kde.org/websites/kid3-kde-org) améliore votre application d'étiquettage pour vos fichiers de musique.

En plus des corrections de bogues, cette mise à jour propose des améliorations d'ergonomie comme plus de raccourcis clavier de navigation et la possibilité de les personnaliser. La détection de type de fichiers est plus tolérant.

Obtenez le à partir du [paquet « Windows Chocolatey »](https://chocolatey.org/packages/kid3/), du [paquet macOS Homebrew Package](https://formulae.brew.sh/cask/kid3), du [paquet Android F-Droid](https://f-droid.org/en/packages/net.sourceforge.kid3/), de [Flatpak](https://flathub.org/apps/details/org.kde.kid3), [macOS binary](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download), du binaire [Windows](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download)

# Corrections de bogues

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

Pour les développeurs externes, une [version corrective de « Heaptrack »](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0) est disponible. « Heaptrack » trace les allocations de mémoire et annote ces évènements avec des traces de pile. Des outils dédiés d'analyse vous permettent ainsi d'interpréter le profil de mémoire de la pile.

Il y a des améliorations de la stabilité de « Heaptrack », le rendant plus résistant aux erreurs lors de l'enregistrement de données. De plus, certains réglages mineurs dans l'outil graphique d'analyse le rendent plus efficace dans son utilisation. La performance dans l'étape d'analyse a été aussi légèrement améliorée. Enfin, un bogue important dans l'algorithme de comparaison de données a été corrigé, ce qui devrait rendre cette fonctionnalité plus utile une nouvelle fois.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

KDiff3 vous permet de comparer jusqu'à trois fichiers de texte en mème temps.

(Cette mise à jour)[https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html] corrige les erreurs typographiques, le rechargement de fichiers sous Windows et la copie sur le réseau.

KDiff3 peut être [télécharger](https://download.kde.org/stable/kdiff3/) pour Linux, Mac et Windows.

## Tellico 3.3.3

Le gestionnaire de collection [Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) a amélioré la source de données « Entrez » (Pubmed) pour l'utilisation d'une clé « API » lorsque disponible et pour respecter la limite de taux. Les développeurs ont aussi améliorés la source de données « ISDBdb » pour la recherche de valeurs multiples « ISBN ».

## Konversation 1.7.6

L'application de discussion « IRC » [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) a reçu un correction de bogue lui permettant d'être de compiler avec plus de versions « Qt » et corrigeant le chargement d'un thème d'icônes de pseudos.

## Module externe pour l'affichage « Markdown »

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="Truc" style="width: 600px">}}

Une paire de modules externes ont été mis à jour ce mois-ci. [L'afficheur « Markdown » KPart](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) et [KMarkdownWebView](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) sont livrés avec de nouvelles versions, avec de nouvelles traductions et la correction des compilations avec d'anciennes versions « Qt ». Ces modules externes donnent tous les deux des aperçus des fichiers formatées en « Markdown » pour Kate et d'autres applications de KDE.

# Entrant

KTechLab, un environnement de développement intégré « IDE » pour les micro-contrôleurs et l'électronique, est une ancienne application, n'ayant pas été mise à jour depuis longtemps car elle utilisait d'anciennes technologies. Le code de base de KTechLab a été mis à jour et la [version 0.50.0](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) est maintenant disponible pour test.

# Mises à jour 20.08.2

Les mises à jour de certains projets se font selon leurs propres plannings et quelques uns sont mis à jour de façon synchronisée. Le groupe de projets pour la version 20.08.2 a publié leurs mises à jour aujourd'hui. avec des douzaines de corrections de bogues. Celles-ci devraient être disponibles très bientôt dans les dépôts d'applications et dans les distributions. Veuillez consulter [la page des mises à jour 20.08.2] (https://www.kde.org/info/releases-20.08.2.php) pour plus de détails.

Certaines de ces corrections de la mise à jour de ce jour :

* Le bogue qui masquait les éléments de l'interface utilisateur dans l'éditeur de texte Kate après fermeture d'un onglet a été corrigé.

* Le téléchargement des documents de bureautique grâce à l'intégration de « GDrive » dans Dolphin ne se plante plus durant la gestion de la redirection nécessaire vers le serveur.

* Le défilement diagonal (par exemple avec les pavés tactiles) dans un terminal de Konsole a été corrigé.

* L'afficheur de documents, Okular, vous permet maintenant de cliquer sur les hyperliens qui sont recouverts par les annotations.

* Dans Kontact, la configuration de l'archivage automatique de courriels ou le transfert de messages en ligne ne provoque plus de plantage.

* Maintenant, le découpage automatique lors des partages de scènes dans l'éditeur de vidéo, Kdenlive, coupe actuellement les vidéos. De plus, l'ouverture de projets avec des clips vidéo manquants a été rendu plus robuste.

* L'importateur d'images de Gwenview ne se fige plus après fermeture

* L'outil Krfb, de partage de bureau distant, gère maintenant correctement les écrans étendus.

* Les connexions aux téléphones avec « KDE Connect » sont plus robustes après une revue de la sécurité.

[Notes de la version 20.08]
(https://community.kde.org/Releases/20.08_Release_Notes) • [Page de tutoriel
pour le téléchargement de paquets]
(https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) • [Page
d'informations sur les sources de la version 20.08.2]
(https://kde.org/info/releases-20.08.2) • [Liste complète des modifications pour
la version 20.08.2] (https://kde.org/announcements/changelog-
releases.php?version=20.08.2)