---
layout: page
publishDate: 2019-11-29 00:01:00
summary: "Plus de 120 programmes individuels, plusieurs douzaines de biblioth\xE8\
  ques de programmes et des modules externes ont \xE9t\xE9 mis \xE0 jour de fa\xE7\
  on simultan\xE9e, dans le cadre du service de mise \xE0 jour de KDE."
title: "Versions \xAB\_RC\_\xBB (Release Candidate) 19.12"
type: announcement
---
29 Novembre 2019. plus de 120 programmes individuels, plusieurs douzaines de bibliothèques de programme et des modules externes ont été mis à jour de façon simultanée, dans le cadre du service de mise à jour de KDE. 

Aujourd'hui, ils ont tous reçus les sources candidats à la mise à jour. Cela signifie que toutes leurs fonctionnalités sont prêtes mais qu'elles ont besoin d'être testées pour la correction des derniers bogues.

Les responsables de paquets de distributions et de magasins d'applications devraient mettre à jour leurs canaux de prédiffusion pour permettre la détection de problèmes potentiels.

* [Notes de mises à jour de la version 19.12](https://community.kde.org/Releases/19.12_Release_Notes) pour plus d'informations concernant les archives et les problèmes connus.

+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)

+ [19.12 RC source info page](https://kde.org/info/applications-19.11.90)

## Contacts « Presse »

Pour plus d'informations, nous envoyez un courriel à [press@kde.org]
(mailto:press@kde.org).