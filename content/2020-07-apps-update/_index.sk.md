---
layout: page
publishDate: 2020-07-09 12:00:00
summary: What Happened in KDE's Applications This Month
title: "Aktualiz\xE1cia aplik\xE1ci\xED KDE z j\xFAla 2020"
type: announcement
---
# Nové vydania

### KTorrent 5.2.0

Aplikácia na zdieľanie súborov [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) mala nové vydanie [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

Zlepšenie importu pre zdieľanie je vylepšené pomocou [Distributed Hash Table](https://en.wikipedia.org/wiki/Mainline_DHT), ktoré teraz zavádza známe uzly, takže môžete rýchlejšie sťahovať súbory. Pod kapotou bol aktualizovaný na novší QtWebengine, ktorý je založený na prehliadači Chrome, namiesto staršieho QtWebkit založenom na WebKit (všetky boli kedysi založené na KDE KHTML).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

KTorrent je k dispozícii vo vašej Linuxovej distribúcií.

### Vydané KMyMoney 5.1.0

Banková aplikácia [KMyMoney](https://kmymoney.org/) [verzie 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

Pridáva podporu pre indický symbol Rupie: ₹. Pridáva tiež možnosť „Vrátiť poplatky a platby“ do importu OFX a zobrazenie rozpočtu teraz zobrazuje všetky typy účtov.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[KMyMoney je dostupné](https://kmymoney.org/download.html) vo vašej Linuxovej distribúcií, na stiahnutie pre Windows alebo Mac a teraz tiež v [Homebrew KDE](https://invent.kde.org/packaging/homebrew-kde).

### Poznámky k vydaniu KDiff3 1.8.3

Nástroj na porovnávanie súborov [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) má novú verziu 1.8.3 s množstvom opráv stability.

Použitie KDiff3 ako diftoolu pre Git už nespustí chyby na neexistujúcich súboroch. Chyby pri porovnávaní adresárov sú správne zaradené do poradia, takže sa zobrazí iba jedna správa. Opravy pri opätovnom načítavaní v systéme Windows. Odstránenie zlyhania, keď schránka nie je k dispozícii. Prepínanie na celú obrazovku bolo prepracované, aby sa predišlo problematickému volaniu rozhrania Qt API.

Môžete si [stiahnuť KDiff3](https://download.kde.org/stable/kdiff3/) pre Windows, Mac a vašu Linuxovú distribúciu.

# App Store

### Microsoft Store Štatistiky

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) nám dal aktualizáciu z Microsoft Store. Kate a Okular boli aktualizované a za posledný mesiac mali spolu vyše 4000 inštalácií.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# App Store Interview: Flathub

Flatpak je jedným z nových formátov založených na kontajneroch, ktoré menia spôsob, akým získavame naše aplikácie v systéme Linux. Flatpak môže pracovať s ktorýmkoľvek hostiteľom, ktorý chce zriadiť obchod, ale hlavný obchod je [Flathub](https://flathub.org/home).

Pomocník Flathubu [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [nedávno požiadal o pomoc pri umiestňovaní ďalších aplikácií KDE do obchodu](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514).  Urobili sme s ním rozhovor aby sme zistili viac.

### Povedzte nám niečo o sebe, odkiaľ ste, čím sa živíte, ako ste sa dostali k open source a Flatpakom?

Moje meno je Timothée Ravier a momentálne žijem v Paríži vo Francúzsku. Som systémový inžinier pre Linux a momentálne pracujem v Red Hat na Red Hat CoreOS a Fedora CoreOS.

Keď som prvýkrát nainštaloval Linuxovú distribúciu v roku 2006, dostal som sa do Open Source a odvtedy som sa nikdy nezastavil. Väčšina výskumných projektov, ktorých som sa počas štúdia zúčastnil, sa týkala bezpečnosti Linuxu, sandboxu aplikácií a bezpečnosti grafických rozhraní. Začiatok a vývoj Flatpaku vzbudili môj záujem.

Vo svojom voľnom čase udržiavam neoficiálny [variant KDE (prezývaný Kinoite)]] (https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147 ) vo Fedora Silverblue. Stručne povedané, Fedora Silverblue je nemenný operačný systém a odporúčaným spôsobom inštalácie aplikácií je použitie Flatpakov alebo kontajnerov (cez podman). Ďalšie podrobnosti nájdete v dokumentácii.

### Čo vás viedlo k nedávnemu volaniu po aplikáciách KDE vo Flathube?

Najprv chcem povedať veľké „ďakujem“ súčasným správcom, ktorí už udržiavajú aplikácie KDE na Flathube, pretože odvádzajú skvelú prácu!

Som dlhoročným používateľom KDE (začal som v roku 2006) a vždy som chcel prispievať. Distribúcie už majú tímy etablovaných správcov a Flathubu chýbala dobrá skupina aplikácií KDE, to bolo dobré miesto kde začať.

Zavolala som tiež, pretože bude jednoduchšie, ak rozdelíme prácu a možno to zvýši povedomie ďalších ľudí o Flatpakoch a Flathube.

### Flatpak môže pracovať z akéhokoľvek úložiska, prečo je potreba Flathub?

Táto otázka vyzdvihuje jednu z výhod Flathubu: môžete hosťovať svoje vlastné úložisko aplikácií na svojom vlastnom serveri a distribuovať ich priamo svojim používateľom. Nepotrebujete Flathub.

Ale rovnako ako nepotrebujete GitHub alebo GitLab, atď. Na hosťovanie úložiska Git, je oveľa ľahšie spolupracovať, ak máte jediné miesto, na ktoré môžete nasmerovať používateľov a vývojárov.

Flathub sa stal najjednoduchším miestom na nájdenie a bezpečné vyskúšanie Linuxových aplikácií, a to ako open source, tak i proprietárnych. Myslím si, že je to rozhodujúce, ak chceme zlepšiť atraktívnosť ekosystému Linux ako platformy pre osobné počítače.

### Aké ďalšie komunity s otvoreným zdrojom prijali uvedenie svojich aplikácií na Flathub?

Myslím, že veľa (možno väčšina) aplikácií GNOME je teraz k dispozícii na FlatHub.

### Teraz, keď vývojári aplikácií môžu vydávať náš softvér priamo v obchodoch, ako je Flathub, existujú nové povinnosti, ako napríklad bezpečnosť a aktuálnosť softvéru. Môžete povedať, ako dobre sa s nimi pracuje vo Flathube?

S Flathubom sú zodpovednosti rozdelené medzi správcov platformy a správcov aplikácií.

Platformy obsahujú základné knižnice spoločné pre mnoho aplikácií (existujú platformy Freedesktop, GNOME a KDE) a sú udržiavané tak, aby sa zachovala kompatibilita ABI a zabezpečili rýchle bezpečnostné  aktualizácie.

Za aktualizácie zostávajúcich knižníc požadovaných aplikáciou a samotnou aplikáciou je zodpovedný správca aplikácie.

### Ktoré aplikácie KDE sú pre vás najužitočnejšie?

Denne používam Dolphin, Konsole, Yakuake, Okular a Ark a naozaj sa mi páčia. Z času na čas tiež oceňujem a používam Gwenview, KCachegrind a Massif-Visualizer.

### Mnohé z našich aplikácií sú balené ako Flatpaky prostredníctvom našich [invent](https://invent.kde.org/packaging/flatpak-kde-applications) a [binary-factory serverov](https://binary-factory.kde.org/view/Flatpak/) pracujete s týmito procesmi alebo samostatne?

Flatpaky, ktoré sú postavené na infraštruktúre KDE, sú určené na budovanie každú noc, aby si ich vývojári a používatelia mohli vyskúšať. Toto je dobrý súbor aplikácií Flatpak, aby ste mohli začať, ale niektoré z nich je potrebné aktualizovať. Aktualizácia tohto archívu nám pomôže s najnovším vývojom, ktorý si môže vyžadovať zmeny balenia na Flathube. Ešte som ich nezačal aktualizovať, ale pokúsim sa to urobiť po podaní žiadostí do Flathubu.

### Vidíte čas, keď RPM a Apt už nie sú a Linux distribúcie všetky používajú kontajnerové balíčky?

Nemyslím si, že sa to niekedy stane, pretože distribúcie v súčasnosti balia aplikácie, a to aj napriek tomu, že to má svoje problémy. Myslím si však, že menej distribúcií bude vynakladať úsilie na dosiahnutie tohto cieľa. Napríklad Fedora zostavuje Flatpaky z RPM balíkov a sprístupňuje ich všetkým. To isté by ste mohli urobiť aj s balíčkami Debianu. Prínos tu nie je v tom čo, ale kto: Dôverujete tejto distribúcii? Jej hodnotám? Je to záväzok iba k slobodnému softvéru? Potom ste si istí, že aplikácie, ktoré inštalujete z ich repo aplikácií, budú mať rovnaké požiadavky ako všetky ostatné balíčky. Flathub má otvorené i vlastné aplikácie a to nemusí byť pre každého.

# Vydania teraz na kde.org/applications

Na našich [webových stránkach pre aplikácie](https://kde.org/applications) sa začali zobrazovať informácie o vydaniach. Očakávajte viac už čoskoro. Ak ste správca aplikácie, nezabudnite pridať informácie o novom vydaní do súborov Appstream.

{{< img class="text-center" src="krita-release.png" caption="Informácie o vydaní" style="width: 800px">}}

# Vydania 20.04.3

Niektoré z našich projektov sa vydávajú podľa vlastného časového harmonogramu a niektoré sa vydávajú hromadne. Balík projektov 20.04.3 bol dnes vydaný a čoskoro bude k dispozícii prostredníctvom obchodov s aplikáciami a distribúcií. Pozrite si [Stránku vydaní 20.04.3](https://kde.org/info/releases-20.04.3) pre podrobnosti.

Niektoré opravy v dnešných vydaniach:

* Ukážky súborov na pracovnej ploche v Dolphin boli opravené pre absolútne cesty k ikonám

* Dokončené úlohy sa teraz správne zaznamenávajú do denníka KOrganizer

* Viacriadkový text vložený z aplikácií GTK do Konsole už nemá viac znakov „nový riadok“

* Správanie pri maximalizácií Yakuake bolo opravené

[20.04 poznámky k
vydaniu](https://community.kde.org/Releases/20.04_Release_Notes) &bull; [Wiki
stránka preberania
balíčkov](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [20.04.3 stránka informácií o
zdrojoch](https://kde.org/info/releases-20.04.3) &bull; [20.04.3 úplný zoznam
zmien](https://kde.org/announcements/changelog-releases.php?version=20.04.3)