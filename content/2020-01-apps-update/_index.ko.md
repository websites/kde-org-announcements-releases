---
layout: page
publishDate: 2020-01-09 12:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's January 2020 Apps Update
type: announcement
---
올해는 2020년입니다. 미래가 다가왔죠. 지난 한 달 동안의 KDE 앱 변경 사항을 알아 봅시다!

## KTimeTracker가 KDE 프레임워크 5로 이식됨

The long-awaited modernized version of KTimeTracker is finally released.The application is a personal time tracker for busy people which is nowavailable on Linux, FreeBSD and Windows. Over the course of 2019 it had beenported to Qt5 and KDE Frameworks after being unmaintained since around 2013.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

사용자 인터페이스를 다듬음과 동시에 새로운 주요 기능으로 내보내기 대화 상자에서 라이브 미리 보기 기능을 지원합니다(아래 그림 참조).

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="Export dialog in KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/It is available through your Linux distro or as a [Windows installer](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) and there's even [untested MacOS builds](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/) built nightly.

[관리자 Alexander Potashev의 블로그에 자세한 릴리스 공지가 있습니다](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

천문학 프로그램 [KStars 3.3.9에 새로운 기능이 추가되었습니다](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

그림자, 미드톤, 강조 그림에 미세한 변경 사항을 적용할 수 있습니다. 어두운 별을 더 쉽게 볼 수 있습니다.

Western Sky Culture의 대체 은하 기능도 [읽어 보세요](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Alternative constellations" alt="Western Sky Culture" >}}

[KStars](https://edu.kde.org/kstars/)는 안드로이드, Windows, macOS, Shap 스토어 및 리눅스 배포판에서 만나 볼 수 있습니다.

## 공통 프레임워크 - KNewStuff

Here on the Apps Update we focus on the apps rather than coding libraries. But new features in the common libraries will mean new features in all your apps :)

This month saw a redesigned UI for KNewStuff, the framework to download addons for your applications. The browse and download dialog was redesigned and the comments section can now be filtered. It'll be appearing in all your apps and settings modules shortly starting with the Global Theme module in System Settings.

{{< img src="knewstuff-comments.png" caption="Filters on Comments" alt="Filters on Comments" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Redesigned Browse Dialog" alt="Redesigned Browse Dialog" style="width: 800px">}}

## 버그 수정

[KDevelop 월간 버그 수정 업데이트 5.4.6](https://www.kdevelop.org/news/kdevelop-546-released)에서는 GPL과 LGPL 헤더가 뒤바뀌는 문제를 해결했습니다. 배포판이나 Appimage 업데이트를 설치해서 라이선스 조항이 올바르게 적용되어 있는지 확인하세요.

[Latte 독 0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html)은 Qt 5.14 사용 시 문제 및 여러 충돌 사항을 개선했습니다.

Dolphin 플러그인 19.12.1: SVN 커밋 대화 상자 문제를 해결했습니다.

There was improved file indexing in Elisa. It also fixed some compilation issues on Android and without Baloo.

KPat의 새 릴리스는 [OARS](https://hughsie.github.io/oars/index.html) 기준 연령 제한 없음으로 표기되었습니다.

Okular에서 인쇄 미리 보기 대화 상자를 닫을 때 충돌 문제를 수정했습니다.

이번 달의 Kdenlive 동영상 편집기 릴리스에는 버그 수정 및 메타 정보에 사용되는 [스크린샷](https://kde.org/applications/multimedia/org.kde.kdenlive) 업데이트가 있었습니다. 타임라인 기능 및 미리 보기 처리의 다양한 기능 개선과 버그 수정이 있었습니다.

Kate의 [LSP](https://langserver.org/) 클라이언트에 자바스크립트 지원이 추가되었습니다.

## Flathub 스토어에서 KDE를 만나 보세요

KDE is embracing all the app stores. We can now deliver more and more of our programs directly to you the user. One of the leading app stores on Linux is [Flathub](https://flathub.org/home) which uses the FlatPak format.

You may well already have Flatpak and Flathub configured on your system and ready to use in Discover or other app installers. For example KDE neon has set it up by default on installs for over a year now. If not it's a quick [setup process](https://flatpak.org/setup/) for all the major distros.

기술적인 정보를 더 알아 보려면 [KDE Flatpak 패키지 저장소](https://phabricator.kde.org/source/flatpak-kde-applications/)와 [KDE Flatpak 가이드](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak)를 읽어 보십시오.

그러나 대부분 사용자라면 앱 설치에 관심이 있을 것이므로 [Flathub 스토어의 KDE](https://flathub.org/apps/search/kde) 앱을 찾아 보세요.

{{< img src="flathub.png" alt="KDE on Flathub" style="width: 800px" >}}

## Chocolatey에서 LabPlot 사용 가능

[Chocolatey](https://chocolatey.org/) is a package manager for Windows. If you want full control over what software is installed on your Windows machine or whole office of machines then Chocolatey gives you easy control over that just like you are used to on Linux.

[LabPlot](https://chocolatey.org/packages/labplot) is KDE's app for interactive graphing and analysis of scientific data and it is now available through Chocolatey. Give it a try!

[LabPlot 블로그](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot on Chocolatey" style="width: 800px" >}}

## 웹 사이트 업데이트

최근 개편된 KDE 웹 팀에서는 오래된 웹 사이트를 업데이트하기 시작했습니다. 사진 저장 및 검색 앱인 [KPhotoAlbum](https://www.kphotoalbum.org/)의 웹 사이트는 웹 팀의 최근 결과물을 보여 줍니다.

{{< img src="kphotoalbum.png" alt="New KPhotoAlbum website" >}}

비록 기능이 강력한 음악 재생기지만 오래되어 보였던 웹 사이트에서 소개하고 있었던 [JuK](https://juk.kde.org/)의 웹 사이트도 업데이트되었습니다.

## 릴리스 19.12.1

Some of our projects release on their own timescale and some get released en-masse. The 19.12.1 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12.1 releases page](https://www.kde.org/info/releases-19.12.1.php). This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because it is dozens of different products rather than a single whole.