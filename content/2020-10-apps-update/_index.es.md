---
draft: false
layout: page
publishDate: '2020-09-10T12:00:00+00:00'
summary: "\xBFQu\xE9 ha ocurrido este mes en las Aplicaciones de KDE?"
title: "Actualizaci\xF3n de las Aplicaciones de KDE de octubre de 2020"
type: announcement
---
Tras la Akademy, ha llegado un mes cargado de nuevas actualizaciones de software y de correcciones para las aplicaciones de KDE.

# Nuevos lanzamientos

## digiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="digiKam" style="width: 600px">}}

El mes ha empezado con el lanzamiento de nuestra popular [aplicación de gestión de fotos, digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

Mejor compatibilidad con los metadatos CR3 de Canon. Los formatos de imagen RAW de las cámaras están a menudo sin documentar y son difíciles de procesar. Los desarrolladores de digiKam 7.1.0 han escrito una interfaz de metadatos basada en «libraw» para el formato CR3 y la aplicación puede ahora leer una gran parte de las etiquetas Exif, incluyendo información GPS, perfiles de color y, por supuesto, los contenedores IPTC y XMP estándares.

Se ha introducido un nuevo complemento de gestión de colas por lotes, que le permite aplicar texturas a las imágenes, y otro para corregir los píxeles calientes de forma automática (es decir, los «píxeles incorrectos» que se obtienen al usar velocidades obturación lentas o al trabajar con imágenes nocturnas.

El [modelo de intercambio de información IPTC](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) es un antiguo, aunque todavía común, formato de metadatos usado en fotografía. Esta versión mejora el uso de Unicode en él.

digiKam está disponible en su distribución de L64 bits para Linux, paquete para macOS e instaladores de 32 y 64 bits para Windows. [Descargue digiKam 7.1.0 aquí](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

LabPlot es una aplicación de KDE para gráficos interactivos y análisis de datos científicos. LabPlot proporciona una modo sencillo de crear, gestionar y editar gráficos para realizar análisis de datos. Se ha publicado [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) tras un año de trabajo.

En 2.8 ya es más fácil acceder a numerosos recursos en línea que proporcionan datos con propósitos educativos. Ahora existen cinco colecciones con unos 2000 conjuntos de datos documentados. Vea el siguiente vídeo para más información:

{{< youtube pyP-5J3seQ8 >}}

Este lanzamiento llega con dos nuevos objetos para la hoja de trabajo: líneas de referencia y elementos de imagen. Las líneas de referencia se pueden colocar libremente en el gráfico para resaltar ciertos valores en los datos mostrados. Los elementos de imagen le permiten añadir una imagen al gráfico o a la hoja de trabajo.

La hoja de cálculo proporciona estadísticas descriptivas para los conjuntos de datos. Hemos extendido dicha información y añadido el cálculo de cuartiles, trimedia y el modo estadístico.

Gracias al trabajo llevado a cabo en Cantor durante los últimos meses, los desarrolladores han podido añadir compatibilidad con los archivos del proyecto Jupyter en este lanzamiento de LabPlot.

Se ha dedicado más trabajo a la versión de LabPlot para macOS. Además de numerosas pequeñas correcciones específicas para macOS, se ha añadido compatibilidad con la barra táctil disponible en los nuevos modelos de MacBook Pro.

[Labplot se puede descargar](https://labplot.kde.org/download/) para Linux, Windows y Mac.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

Nuestro IDE de desarrollo de software, [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released), contiene medio año de trabajo, centrado principalmente en la estabilidad, el rendimiento y el mantenimiento futuro. Añade la útil funcionalidad de [mostrar de forma opcional notas en línea de los problemas al final de las líneas](https://blog.david-redondo.de/kde/2020/02/28/problems.html).

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

Se ha publicado la aplicación de calendario para móviles [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html). Aunque ya se habían etiquetado un par de versiones, este es el primer lanzamiento estable de Calindori como aplicación de KDE.

Calindori está disponible en KDE Neon para Plasma Mobile así como en postmartketOS. También está disponible en el repositorio flatpak de compilaciones nocturnas de aplicaciones de KDE para ARM y x86_64. Finalmente, también se puede compilar su código fuente en cualquier estación de trabajo de escritorio Linux. El código fuente y sus firmas se pueden descargar de [download.kde.org](https://download.kde.org/stable/calindori/).

## Kid3 3.8.4

[Kid3](https://invent.kde.org/websites/kid3-kde-org) mejora nuestra aplicación de etiquetado de archivos musicales.

Además de correcciones de errores, este lanzamiento proporciona mejoras de usabilidad, como más accesos rápidos de teclado para navegación y la posibilidad de personalizarlos. La detección del tipo de archivo es más tolerante.

Se puede obtener como [paquete de Windows Chocolatey](https://chocolatey.org/packages/kid3/), [paquete de macOS Homebrew](https://formulae.brew.sh/cask/kid3), [paquete de Android F-Droid](https://f-droid.org/en/packages/net.sourceforge.kid3/), [Flatpak](https://flathub.org/apps/details/org.kde.kid3), [binario para macOS](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download) y [binario para Windows](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download).

# Corrección de errores

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

Para los desarrolladores, ya tenemos un [lanzamiento de corrección de fallos de Heaptrack](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0). Heaptrack sigue el rastro de todas las asignaciones de memoria y anota estos eventos con trazas de la pila. Con ellas, las herramientas dedicadas a su análisis le ayudan a interpretar el perfil de memoria de la pila.

Algunas de las mejoras de estabilidad para Heaptrack la hacen más resistente a errores cuando está grabando datos. De forma adicional, algunas modificaciones menores en la herramienta de análisis gráfico la hacen más eficiente. El rendimiento del paso de análisis también se ha mejorado un poco. Finalmente, se ha corregido un importante fallo en el algoritmo de búsqueda de diferencias en los datos, que debería hacer que esta funcionalidad vuelva a ser más útil.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

KDiff3 le permite comparar hasta tres archivos de texto a la vez.

[Esta actualización](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html) corrige errores tipográficos, la recarga de archivos en Windows y la copia en red.

KDiff3 se puede [descargar](https://download.kde.org/stable/kdiff3/) para Linux, Mac y Windows.

## Tellico 3.3.3

En el gestor de colecciones [Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) se ha mejorado la fuente de datos Entrez (Pubmed) al usar una clave API cuando esté disponible y ahora sigue el límite de velocidad. Los desarrolladores también han mejorado la fuente de datos ISDBdb para que sea posible buscar múltiples valores ISBN.

## Konversation 1.7.6

La aplicación de charla IRC [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) contiene la corrección de un error que ahora permite que se pueda compilar con más versiones de Qt. También se ha corregido la carga del tema de iconos para alias.

## Complementos de visualización de Markdown

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="Heaptrack" style="width: 600px">}}

Este mes se han actualizado un par de complementos. El [KPart del visor de Markdown](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) y [KMarkdownWebView](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) tienen nuevas versiones que actualizan sus traducciones y corrigen la compilación con versiones más antiguas de Qt. Estos dos complementos proporcionan vistas previas de archivos con formato Markdown en Kate y en otras aplicaciones de KDE.

# Novedades

KTechLab, un IDE para microcontroladores y electrónica, es una antigua aplicación que no ha tenido nuevas versiones durante algún tiempo debido al hecho de que usaba tecnología antigua. La base de código de KTechLab se ha actualizado y se acaba de publicar la [versión 0.50.0](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) para poder probarla.

# Lanzamientos de 20.08.2

Algunos de nuestros proyectos se publican según su propio calendario, mientras que otros se publican en masa. El paquete de proyectos 20.08.2 se ha publicado hoy con docenas de correcciones de errores y debería estar disponible próximamente en las tiendas de aplicaciones y en las distribuciones. Consulte la [página de lanzamientos de 20.08.2](https://www.kde.org/info/releases-20.08.2) para más detalles.

Algunas de las correcciones de los lanzamientos de hoy:

* Se ha corregido un error que hacía que el editor de texto Kate ocultara elementos de la interfaz de usuario tras cerrar una pestaña.

* La descarga de documentos de oficina usando la integración de GDrive en Dolphin ya no falla al manejar la redirección del servidor necesaria.

* Se ha corregido el desplazamiento en diagonal (por ejemplo, con paneles táctiles) en la vista de terminal de Konsole.

* El visor de documentos Okular le permite ahora pulsar hiperenlaces que están cubiertos por anotaciones.

* En Kontact, la configuración del archivado automático de correo y el reenvío de mensajes en línea ya no causa un fallo de la aplicación.

* El recorte automático de divisiones de escenas en el editor de vídeo Kdenlive corta ahora el vídeo realmente. La apertura de proyectos con clips de vídeo ausentes se ha hecho más robusta.

* El importador de imágenes de Gwenview ya no falla cuando se cierra.

* La herramienta para compartir el escritorio de forma remota Krfb maneja ahora las pantallas escaladas de forma correcta.

* Las conexiones de teléfono con KDE Connect son ahora más robustas tras una revisión de seguridad.

[Notas del lanzamiento de
20.08](https://community.kde.org/Releases/20.08_Release_Notes) &bull; [Página
wiki de descarga de
paquetes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Página de información principal sobre
20.08.2](https://kde.org/info/releases-20.08.2) &bull; [Registro completo de
cambios de 20.08.2](https://kde.org/announcements/changelog-
releases.php?version=20.08.2)