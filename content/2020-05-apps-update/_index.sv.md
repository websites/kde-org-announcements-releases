---
layout: page
publishDate: 2020-05-15 13:00:00
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i maj 2020
type: announcement
---
# Nya utgåvor

## Kid3

[Kid3](https://kid3.kde.org/) är ett praktiskt men kraftfullt program för musiktaggning, som låter dig redigera ID3-taggar och liknande format för MPR och andra musikfiler.

Denna månad har det flyttats så att KDE är värd för det, och har gjort sin första utgåva som ett KDE-program. Utgivningsmeddelandet lyder:

"Förutom felrättningar, ger den här utgåvan användbarhetsförbättringar, ytterligare snabbtangenter och användaråtgärdsskript. Särskilt tack till diverse folk hos KDE, som översatt användargränssnittet och handboken till nya språk."

Kid3 är tillgängligt för Linux, Windows, Mac och Android. Det går att ladda ner från webbplatsen eller via din distribution och butikerna Chocolatey, Homebrew och F-droid.

{{< img class="text-center" src="kid3-android.png" caption="Krita på Android" style="width: 351px">}}

{{< img class="text-center" src="kid3-linux.png" caption="Kid3 på Linux" style="width: 598px">}}

{{< img class="text-center" src="kid3-mac.png" caption="Kid3 på Mac"style="width: 586px" >}}

{{< img class="text-center" src="kid3-fdroid.png" caption="Kid3 finns på FDroid" style="width: 319px">}}

{{< img class="text-center" src="kid3-homebrew.png" caption="Kid3 finns på Homebrew" style="width: 660px">}}

{{< img class="text-center" src="kid3-chocolatey.png" caption="Kid3 finns på Chocolatey" style="width: 175px">}}

## Calligra

[Calligra](https://calligra.org/news/calligra-3-2-0-released/index.html) är en funktionsklar och standardbaserad kontorssvit.  Den har inte givits ut under några år, men är nu tillbaka med stil.

Version 3.2 har Gemini, kontorssviten särskilt konstruerad för två-i-ett apparater, det vill säga bärbara datorer med pekskärmar som också kan fungera som surfplattor konverterade till vårt vackra Kirigami-ramverk. Ritprogrammet Karbon stöder nu dokument som innehåller flera sidor, medan Stage, Calligras presentationseditor, nu stöder automatiska bildövergångar.

{{< img class="text-center" src="gemini-welcome.png" caption="Gemini" style="width: 400px">}}

{{< img class="text-center" src="karbon-complex.png" caption="Karbon teckning" style="width: 400px">}}

{{< img class="text-center" src="stage.jpg" caption="Stage presentationer" style="width: 400px">}}

Calligra är tillgängligt via din Linux-distribution.

## Tellico 3.3 uppdaterar datakällor

[Tellico](https://tellico-project.org/tellico-3-3-released/) är en samlingshanterare för att hålla reda på allt som man samlar, såsom böcker, bibliografier, videor, musik, videospel, mynt, frimärken, samlarkort, serietidningar, och viner.

För att göra det enklare har den en mängd datagränssnitt för att göra det enkelt att visa vad du äger. Den nya version 3.3 som släpptes tidigare under månaden lägger till en datakälla för [Colnect](https://colnect.com/), en omfattande katalog över samlarobjekt. Den uppdaterar också ett antal datakällor såsom Amazon och MobyGames.

{{< img class="text-center" src="colnect-search1.png" caption="Tellico med Colnect datamodell" >}}

# Inkommande projekt

Nya projeckt i KDE denna månad omfattar pvfviewer, ett visningsprogram för sömnadsmönster så att man kan skapa vackra bonader, och Alligator, ett RSS-läsprogram som använder vårt vackra Kirigami-ramverk.

# KDE AppImage

Varje månad vill vi profilera ett av de nya paketbehållarformaten. AppImage är inte nytt, det har funnits över tio år, men det är värt att påminna oss om eftersom ett antal KDE-program använder det.

## Vad är AppImage?

AppImage är ett paketeringsformat som tillhandahåller ett sätt för utvecklare uppströms att erbjuda "ursprungliga" binärfiler för användare av GNU/Linux precis på samma sätt som de kunnat göra för andra operativsystem. Det tillåter att program paketeras för alla vanliga Linux-baserade operativsystem, t.ex. Ubuntu, Debian, openSUSE, RHEL, CentOS, Fedora etc. En AppImage levereras med alla beroenden som inte kan antas vara en del av varje målsystem i en tillräckligt aktuell version, och kör på de flesta Linux-distributioner utan ytterligare ändringar.

## Vad behöver man installera för att de ska köra så bra som möjligt?

För att köra en AppImage behöver du Linux-kärnan (> 2.6) och `libfuse`. Dessa beroenden är uppfyllda i majoriteten av GNU/Linux-distributioner så du behöver inte installera något särskilt.

Ledsamt nog är inte stöd för AppImage fullständigt på de större skrivbordsmiljöerna (KDE och Gnome), så du kan behöva ytterligare ett verktyg för att skapa ett menyalternativ för ditt program. Om så är fallet, kan du välja mellan följande beroende på vilket UX du föredrar:

* [AppImageLauncher](https://www.appimagehub.com/p/1228228) meddelanderuta för integrering vid första körning eller

* [appimaged](https://github.com/AppImage/appimaged) för automatiserad integrering av alla AppImage som installeras i din hemkatalog.

För att uppdatera en AppImage använder du  [AppImageUpdate](https://github.com/AppImage/AppImageUpdate). Den är inbyggd i AppImageLauncher, så om du redan har installerat den finns inget behov av installera ytterligare någonting. Högerklicka bara på AppImage-filen och välj uppdatera. Observera att alla paket har inte uppdateringsinformationen inbäddad i binärfilerna, så det kan finnas fall då du måste ladda ner den nya versionen manuellt.



## Vilka KDE-program kör med den?

Det finns flera KDE-program som redan distribueras som AppImage. De mest relevanta är:

* kdenlive

* krita

* kdevelop

## Vad är appimagehub?

Det rekommenderade sättet att hämta en AppImage är från programmets originalupphovsmän, men det är inte så praktiskt om du inte ännu vet vilket program du behöver. Det är då [AppImageHub](appimagehub.com) kommer in. Det är en programvarubutik som bara är tillägnad AppImage. Där finns en katalog med över 600 program för dina dagliga uppgifter.

Webbplatsen är en del av plattformen [OpenDesktop.org](opendesktop.org), som tillhandahåller ett komplett ekosystem för användare och utvecklare av fria programvaror med öppen källkod.

## Hur skapar man en AppImage?

Att skapa en AppImage handlar om att samla ihop alla dina programberoenden i en enda katalog (AppDir). Därefter buntas den ihop i en squashfs-avbild och läggs till sist i ett körtidsprogram som tillåter att den körs.

För att utföra uppgiften kan följande verktyg användas:

* https://github.com/linuxdeploy

* https://github.com/AppImageCrafters/appimage-builder

I [AppImage Package Guide](https://docs.appimage.org/packaging-guide/index.html) hittar du omfattande dokumentation om hur sådana verktyg används. Du kan också gå med i [AppImage IRC -kanalen](irc://irc.freenode.net/#AppImage).



# 20.04.1 utgåvor

Vissa av våra projekt ges ut med sin egen tidsskala och vissa ges ut tillsammans. Packen med projekt 20.04.1 gavs ut idag och ska snart vara tillgänglig via programvarubutiker och distributioner. Se [20.04.1 utgivningssida](https://www.kde.org/info/releases-20.04.1.php) för detaljerad information.

Några av felrättningarna som ingår i den här utgåvan är:

* kio-fish: Lagra bara lösenord i plånboken om användaren ber om det.

* [Umbrello](https://umbrello.kde.org/) Rättningar för att lägga till stöd för flerraderskommentarer i C++

* Rullningsbeteendet i [Okular](https://kde.org/applications/en/graphics/org.kde.okular)-dokument har förbättrats och är mer användbart med fritt snurrande mushjul

* En regression som ibland orsakade att musikspelaren  [JuK](https://kde.org/applications/en/multimedia/org.kde.juk) kraschade vid start har rättats

* [Kdenlive video editor](https://kde.org/applications/en/multimedia/org.kde.kdenlive) har fått många stabilitetsuppdateringar, inklusive en rättning av skapa DVD-kapitel och en rättning som förbättrar  hanteringen av tidskoder, förbättrad hantering av saknade klipp, rita "fotoram" på bildklipp för att skilja dem från videoklipp, och förhandsgranskningar i tidslinjen

* [KMail](https://kde.org/applications/en/office/org.kde.kmail2) hanterar nu befintliga maildir-kataloger riktigt när en ny maildir-profil läggs till och kraschar inte längre när för många mottagare läggs till

* Import och export av inställningar i [Kontact](https://kde.org/applications/en/office/org.kde.kontact) har förbättrats för att inkludera mer data

[20.04 versionsfakta](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Wikisida för paketnerladdning](https://community.kde.org/Get_KDE_Softwar
e_on_Your_Linux_Distro) &bull; [20.04.1
källkodsinformationssida](https://kde.org/info/releases-20.04.1) &bull; [20.04.1
fullständig ändringslogg](https://kde.org/announcements/changelog-
releases.php?version=20.04.1)