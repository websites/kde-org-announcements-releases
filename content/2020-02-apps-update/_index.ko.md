---
layout: page
publishDate: 2020-02-06 12:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's February 2020 Apps Update
type: announcement
---
이번 달에는 다양한 버그 수정 릴리스가 출시되었습니다. 새로운 기능을 기대하고 계신다는 건 잘 알고 있지만, 잘못 구현된 기능과 충돌을 수정하는 것 또한 중요하죠!

# 새 릴리스

## KDevelop 5.5

이번 달의 가장 큰 릴리스는 C++, Python, PHP로 프로그램을 작성하는 것을 도와 주는 [KDevelop 5.5](https://www.kdevelop.org/news/kdevelop-550-released)입니다. KDevelop의 버그를 수정하고 코드의 여러 부분을 다듬었습니다. C++에는 Lambda 초기화 캡처, Clazy-tidy와 Clang의 사전 설정된 체크셋 설정 지원, 룩어헤드 자동 완성 기능 개선이 추가되었습니다. PHP에는 PHP 7.4의 형식 지정된 속성 지원, 형식 지정된 배열, 클래스 상수 가시성 지원이 추가되었습니다. Python에는 Python 3.8 지원이 추가되었습니다.

KDevelop은 사용 중인 리눅스 배포판이나 AppImage에서 만나 볼 수 있습니다.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/)은 Kontact와 통합되는 할 일 목록 관리자입니다. 최근 Kontact 릴리스에서 Zanshin 지원이 깨져서 더 이상 무엇을 해야 할 지를 알 수 없는 사용자들이 생겼습니다. 이번 달에 Zanshin이 [다시 작동하도록](https://jriddell.org/2020/01/14/zanshin-0-5-71/) 릴리스되었습니다. 이제 다시 할 일에 집중하세요!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte 독 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

KDE의 16진수 편집기 [Okteta](https://kde.org/applications/utilities/org.kde.okteta) 릴리스에는 버그 수정 및 CRC-64 체크섬 알고리즘이 추가되었습니다. Okteta 코드는 새로운 Qt와 KDE 프레임워크를 지원하도록 업데이트되었습니다.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

재무 관리 KDE 소프트웨어인 [KMyMoney](https://kmymoney.org/)의 [버그 수정](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) 및 분할 프로토콜을 사용하는 폼 검사 기능 지원이 추가되었습니다.

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# 새로 추가됨

Keysmith는 Plasma 모바일 및 데스크톱에서 사용할 수 있는 oath-toolkit을 사용하는 2단계 인증 코드 생성기입니다. 사용자 인터페이스는 Kirigami로 작성되었으며, 여러 화면 크기에 대응합니다. 곧 릴리스가 예정되어 있습니다.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org)는 Windows 운영 체제용 패키지 관리자입니다. Chocolatey는 시스템에 소프트웨어를 설치 및 업데이트를 관리하며 Windows 사용자의 관리 어려움을 덜어 줍니다.

[KDE는 Chocolatey에 계정이 있습니다.](https://chocolatey.org/profiles/KDE) 여기에서 Digikam, Krita, KDevelop, Kate를 설치할 수 있습니다. 설치 과정은 매우 간단합니다: 단지 `choco install kdevelop` 명령을 실행하면 Chocolatey가 나머지 과정을 처리합니다. Chocolatey 관리자의 엄격한 표준을 따르며, 여기에 있는 패키지는 테스트되었으며 안전합니다.

KDE 앱 관리자라면 여기에 앱을 추가하는 것을 추천합니다. 대량의 Windows 시스템을 관리하고 있다면 설치해 보는 것을 추천합니다.

# 웹 사이트 업데이트

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}Our web team continues to update our online presence and has recently refreshed the [KMyMoney website](https://kmymoney.org).

# 릴리스 19.12.2

일부 프로젝트는 개별적인 일정에 따라 릴리스되며 일부는 다 같이 릴리스됩니다. 19.12.2 프로젝트 번들은 오늘 출시되었으며, 곧 앱 스토어와 배포판에서 사용할 수 있습니다. 자세한 정보는 [19.12.2 릴리스 페이지](https://www.kde.org/info/releases-19.12.2.php)를 참조하십시오. 이 번들은 KDE 프로그램으로 불렸으나, KDE의 다른 프로그램과의 혼동 방지 및 개별 프로젝트를 더 잘 드러내기 위해서 더 이상 이 이름을 사용하지 않습니다.

이 릴리스에서 수정된 주요 버그:

* [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) 음악 재생기에서 메타데이터가 없는 음악 파일을 올바르게 처리합니다

* [KMail](https://kde.org/applications/internet/org.kde.kmail2)에서 편집한 메시지를 다시 열 때 *임시 보관함* 폴더에 저장된 첨부 파일이 더 이상 사라지지 않습니다

* [Okular](https://kde.org/applications/office/org.kde.okular) 문서 뷰어의 렌더링을 멈출 수 있는 타이밍 문제를 해결했습니다

* [Umbrello](https://umbrello.kde.org/) UML 디자이너의 Java 가져오기 기능을 개선했습니다

[19.12.2 릴리스 노트](https://community.kde.org/Releases/19.12_Release_Notes) &bull;
[패키지 다운로드 위키
페이지](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[19.12.2 소스 정보 페이지](https://kde.org/info/releases-19.12.2) &bull; [19.12.2 전체 변경
기록](https://kde.org/announcements/changelog-releases.php?version=19.12.2)