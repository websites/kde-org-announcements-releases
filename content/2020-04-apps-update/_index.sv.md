---
layout: page
publishDate: 2020-04-23 13:00:00
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i april 2020
type: announcement
---
En ny packe KDE-program är här! I dessa utgåvor kan du förvänta dig mer funktioner, stabilitetsförbättringar och mer användarvänliga verktyg som hjälper dig att arbeta effektivare.

Det finns dussintals ändringar att se fram emot i de flesta av dina favoritprogram. Ta exempelvis Dolphin, där delade Windows Samba-kataloger nu är helt möjliga att upptäcka.

När ämnet är spela musik så har musikspelaren Elisa lagt till funktioner med stormsteg. Den här utgåvan har en ny vy "Spelar nu", enkel åtkomst via systembrickan och ett alternativ för att minimera spellistan när du vill. Tack vara det nyligen tillagda visuella blandningsläget är det mycket enklare att arrangera om låtar i spellistorna.

Det här är bara höjdpunkterna av vad som är nytt i KDE:s program den här månaden. Läs vidare för att ta reda på allt som vi har förberett åt dig.

## [Okular](https://okular.kde.org)

Okular är KDE:s program som låter dig läsa PDF, Markdown-dokument, serieböcker och många andra dokumentformat. I den här utgåvan har Okular erhållit några handikappförbättringar både för skrivbords- och pekskärmsanvändare. I det förra fallet implementerar Okular nu jämn rullning både när mushjulet och tangentbordet används. För pekskärmsanvändare levereras den senaste versionen av Okular med tröghetsrullning.

{{<video src="okular.mp4" caption="Jämn rullning i Okular. Illustration: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

KDE:s kraftfulla filhanterare Dolphin lägger till nya funktioner som låter dig hantera fjärrfilsystem såsom delade Samba-kataloger och SSH-servrar. Nu kan du titta på filer lagrade på fjärrplatser utan att behöva ladda ner dem, och istället direkt strömma dem från molnet till din spelare via Dolphin.

På liknande sätt, har det aldrig varit enklare att ansluta delade Windows Samba-kataloger och dra fördel av tjänsterna de erbjuder, eftersom delade Samba-kataloger går nu att upptäcka via protokollet WS-Discovery som används av moderna Windows-versioner. Det låter dig sömlöst integrera Linux-datorn med ett Windows-nätverk.

När det gäller nyheterna från användbarhetsavdelningen: du behöver inte längre musen för att flytta fokus till och från terminalpanelen. Nu kan du helt enkelt använda snabbtangenten <kbd>Ctrl</kbd> + <kbd>Skift</kbd> + <kbd>F4</kbd>. En annan efterlängtad funktion är att söka efter filer inte bara enligt namn eller innehåll, utan också enligt deras etiketter.

Stöd för 7Zip-filer är nu inbyggt i Dolphin, vilket betyder att du kan navigera i de komprimerade filerna som om de var kataloger i filsystemet. När du installerar externa sökprogram som Kfind, låter Dolphin dej snabbt komma åt dem genom att skapa en länk.

Till sist, en synbart mindre funktion som kan ge stor tidsbesparing är den nya "Duplicera". Markera en eller flera filer eller kataloger, tryck på <kbd>Ctrl</kbd>+<kbd>D</kbd> (eller högerklicka och välja "Duplicera här") och - bang! En kopia av varje markerat objekt dyker upp bredvid originalen.

![Dolphin duplicate feature](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

Lokalize är ett verktyg som du kan använda för att översätta gettext- och xliff-filer. Det stöder nu rättning av grammatik med Language-Tool, som hjälper översättare genom att färglägga grammatikfel i texten.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

Kmail är KDE:s populäraste e-postprogram och en del av Kontact, en programsvit som hjälper dig att hantera din e-post, kontakter, och uppgifter. En hel del förbättringar ingår i den här nya utgåvan. Till en början, kan e-post nu enkelt exporteras som PDF, och brev formaterade med Markdown visas bättre. Säkerhetsaspekter har också erhållit viss uppmärksamhet. Mer specifikt, visar Kmail en varning när brevfönstret visas efter att ha klickat på en länk som ber dig bilägga en fil.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

Terminal är en terminalemulator som används för att utföra kommandoradsinstruktioner, processer och program. Den senaste versionen av Terminal låter dig använda tangenten <kbd>Alt</kbd> + en siffertangent för att gå direkt till någon av de första nio flikarna.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

Gwenview är KDE:s favoritbildvisare, och med den här versionen har utvecklarna löst två större problem. Programmet hänger sig inte längre vid start när systemets klippbord innehåller text från KDE-anslut, och import av foton från fjärrplatser har rättats.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

Elisa är en enkel och populär musikspelare, och den här utgåvan har funktionen "Spelar nu" som gör den attraktivare.

Men det handlar inte bara om utseendet. Elisa har också bättre funktioner, eftersom du nu kan komma åt det via systembrickan och minimera spellistan när du vill. Det betyder att du kan fortsätta att lyssna på din favoritmusik även när Elisas huvudfönster är stängt. Det nya visuella blandningsläget gör det enklare att arrangera om låtar i spellistan, och visar vilken låt som spelas härnäst.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

Kdenlive är KDE:s avancerade videoredigeringsprogram, och den nya versionen gör redigering snabbare tack vare förhandsgranskningens inställningsbara upplösning. Förhandsgranskningen förbättrar också visning av flera spår, vilket gör det enklare att välja spår bland de som redigeras samtidigt.

Nere på tidslinjen har gruppen förbättrat låsning av klipp genom att bara låsa aktiva spår. De har också rättat grupplåsningsfunktionen och lagt till en genväg för att gå förbi låsning vid förflyttning. En annan sak som går att göra nu är att direkt släppa filer (videor, musik och bilder) från filutforskaren på tidslinjen. Den nya funktionen för att "zooma på nyckelbilder" hjälper till att utföra mer precist arbete. Professionella videoredigerare och användare som kommer från andra redigeringsverktyg uppskattar utan tvekan funktionen för OpenTimeLineIO import och export.

Den här utgåvan av Kdenlive också levereras med många felrättningar, som den för rörelsedetektering, som var sönder på vissa system. Nu låter den dig lägga till ett kompensationsfilter för tonhöjd, samt lägga till "Flytta och ändra storlek" för en rotoskopiform. Kraschen av DVD-guiden och ett fel som har hemsökt ändring av inspelningsvolym från mixern har också lösts. Kdenlive-gruppen har också lagt till etikettering, betygsättning och filtreringsfunktioner i projektkorgen.

Den nya Windows-versionen av Kdenlive levereras också med rättningar av tidslinjedialogrutorna, som tidigare inte accepterade inmatning från tangentbordet. Nu kan du dessutom välja ljudgränssnitt på Windows.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

Yakuake är ett terminalfönster som du kan dra ner från överkanten av Plasma-skrivbordet genom att trycka på <kbd>F12</kbd>. Det är praktiskt för att snabbt utföra instruktioner på kommandoraden, och sedan flytta undan det.

När du öppnar en ny flik eller delar rutor i den här versionen av Yakuake, kan du starta dem i samma katalog som den nuvarande fliken eller delade rutan. Nu kan du ändra storlek på Yakuakes fönster vertikalt genom att dra dess underkant.

## Systeminställningar

Integreringssidan för nätkonton i systeminställningarna låter dig ställa in saker som ditt Google-konto, eller ställa in åtkomst av ditt Nextcloud-konto. En fullständig översyn har gjorts av sidan, och den ståtar nu med ett rent och modernt utseende och en mycket pålitligare funktion.

## [KDE-anslut](https://kdeconnect.kde.org/)

KDE-anslut är ett program som hjälper dig att integrera din telefon med ditt skrivbordet och vice versa. En av de nyaste funktionerna som levereras med den här utgåvan är möjligheten för att starta nya konversationer med SMS-programmet.

Andra ändringar inkluderar att fjärrmediaspelare nu dyker upp i miniprogrammet för mediespelare, en ny uppsättning statusikoner för anslutning, och förbättrade samtalsunderrättelser.

När det gäller felrättningar, har felet som gjorde att fildelning blockera det anropande programmet lösts, och problemen med överföring av mycket stora filer har retts ut.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

För Spectacle, KDE:s programvara för att ta skärmbilder, har användbarhetsfunktioner lagts till och några irritationsmoment har lösts. En av dem förhindrade dig från att kopiera en skärmbild till klippbordet när du öppnade Spectakle från kommandoraden med väljaren --nonotify. Även om det kan vara ett nischproblem, är poängen att nu kan du göra precis det. Likaså hade äldre versioner problem när du försökte dra en skärmbild från Spectacle till standardfilnamnsmönstret som inkluderade underkataloger. Dessa problem har lösts.

En annan användbar förbättring i den nya versionen av Spectacle levereras i form av knapparna "Förval" och "Återställ". Du kan använda dem för att gå tillbaka till en tidigare inställning, eller till och med ta bort alla tidigare egna inställningar och återställa allting till förvalda värden.

Spectacle kommer inte bara ihåg standardinställningen, utan också det senaste skärmbildsområdet. Det hjälper dig att förbli konsekvent, och gör det enklare att skapa bilder av samma form och storlek som de tidigare.

## [Krita](https://krita.org/en/) 4.2.9

Ett antal tekniska uppdateringar bakom kulisserna har gjorts i den här utgåvan, men också jämnare konturer av målarpenslar som inte flimrar när musen hålls över duken. "Retuschspruta" och "Frekvens för retuschspruta" har lagts till i färgsmetningspenseln, och en ny inställning av förhållande, också för färgsmetningspenseln, vilket gör det möjligt att göra penselformen plattare genom att använda de olika sensorerna. En ny funktion för att dela lager till markeringsmasker låter dig skapa ett nytt lager för varje färg på det aktiva lagret.

{{< youtube fyc8-qgxAww >}}

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

Utgåva 3.0.4 av Smb4K rättar flera problem och krascher, i synnerhet i monteringsverktyget. Den gör också Smb4K kompatibel med Qt version 5.9 eller tidigare.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

Den här huvudugåvan av insticksprogrammet som integrerar Google Drive med KDE:s program lägger till stöd för funktionen Shared Drives i Google Drive.

Det innehåller också ett nytt alternativ, "Kopiera Google webbadress till klippbordet", i Dolphins sammanhangsberoende meny.

[20.04 versionsfakta](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Wikisida för paketnerladdning](https://community.kde.org/Get_KDE_Softwar
e_on_Your_Linux_Distro) &bull; [20.04
källkodsinformationssida](https://kde.org/info/releases-20.04.0) &bull; [20.04
fullständig ändringslogg](https://kde.org/announcements/changelog-
releases.php?version=20.04.0)