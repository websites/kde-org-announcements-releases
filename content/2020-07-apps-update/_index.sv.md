---
layout: page
publishDate: 2020-07-09 12:00:00
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i juli 2020
type: announcement
---
# Nya utgåvor

### KTorrent 5.2.0

Fildelningsprogrammet [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) har en ny utgåva [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

Importförbättringen för delning är förbättringar av [Distributed Hash Table](https://en.wikipedia.org/wiki/Mainline_DHT), som nu startar med välkända noder så att nerladdningarna kan göras snabbare.  Under huven uppdaterades den till det nyare QtWebengine, som är baserat på Chrome, från det äldre QtWebkit baserat på WebKit (alla baserade på KDE:s KHTML förr i tiden).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

Ktorrent är tillgängligt via din Linux-distribution.

### KMyMoney 5.1.0 utgivet

Bankprogrammet [KMyMoney](https://kmymoney.org/) [released 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

Det lägger till stöd för symbolen för indiska rupier: ₹. De har också lagt till alternativet för att "vända på in- och utbetalningar" i OFX-import, och budgetvyn visar nu alla kontotyper.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[KMyMoney är tillgängligt](https://kmymoney.org/download.html) i din Linux-distribution, som en Windows-nerladdning, en Mac-nerladdning och nu i [Homebrew KDE] (https://invent.kde.org/packaging/homebrew-kde).

### Kdiff3 1.8.3 versionsfakta

Filjämförelseverktyget [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) gav ut en ny version 1.8.3, som gavs ut med en mängd stabilitetsförbättringar.

Vid användning av Kdiff3 som jämförelseverktyg för Git orsakar inte längre fel för icke-existerande filer. Fel vid katalogjämförelser köas på ett riktigt sätt så att bara ett meddelande visas. Rättar uppdatering på Windows. Tog bort en krasch när klippbordet inte är tillgängligt. Visning med fullskärm har arbetats om för att undvika ett problematiskt anrop till Qt-programmeringsgränssnittet.

Du kan [ladda ner Kdiff3](https://download.kde.org/stable/kdiff3/) för Windows, Mac och din Linux-distribution.

# Programbutik

### Statistik för Microsoft Store

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) har bidragit med en uppdatering på Microsoft store.  Kate och Okular har uppdaterats, och under den senaste månaden har båda installerats över 4000 gånger.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# Intervju om programbutik: Flathub

Flatpak är ett av de nya behållarbaserade formatet som ändrar hur vi hämtar våra program på Linux.  Flatpak kan arbeta med vilken värddator som helst som vill ha en butik, men den slutgiltiga butiken är [Flathub](https://flathub.org/home).

Nyligen frågade Flathub hjälpredan [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [om hjälp att lägga till fler KDE-program i butiken](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514). Vi intervjuade honom för att ta reda på mer.

### Berätta för oss om dig själv, var kommer du från, vad arbetar du med, hur kom du i kontakt med öppen källkod och Flatpak?

Jag heter Timothée Ravier och bor för närvarande i Paris i Frankrike. Jag är en Linux systemingenjör och arbetar för närvarande på Red Hat med Red Hat CoreOS och Fedora CoreOS.

Jag började med öppen källkod när jag först installerade en Linux-distribution 2006, och har inte slutat sedan dess. De flesta forskningsprojekt jag utförde under min studietid var relaterade till Linux säkerhet, programisolering och säkerhet i grafiska gränssnitt. Sålunda väckte introduktionen och utvecklingen av Flatpak mitt intresse.

Under min fritid underhåller jag den inofficiella [KDE varianten (med smeknamnet Kinoite)](https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) av Fedora Silverblue. I korthet är Fedora Silverblue ett oföränderligt operativsystem och det rekommenderade sättet att installera program är att använda Flatpak eller behållare (via podman). Se dokumentationen för mer detaljerad information.

### Vad gjorde att du nyligen skickade ut en appell för KDE-program i Flathub?

För det första vill jag ge de nuvarande underhållsansvariga, som redan underhåller KDE-program på Flathub, ett stort "tack så mycket", eftersom de gör ett utmärkt jobb!

Jag har använt KDE under lång tid (jag började 2006) och har alltid velat bidra själv. Distributioner har redan grupper av etablerade underhållsansvariga och Flathub saknade en hel del KDE-program, så det verkade vara ett bra ställe att börja.

Jag skickade också ut en förfrågan eftersom det vore enklare om vi kan dela på arbetet, och för att det kan göra fler medvetna om Flatpak och Flathub.

### Flatpak kan arbeta med alla arkiv, varför behövs då Flathub?

Frågan belyser en av fördelarna med Flathub: man kan ha sitt eget programarkiv på en egen server och distribuerad dem direkt till användare. Man "behöver" inte Flathub.

Men precis som man inte behöver GitHub eller GitLab, etc., för att ha ett Git-arkiv, är det mycket enklare att samarbeta om man har ett enda ställe att peka användare och utvecklare på.

Flathub har blivit det enklaste stället att hitta och säkert prova Linux-program, både med öppen källkod och proprietära. Jag anser att det är kritiskt om vi vill förbättra Linux-ekosystemets dragningskraft som skrivbordsplattform.

### Vilka andra öppna källkodsgemenskaper har anammat att lägga sina program på Flathub?

Jag tror att många (kanske de flesta) GNOME-program nu är tillgängliga på Flathub.

### Nu när utvecklarna kan placera vår programvara direkt i butiker som Flathub, finns det nya ansvarsområden, som säkerhet och att hålla programvaran uppdaterad. Kan du berätta hur bra det hanteras med Flathub?

Med Flathub delas ansvaret mellan plattformsansvariga och programansvariga.

Plattformen innehåller kärnbiblioteken gemensamma för många program (det finns Freedesktop-, GNOME- och KDE-plattformar) och underhålls för att både bevara kompatibilitet för det binära programgränssnittet och säkerställa skyndsamma säkerhetsuppdateringar.

Uppdateringar av återstående bibliotek som krävs av ett program och själva programmet är de programansvarigas uppgifter.

### Vilka KDE-program tycker du är mest användbara?

Jag använder Dolphin, Konsole, Yakuake, Okular och Ark dagligen och tycker verkligen om dem. Jag uppskattar och använder också Gwenview, KCachegrind och Massif-Visualizer då och då.

### Många av våra program paketeras som Flatpak via våra [invent-servrar](https://invent.kde.org/packaging/flatpak-kde-applications) och [binärfabrikservrar](https://binary-factory.kde.org/view/Flatpak/). Arbetar du med dessa processer eller separat?

Flatpak som byggs med KDE:s infrastruktur är avsedda som nattliga byggen att provas av utvecklare och användare. Det är en god mängd Flatpak-program att komma igång med, men några av dem behöver uppdateras. Att hålla det här arkivet uppdaterat hjälper oss med senaste utvecklingar som kan kräva paketeringsändringar på Flathub. Jag har inte börjat uppdatera dem ännu, men jag försöker göra det i samband med inlämning av program till Flathub.

### Kan du föreställa dig en tid när RPM:er och Apt inte längre finns, och alla Linux-distributioner använder behållarbaserade paket?

Jag tror inte att det någonsin kommer att ske, eftersom det finns ett värde i hur distributioner för närvarande paketerar program även om det också har problem. Men jag tror att färre distributioner kommer att lägga ner ansträngningen för att göra det. Exempelvis bygger Fedora Flatpak från RPM-paket och gör dem tillgängliga för alla. Man skulle också potentiellt kunna göra samma sak med Debian-paket. Värdet här är inte vad utan vem: litar du på distributionen? Dess värden? Dess engagemang för enbart fri programvara? Då är du säker på att programmen som du installerar från deras arkiv kommer att ha samma krav som alla andra paket. Flathub har både öppen källkod och proprietära program, och det kanske inte passar alla.

# Utgåvor som nu finns på kde.org/applications

Vår [underliggande programwebbplats](https://kde.org/applications) har börjat visa utgivningsinformation. Förvänta dig att det snart kommer mer. Om du är en programansvarig, kom ihåg att lägga till utgivningsinformation i Appstream-filer.

{{< img class="text-center" src="krita-release.png" caption="Utgivningsinformation" style="width: 800px">}}

# Utgåvor 20.04.3

Vissa av våra projekt ges ut med sin egen tidsskala och vissa ges ut tillsammans. Packen med projekt 20.04.3 gavs ut idag och kommer snart vara tillgänglig via programvarubutiker och distributioner. Se [20.04.3 utgivningssida](https://www.kde.org/info/releases-20.04.3) för detaljerad information.

Några av felrättningarna som ingår i dagens utgåvor:

* Förhandsgranskningar av skrivbordsfiler i Dolphin har rättats för absoluta ikonsökvägar

* Färdiga uppgiftsobjekt registreras nu korrekt i Korganizers journal

* Flerraderstext som klistras in från GTK-program till Terminal har inte längre extra "nyradstecken"

* Yakuakes maximeringsbeteende har rättats

[20.04 versionsfakta](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Wikisida för paketnerladdning](https://community.kde.org/Get_KDE_Softwar
e_on_Your_Linux_Distro) &bull; [20.04.3
källkodsinformationssida](https://kde.org/info/releases-20.04.3) &bull; [20.04.3
fullständig ändringslogg](https://kde.org/announcements/changelog-
releases.php?version=20.04.3)