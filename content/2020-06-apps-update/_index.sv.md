---
layout: page
publishDate: 2020-06-11 12:00:00
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i juni 2020
type: announcement
---
Det är alltid glädjande att KDE-familjen växer, och det är därför vi är särskilt glada denna månad att välkomna säkerhetskopieringshanteraren Kup och ett helt nytt paketeringsinitiativ: Homebrew.

# Nya utgåvor

## Kup 0.8

[Kup](https://store.kde.org/p/1127689) är ett säkerhetskopieringsverktyg som du använder för att hålla dina filer säkra.

Det utvecklades tidigare utanför KDE, men den senaste månaden har det passerat inkubationsprocessen och gått med i gemenskapen, och officiellt blivit ett KDE-projekt. Huvudutvecklaren, Simon Persson, har firat med en ny utgåva.

Här är ändringarna som finns i den nya versionen:

* Ändrade hur säkerhetskopior av typen rsync lagras när bara en källkatalog är vald. Ändringen försöker minimera risken att filer tas bort för en användare som väljer en målkatalog som inte är tom. Flyttningskod för att detektera och flytta filer vid första körningen har lagts till, och på så sätt undvika att kopiera allting igen och fördubbla lagringsutrymmet.

* Tillägg av avancerat alternativ som gör att man kan ange mönster för filer som Kub ska undvika att läsa, som exempelvis låter dig tala om för Kub att aldrig spara *.bak-filer.

* Ändrade standardinställningar, som förhoppningsvis gör dem bättre.

* Reducerade varningar om filer som inte inkluderas, eftersom det orsakade för många falska alarm.

* Kup frågar inte längre efter ett lösenord för att låsa upp externa krypterade drivenheter, bara för att visa hur mycket utrymme är tillgängligt.

* Rättade att en säkerhetskopia inte behandlas som misslyckad bara eftersom filer saknas under åtgärden, både för rsync och bup.

* Började köra integritetskontroller och reparationer i parallell baserat på antal processorer.

* Tillägg av stöd för bup metadata version 3, vilket har lagts till i bup version 0.30.

* Många mindre rättningar av användargränssnittet.

Kup kan säkerhetskopiera genom att använda rsync, eller göra versionsbaserade säkerhetskopior med Python-verktyget Bup. Bup fungerar för närvarande bara med Python 2, vilket betyder att alternativet inte är tillgängligt på många distributioner, men en konvertering till Python 3 är på gång.

{{< img class="text-center" src="kup.png" caption="Kup" style="width: 600px">}}

För att ta reda på mer Kup, gjorde  [Average Linux User en recension](https://averagelinuxuser.com/kup-backup/) och en video på Kup för  inte så länge sedan:

{{< youtube y53IE0G8Brg >}}

# Krita på Android läsplattor

Tack vare Sharaf Zamans hårda arbete är Krita nu tillgänglig på Google Play Store för Android läsplattor och Chromebook (men inte för Android telefoner).

Denna betaversion, baserad på Krita 4.2.9, är det fullständiga skrivbordsversionen av Krita, så den har inte något särskilt pekanvändargränssnitt. Men den finns där, och man kan prova den.

I motsats till Windows och Steam, begär de inte pengar för Krita eftersom det är enda sättet som man kan installera Krita på apparaterna. Dock kan man köpa ett supportermärke inne i Krita för att stödja utvecklingen.

För att installera

* Hämta [Krita från Google Play](https://play.google.com/store/apps/details?id=org.krita)

* Som ett alternativ, byt till fliken "Tidig åtkomst" och sök efter org.krita (se Googles officiella instruktioner om tidig åtkomst). Tills vi får ett rimligt antal nerladdningar, måste du rulla ner en del.

* Du kan också [ladda ner APK-filerna själv](https://files.kde.org/krita/android/). Fråga INTE om hjälp att installera filerna.

* De är alla officiella platserna. Installera inte Krita från andra källor. Vi kan inte garantera deras säkerhet.

Anmärkningar

* Stöder Android läsplattor och Chromebook. Android-versioner som stöds: Android 6 (Marshmallow) och senare.

* För närvarande inte kompatibelt med: Android-telefoner.

* Om du har installerat ett av Sharafs byggen eller ett bygge som du själv har signerat, måste du avinstallera det först, för alla användare.

{{< img class="text-center" src="krita-android.jpg" caption="Krita på Android" style="width: 600px">}}

# Inkommande

[KIO Fuse](https://techbase.kde.org/Projects/KioFuse) gjorde sin första [betautgåva](https://mail.kde.org/pipermail/kde-announce-apps/2020-May/005587.html) denna månad.

## Felrättningar

Felrättningsutgåvor har getts ut för

* Samlingshanteraren [Tellico](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005588.html) med en uppdaterad filterdialogruta för att tillåta matchning av tom text.

* Lokala nätverksbläddraren [SMB4K](https://sourceforge.net/p/smb4k/blog/2020/06/smb4k-306-released/) rättade att spara inställningar vid stängning.

* Den integrerade utvecklingsmiljön [KDevelop](https://www.kdevelop.org/news/kdevelop-552-released) har uppdaterats för de flyttade KDE-arkiven.

# Programbutik

{{< img class="text-center" src="homebrew.png" caption="Homebrew" style="width: 391px">}}

Medan vi gradvis blir vana vid att kunna installera individuella program från en applikationsbutik i Linux, händer det omvända i macOS- och Windows-världen. För dessa system introduceras pakethanterare för de som tycker om en källa för att hantera allt på sina system.

Det ledande paketarkivet med öppen källkod för macOS är Homebrew, hanterat av ett expertlag av utvecklare, inklusive före detta KDE-utvecklaren Mike McQuaid.

Denna månad har projektet [KDE Homebrew](https://invent.kde.org/packaging/homebrew-kde), som tidigare har fungerat utanför KDE, flyttat in i KDE för att bli en fullvärdig medlem av vår gemenskap.

Du kan lägga till KDE Homebrew-arkivet för macOS och ladda ner KDE-källkod färdigkompilerad åt dig redo att köras.

Vi fick tag på huvudutvecklaren Yurii Kolesnykov och frågade honom om projektet.

### Berätta för oss om dig själv. Vad heter du, var är du från, vad intresserar dig i KDE och mac, och vad jobbar du med?

Jag heter Yurii Kolesnykov. Jag är från Ukraina. Jag brinner för fri programvara sedan jag först hörde om det, någon gång i slutet av gymnasiet. Jag tycker helt enkelt att KDE är den bästa skrivbordsmiljön för Linux- och Unix-system med många utmärkta program. Mitt intresse för Mac kommer från mitt huvudjobb. Jag utvecklar iOS mobilprogramvara i mitt dagliga jobb.

### Vad är Homebrew?

Homebrew är den populäraste pakethanteraren för macOS, precis som apt eller yum. Eftersom macOS är Unix och Apple tillhandahåller bra kompilatorer och verktygskedjor för det, bestämde sig folk för att skapa pakethanterare för det, så att man kan installera så mycket fri programvara med öppen källkod som möjligt på Mac. Homebrew har också ett delprojekt som kallas Homebrew Cask, som gör det möjligt att installera många binärprogram, dvs. proprietära eller med grafiska användargränssnitt. Eftersom program med grafiska användargränssnitt är svåra att integrera med systemet om de installeras via Homebrew.

### Vilka KDE-paket har du gjort för Homebrew?

Jag körde just grep på vår tappning, och jag ser att vi har totalt 110 paket, 67 av dem är ramverk och ungefär 39 är program. Vi har redan de populäraste programmen, såsom Kate, Dolphin och KDevelop, på grund av användarförfrågningar.

### Vad behöver man göra som Mac-användare för att få programmen installerade?

Till en början måste man följa installationshandledningen för Homebrew, om du inte redan har det. Det är tillgängligt på [brew.sh](https://brew.sh). Därefter måste du tappa av vårt arkiv med följande:

`brew tap kde-mac/kde https://invent.kde.org/packaging/homebrew-kde.git`

Tyvärr är inte många KDE-paket färdiga att använda direkt, men vi skapade ett skript som gör alla nödvändiga ändringarna, så efter att ha tappat av programmet måste följande kommando köras:

`"$(brew --repo kde-mac/kde)/tools/do-caveats.sh"`

### Vet du hur populärt Homebrew är som sätt att skaffa program för Mac?

Bra fråga. Tyvärr har vi inte arrangerat några analysverktyg ännu, jag lägger till det i min uppgiftslista. Men givet det faktum att Homebrew är den populäraste pakethanteraren för Mac och att den kräver att användare inte blandar den med andra liknande projekt för att installera programvara på samma Mac, på grund av konflikter, så tror jag att den är rätt populär.

### Hur mycket arbete behövde du göra för att få KDE-program att fungera i Homebrew?

När de nuvarande paketen skapades, tog vi redan hand om många vanliga problem, så att ta med ny programvara är relativt lätt. Jag lovar att skriva en handledning om det, användare har redan efterfrågat det många gånger.

### För närvarande måste paket kompileras lokalt, kommer du att ha förkompilerade paket tillgängliga?

Homebrew gör att man kan installera programvara via flaskor, dvs. förkompilerade binärpaket. Men processen för att skapa flaskor är tätt integrerad med infrastrukturen i Homebrew, dvs. vi måste köra kontinuerlig integrering med tester på varje paket innan det tappas i en flaska. Så vi bestämde oss för att integrera så många paket som möjligt i huvudarkivet brew för att eliminera underhållsarbetet.

### Finns det mycket annan skrivbordsprogramvara tillgänglig i Homebrew?

Ja, i allmänhet. Om ett program är populärt och har en distributionskanal utanför Mac AppStore är det en stor chans att det redan är tillgängligt att installera via Homebrew Cask.

### Hur kan upphovsmän till KDE-program hjälpa till att få in sin programvara i Homebrew?

Hårdvara från Apple är mycket dyr, så att skaffa en Mac för alla KDE-utvecklare är inte en god idé. Så för närvarande är de välkomna att skapa en funktionsförfrågan i vårt arkiv. Därefter kan underhållsansvariga eller användare av Homebrew KDE rapportera fel om något inte fungerar som avsett. Och vi försöker tillhandahålla så mycket information som möjligt vid förfrågningar från KDE-utvecklare. Men för närvarande har vi en mängd väntande felrapporter för KDE-program med mindre, men mycket irriterande fel. Jag hoppas att vi blir mer integrerade med KDE:s infrastruktur, dvs. vi ska kunna länka fel i vårt arkiv med projekten uppströms. Vi hade redan konverterat till KDE Invent, och jag hoppas KDE:s felspårning snart kommer att konverteras från Bugzilla till KDE Invent.

### Det andra sättet att få KDE-program byggda för Mac är med Craft. Hur liknar program byggda med Homebrew de som byggs med Craft?

Jag tycker fortfarande att Homebrew är vänligare mot slutanvändarna. Dess installationsprocess är så enkel som att köra ett enradskommando. För att lägga till vårt arkiv och börja installera program från det måste man köra ytterligare två rader.

### Tack för din tid, Yurii.

# Utgåvor 20.04.2

Vissa av våra projekt ges ut med sin egen tidsskala och vissa ges ut tillsammans. Packen med projekt 20.04.2 gavs ut idag och kommer snart vara tillgänglig via programvarubutiker och distributioner. Se [20.04.2 utgivningssida](https://www.kde.org/info/releases-20.04.2.php) för detaljerad information.

Några av felrättningarna som ingår i dagens utgåvor:

* Skrivningar delas upp i många förfrågningar för SFTP-servrar som begränsar överföringsstorleken

* Terminal uppdaterar markörpositionen för inmatningsmetoder (såsom IBus eller Fcitx) och kraschar inte längre när den stängs via menyn

* KMail skapar bättre HTML när en HTML-signatur läggs till i brev

[20.04 versionsfakta](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Wikisida för paketnerladdning](https://community.kde.org/Get_KDE_Softwar
e_on_Your_Linux_Distro) &bull; [20.04.2
källkodsinformationssida](https://kde.org/info/releases-20.04.2) &bull; [20.04.2
fullständig ändringslogg](https://kde.org/announcements/changelog-
releases.php?version=20.04.2)