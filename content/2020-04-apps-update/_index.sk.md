---
layout: page
publishDate: 2020-04-23 13:00:00
summary: What Happened in KDE's Applications This Month
title: KDE's April 2020 Apps Update
type: announcement
---
A new bundle of KDE applications is here! In these releases, you can expect to find more features, stability improvements, and more user-friendly tools that will help you work more effectively.

There are dozens of changes to look forward to in most of your favorite applications. Take Dolphin, for example. Windows Samba shares are now fully discoverable.

On the topic of playing music: the Elisa music player is adding features by leaps and bounds. This release brings a new "Now Playing" view, easy accessibility through the system tray, and an option to minimize the playlist whenever you want. Thanks to the recently-added visual shuffle mode, it's much easier to rearrange your music in the playlists.

These are just the highlights of what's new in KDE's applications this month. Read on to find out about everything we've prepared for you.

## [Okular](https://okular.kde.org)

Okular is KDE's application that allows you to read PDFs, Markdown documents, comic books, and many other document formats. In this release, Okular received some accessibility improvements for desktop and touchscreen users alike. For the former, Okular now implements smooth scrolling both when you are using the mouse wheel and the keyboard. For touchscreen users, the latest version of Okular comes with inertial scrolling.

{{<video src="okular.mp4" caption="Okular smooth scrooling. Illustration: David Revoy, CC-BY-SA-4.0" style="max-width: 900px">}}

## [Dolphin](https://kde.org/applications/system/org.kde.dolphin)

KDE’s powerful file manager Dolphin adds new features, allowing you to better interact with remote file systems like Samba shares and SSH servers. Now you can start watching movies stored on remote sites without having to download them, and instead stream them directly from the cloud to your player through Dolphin.

In a similar vein, connecting with Windows Samba shares and taking advantage of all the services they offer has never been easier, as Samba shares are now discoverable via the WS-Discovery protocol used by modern versions of Windows. This allows you to seamlessly integrate your Linux box into a Windows network.

As for the news from the usability department: you don’t need your mouse anymore to move focus to and from the terminal panel. Now you can just use the keyboard shortcut <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F4</kbd>. Another long-awaited feature is searching for files not just by their names or content, but also by their tags.

Support for 7Zip files is now built into Dolphin, meaning you can navigate these compressed files as if they were file system folders. When you install external search applications like KFind, Dolphin lets you quickly access them by creating a link.

Finally, a seemingly small feature that can be a big time-saver is the new "Duplicate" function. Select one or more files or folders, hit <kbd>Ctrl</kbd> + <kbd>D</kbd> (or right-click and choose "Duplicate Here") and - bam! A copy of each selected item will appear right alongside the originals.

![Dolphin duplicate feature](dolphin-duplicate.png)

## [Lokalize](https://kde.org/applications/development/org.kde.lokalize)

Lokalize is a utility you can use to translate gettext and xliff files. It now supports the Language-Tool grammatical correction, which helps translators by highlighting grammatical errors in the text.

## [KMail](https://kde.org/applications/internet/org.kde.kmail2)

KMail is KDE's most popular email client and part of Kontact, a suite of programs that help you manage your emails, contacts, and tasks. Quite a few improvements are included in this new release. For starters, email messages can now be easily exported to PDF, and messages formatted with Markdown are displayed better. The security aspect has also received some attention. More specifically, KMail shows a warning when the message composer opens after clicking a link that asks you to attach a file.

![KMail](kmail-mailto.png)

## [Konsole](https://kde.org/applications/system/org.kde.konsole)

Konsole is a terminal emulator that you use to run command-line instructions, processes, and programs. The latest version of Konsole lets you use the <kbd>Alt</kbd> key + a number key to jump directly to any of the first 9 tabs.

## [Gwenview](https://kde.org/applications/graphics/org.kde.gwenview)

Gwenview is KDE's favorite image viewer, and in this version, the developers have resolved two major issues. The application no longer hangs on launch when the system clipboard contains text from KDE Connect, and importing photos to or from remote locations has been fixed.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa)

Elisa is a simple and popular music player, and this release features the "Now Playing" view that makes it more attractive.

It's not all about the looks, though. Elisa is also more functional, as you can now access it through the system tray and minimize the playlist whenever you want. This means you can keep listening to your favorite music even when the main Elisa window is closed. The new visual shuffle mode makes it easier to rearrange your music in the playlists, and shows you which song will be played next.

![Elisa](elisa.png)

## [Kdenlive](https://kdenlive.org)

Kdenlive is KDE's advanced video-editing application, and the new version makes editing faster thanks to its configurable preview resolution. The preview also improves multitrack viewing, making it easy to select tracks from the ones you are editing all at the same time.

Down on the timeline, the team has improved snapping clips by only snapping on active tracks. They have also fixed the group snap feature and added a shortcut to bypass snapping on move. Another thing you can now do is directly drop your files (videos, music and images) from your file explorer into the timeline. The new "zooming on keyframes" feature will help carry out more precise work. Professional video editors and users coming from other editing tools will undoubtedly appreciate the OpenTimelineIO import/export feature.

This release of Kdenlive also comes with many bug fixes, like the ones for the Motion Tracker, which was broken on some systems. It now lets you add a Pitch compensation filter, as well as add "Move and resize" to a Rotoscoping shape. The crash of the DVD Wizard and a bug that plagued the changing of the record volume from the mixer have also been sorted. The Kdenlive team has also added tagging, rating, and filtering functionality to the project bin.

The new Windows version of Kdenlive also comes with fixes to the timeline dialogs, which previously didn't accept keyboard input. Additionally, you can now select the audio backend on Windows.

![Kdenlive](kdenlive.png)

## [Yakuake](https://kde.org/applications/system/org.kde.yakuake)

Yakuake is a terminal window that you can pull down from the top of the Plasma desktop by pressing <kbd>F12</kbd>. It is handy for running quick instructions on the command-line, and then moving out of the way.

When you open new tabs or split panes in this version of Yakuake, you can now start them in the same directory as the current tab/split. You can also resize Yakuake’s window vertically by dragging on its bottom bar.

## System Settings

The online accounts integration page in System Settings lets you configure things like your Google account, or set up access to your Nextcloud account. The page has been completely overhauled, and now sports a clean, modern look with a much more reliable functionality.

## [KDE Connect](https://kdeconnect.kde.org/)

KDE Connect is an application that helps you integrate your phone with your desktop and vice versa. One of the newest features shipped with this release is the ability to start new conversations with the SMS app.

Other changes include remote media players now showing up in the media player applet, a new set of connection status icons, and improved call notification handling.

In the bug fix department, the error that caused file sharing to block the calling application has been resolved, and the problems with transferring really large files have been sorted out.

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle)

Spectacle, KDE's screen-capturing software, added usability features and resolved some papercuts. One of them prevented you from copying a screenshot to the clipboard when you opened Spectacle from the command-line with the --nonotify option. Although this may have been a niche problem, the point is now you can do exactly that. Likewise, older versions had problems when you tried to drag a screenshot from Spectacle to the default filename pattern that included subfolders. Those problems have been solved.

Another useful improvement in the new version of Spectacle comes in the form of “Defaults” and “Revert” buttons. You can use them to go back to a prior configuration, or even remove all prior custom configurations and restore everything to its default value.

Spectacle will not only remember your default configuration, but also the last used screenshot area. This helps you stay consistent, making it easier to create shots of the same shape and size as your previous ones.

## [Krita](https://krita.org/en/) 4.2.9

There were a number of technical upgrades behind the scenes in this release but also smoother paint brush outlines which don't flicker when you hover over the canvas. “Airbrush” and “Airbrush Rate” was added to the Color Smudge brush, and a new Ratio setting, also for the Color Smudge brush, which allows making the shape of the brush flatter using the different sensors. A new Split Layer into Selection Mask feature allows you to create a new layer for every color in the active layer.

{{< youtube fyc8-qgxAww >}}

## [Smb4K](https://kde.org/applications/utilities/org.kde.smb4k)

The 3.0.4 release of Smb4K fixes several issues and crashes, especially in the mounter. It also makes Smb4K compilable again with Qt version 5.9 and lower.

## [KIO GDrive](https://kde.org/applications/internet/org.kde.kio_gdrive) 1.3

This major release of the plugin which integrates Google Drive with KDE's applications adds a support for the Shared Drives feature of Google Drive.

It also includes a new "Copy Google URL to clipboard" action in the Dolphin context-menu.

[20.04 release notes](https://community.kde.org/Releases/20.04_Release_Notes) • [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) • [20.04 source info page](https://kde.org/info/releases-20.04.0) • [20.04 full changelog](https://kde.org/announcements/changelog-releases.php?version=20.04.0)

