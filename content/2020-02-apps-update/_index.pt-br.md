---
layout: page
publishDate: 2020-02-06 12:00:00
summary: "O que aconteceu com o KDE Applications este m\xEAs"
title: "Atualiza\xE7\xE3o de fevereiro de 2020 dos aplicativos da KDE"
type: announcement
---
Este mês inclui um grande número de lançamentos com correções de erros. Embora todos nós gostamos de novas funcionalidades, sabemos também que as pessoas também amam quando erros e crashes são arrumados e resolvidos!

# Novos lançamentos

## KDevelop 5.5

O grande lançamento deste mês vem do [KDevelop 5.5](https://www.kdevelop.org/news/kdevelop-550-released), a IDE que permite escrever facilmente programas em C++, Python e PHP. Nós consertamos vários erros e organizamos um bocado do código do KDevelop. Incluímos capturas de init Lamba para C++, seleções de checkset pré-definidas e configuráveis para o Clazy-tidy e o Clang, além de auto- completamento do tipo look-ahead incluso para arrays de tipo e classe constant visibility. No Python, agora há suporte ao Python 3.8.

O KDevelop está disponível na sua distribuição Linux ou como AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

O [Zanshin](https://zanshin.kde.org/) é o nosso aplicativo de gerenciamento de tarefas do tipo TODO com integração com o Kontact. Lançamentos recentes do Kontact haviam quebrado o Zanshin e os usuários acabavam ficando sem saber que tarefas fazer a seguir. Porém, neste mês, um lançamento foi feito para [fazer o Zanshin funcionar novamente](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Agora podemos novamente encontrar as tarefas que precisamos terminar de fazer!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

O [Okteta](https://kde.org/applications/utilities/org.kde.okteta), o editor de Hex da KDE, teve correções de erros implementadas e inclui uma nova funcionalidade: um algoritmo CRC-64 para sua ferramenta de checksum. O Okteta também atualizou o código para o novo Qt e KDE Frameworks.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

O [KMyMoney](https://kmymoney.org/), o aplicativo da KDE que permite a você gerenciar suas finanças, [teve várias correções de erros](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) e uma melhoria que fornece suporte a verificar formulários com o protocolo split.

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# O que está por vir

O Keysmith é um gerador de código de dois fatores para o Plasma Mobile e o Plasma Desktop que usa o oath-toolkit. A interface de usuário é escreta em Kirigami, tornando-o adaptável para qualquer tamanho de tela. Esperamos lançamentos em breve.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

O [Chocolatey](https://chocolatey.org) é um gerenciador de pacotes para o sistema operacional Windows. O Chocolatey gerencia a instalação e atualização de todos os programas no sistema e tornam a vida mais fácil para usuários do Windows.

A comunidade [KDE possui uma conta no Chocolatey](https://chocolatey.org/profiles/KDE) e você pode instalar o Digikam, Krita, KDevelop e Kate por ele. A instalação é bastante fácil: basta rodar `choco install kdevelop` e o Chocolatey fará o resto por você. Os mantenedores do Chocolatey tem requisitos sérios para inclusão de software, de modo que você tenha garantia de receber pacotes testados e seguros.

Se você é um mantenedor de aplicativo da comunidade KDE, considere ter seu aplicativo adicionado e, se você é um distribuidor do aplicativo para Windows, considere o Chocolatey para suas instalações.

# Atualizações dos sites

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}Our web team continues to update our online presence and has recently refreshed the [KMyMoney website](https://kmymoney.org).

# Lançamentos 19.12.2

Alguns dos nossos projetos lançam a seu próprio tempo e alguns são lançados em lotes. O conjunto de projetos do 19.12.2 foi lançado hoje e deve ficar disponível nas lojas de aplicativos e distribuições em breve. Dê uma olhada na [página de lançamentos do 19.12.2](https://www.kde.org/info/releases/1912.2.php). Este conjunto era chamado anteriormente de KDE Applications mas teve sua nomeação alterada para se tornar um serviço de lançamentos, deste modo evitando confusão com todos os outros aplicativos da KDE e também por ser dezenas de produtos diferentes ao invés de um conjunto só.

Algumas das correções inclusas neste lançamento são:

* O reprodutor de música [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) agora consegue lidar com arquivos que não contém metadados

* Anexos salvos na pasta *Rascunhos* não desaparecem mais ao reabrir uma mensagem para edição no [KMail](https://kde.org/applications/internet/org.kde.kmail2)

* Um problema de temporização capaz de fazer o [Okular](https://kde.org/applications/office/org.kde.okular) parar de renderizar foi corrigido

* A função UML Designer do [Umbrello](https://umbrello.kde.org/) agora vem com importação Java melhorada

[19.12.2 release notes](https://community.kde.org/Releases/19.12_Release_Notes)
&bull; [Página da wiki para baixar
pacotes](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;
[página com informações da fonte do
19.12.2](https://kde.org/info/releases-19.12.2) &bull; [registro completo de
alterações do 19.12.2](https://kde.org/announcements/changelog-
releases.php?version=19.12.2)