---
layout: page
publishDate: 2020-02-06 12:00:00
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di febbraio 2020
type: announcement
---
Questo mese è testimone di molti rilasci per risoluzione di errori. Sebbene a tutti noi piacciano le novità, sappiamo anche che le persone desiderano che gli errori e i crash siano sistemati e risolti!

# Nuovi rilasci

## KDevelop 5.5

Il maggior rilascio di questo mese è di [KDevelop 5.5](https://www.kdevelop.org/news/kdevelop-550-released), l'ambiente di sviluppo che semplifica la scrittura di programmi in C++, Python e PHP. Abbiamo risolto tanti errori e sistemato un sacco di codice del programma. Abbiamo aggiunto la funzioni di lambda init capture in C++; sono ora presenti selezioni predefinite e di insiemi di controlli per Clazy-tidy e Clang; e il completamento del codice in avanti si impegna maggiormente. PHP dispone ora delle proprietà tipizzate di PHP 7.4 ed è stato aggiunto il supporto per gli array di tipo e la visibilità delle costanti di classe. In Python è stato ora aggiunto il supporto per Python 3.8.

KDevelop è disponibile per la tua distribuzione Linux o come AppImage.

{{< img class="text-center" src="KDevelop_5.5.0.png" link="https://www.kdevelop.org/news/kdevelop-550-released" caption="KDevelop" >}}

## Zanshin 0.5.71

[Zanshin](https://zanshin.kde.org/) è il nostro gestore delle COSE_DA_FARE integrato in Kontact. I recenti rilasci Kontact avevano danneggiato Zanshin e gli utenti non erano più in grado di tracciare le loro attività future. Questo mese è stato eseguito un rilascio al fine di [farlo funzionare nuovamente](https://jriddell.org/2020/01/14/zanshin-0-5-71/). Ora tutte le attività da seguire sono facilmente rintracciabili!

{{< img class="text-center" src="zanshin.png" link="https://jriddell.org/2020/01/14/zanshin-0-5-71/" caption="Zanshin" >}}

## Latte-dock 0.9.8

https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html

{{< img class="text-center" src="latte.png" link="https://psifidotos.blogspot.com/2020/01/latte-bug-fix-release-v0981.html" caption="Latte Dock" >}}

## RKWard 0.7.1

https://rkward.kde.org/News.html

{{< img class="text-center" src="rkward.png" link="https://rkward.kde.org/News.html" caption="RKWard" >}}

## Okteta 0.26.3

[Okteta](https://kde.org/applications/utilities/org.kde.okteta), l'editor esadecimale di KDE, è stato oggetto di un rilascio per correzione errori e include una nuova funzionalità: un algoritmo CRC-64 per il codice di controllo. Okteta aggiorna anche il codice per i nuovi Qt e KDE Frameworks.

{{< img class="text-center" src="okteta.png" link="https://kde.org/applications/utilities/org.kde.okteta" caption="Okteta" >}}

## KMyMoney 5.0.8

[KMyMoney](https://kmymoney.org/), l'applicazione KDE che ti aiuta a gestire le tue finanze, [include diverse correzioni errori](https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html) e un aggiornamento che aggiunge supporto per i moduli di controllo con protocollo diviso

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/changelogs/kmymoney-5-0-8-release-notes.html" caption="KMyMoney" >}}

# In arrivo

Keysmith è un generatore di codici a due fattori per Plasma Mobile e Plasma Desktop che utilizza il kit di strumenti oath. L'interfaccia utente è scritta con Kirigami, rendendola agile con qualsiasi dimensione dello schermo. È atteso un rilascio a breve.

# Chocolatey

{{< img class="text-center" src="chocolatey.png" link="https://chocolatey.org/profiles/KDE" caption="Chocolatey" >}}

[Chocolatey](https://chocolatey.org) è un gestore di pacchetti per il sistema operativo Windows. Chocolatey gestisce l'installazione e l'aggiornamento di tutto il software nel sistema e può rendere più semplice la vita degli utenti Windows.

[KDE possiede un account in Chocolatey](https://chocolatey.org/profiles/KDE) e con esso puoi installare Digikam, Krita, KDevelop e Kate. L'installazione è molto semplice: tutto quello che devi fare è digitare `choco install kdevelop` Chocolatey farà il resto. I responsabili di Chocolatey lavorano con standard elevati; dunque puoi esser certo che i pacchetti sono verificati e sicuri.

Se sei il responsabile di un'applicazione KDE considera di farvi aggiungere la tua applicazione, e se sei gestisci della installazioni consistenti di sistemi Windows tienilo presente per le tue installazioni.

# Aggiornamenti sito web

{{< img class="text-center" src="kmymoney.png" link="https://kmymoney.org/" caption="KMyMoney" >}}Our web team continues to update our online presence and has recently refreshed the [KMyMoney website](https://kmymoney.org).

# Releases 19.12.2

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri vengono rilasciati in massa. Il gruppo 19.12.2 dei progetti è stato rilasciato oggi e sarà presto disponibile negli app store nelle distribuzioni.  Per i dettagli, consulta la [pagina dei rilasci 19.12.2](https://www.kde.org/announcements/releases/19.12.2.php).  Questo gruppo era chiamato in precedenza KDE Applications ma tale nome è stato modificato ed è diventato un servizio di rilascio in modo da evitare confusione con tutte le altre applicazioni create da KDE e in quanto è composto da decine di prodotti differenti che non vanno considerati come un singolo prodotto.

Alcune delle correzioni incluse in questo rilascio sono:

* Il lettore musicale [Elisa](https://kde.org/applications/multimedia/org.kde.elisa) ora gestisce i file che non contengono metadati

* Gli allegati salvati nella cartella *Bozze* ora non scompaiono più quando si riapre un messaggio per la modifica in [KMail](https://kde.org/applications/internet/org.kde.kmail2)

* È stato risolto un problema di temporizzazione che poteva causare un blocco del rendering in [Okular](https://kde.org/applications/office/org.kde.okular), il visore di documenti

* [Umbrello](https://umbrello.kde.org/), il software di progettazione UML, ora possiede un'importazione Java migliorata

[note di rilascio
19.12.2](https://community.kde.org/Releases/19.12_Release_Notes) &bull; [Pagina
wiki per lo scaricamento dei
pacchetti](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [Pagina informativa sui sorgenti
19.12.2](https://kde.org/info/releases-19.12.2) &bull; [Elenco completo delle
modifiche 19.12.2](https://kde.org/announcements/changelog-
releases.php?version=19.12.2)