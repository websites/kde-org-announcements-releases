---
layout: page
publishDate: 2020-07-09 12:00:00
summary: Wat gebeurde er in de toepassingen van KDE deze maand
title: Update van toepassingen van KDE in juli 2020
type: announcement
---
# Nieuwe uitgaven

### KTorrent 5.2.0

Toepassing voor delen van bestanden [KTorrent](https://kde.org/applications/en/internet/org.kde.ktorrent) heeft een nieuwe uitgave [5.2.0](https://mail.kde.org/pipermail/kde-announce-apps/2020-June/005591.html).

De verbetering van importeren voor delen is [Distributed Hash Table](https://en.wikipedia.org/wiki/Mainline_DHT) verbeteringen die nu start met welbekende nodes zodat u uw downloads sneller krijgt. Onder de motorkap werkt het de nieuwere QtWebengine bij die gebaseerd is op Chrome weg van het oudere QtWebkit gebaseerd op WebKit (allen zijn gebaseerd op het vroegere KHTML van KDE ).

{{< img class="text-center" src="ktorrent.png" caption="KTorrent" style="width: 600px">}}

KTorrent is beschikbaar via uw Linux distributie.

### KMyMoney 5.1.0 vrij gegeven

Toepassing voor bankieren [KMyMoney](https://kmymoney.org/) [uitgave 5.1](https://blog.bembel.net/2020/06/kmymoney-5-1-0-released/).

Er is ondersteuning voor het Indiase Rupee-symbool: ₹ toegevoegd. Er is ook de optie om “Kosten en betalingen terug te draaien” aan OFX importeren en de budgetweergave toont nu alle typen rekeningen.

{{< img class="text-center" src="kmymoney.png" caption="KMyMoney" style="width: 600px">}}

[KMyMoney is beschikbaar](https://kmymoney.org/download.html) in uw Linux distributie, als een download in Windows, een download in Mac en nu in [Homebrew KDE](https://invent.kde.org/packaging/homebrew-kde).

### Uitgavenotities van KDiff3 1.8.3

Hulpmiddel voor vergelijken van bestanden [KDiff3](https://kde.org/applications/en/development/org.kde.kdiff3) heeft een nieuwe versie 1.8.3 vrijgegeven met een reeks van reparaties voor stabiliteit.

KDiff3 gebruiken als een difftool voor Git zal niet langer een fout geven op niet-bestaande bestanden. Fouten tijdens vergelijken van mappen worden op de juiste manier in een rij gezet zodat slecht één bericht zal verschijnen. Opnieuw laden op Windows is gerepareerd. Een crash is verwijderd wanneer het klembord niet beschikbaar is. Omschakelen van volledig scherm is herzien om een problematische aanroep van een Qt API wordt vermeden.

U kunt [KDiff3 downloaden](https://download.kde.org/stable/kdiff3/) voor Windows, Mac en uw Linux distributie.

# App Store

### Microsoft Store Stats

[Christoph Cullmann](https://kate-editor.org/post/2020/2020-06-20-windows-store-monthly-statistics/) gaf ons eenn update op de Microsoft store. Kate en Okular zijn bijgewerkt en in de laatste maand hebben beiden meer dan 4000 installaties gehad.

{{< img class="text-center" src="kate-ms-store.png" caption="Kate" style="width: 600px">}}

# App Store Interview: Flathub

Flatpak is een van de nieuwe op containers gebaseerde formaten die wijzigt hoe we onze toepassingen op Linux verkrijgen. Flatpak kan met elke host werken die een store (winkel) wil starten maar de definitieve store is [Flathub](https://flathub.org/home).

Recent heeft de helper Flathub [Timothée Ravier](https://discourse.flathub.org/u/Siosm) [gevraagd voor hulp om meer KDE toepassingen in de store te stoppen](https://discourse.flathub.org/t/kde-apps-call-for-contributors/514). We hebben hen geïnterviewd om meer te weten te komen.

### Vertel ons iets over jezelf, waar km je vandaan, wat doe je voor de kost, hoe kwam je terecht in open source en Flatpaks?

Mijn naam is Timothée Ravier en ik woon op dit moment in Parijs, Frankrijk. Ik ben een Linux systeemingenieur en ik werk bij Red Hat op Red Hat CoreOS en Fedora CoreOS.

Ik kwam in open source terecht toen ik voor de eerste keer een Linux-distributie installeerde in 2006 en daarna nooit meer ben gestopt. De meeste van de onderzoekprojecten die ik ondernam gedurende mijn studies waren gerelateerd aan de beveiliging van Linux, sandboxing van toepassingen en beveiliging van het grafische interface. De Flatpak introductie en ontwikkeling kreeg mijn interesse.

In mijn vrije tijd onderhoud ik de niet-officiële [KDE variant (nicknamed Kinoite)](https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147) van Fedora Silverblue. In het kort, Fedora Silverblue is een niet-te-wijzigen besturingssysteem en de aanbevolen manier om toepassingen te installeren is om Flatpaks of containers (via podman) te installeren. Zie de documentatie voor meer details.

### Waarom stuurde je je recente oproep uit voor KDE toepassingen in Flathub?

Ten eerste wil ik een geweldig “Dank u” uitspreken naar de huidige onderhouders die al KDE toepassingen onderhouden op Flathub omdat ze een geweldige taak uitvoeren!

Ik ben al heel lang een KDE gebruiker (ik begon in 2006) en ik wilde altijd al iets terugdoen. Distributies hebben al teams van gevestigde onderhouders en in Flathub ontbrak een groot aantal KDE toepassingen en dus voelde het als een goede plek om te beginnen.

Ik heb ook een oproep gedaan omdat het gemakkelijker zal zijn als we het werk splitsen en misschien dat ook meer mensen bewust zullen worden van het bestaan van Flatpaks en Flathub.

### Flatpak kan vanuit elke opslagruimte werken, waarom is Flathub nodig?

Deze vraag accentueert een van de voordelen van Flathub: u kunt uw eigen opslagruimte van toepassingen hebben op uw eigen server en ze direct distribueren naar uw gebruikers. U hebt Flathub niet “nodig” .

Maar zoals u GitHub of GitLab, etc. niet nodig hebt om een Git opslagruimte te hebben, is het veel gemakkelijker om samen te werken als u een enkele plaats hebt om gebruikers en ontwikkelaars naar te verwijzen.

Flathub is de gemakkelijkste plaats geworden om veilig Linux toepassingen te zoeken en te proberen, zowel open source als in eigendom. Ik denk dat dit krietiek is als we de attractiviteit van het Linux ecosystem als een bureaubladplatform willen vergroten.

### Welke andere open source gemeenschappen hebben het zetten van hun toepassingen op Flathub omarmd?

Ik denk dat heel wat (misschien de meeste) GNOME toepassingen nu beschikbaar zijn op FlatHub.

### Nu de ontwikkelaars van toepassingen onze software direct in de stores zoals Flathub kunnen stoppen zijn er nieuwe verantwoordelijkheden zoals beveiliging en software up-to-date houden bijgekomen. Kun je zeggen hoe goed deze behandeld worden in Flathub?

Met Flathub worden de verantwoordelijkheden gedeeld tussen de onderhouders van het Platform en de onderhouders van de toepassingen.

De Platforms bevatten de kernbibliothhek gemeenschappelijk met veel toepassingen (er zijn Freedesktop, GNOME en KDE platforms) en worden onderhouden om zowel ABI compatibiliteit te behouden als het verzekeren van prompt bijwerken voor veiligheid.

Het bijwerken naar de overblijvende bibliotheken vereist door een toepassing en de toepassing zelf is de verantwoordelijkheid van de onderhouder van de toepassing.

### Welke KDE toepassingen vind je het meest nuttig?

Ik gebruik Dolphin, Konsole, Yakuake, Okular en Ark dagelijks en ik vind ze echt goed. Ik waardeer en gebruik Gwenview, KCachegrind en Massif-Visualizer af en toe.

### Veel van onze toepassingen zijn ingepakt als Flatpaks via onze [invent](https://invent.kde.org/packaging/flatpak-kde-applications) en [binary-factory servers](https://binary-factory.kde.org/view/Flatpak/) werk je met deze processen of afzonderlijk?

De Flatpaks die gebouwd zijn op de KDE infrastructuur zijn bedoeld als nachtlelijk gebouwd voor ontwikkelaars en gebruikers om uit te proberen. Dit is een goede pool met Flatpak toepassingen om mee te beginnen maar sommigen moeten ook bijgewerkt worden. Deze opslagruimte bijgewerkt te houden zal ons helpen met recente ontwikkelaars die wijzigingen in pakketten maken op Flathub vereisen. Ik ben nog niet begonnen met ze bij te werken, maar ik zal het proberen te doen in lijn met het inbrengen van de toepassingen in Flathub.

### Kunt u een tijd voorstellen dat RPM's en Apt er niet meer zijn en Linux distributies allemaal containerpakketten gebruiken?

I denk niet dat dit ooit zal gebeuren omdat er waarde is in hoe distributies op dit moment toepassingen verpakken hoewel het ook zijn problemen heeft. Maar ik denk dat minder distributies de inspanning willen doen om het te doen. Fedora, bijvoorbeeld, bouwt Flatpaks uit RPM pakketten en maakt ze voor iedereen beschikbaar. U kunt datzelfde potentieel ook doen met Debian pakketten. De waarde is hier niet in het wat maar in het wie: vertrouwt u deze distributie? Zijn waarden? Zijn toewijding aan alleen vrije software? Dan zult u zeker zijn dat de toepassingen die u installeert uit hun opslagruimten dezelfde vereisten hebben die ieder ander pakket heeft. Flathub heeft beide, open source en gepatenteerde toepassingen, en dat wil niet iedereen.

# Uitgaven nu op kde.org/applications

Onze [Toepassingensub-site](https://kde.org/applications) is begonnen met het tonen van informatie over uitgaven. Verwacht dat er spoedig meer komt. Als u een onderhouder van een toepassing bent denk er dan om de informatie over de uitgave aan de Appstream-bestanden toe te voegen.

{{< img class="text-center" src="krita-release.png" caption="Informatie over uitgaven" style="width: 800px">}}

# Uitgaven 20.04.3

Sommigen van onze projecten worden vrijgegeven in hun eigen tijdschaal en sommigen in massa vrijgegeven. De bundel 20.04.3 van projecten is vandaag vrijgegeven en zouden spoedig beschikbaar moeten zijn via opslagruimten voor toepassingen en distributies. Zie de [20.04.3 uitgavepagina](https://kde.org/info/releases-20.04.3) voor details.

Enige van de reparaties in de uitgave van vandaag:

* Voorbeelden van desktop-bestanden in Dolphin zijn gerepareerd voor absolute paden naar pictogrammen

* Items voor voltooide taken worden nu juist opgenomen in het journaal van KOrganizer

* Tekst met meerdere regels geplakt uit GTK-toepassingen naar Konsole krijgen niet langer extra "nieuwe-regel" tekens

* Het maximaliseren van Yakuake is gerepareerd

[20.04 uitgavenotities](https://community.kde.org/Releases/20.04_Release_Notes)
&bull; [Wiki-pagina voor downloaden van
pakketten](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
&bull; [20.04.03 informatiepagina van
broncode](https://kde.org/info/releases-20.04.3) &bull; [20.04.03 volledige log
met wijzigingen](https://kde.org/announcements/changelog-
releases.php?version=20.04.3)