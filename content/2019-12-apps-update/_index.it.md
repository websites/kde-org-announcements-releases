---
layout: page
publishDate: 2019-12-12 13:01:00
summary: "Che cosa \xE8 successo questo mese alle applicazioni KDE"
title: Aggiornamento applicazioni KDE di dicembre 2019
type: announcement
---
# Nuove versioni delle applicazioni KDE in arrivo a dicembre

Il rilascio delle nuove versioni delle applicazioni KDE fa parte del continuo sforzo di KDE per offrirti un catalogo completo e aggiornato di programmi belli e ricchi di funzionalità per il tuo sistema.

Sono disponibili ora nuove versioni di: Dolphin, il gestore dei file di KDE; Kdenlive, uno degli editor video open source più completi; il visore di documenti Okular; il visore di immagini di KDE, Gwenview e tutte le tue altre applicazioni e accessori di KDE preferite. Tutte queste applicazioni sono state migliorate, rendendole più veloci e stabili, e aggiungendo loro nuove fantastiche funzionalità. Le nuove versioni delle applicazioni KDE ti permetteranno di essere più produttivo e creativo, rendendo allo stesso tempo più facile e divertente l'uso del software KDE.

Speriamo che tutte le nuove funzionalità e i miglioramenti delle applicazioni KDE siano di tuo gradimento!

## [Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) Reloaded

[Calligra Plan](https://kde.org/applications/office/org.kde.calligraplan) fa parte di un rilascio principale in dicembre. Lo strumento di gestione e pianificazione dei progetti di KDE ha raggiunto un rilascio fondamentale, dato che la versione precedente rilasciata è vecchia di quasi due anni.

{{< img class="text-center" src="kplan.png" link="https://dot.kde.org/sites/dot.kde.org/files/kplan.png" caption="Calligra Plan" >}}

Plan ti permette di gestire progetti grandi e piccoli, con più risorse. Per modellare il tuo progetto, Plan offre diversi tipi di dipendenze per attività e vincoli di tempo. Puoi definire attività, stimare lo sforzo necessario per raggiungerle, allocare risorse e pianificare il progetto in base alle tue necessità e risorse disponibili.

Uno dei punti di forza di Plan è il suo supporto eccellente dei grafici Gantt. I grafici Gantt sono formati da barre orizzontali che forniscono una visione grafica di un programma e aiutano a pianificare, coordinare e tenere traccia di attività specifiche in un progetto. L'uso dei grafici Gantt in Plan ti aiuterà a monitorare meglio il flusso di lavoro dei tuoi progetti.

## Alza il volume in [Kdenlive](https://kdenlive.org)

Gli sviluppatori di [Kdenlive](https://kdenlive.org) hanno aggiunto nuove funzionalità ed eliminato errori in tempi rapidissimi. Questa versione è frutto di più di 200 commit.

Un sacco di lavoro è stato fatto per migliorare il supporto audio. Nel reparto dei bug risolti, sono stati risolti problemi di consumo eccessivo di memoria, inefficienza delle anteprime audio e aumentato la velocità di salvataggio dei file.

Fatto ancora più entusiasmante, Kdenlive ora è rilasciato con un nuovo e spettacolare mixer del suono (vedi immagine). Gli sviluppatori hanno anche aggiunto una nuova visualizzazione per le clip audio nel monitor delle clip e il contenitore dei progetti in modo da sincronizzare meglio le immagini in movimento con la traccia audio.

{{< img class="text-center" src="Kdenlive1912BETA.png" link="https://kdenlive.org/wp-content/uploads/2019/11/Kdenlive1912BETA.png" caption="Mixer audio di Kdenlive" >}}

Per quanto riguarda il reparto di «risoluzione delle spigolosità», Kdenlive ha beneficiato di molte migliorie nelle prestazioni e nell'usabilità, e gli sviluppatori hanno corretto molti errori nella versione Windows dell'editor.

## Anteprime e navigazione in [Dolphin](https://userbase.kde.org/Dolphin)

[Dolphin](https://userbase.kde.org/Dolphin), il navigatore completo di file di KDE, aggiunge nuove funzionalità che ti aiutano a cercare e trovare quello che stai cercando nel tuo file system. Una tra esse sono le opzioni avanzate di ricerca riprogettate e un'altra è la possibilità di navigare più volte nella cronologia visitata premendo a lungo l'icona a forma di freccia nella barra degli strumenti. La funzionalità che ti permette di eseguire la ricerca rapida dei file salvati o aperti di recente è stata rivista e le sue asperità eliminate.

Una delle preoccupazioni principali degli sviluppatori di Dolphin è permettere agli utenti di visualizzare in anteprima il contenuto dei file prima di aprirli. Per esempio, nella nuova versione di Dolphin puoi vedere in anteprima le GIF selezionandole e passandovi sopra il mouse nel pannello delle anteprime. Nello stesso pannello puoi riprodurre anche file video e audio.

Se sei un appassionato di fumetti, Dolphin ora è in grado di visualizzare miniature per i fumetti .cb7 (verifica anche il supporto per il formato dei fumetti anche in Okular, vedi più avanti). E, per quanto riguarda le miniature, in Dolphin è possibile da tempo anche ingrandirle: tieni premuto <kbd>Ctrl</kbd>, gira la rotellina e la miniatura ingrandirà o rimpicciolirà. La nuova funzione è la possibilità di azzerare lo zoom al livello predefinito premendo <kbd>Ctrl</kbd> + <kbd>0</kbd>.

Un'altra utile funzione di Dolphin è l'indicazione di quali programmi impediscono di smontare un dispositivo.

{{<img src="dolphin.png" style="max-width: 500px" >}}

## [KDE Connect](https://community.kde.org/KDEConnect): lascia che il tuo telefono controlli il tuo desktop

L'ultima versione di [KDE Connect](https://community.kde.org/KDEConnect) è ricca di nuove funzionalità. Una di spicco è una nuova app SMS che ti permette di leggere e scrivere SMS con l'intera cronologia della conversazione.

Una nuova interfaccia utente basata su [Kirigami](https://kde.org/products/kirigami/) significa che puoi avviare KDE Connect non solo su Android ma anche su tutte le piattaforme Linux mobili in arrivo quali PinePhone e Librem 5. L'interfaccia Kirigami offre anche nuove funzionalità agli utenti desktop-desktop, tipo controlli dei dispositivi multimediali, input telecomando, suoneria dispositivo, trasferimento di file ed esecuzione di comandi.

{{<img src="kdeconnect2.png" style="max-width: 600px" caption="La nuova interfaccia sul desktop" >}}

Puoi già utilizzare KDE Connect per controllare il volume del dispositivo multimediale in riproduzione sul tuo desktop, ma ora puoi utilizzarlo anche per controllare dal tuo telefono il volume globale del tuo sistema -- comodissimo quando utilizzi il tuo telefono come telecomando. Durante un talk, puoi utilizzare KDE Connect anche per controllare la tua presentazione e scorrere avanti e indietro le diapositive.

{{<img src="kdeconnect1.png" style="max-width: 400px" >}}

Una funzionalità già presente da un po' di tempo è l'uso di KDE Connect per inviare un file al tuo telefono, e con la nuova versione è stata implementata l'opzione che permette di aprire immediatamente il file una volta ricevuto nel telefono. KDE Itinerary usa questa opzione per inviare informazioni sul viaggio al telefono dall'interno di KMail.

Restando in tema, puoi anche inviare file da Thunar (il gestore file di Xfce) e dalle applicazioni di Elementary, tipo Pantheon Files. L'avanzamento grafico della copia di più file è stato molto migliorato e i file ricevuti da Android ora mostrano l'ora corretta di modifica.

Quando si ricevono notifiche ora puoi compiere azioni dalle notifiche Android che appaiono nel desktop, mentre KDE Connect sfrutta le funzionalità avanzate del centro di notifiche Plasma per fornire notifiche migliorate. La notifica di conferma dell'accoppiamento dei dispositivi ora non scade e ti permette di eseguire l'operazione in modo ottimale.

Per finire, se stai utilizzando KDE Connect dalla riga di comando, *kdeconnect-cli* ora offre funzioni di completamento automatico per *zsh* migliorate.

## Immagini remote [Gwenview](https://userbase.kde.org/Gwenview)

[Gwenview](https://userbase.kde.org/Gwenview) ora ti permette di regolare il livello di compressione JPEG durante il salvataggio delle immagini modificate. Gli sviluppatori hanno migliorato anche le prestazioni per le immagini remote e ora Gwenview può importare e esportare foto da posizioni remote.

## [Okular](https://kde.org/applications/office/org.kde.okular) supporta il formato .cb7

[Okular](https://kde.org/applications/office/org.kde.okular), il visore di documenti che ti permette di leggere, annotare e verificare tutti i tipi di documento, inclusi PDF, EPub, ODT e MD, ora supporta anche i file che utilizzano il formato per fumetti .cb7.

## [Elisa](https://kde.org/applications/multimedia/org.kde.elisa): il lettore musicale semplice ma completo

[Elisa](https://kde.org/applications/multimedia/org.kde.elisa) è il lettore musicale di KDE che combina la semplicità a un'interfaccia moderna ed elegante. Nell'ultima versione Elisa ha ottimizzato il suo aspetto per adattarsi meglio agli schermi ad alta risoluzione. Ha migliorato anche l'integrazione con le altre applicazioni KDE e ha ottenuto il supporto per il sistema di menu globale di KDE.

L'indicizzazione dei file musicali è stata migliorata e ora Elisa supporta le radio web e contiene alcuni esempi da provare.

{{< img src="elisa.png" caption="Interfaccia aggiornata di Elisa" >}}

## [Spectacle](https://kde.org/applications/utilities/org.kde.spectacle): pronto per i touchscreen

[Spectacle](https://kde.org/applications/utilities/org.kde.spectacle), il programma di cattura schermo predefinito di KDE, è un'altra applicazione che si è evoluta per acquisire capacità con gli schermi a sfioramento. Il suo nuovo sistema per il trascinamento rende molto più semplice trascinare rettangoli per catturare parti del desktop nei dispositivi abilitati allo sfioramento di tocco (touchscreen).

Altri miglioramenti sono la nuova funzione di salvataggio automatico, comoda da usare per ottenere rapidamente più schermate, e la barra di avanzamento animata, che ti permette di osservare in modo chiaro l'operazione intrapresa da Spectacle.

{{<img style="max-width:500px" src="spectacle.png" caption="Nuovo gestore del trascinamento di Spectacle" >}}

## Miglioramenti a [Plasma Browser Integration](https://community.kde.org/Plasma/Browser_Integration)

Il nuovo rilascio di [Plasma Browser Integration](https://community.kde.org/Plasma/Browser_Integration) ora offre una lista nera per le funzionalità dei controlli multimediali. Utile quando visiti un sito con molti contenuti multimediali da riprodurre, che può rendere difficile la selezione di cosa controllare dal tuo desktop. La lista nera ti permette di escludere questi siti dai controlli multimediali.

La nuova versione ti permette di mantenere l'URL di origine nei metadati del file e aggiunge supporto per l'API Web Share. L'API Web Share permette ai browser di condividere collegamenti, testo e file con altre applicazioni in modo simile a quello utilizzato dalle applicazioni KDE, migliorando l'integrazione di browser non nativi quali Firefox, Chrome/Chromium e Vivaldi col resto delle applicazioni KDE.

{{<learn-more href="https://blog.broulik.de/2019/11/plasma-browser-integration-1-7/" >}}

## Microsoft Store

Poche applicazioni KDE possono essere scaricate da Microsoft Store. Krita e Okular sono presenti da un po' e ora sono state raggiunte da [Kile](https://kde.org/applications/office/org.kde.kile), un editor LaTeX facile da usare.

{{<microsoft-store href="https://www.microsoft.com/store/apps/9pmbng78pfk3" >}}

## Nuovo

Che ci porta ai nuovi arrivi: [SubtitleComposer](https://invent.kde.org/kde/subtitlecomposer), applicazione che permette di creare con facilità sottotitoli per video, ora è in KDE Incubator, sul cammino per diventare un membro a tutti gli effetti della famiglia delle applicazioni KDE. Benvenuto!

Nel frattempo, [plasma-nano](https://invent.kde.org/kde/plasma-nano), una versione minimalista di Plasma progettata per dispositivi incorporati, è stato spostato nei repository Plasma pronta per il rilascio con la versione 5.18.

## Rilascio 19.12

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri
vengono rilasciati in massa. Il gruppo 19.12 dei progetti è stato rilasciato
oggi e sarà presto disponibile negli app store nelle distro.  Consulta la
[pagina dei rilasci 19.12](https://www.kde.org/announcements/releases/19.12).
Questo gruppo era chiamato in precedenza KDE Applications ma tale nome è stato
modificato ed è diventato un servizio di rilascio in modo da evitare confusione
con tutte le altre applicazioni create da KDE e in quanto è composto da decine
di prodotti differenti che non vanno considerati come un singolo prodotto.