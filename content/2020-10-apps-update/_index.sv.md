---
draft: false
layout: page
publishDate: '2020-09-10T12:00:00+00:00'
summary: "Vad har h\xE4nt i KDE:s program den h\xE4r m\xE5naden"
title: KDE:s programuppdateringar i oktober 2020
type: announcement
---
Efter Akademy, har en bräddfull månad med nya uppdateringar av programvara och felrättningar i KDE:s program anlänt.

# Nya utgåvor

## digiKam 7.1

{{< img class="text-center img-fluid" src="digikam.png" caption="digiKam" style="width: 600px">}}

Månaden inleddes med utgivningen av vår populära  [fotohanteringsprogram, digiKam](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

Bättre stöd för Canon CR3 metadata, obehandlade format från kameror är ofta odokumenterade och svåra att behandla. Utvecklarna av digiKam 7.1.0 har skrivit ett metadatagränssnitt baserat på libraw för CR3, och programmet kan nu läsa en större del av Exif-taggarna, inklusive GPS-information, färgprofil, och naturligtvis IPTC- och XMP-standardbehållare.

Ett nytt insticksprogram för bakgrundsköhantering har introducerats som låter dig utföra rättningar för bilder och ett annat som automatiskt rättar heta bildpunkter, det vill säga de "dåliga bildpunkter" man får när långsamma slutarhastigheter används eller vid arbete med nattbilder.

[IPTC informationsutbytesmodellen](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) är det gamla, men fortfarande vanliga metadataformatet som används för foton, och den här utgåvan förbättrar Unicode-stöd för det.

digiKam är tillgängligt i Linux-distributioner som ett tar-källkodsarkiv, Linux 32/64-bitars AppImage-packar, macOS-paket och Windows 32/64-bitars installationsprogram. [Ladda ner digiKam 7.1.0 här](https://download.kde.org/stable/digikam/).

## Labplot 2.8

{{< img class="text-center img-fluid" src="labplot.png" caption="Labplot" style="width: 600px">}}

LabPlot är ett KDE-program för interaktiv grafik och analys av vetenskaplig data. LabPlot tillhandahåller ett enkelt sätt att skapa, hantera och redigera diagram, och utföra dataanalys. [Labplot 2.8](https://labplot.kde.org/2020/09/16/labplot-2-8-released/) har givits ut efter ett årslångt arbete.

I 2.8 är det nu enklare att komma åt många nätresurser som tillhandahåller datauppsättningar i utbildningssyfte. Det finns nu fem samlingar med över 2000 dokumenterade datauppsättningar. Titta på den här videon för mer information:

{{< youtube pyP-5J3seQ8 >}}

Utgåvan levereras med två nya arbetsbladsobjekt: referenslinjer och bildelement. Referenslinjer kan placeras fritt på diagrammet för att markera vissa värden i visualiserad data. Bildelement låter dig lägga till en bild på diagrammet eller arbetsbladet.

Arbetsbladet tillhandahåller beskrivande statistik för datamängderna. Vi har utökat den här informationen och lagt till beräkningar av kvartiler och trimedelvärden i statistikläge.

Tack vare arbetet som utförts i Cantor under de senaste månaderna, har utvecklarna kunnat lägga till kompatibilitet med Jupyter projektfiler i den här utgåvan av Labplot.

Mer arbete har lagts ner på macOS-versionen av Labplot. Förutom många små macOS-specifika rättningar, har vi också lagt till stöd för pekfältet på nyare MacBook Pro modeller.

[Labplot kan laddas ner](https://labplot.kde.org/download/) för Linux, Windows och Mac.

## KDevelop 5.6

{{< img class="text-center img-fluid" src="KDevelop_5.6.0.png" caption="KDevelop" style="width: 600px">}}

Vår integrerade utvecklingsmiljön för programvaruutveckling, [KDevelop 5.6](https://www.kdevelop.org/news/kdevelop-560-released), för med sig ett halvårs arbete, i huvudsak fokuserat på stabilitet, prestanda, och framtida hållbarhet. Det lägger till en användbar funktion att [valfritt visa anmärkningar på plats för problem i slutet av raden](https://blog.david-redondo.de/kde/2020/02/28/problems.html).

## Calindori 1.2

{{< img class="text-center img-fluid" src="dayview_desktop.png" caption="Calindori" style="width: 600px">}}

Det mobilbaserade kalenderprogrammet [Calindori 1.2](https://dimitris.cc/kde/2020/09/11/Calindori_1_2.html) har getts ut! Även om några versioner också har taggats, är det här den första stabila utgåvan av Calindori som ett KDE-program.

Calindori är tillgänglig på KDE Neon för Plasma Mobil samt på postmartketOS. Det är också tillgänglig i flatpak nattliga kdeapps-arkiv för ARM och x84_64. Slutligen kan det också byggas från källkod på Linux skrivbordsarbetsstationer. Källkoden och signaturer kan laddas ner från [download.kde.org](https://download.kde.org/stable/calindori/).

## Kid3 3.8.4

[Kid3](https://invent.kde.org/websites/kid3-kde-org) förbättrar vårt program för musikfilstaggning.

Förutom felrättningar tillhandahåller utgåvan användbarhetsförbättringar, såsom fler snabbtangenter för navigering och möjlighet att anpassa dem. Detektering av filtyper är mer tolerant.

Hämta det på [Windows Chocolatey Package](https://chocolatey.org/packages/kid3/), [macOS Homebrew-paket](https://formulae.brew.sh/cask/kid3), [Android F-Droid paket](https://f-droid.org/en/packages/net.sourceforge.kid3/), [Flatpak](https://flathub.org/apps/details/org.kde.kid3), [macOS binärfil](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-Darwin.dmg?download), [Windows binärfil](https://prdownloads.sourceforge.net/kid3/kid3-3.8.4-win32-x64.zip?download)

# Felrättningar

## Heaptrack 1.2

{{< img class="text-center img-fluid" src="gui_allocations_chart.png" caption="Heaptrack" style="width: 600px">}}

För utvecklarna där ute, har en [felrättningsutgåva av Heaptrack](https://invent.kde.org/sdk/heaptrack/-/releases/v1.2.0) gjorts. Heaptrack spårar alla minnestilldelningar och förser händelserna med noter om bakåtspårningar av stacken. Särskilda analysverktyg låter dig sedan tolka heap-minnesprofilen.

Det har gjorts förbättringar av stabiliteten för Heaptrack som gör det mer felokänsligt när data spelas in. Dessutom har några mindre justeringar av det grafiska analysverktyget gjorts för att göra det effektivare att använda. Analysstegets prestanda har också förbättrats något. Slutligen har ett viktigt fel i datajämförelsealgoritmen rättats, som bör göra funktionen mer användbar igen.

## KDiff3 1.8.4

{{< img class="text-center img-fluid" src="diffscreen_two_way.png" caption="KDiff3" style="width: 600px">}}

KDiff3 låter dig jämföra upp till tre textfiler samtidigt.

[Den här uppdateringen](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005601.html) rättar felskrivningar, återinläsning av filer på Windows och nätverkskopiering.

KDiff3 kan [laddas ner](https://download.kde.org/stable/kdiff3/) för Linux, Mac och Windows.

## Tellico 3.3.3

Samlingshanteraren [Tellico 3.3.3](https://tellico-project.org/tellico-3-3-3-released/) har förbättrat datakällan Entrez (Pubmed) för att använda en API-nyckel om tillgänglig och att respektera hastighetsgränsen. Utvecklarna har också förbättrat ISDBdb datakällan så att den söker efter flera ISBN-värden.

## Konversation 1.7.6

IRC-chattprogrammet [Konversation](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005610.html) har en felrättning som kan byggas med flera Qt-versioner och rättar inläsning av ikonteman för smeknamn.

## Insticksprogram för visning av Markdown

{{< img class="text-center img-fluid" src="kmarkdownwebview.png" caption="Brand" style="width: 600px">}}

Ett par insticksprogram har uppdaterats den här månaden. [Delprogrammet för Markdown visning](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005607.html) och [Webbvisning av Markdown](https://mail.kde.org/pipermail/kde-announce-apps/2020-September/005608.html) har båda nya utgåvor som uppdaterar deras översättningar och rättar byggning med äldre Qt-versioner. Båda insticksprogrammen förhandsvisar filer formaterade med Markdown i Kate och andra KDE-program.

# Inkommande

KTechLab, en integrerad utvecklingsmiljö för mikrokontrollenheter och elektronik, är ett äldre program som inte har getts ut under en tid på grund av det faktum att det använde äldre teknologi. KTechLabs kodbas har uppdaterats och [version 0.50.0](https://zoltanp.github.io/2020/09/ktechlab-0-50-0.html) har nu släppts för test.

# Utgåvor 20.08.2

Vissa av våra projekt ges ut med sin egen tidsskala och vissa ges ut tillsammans. Packen med projekt 20.08.2 gavs ut idag med dussintals felrättningar och kommer snart vara tillgänglig via programvarubutiker och distributioner. Se [20.08.2 utgivningssida](https://www.kde.org/info/releases-20.08.2.php) för detaljerad information.

Några av felrättningarna som ingår i dagens utgåvor:

* Ett fel som gjorde att texteditorn Kate dolde element i det grafiska användargränssnittet efter en flik stängdes har rättats

* Nerladdning av kontorsdokument via Dolphins GDrive-integrering misslyckas inte längre med att hantera den nödvändiga serveromdirigeringen.

* Diagonal panorering (t.ex. med tryckplattor) i Terminal har rättats.

* Okulars dokumentvisning tillåter nu att klicka på hyperlänkar som täcks av kommentarer

* I Kontact orsakar inte längre inställning av automatisk brevarkivering eller vidarebefordring av ett brev på plats en krasch

* Automatisk skärning av scendelningar i videoeditorn Kdenlive skär nu verkligen videon, och att öppna projekt med saknade videoklipp har gjorts robustare

* Bildimportverktyget i Gwenview hänger inte längre efter det stängs

* Delningsverktyget för fjärrskrivbord Krfb hanterar nu skalade skärmar korrekt

* Telefonanslutningar med KDE anslut är robustare efter en säkerhetsgranskning

[20.08 versionsfakta](https://community.kde.org/Releases/20.08_Release_Notes)
&bull; [Wikisida för paketnerladdning](https://community.kde.org/Get_KDE_Softwar
e_on_Your_Linux_Distro) &bull; [20.08.2
källkodsinformationssida](https://kde.org/info/releases-20.08.2) &bull; [20.08.2
fullständig ändringslogg](https://kde.org/announcements/changelog-
releases.php?version=20.08.2)